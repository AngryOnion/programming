var fileName = "pagecontent.txt";
var express = require('express');
var path = require('path');
var app = express();
var bodyParser = require('body-parser') //parser for json
var fs = require('fs');
var start = '<div class="row"><div class="col-md-2"></div><div class="jumbotron col-md-8"><h1 style="text-align: center">Wikipedia</h1>';
var end = '</div></div>';
var header = '<script src="js/jquery-3.1.0.min.js"></script><script src="js/bootstrap.min.js"></script><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" ><link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"><link rel="stylesheet" href="/style/main.css">';

app.use(express.static(path.join(__dirname, 'public'))) //path to files
app.use(bodyParser.json() );       // JSON encoder
app.use(bodyParser.urlencoded({     // URL encoder
  extended: true
})); 

app.listen(7666, function () {
  console.log('Beep boop server started, i think')
})

app.get('/', function (req, res) {
    var page = header;
    page += start;
    page += '<pre>'
    page += getPageText(); //put file text here
    page += '<div class="text-center"><form method="post" action="/edit"> <input type=submit value="edit" class="btn btn-default"></input></form></div>';
    page += '</pre>';
    page += end;
    res.send(page)
})



app.post('/edit', function (req, res) {
    var page = header;
    page += start;
    page += '<div class="text-center"><form method="post" action="/save"><textarea name="text" class="edit" style="width: 100% !important;">' + getPageText() + '</textarea><input type=submit value="save" class="btn btn-default"></input></form></div>';
    page += end;
    res.send(page)
})


app.post('/save', function (req, res) {
    var text = req.body.text;
    fs.writeFile(fileName, text, function (err) {
      res.redirect('/');
    });
})
         
app.use(function (req, res, next) {
  res.status(404).send('Beep boop, error 404!')
})

var getPageText = function() {
  var text = fs.readFileSync(fileName,"utf8");
  return text;
};
#!/usr/bin/python
import random
import json
import cgi
import cgitb
import datetime
import sys
import os
from decimal import Decimal

cgitb.enable()

multiplayer = False
paper = 0
rock = 1
scissors = 2
datafile = "data.txt"

aiChoice = 99


def play_with_ai(choice):
    playerChoices = []
    if len(playerChoices) < 3:
        return decide_winner(choice, ai_random_choice())
    else:
        if playerChoices[len(playerChoices) - 1] == playerChoices[len(playerChoices) - 2] and playerChoices[
                    len(playerChoices) - 2] == playerChoices[len(playerChoices) - 3]:
            if playerChoices[len(playerChoices) - 1] == paper:
                return decide_winner(choice, scissors)
            else:
                return decide_winner(choice, playerChoices[len(playerChoices) - 1] - 1)
        else:
            return decide_winner(choice, ai_random_choice())



def ai_random_choice():
    return random.randint(0, 2)


def decide_winner(playerOneChoice, playerTwoChoice):
    global aiChoice
    aiChoice = playerTwoChoice
    if (not (playerOneChoice == 0 and playerTwoChoice == 2)
        and ((playerOneChoice == 2 and playerTwoChoice == 0)
             or playerOneChoice < playerTwoChoice)):
        #win
        return 1
    elif playerOneChoice == playerTwoChoice:
        #draw
        return 0
    else:
        #lose
        return -1

def getDecimal(number):
    return str(float("{0:.2f}".format(number)))


def write_statistics(name, result, pTwo, time):
    oneScore = 0
    twoScore = 0
    if result == 0:
        oneScore = 1
        twoScore = 1
    if result == 1:
        oneScore = 1
    if result == -1:
        twoScore = 1
    date = str(datetime.datetime.today().strftime('%Y-%m-%d'))
    if os.stat(datafile).st_size == 0:
        with open(datafile, "a") as the_file:
            the_file.write(date + "," + name + "," + str(oneScore) + "," + pTwo + "," + str(twoScore) + "," + getDecimal(float(time)))
    else:
        with open(datafile, "r") as the_file:
            lines = file.readlines(the_file)
        lastrow = lines[len(lines) - 1]
        lines = lines[:-1]
        variables = lastrow.split(',')
        if name == variables[1] and pTwo == variables[3]:
            new_line = variables[0] + "," + name + "," + str(int(variables[2]) + oneScore) + "," + pTwo + "," \
                       + str(int(variables[4]) + oneScore) + "," + getDecimal(float(variables[5]) + float(time))
            lines.append(new_line)
            open(datafile, 'w').close()
            with open(datafile, "a") as the_file:
                for line in lines:
                    the_file.write(line)
        else:
            with open(datafile, "a") as the_file:
                the_file.write("\n")
                the_file.write(date + "," + name + "," + str(oneScore) + "," + pTwo + "," + str(twoScore) + "," + getDecimal(float(time)))



myjson = json.load(sys.stdin)

name = myjson['name']
playerChoice = myjson['playerChoice']
playerTime = myjson['playerTime']

result = play_with_ai(int(playerChoice))
write_statistics(name, result, "GeniusAI", playerTime)
print 'Content-Type: application/json\n\n'
print json.dumps({"ai": aiChoice, "result": result})












# if not lastline:
#     with open(data, "a") as csvfile:
#         writer = csv.writer(csvfile, delimiter=',')
#         writer.writerow([date, name, oneScore, pTwo, twoScore, time])
# else:
#     if name == lastline[1]:
#         f = open(data, "r+w")
#         lines = f.readlines()
#         lines = lines.pop()
#         if len(lines) == 0:
#             f.close()
#             with open(data, "r+w") as csvfile:
#                 writer = csv.writer(csvfile, delimiter=',')
#                 writer.writerow([lastline[0], lastline[1], int(lastline[2]) + oneScore, lastline[3],
#                                  int(lastline[4]) + twoScore, time])
#         else:
#             for line in lines:
#                 f.write(line)
#                 f.write(str(len(lines)))
#             f.close()
#             with open(data, "a") as csvfile:
#                 writer = csv.writer(csvfile, delimiter=',')
#                 writer.writerow([lastline[0], lastline[1], int(lastline[2]) + oneScore, lastline[3], int(lastline[4]) + twoScore, time])
#     else:
#         with open(data, "a") as csvfile:
#             writer = csv.writer(csvfile, delimiter=',')
#             writer.writerow([date, name, oneScore, pTwo, twoScore, time])



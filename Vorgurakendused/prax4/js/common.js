$(document).ready(function() {
    $(".btn-pref .btn").click(function () {
        $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
        // $(".tab").addClass("active"); // instead of this do the below
        $(this).removeClass("btn-default").addClass("btn-primary");
    });
});

function changeview() {
    $("#loginblock").toggle("");
    $("#signupblock").toggle("");
}

function showSignUp() {
    $("#loginblock").hide();
    $("#signupblock").show();
}

function editAboutInformation() {
    $("#edit").toggle();
    $("#aboutTextField").toggle();
    $("#newTweetField").toggle();
}

function openModal() {
    $('#myModal').modal('show');
}


<?php
include('session.php');
include('config.php');
//error_reporting(E_ALL);
ini_set('display_errors', '1');
if (!isset($_SESSION['username'])) {
    header("location: index.php");
}
$db = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
if (!empty($_GET["username"])) {
    $username = $_GET["username"];
    $query = "SELECT username FROM 155407_users WHERE username='$username'";
    $result = mysqli_query($db, $query);
    $count = mysqli_num_rows($result);
    if ($count == 0) {
        header("location: main.php?username=$sessionUsername");
    }
} else {
    header("location: main.php?username=$sessionUsername");
}

$query = "SELECT * FROM 155407_users WHERE username='$username'";
$result = mysqli_query($db, $query);
$dataAboutUser = mysqli_fetch_assoc($result);
$about = $dataAboutUser["about"];
$following = $dataAboutUser["following"];
$followers = $dataAboutUser["followers"];
$picture = $dataAboutUser["picture"];

if (!empty($_POST["save"])) {
    $aboutText = escape($db,$_POST['about']);
    $query = "UPDATE 155407_users SET about='$aboutText' WHERE username='$username'";
    $db->query($query);
    header("location: main.php?username=$sessionUsername");
}
function escape($db, $string) {
    $string = mysqli_real_escape_string($db, $string);
    $string = htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
    return $string;
}
$query = "SELECT * FROM 155407_users WHERE username='$sessionUsername'";
$result = mysqli_query($db, $query);
$data = mysqli_fetch_assoc($result);
$sessFol = $data["following"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ilja twitter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="js/jquery-3.1.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="styles/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/common.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/npm.js"></script>
</head>
<body class="mainpage">
<!--Navbar start-->
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Twitter for rocks</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="/~ilsamo/prax4/main.php?username=<?php echo $sessionUsername?>">Home</a></li>
            <li><a href="logout.php">Logout</a></li>
        </ul>
        <form class="navbar-form navbar-left" method="post">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" name="search">
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
</nav>
<!--Navbar end-->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Search results:</h4>
            </div>
            <div class="modal-body">

                <?php
                if (!empty($_POST["search"])) {

                    echo ("<script type=\"text/javascript\">openModal();</script>");
                    $search = $_POST["search"];
                    $query = "SELECT username FROM 155407_users WHERE username='$search'";
                    $result = mysqli_query($db, $query);
                    $array = mysqli_fetch_array($result);
                    if (count($array) > 0) {
                            echo ("<ul class=\"list-unstyled\">");
							$lastuser = "";
                            foreach ($array as &$user) {
                                if ($lastuser === $user) {

                                } else {
                                    echo ("<li><a href=\"main.php?username=" . $user . "\"><strong>" . $user  . "</strong></a></li>");
                                    $lastuser = $user;
                                }
                            }
                            echo ("</ul>");
                    } else {
                        echo ("<h3>Nothing found</h3>");
                    }


                }

                ?>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="mainpage">
    <div class="container-fluid">
        <div class="col-md-3">
            <div class="profile-sidebar" style="background-iamge: #linear-gradient(to bottom,#e8e8e8 0,#f5f5f5 100%) !important;">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                         <img src="<?php echo $picture ?>"
                         class="img-responsive" alt="">
                </div>

                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        <?php
                        echo($username);
                        ?>
                    </div>
                </div>

                <div class="profile-userbuttons">
                    <form method="post">
                    <?php
                    if ($username == $_SESSION['username']) {
                        echo(DISABLED_FOLLOW_BUTTON);
                    } else if(strpos($followers, $sessionUsername) !== false) {
                        echo(UN_FOLLOW_BUTTON);
                    } else {
                        echo(FOLLOW_BUTTON);
                    }
                    if (!empty($_POST["follow"])) {
						$newsessFol = $sessFol . $username;
                        $newFollowers = $followers . $sessionUsername;
                        $query = "UPDATE 155407_users SET followers = '$newFollowers,' WHERE username = '$username'";
                        $db->query($query);
                        $query = "UPDATE 155407_users SET following = '$newsessFol,' WHERE username = '$sessionUsername'";
                        $db->query($query);
                       
                        header("location: main.php?username=$username");
                    }
                    if (!empty($_POST["unfollow"])) {
                        if (!empty($_POST['user'])) {
                            $unFollowUser = $_POST['user'];
                            $replaced = str_replace($sessionUsername . ',', "" , $followers);
                            $query = "UPDATE 155407_users SET followers = '$replaced' WHERE username = '$unFollowUser'";
                            $db->query($query);
                            $query = "SELECT * FROM 155407_users WHERE username='$sessionUsername'";
                            $result = mysqli_query($db, $query);
                            $dataAboutSessionUser = mysqli_fetch_assoc($result);
                            $replaced = str_replace($unFollowUser . ',', "", $dataAboutSessionUser["following"]);
                            $query = "UPDATE 155407_users SET following = '$replaced' WHERE username = '$sessionUsername'";
                            $db->query($query);
							header("location: main.php?username=$username");
                        } else {
                            if (strpos($followers, $sessionUsername) !== false) {
                                $replaced = str_replace($sessionUsername . ',', "" , $followers);
                                $query = "UPDATE 155407_users SET followers = '$replaced' WHERE username = '$username'";
                                $db->query($query);
                                $query = "SELECT * FROM 155407_users WHERE username='$sessionUsername'";
                                $result = mysqli_query($db, $query);
                                $dataAboutSessionUser = mysqli_fetch_assoc($result);
                                $replaced = str_replace($username . ',', "", $dataAboutSessionUser["following"]);
                                $query = "UPDATE 155407_users SET following = '$replaced' WHERE username = '$sessionUsername'";
                                $db->query($query);
                                header("location: main.php?username=$username");
                            }
                        }
                    }
                    ?>
                    </form>
                </div>
                <br/>

                <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <button type="button" id="stars" class="btn btn-primary" href="#tab1" data-toggle="tab"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                            <div class="hidden-xs">About</div>
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                            <div class="hidden-xs">Followers</div>
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" id="following" class="btn btn-default" href="#tab3" data-toggle="tab"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                            <div class="hidden-xs">Following</div>
                        </button>
                    </div>
                </div>

                <div class="well">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1">
                            <p class="about"><strong>
                                    <?php
                                    echo($about);
                                    ?></strong>
                            </p>

                            <?php
                            if ($username == $_SESSION['username']) {
                                echo (USER_EDIT_MENU);
                            }
                            ?>
                        </div>
                        <div class="tab-pane fade in" id="tab2">
                            <ul class="list-unstyled">
                            <?php
                                $followersArray = explode(',', $followers);
                                foreach ($followersArray as &$user) {
                                    if (!empty($user)) {
                                        echo ("<li><a href=\"/~ilsamo/prax4/main.php?username=" . $user . "\"><strong>" . $user  . "</strong></a></li>");
                                    }
                                }
                                if (count($followersArray) == 1) {
                                    if ($username == $_SESSION['username']) {
                                        echo("<h3 style='text-align: center'><strong>Sorry no one follows you</strong></h3>");
                                    } else {
                                        echo("<h3 style='text-align: center'><strong>No followers</strong></h3>");
                                    }
                                }
                            ?>
                            </ul>
                        </div>
                        <div class="tab-pane fade in" id="tab3">
                            <ul class="list-unstyled">
                                <?php
                                    $followingArray = explode(',', $following);
                                    if ($username == $_SESSION['username']) {
                                        foreach ($followingArray as &$user) {
                                            if (!empty($user)) {
                                                echo (START_OF_ITEM . $user . "\"><strong>" . $user . MIDDLE_OF_ITEM . $user . END_OF_ITEM);
                                            }
                                        }
                                    } else {
                                        foreach ($followingArray as &$user) {
                                            if (!empty($user)) {
                                                echo ("<li><a href=\"~ilsamo/prax4/main.php?username=" . $user . "\"><strong>" . $user  . "</strong></a></li>");
                                            }
                                        }
                                    }
                                    if (count($followingArray) == 1) {
                                        if ($username == $_SESSION['username']) {
                                            echo("<h3 style='text-align: center'><strong>First you need to follow someone</strong></h3>");
                                        } else {
                                            echo("<h3 style='text-align: center'><strong>No followings</strong></h3>");
                                        }
                                    }
                                ?>
<!--                                <li><a href="href="/~ilsamo/prax4/main.php?username=--><?php //echo $sessionUsername?><!--"/><input type="submit" class="btn btn-success btn-sm pull-right"  id="unFollowButton" name="unfollow" value="Unfollow"/></li>-->
<!--                                <li>Coffee <input type="submit" class="btn btn-success btn-sm pull-right"  id="unFollowButton" name="unfollow" value="Unfollow"/></li>-->
<!--                                <li>Coffee <input type="submit" class="btn btn-success btn-sm pull-right"  id="unFollowButton" name="unfollow" value="Unfollow"/></li>-->
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="well" style="padding-bottom:0" id="newTweetField">
                    <form accept-charset="UTF-8" action="" method="POST">
                    <textarea style="min-width: 100%" name="tweet" placeholder="Type in your message"
                              rows="5"></textarea>
                        <div class="profile-userbuttons">
                            <span class="glyphicon glyphicon-plus"></span><input class="btn btn-default" type="submit" name="addTweet" value="Add new tweet">
                        </div>
                    </form>
                </div>

                <?php

                if (!empty($_POST["addTweet"])) {
					
                    date_default_timezone_set('EET');
                    $tweet = escape($db,$_POST['tweet']);
					if (strlen($tweet) > 1) {
						$timestamp = date('Y-m-d H:i:s');
						$query = "INSERT INTO 155407_tweets(date,author,text) VALUES('$timestamp','$sessionUsername','$tweet')";
						$db->query($query);
					}
                }
                ?>

        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-6">
        <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
            <div class="btn-group" role="group">
                <button type="button" id="stars" class="btn btn-primary" href="#tab4" data-toggle="tab"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span>
                    <div class="hidden-xs">Feed</div>
                </button>
            </div>
            <div class="btn-group" role="group">
                <button type="button" id="favorites" class="btn btn-default" href="#tab5" data-toggle="tab"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                    <div class="hidden-xs">User`s tweets</div>
                </button>
            </div>
        </div>
        <div class="well">
            <div class="tab-content">
                <div class="tab-pane fade in active" id="tab4">
                    <div>
                    <?php
                        $query = "SELECT * FROM 155407_users WHERE username='$sessionUsername'";
                        $result = mysqli_query($db, $query);
                        $data = mysqli_fetch_assoc($result);
                        $sessFol = $data["following"];
                        $sessFolArray = explode(',',$sessFol);
                        if (count($sessFolArray) == 1) {
                            echo("<h3 style='text-align: center'><strong>First you need to follow someone</strong></h3>");
                        } else {
                            $followingString = "";
                            array_pop($sessFolArray);
                            foreach ($sessFolArray as &$user) {
                                $followingString = $followingString . "'" . $user . "',";
                            }
                            $followingString = rtrim($followingString,',');
                            $query = "select * FROM 155407_tweets WHERE author IN ($followingString) order by date DESC limit 10";
                            $result = mysqli_query($db, $query);
                            $index = 0;
                            while($row = mysqli_fetch_assoc($result)) {
                                $array[] = $row;
                                $author = $array[$index]["author"];
                                echo ("<div class=\"well\"> <p><a href=/~ilsamo/prax4/main.php?username=$author><strong>$author</strong></a></p>" . $array[$index]["text"] . "</div>");
                                $index++;
                            }
                        }
                    ?>
<!--                    <div class="well"><p><a href=/~ilsamo/prax4/main.php?username=ilja"><strong>ilja</strong></a></p>One morning,-->
<!--                    </div>-->
<!--                    <div class="well">One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his-->
<!--                        bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see-->
<!--                        his brown belly-->
<!--                    </div>-->
<!--                    <div class="well">One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his-->
<!--                        bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see-->
<!--                        his brown belly-->
<!--                    </div>-->
<!--                    <div class="well">One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his-->
<!--                        bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see-->
<!--                        his brown belly-->
<!--                    </div>-->
<!--                    <div class="well">Overthrew some privately cowardly underneath but angry lobster across egret more elephant-->
<!--                        through staunch in much less much more for much a this inescapably famous stealthy much much rhinoceros more-->
<!--                        far around smilingly or lingeringly beside alas far because and when juggled jeepers giggled overrode-->
<!--                        irefully far near checked.-->
<!--                    </div>-->
                    </div>
                </div>
                <div class="tab-pane fade in" id="tab5">
                    <div>
                        <?php
                        $newQuery = "select * FROM 155407_tweets WHERE author='$username' order by date DESC limit 10";
                        $res = mysqli_query($db, $newQuery);
                        $index = 0;
                        while($roww = mysqli_fetch_assoc($res)) {
                            $newArray[] = $roww;
						
                            echo ("<div class=\"well\"> <p><a href=main.php?username=$username><strong>$username</strong></a></p>" . $newArray[$index]["text"] . "</div>");
                            $index++;
                        }
                        if ($index == 0) {
                            echo("<h3 style='text-align: center'><strong>No tweets :(</strong></h3>");
                        }
                        ?>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
</div>

</body>
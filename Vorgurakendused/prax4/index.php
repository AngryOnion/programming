<?php
    include("config.php");

    $db = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
    session_start();
    if ($db == false) {
        echo "<p>error</p>";
    }

function escape($db, $string) {
    $string = mysqli_real_escape_string($db, $string);
    $string = htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
    return $string;
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">  
    <title>Ilja twitter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="js/jquery-3.1.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/npm.js"></script>
    <script src="js/common.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="styles/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>
<body>
    <div class="jumbotron">
        <div class="container " >
            <div class="row vertical-offset-100">
            <h1 style="text-align: center">Twitter for rocks</h1>
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-default" id="loginblock">
                        <div class="panel-heading">
                            <h3 class="panel-title" style="text-align: center;">Please sign in</h3>
                        </div>
                        <div class="panel-body" id="loginBody">
                            <?php
                            if(!empty($_POST['login'])) {
                                // username and password sent from form
                                $username = escape($db,$_POST['username']);
                                $password = escape($db,$_POST['password']);

                                $sql = "SELECT username FROM 155407_users WHERE username = '$username' and password = '$password'";
                                $result = mysqli_query($db,$sql);
                                $errorText = "<div class=\"alert alert-danger\"><strong>Your Login Name or Password is invalid!</strong></div>";
                                if (!mysqli_fetch_array($result)) {
                                    echo $errorText;
                                } else {
                                    $_SESSION['username'] = $username;
                                    header("location: main.php?username=$username");
                                }
                            }
                            ?>
                            <form accept-charset="UTF-8" action="" role="form" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="username" type="text">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" >
                                </div>
                                <br/>
                                <input class="btn btn-lg btn-default btn-block" value="Sign Up" onclick="changeview();">
                                <input class="btn btn-lg btn-success btn-block" type="submit" value="Login" name="login">
                            </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="panel panel-default" style="display: none" id="signupblock">
                        <div class="panel-heading">
                            <h3 class="panel-title" style="text-align: center;">Sign up</h3>
                        </div>
                        <div class="panel-body" id="signupBody">
                            <?php

                            if(!empty($_POST['register'])) {

                                $error = false;
                                $username = escape($db,$_POST['username']);
                                $email = escape($db,$_POST['email']);
                                $password = escape($db,$_POST['password']);

                                $query = "SELECT username FROM users WHERE username='$username'";
                                $result = mysqli_query($db,$query);
                                $count = mysqli_num_rows($result);
                                $change = '<script type="text/javascript">showSignUp();</script>';
                                if (empty($username) || empty($email) || empty($password)) {
                                    $error = true;
                                    echo "<div class=\"alert alert-danger\"><strong>Invalid credentials!</strong></div>";
                                    echo $change;
                                }
                                if ($count > 0) {
                                    $error = true;
                                    echo "<div class=\"alert alert-danger\"><strong>Such username is already in use</strong></div>";
                                    echo $change;
                                }
                                $query = "SELECT email FROM 155407_users WHERE email='$email'";
                                $result = mysqli_query($db,$query);
                                $count = mysqli_num_rows($result);
                                if ($count > 0) {
                                    $error = true;
                                    echo "<div class=\"alert alert-danger\"><strong>Such email is already in use</strong></div>";
                                    echo $change;
                                }
                                if ($error == false) {
                                    $query = "INSERT INTO 155407_users(username,password,email) VALUES('$username','$password','$email')";
                                    $result = mysqli_query($db,$query);
                                    $_SESSION['username'] = $username;
                                    header("location: main.php?username=$username");                                }
                            }
                            ?>
                            <form accept-charset="UTF-8" role="form" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="username" type="text">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Email" name="email" type="email">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <br/>
                                <input class="btn btn-lg btn-warning btn-block" value="Back to login" onclick="changeview();">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Sign Up" name="register">
                            </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
#!/usr/bin/python

import cgitb; cgitb.enable()
import cgi
# sendMessage.py?name=Jacob&text=Some message

form = cgi.FieldStorage()
name = form['name'].value
text = form['text'].value

file = open('/tmp/messages.txt', 'a')
file.write(name + '\t' + text + '\n')
file.close()

print 'Content-type: text\html\n'
print 'Success: ' + name + '____' + text

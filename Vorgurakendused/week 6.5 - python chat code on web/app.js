$('#send').click(function () {
	var username = $('#usernameField').val();
	var message = $('#messageField').val();
	
	$.get('cgi-bin/sendMessage.py', {
		name: username,
		text: message
	});
	$('#messageField').val('');
});

function readMessages() {
	$.get('cgi-bin/readMessages.py').done(function (response) {
		$('#result').empty();
		
		response.forEach(function (item) {
			// <p><b>User</b>: Message</p>
			var row = '<p><b>'+item.name+'</b>: '+
				item.text+'</p>';
			$('#result').prepend(row);
		});
		
		setTimeout(readMessages, 500);
	});
}
readMessages();
var playerPicks = [];

var paper = 0;
var rock = 1;
var scissors = 2;

var aiLastChoice = NaN;

function play(choice) {
    
    if (playerPicks.length < 3) {
        makeRandomChoice(choice);    
    } else {
        //control if player choosed more than 3 same in a row
        if(playerPicks[playerPicks.length - 1] === playerPicks[playerPicks.length - 2] 
           && playerPicks[playerPicks.length - 2] === playerPicks[playerPicks.length - 3]) {
                
            console.log('AI TURN');
            
            if(playerPicks[playerPicks.length - 1] === 0) {
                makeTurn(choice, 2);
            } else {
                makeTurn(choice, playerPicks[playerPicks.length - 1] - 1);
            }
        } else {
            makeRandomChoice(choice);
        }
        
    }
    playerPicks.push(choice);
}

function makeTurn(playerChoice, aiChoice) {
    changeOpacity(aiChoice);
    if(!(playerChoice === 0 && aiChoice === 2)
            && ((playerChoice === 2 && aiChoice === 0) 
            || playerChoice < aiChoice)) {
            //WON
            console.log('Win ' + playerChoice + ' ' + aiChoice);
        changeScore(1,0);
        } else if (playerChoice === aiChoice) {
            //DRAW
            console.log('Draw ' + playerChoice + ' ' + aiChoice);
            changeScore(1,1);
        } else {
            //LOSE
            console.log('Lose ' + playerChoice + ' ' + aiChoice);
            changeScore(0,1);
        }
}
    
function changeScore(win, lose) {
    var score = $('#score').text();
    win = win + parseInt(score.substr(0, score.indexOf('-'))); 
    lose = lose + parseInt(score.substr(score.indexOf('-')+1));
    $('#score').text(win + '-' + lose);
}

function changeOpacity(aiChoice) {
    if(aiLastChoice != NaN) {
        $('#AI').children().eq(aiLastChoice + 1).toggleClass('selected', false).toggleClass('aipicks');
    }
    $('#AI').children().eq(aiChoice + 1).toggleClass('selected').toggleClass('aipicks', false);
    aiLastChoice = aiChoice;
}
61
function makeRandomChoice(choice) {
    var aiChoice = Math.floor(Math.random() * 3);
    console.log(aiChoice);
    makeTurn(choice, aiChoice);
}


$('#PlayerPaper').click(function() {
    play(paper);
});
$('#PlayerRock').click(function() {
    play(rock);
});
$('#PlayerScissors').click(function() {
    play(scissors);
});

import java.util.Objects;
import java.util.Scanner;
import java.util.Random;
/**
 * Created by Ilja on 19.02.2016.
 *
 *
 * Template for HW01: Treasurehunt.
 * More information:
 * https://courses.cs.ttu.ee/pages/ITI0011:Aardejaht
 */
public class HW01 {
    /** value to play hotncold. */
    public static final int CELL_NO_TREASURE = 100;
    /**
     * Value to return in makeMove in case.
     * the cell was empty.
     */
    public static final int CELL_EMPTY = 0;
    /**
     * Value to return in makeMove in case.
     * the cell contained treasure.
     */
    public static final int CELL_TREASURE = 10;
    /**
     * Value to return in makeMove in case.
     * the cell does not exist.
     */
    public static final int CELL_ERROR = -1;
    /**
     * Value to return in makeMove in case.
     * the treasure has been already found.
     */
    public static final int CELL_FOUND_TREASURE = 50;

    /**
     * Value to return in makeMove in case.
     * the cell was already used.
     */
    public static final int CELL_USED = 60;
    /**
     * Used to indicate that the score is wrong.
     * Used by getHighscoreScoreAtIndex()
     */
    public static final int HIGHSCORE_WRONG = Integer.MIN_VALUE;
    /**
     * Minsweeper-like clearing in case of empty cells.
     */
    public static final int GAME_TYPE_MINESWEEPER = 1;

    /**
     * Cell contains the distance to the closest treasure.
     */
    public static final int GAME_TYPE_HOT_COLD = 2;

    /** The count of settings parameters. */
    public static final int INITIAL_PARAMETER_ARRAY_LENGTH = 3;

    /** Settings parameter array index for map height value.*/
    public static final int MAP_HEIGHT = 0;

    /** Settings parameter array index for map width value. */
    public static final int MAP_WIDTH = 1;

    /** Settings parameter array index for matrix target x value. */
    public static final int FIELD_MATRIX_TREASURE = 2;

    /** The count of guess parameters. */
    public static final int GUESS_ARRAY_LENGTH = 2;

    /** Guess parameter array index for guess row value.*/
    public static final int MAP_ROW = 0;

    /** Guess parameter array index for guess column value.*/
    public static final int MAP_COL = 1;

    /** storing of the map for the game.*/
    public static int[][] map;

    /** map valaues storage. */
    public static int[][] mapValues;
    /**
     * Value for a cell which is hidden.
     * (the contents of the cell is not visible).
     * Used in bonus part 1.
     */
    public static final int CELL_HIDDEN = 9;


    /**
     * Main entry.
     * @param args Command-line arguments.
     */

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        boolean start = true;
        while (start) {
            if (gameStarter(in) == GAME_TYPE_HOT_COLD) {
                hotCold(in);
            } else {
                regularGame(in);
            }
            start = gameRetry(in);
        }

    }

    /**
     * Asks which gametype to choose.
     * @param in Sccanner in
     * @return which gametype to play
     */
    public static int gameStarter(Scanner in) {
        String input;
        do {
            System.out.print("Valiga mängurežiimi (tava "
                    + "või soekulm) ja sisestage reziimi nimi):");
            input = in.nextLine().replaceAll(" ", "").toLowerCase();
        } while (!(Objects.equals(input, "tava")
                || Objects.equals(input, "soekulm")));
        if (Objects.equals(input, "tava")) {
            return GAME_TYPE_MINESWEEPER;
        } else {
            return GAME_TYPE_HOT_COLD;
        }
    }

    /**
     * Starts a minesweeper game.
     * @param in Scanner input.
     */
    public static void regularGame(Scanner in) {
        int[] fieldParameters = gameParameters(in);
        starter(in, fieldParameters);
        int treasuresCount = fieldParameters[FIELD_MATRIX_TREASURE];
        int foundTreasures = 0;
        int tryCount = 0;
        getAllValuesInMap();
        while (foundTreasures < treasuresCount) {
            int[] moveInput = moveInput(in, fieldParameters);
            int move = makeMove(moveInput[MAP_ROW], moveInput[MAP_COL]);
            if (move == CELL_TREASURE) {
                foundTreasures++;
                tryCount++;
            } else {
                tryCount++;
            }
            System.out.println(
                    String.format("kaevamisi: %s, aardeid jäänud: %s",
                            tryCount, (treasuresCount - foundTreasures)));
            }
        System.out.println(String.format(
                "Mäng läbi! Kokku tehti %s kaevamist.", tryCount));
    }

    /**
     * Starts game.
     * @param in input
     * @param fieldParameters Map parameters
     */
    public static void starter(Scanner in, int[] fieldParameters) {
        createMap(fieldParameters[MAP_HEIGHT],
                fieldParameters[MAP_WIDTH],
                fieldParameters[FIELD_MATRIX_TREASURE]);
        System.out.println("Edukat kaevamist!");
        System.out.println(getAsciiMap(map));
        System.out.println(
                String.format("kaevamisi: 0, aardeid jäänud: %s",
                        fieldParameters[FIELD_MATRIX_TREASURE]));
    }

    /**
     * Makes move to cell in certain row and column
     * and returns the contents of the cell.
     * Use CELL_* constants in return.
     * @param row Row to make move to.
     * @param col Column to make move to.
     * @return Contents of the cell.
     */
    public static int makeMove(int row, int col) {
        if (row < 0 || col < 0) {
            return CELL_ERROR;
        } else if (map[row][col] == CELL_USED
                || map[row][col] == CELL_FOUND_TREASURE) {
            return CELL_USED;
        } else if (map[row][col] == CELL_TREASURE) {
            map[row][col] = CELL_FOUND_TREASURE;
            System.out.println(getAsciiMap(map));
            System.out.println("AARE!");
            return CELL_TREASURE;

        } else {
            map[row][col] = findTreasuresAround(row, col);
            if (map[row][col] == CELL_EMPTY) {
                replaceZerosAround(row, col);
                System.out.println(getAsciiMap(map));
                return CELL_EMPTY;
            } else {
                int value = map[row][col];
                System.out.println(getAsciiMap(map));
                return value;
            }
        }
    }

    /**
     * Creates a map with certain measures and treasures.
     * As this is a static method which doesn't return anything (void),
     * you should store the created map internally.
     * This means you can choose your own implementation of how to store
     * the map.
     * The treasures should be put on the map randomly using setCell method.
     * @param height Height of the map.
     * @param width Width of the map.
     * @param treasures The number of treasures on the map.
     * @return Whether map was created.
     */
    public static boolean createMap(int height, int width, int treasures) {
        map = new int[height][width];
        mapValues = new int[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                setCell(i, j, CELL_HIDDEN);
            }
        }
        int treasureCount = 0;
        int randomHeight;
        int randomWidth;
        while (treasureCount < treasures) {
            Random rand = new Random();
            Random rand2 = new Random();
            randomHeight = rand.nextInt(height);
            randomWidth = rand.nextInt(width);
            if (map[randomHeight][randomWidth] == CELL_TREASURE) {
                randomHeight = rand.nextInt(height);
                randomWidth = rand2.nextInt(width);
            } else {
                setCell(randomHeight, randomWidth, CELL_TREASURE);
                mapValues[randomHeight][randomWidth] = CELL_TREASURE;
                treasureCount++;
            }
        }
        return true;
    }

    /**
     * Recurcive function to replaces all cells with 0 treausres around,
     * around cell with 0 treasures.
     * @param row x of cell.
     * @param col y of cell.
     */
    public static void replaceZerosAround(int row, int col) {
        map[row][col] = CELL_USED;
        mapValues[row][col] = CELL_USED;
        for (int i = -1; i + row <= row + 1; i++) {
            for (int j = -1; j + col <= col + 1; j++) {
                try {
                    if (!(map[row + i][col + j] == CELL_USED
                    || map[row + i][col + j] == CELL_TREASURE
                    || map[row + i][col + j] == CELL_FOUND_TREASURE)) {
                        if (!(mapValues[row + i][col + j] == CELL_EMPTY)) {
                            map[row + i][col + j] = mapValues[row + i][col + j];
                        } else {
                            replaceZerosAround((row + i), (col + j));
                        }
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                }
            }
        }

    }

    /**
     * Function to create map with all values.
     */
    public static void getAllValuesInMap() {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (!(map[i][j] == CELL_TREASURE)
                        || !(map[i][j] == CELL_FOUND_TREASURE)) {
                    mapValues[i][j] = findTreasuresAround(i, j);
                }
            }
        }
    }

    /**
     * Function to find treasures count around cell.
     * @param row x of cell
     * @param col y of cell
     * @return how many treasures is around this cell.
     */
    public static int findTreasuresAround(int row, int col) {
        int treasuresCount = 0;
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                try {
                    if (map[row + i][col + j] == CELL_TREASURE
                            || map[row + i][col + j] == CELL_FOUND_TREASURE) {
                        treasuresCount++;
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                }
            }
        }
        return treasuresCount;
    }


    /**
     * Formatting game map values to be able for printing to player.
     * @param matrix game map
     * @return printeable map
     */
    public static String getAsciiMap(int[][] matrix) {
        StringBuilder asciiMatrix = new StringBuilder();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] == CELL_TREASURE
                        || matrix[i][j] == CELL_HIDDEN) {
                    asciiMatrix.append('.');
                } else if (matrix[i][j] == CELL_FOUND_TREASURE) {
                    asciiMatrix.append('+');
                } else if (matrix[i][j] == CELL_USED
                        || matrix[i][j] == CELL_NO_TREASURE
                        || matrix[i][j] == CELL_EMPTY) {
                    asciiMatrix.append(' ');
                } else {
                    asciiMatrix.append(map[i][j]);
                }
            }
            asciiMatrix.append("\n");

        }
        return asciiMatrix.toString().substring(0, asciiMatrix.length() - 1);
    }

    /**
     * Sets the cell value for the active map (created earlier using.
     * createMap method).
     * This method is required to test certain maps
     * @param row Row index.
     * @param col Column index.
     * @param cellContents The value of the cell.
     * @return Whether the cell value was set.
     */
    public static boolean setCell(int row, int col, int cellContents) {
        map[row][col] = cellContents;
        return true;
    }

    /**
     * Function to play hot or cold game mode.
     * @param in Scanner input
     */
    public static void hotCold(Scanner in) {
        int[] fieldParameters = gameParameters(in);
        createMap(fieldParameters[MAP_HEIGHT],
                fieldParameters[MAP_WIDTH],
                fieldParameters[FIELD_MATRIX_TREASURE]);
        System.out.println("Edukat kaevamist!");
        System.out.println(getAsciiMap(map));
        int treasuresCount = fieldParameters[FIELD_MATRIX_TREASURE];
        int foundTreasures = 0;
        int tryCount = 0;
        while (foundTreasures < treasuresCount) {
            int[] moveInput = moveInput(in, fieldParameters);
            int distance = getDistanceToClosestTreasure(
                    moveInput[MAP_HEIGHT],
                    moveInput[MAP_WIDTH]);
            if (distance == 0) {
                System.out.println("Aare on leitud!");
                setCell(moveInput[MAP_HEIGHT],
                        moveInput[MAP_WIDTH],
                        CELL_FOUND_TREASURE);
                System.out.println(getAsciiMap(map));
                tryCount++; foundTreasures++;
            } else  {
                System.out.println("Kõige lähimolev aare on " + distance
                        + " ruudu kaugusel");
                setCell(moveInput[MAP_HEIGHT],
                        moveInput[MAP_WIDTH],
                        CELL_NO_TREASURE);
                System.out.println(getAsciiMap(map));
                tryCount++;
            }
            System.out.println(
                    String.format("kaevamisi: %s, aardeid jäänud: %s",
                            tryCount, (treasuresCount - foundTreasures)));
            }
        System.out.println(String.format(
                "Mäng läbi! Kokku tehti %s kaevamist.", tryCount));
    }

    /**
     * Returns the distance to the closest treasure.
     * The distance indicates the minimum steps it takes to get from
     * the given (row, col) to the closest treasure.
     * Step can be taken to every adjacent cell (horisontal, vertical and
     * diagonal). From one cell you can step to 8 other cells.
     * <pre>
     * Example:
     * ...C
     * .B..
     * A...
     * ..DP
     * </pre>
     *
     * distances:
     * AP = 3
     * BP = 2
     * CP = 3
     * DP = 1
     *
     * @param row Row index
     * @param col Col index
     * @return The distance to the closest treasure
     */
    public static int getDistanceToClosestTreasure(int row, int col) {
        double shortestDistance = Double.MAX_VALUE;
        double distance;
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (map[i][j] == CELL_TREASURE) {
                    distance = Math.pow(i - row, 2) + Math.pow(j - col, 2);
                    if (distance < shortestDistance) {
                        shortestDistance = distance;
                    }
                }
            }
        }
        double realDistance = Math.sqrt(shortestDistance);
        int rounded = (int) Math.round(realDistance);
        if (rounded >= realDistance) {
            return rounded;
        } else {
            return rounded + 1;
        }
    }

    /**
     * Function to ask player whether retry game or not.
     * @param in Scanner in.
     * @return boolean to play or not to play.
     */
    public static boolean gameRetry(Scanner in) {
        String input;
        do {
            System.out.println("Kas soovid veel mängida?");
            System.out.print("Sisestage jah või ei:");
            input = in.nextLine().replaceAll(" ", "").toLowerCase();
        } while (!(Objects.equals(input, "jah")
                || Objects.equals(input, "ei")));
        return Objects.equals(input, "jah") ? true : false;
    }

    /**
     * Function to ask parameters of map to futher creation.
     * @param in Input.
     * @return parameters of map.
     */
    public static int[] gameParameters(Scanner in) {
        int[] parameters;
        do {
            System.out.print("Sisesta M (ridade arv), "
                    + "N (veergude arv), X (aarete arv):");
            String parameterInput = in.nextLine();
            parameters = splitInput(parameterInput);
        } while (!validate(parameters));

        return parameters;
    }

    /**
     * FUnction to split input.
     * @param input Scanner Input.
     * @return splitted parameters.
     */
    public static int[] splitInput(String input) {

        String[] splitParts = input.replaceAll(" ", "").split(",");
        int[] parameters = new int[splitParts.length];
        for (int i = 0; i < splitParts.length; i++) {
            try {
                parameters[i] = Integer.parseInt(splitParts[i]);
            } catch (NumberFormatException e) {
                parameters[i] = -1;
            }
        }
        return parameters;
    }

    /**
     * Validate map parameters input.
     * @param parameters Parameters of Map.
     * @return boolean if is suitable parameters or not.
     */
    public static boolean validate(int[] parameters) {
        if (parameters.length != INITIAL_PARAMETER_ARRAY_LENGTH) {
            return false;
        }
        for (int parameter : parameters) {
            if (parameter < 1) {
                return false;
            }
        }
        return (parameters[MAP_HEIGHT]
                * parameters[MAP_WIDTH]) >= parameters[FIELD_MATRIX_TREASURE];
    }

    /**
     * Fucntion to get input of moving.
     * @param in Move input
     * @param mapParameters Parameters of Map
     * @return Parameters of Move
     */
    public static int[] moveInput(Scanner in, int[] mapParameters) {
        int[] parameters;
        do {
            System.out.print("Mida kaevame (rida, veerg):");
            String parameterInput = in.nextLine();
            parameters = splitInputStructureToIntArray(parameterInput);
        } while (!validateGuessInput(parameters, mapParameters));
        return parameters;
    }

    /**
     * Converts input that is spearetd by commas to integer array.
     * If value is not convertable to int -1 is used.
     *
     * @param input input string to be parsed
     * @return int[] array of int values.
     */
    public static int[] splitInputStructureToIntArray(String input) {

        String[] splitParts = input.replaceAll(" ", "").split(",");
        int[] parameters = new int[splitParts.length];
        for (int i = 0; i < splitParts.length; i++) {
            try {
                parameters[i] = Integer.parseInt(splitParts[i]);
            } catch (NumberFormatException e) {
                parameters[i] = -1;
            }
        }
        return parameters;
    }

    /**
     * Validate whether the guess input parameters are ok.
     * 1) Guessinput length must be 2 (x,y) coordinates
     * 2) (x,y) must be inside matrix that is specified by the matirxParameters
     *
     * @param guessInput       user guess input,
     * consists of [0] - x [1] - y coordinates
     * @param mapParameters game matrix parameters
     * (look readInitialMatrixParameters for structure information)
     * @return whether the guess input is ok.
     */
    public static boolean validateGuessInput(
            int[] guessInput, int[] mapParameters) {
        if (guessInput.length != GUESS_ARRAY_LENGTH) {
            return false;
        }
        for (int guessValue : guessInput) {
            if (guessValue < 0) {
                return false;
            }
        }
        if (guessInput[0] >= mapParameters[0]
                && guessInput[1] >= mapParameters[1]) {
            return false;
        } else if ((map[guessInput[0]][guessInput[1]] == CELL_USED
                || map[guessInput[0]][guessInput[1]] == CELL_FOUND_TREASURE)) {
            System.out.println("Juba seal on käidud");
            return false;
        } else {
            return true;
        }
    }
}


import java.util.Random;

/**
 * Created by Ilja on 20.02.2016.
 */
public class TEST_HW01 {
    public static int[][]MAP;

    public static void main(String[] args) {
        int height = 3;
        int width = 3;
        createMap(height, width, 1);
        getAllValuesInMap();
        replaceZerosAround(0,0);
        System.out.println(getAsciiMap(MAP));
        System.out.println(getDistanceToClosestTreasure(0,0));

    }

    public static void replaceZerosAround(int row, int col) {
        MAP[row][col] = 50;
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                try {
                    if (MAP[row + i][col + j] == 0) {
                        replaceZerosAround(row + i, col + j);
                    }
                }catch (ArrayIndexOutOfBoundsException e) {
                }
            }
        }
    }

    public static void getAllValuesInMap() {
        for (int i = 0; i < MAP.length; i++){
            for (int j = 0; j < MAP[i].length; j++) {
                if (!(MAP[i][j] == 9)) {
                    setCell(i, j, findTreasuresAround(i, j));
                }
            }
        }
    }

    public static int findTreasuresAround(int row, int col){
        int treasuresCount = 0;
        for(int i = -1; i < 2; i++) {
            for(int j = -1; j < 2; j++){
                try {
                    if(MAP[row + i][col + j] == 9) treasuresCount++;
                }
                catch (ArrayIndexOutOfBoundsException e){
                }
            }
        }
        return treasuresCount;
    }

    public static boolean createMap(int height, int width, int treasures) {
        // initialize map (for example 2D-array)
        //   - set all cells empty (is this needed?)
        // do some random for every treasure and add them to map:
        MAP = new int[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                setCell(i, j, 0);
            }
        }
        int treasureCount = 0;
        int randomHeight;
        int randomWidth;
        while (treasureCount < treasures) {
            Random rand = new Random();
            Random rand2 = new Random();
            randomHeight = rand.nextInt(height);
            randomWidth = rand2.nextInt(width);
            if (MAP[randomHeight][randomWidth] == 9) {
                randomHeight = rand.nextInt(height);
                randomWidth = rand2.nextInt(width);
            } else {
                setCell(randomHeight, randomWidth, 9);
                treasureCount++;
            }
        }
        return true;
    }

    public static int getDistanceToClosestTreasure(int row, int col) {
        double shortestDistance = Double.MAX_VALUE;
        double distance;
        for (int i = 0; i < MAP.length; i++) {
            for (int j = 0; j < MAP[i].length; j++)
                if (MAP[i][j] == 9) {
                    distance = Math.pow(i - row, 2) + Math.pow(j - col, 2);
                    if (distance < shortestDistance) {
                        shortestDistance = distance;
                    }
                }
        }
        double realDistance = Math.sqrt(shortestDistance);
        int rounded = (int) Math.round(realDistance);
        if (rounded >= realDistance) {
            return rounded;
        } else {
            return rounded + 1;
        }
    }

    public static String getAsciiMap(int[][] matrix) {
        StringBuilder asciiMatrix = new StringBuilder();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (MAP[i][j] >= 0 && MAP[i][j] <= 10){
                asciiMatrix.append(MAP[i][j]);
                } else if (MAP[i][j] == 50) {
                    asciiMatrix.append(" ");
                }
            }
            asciiMatrix.append("\n");

        }
        return asciiMatrix.toString().substring(0, asciiMatrix.length() - 1);
    }

    public static boolean setCell(int row, int col, int cellContents) {
        MAP[row][col] = cellContents;
        return true;
    }
}

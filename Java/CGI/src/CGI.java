import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Ilja Samoilov on 12.04.2016. Time for completion: 1h
 */
public class CGI {

    /**Char order got from the file. */
    private static String order;

    /**ArraylList with sorted words. */
    final static ArrayList<String> words = new ArrayList<>();

    /**Arraylist with original word order from file. */
    final static ArrayList<String> originalWords = new ArrayList<>();

    /**
     * Main method.
     * @param args file path from CMD.
     * @throws IOException exception if there is no such file.
     */
    public static void main(String[] args) throws IOException {
        //Get file path from here
        String file = args[0];
        //reading file here.
        readFile(file);
        //sorting.
        sort();
        //Printing order
        System.out.println(order);
        //Using for loop and sys.format for print words like a table.
        for (int i = 0; i < words.size(); i++ ) {
            System.out.format("%-20s%s\n", originalWords.get(i), words.get(i));
        }

    }

    /**
     * Method used to read the txt and get order and words there.
     * @param filename File.
     * @throws IOException exception when file does not exist.
     */
    public static void readFile(String filename) throws IOException {
        Path path = Paths.get(filename);
        Scanner scanner = new Scanner(path);
        order = scanner.nextLine();
        while (scanner.hasNextLine()) {
            words.add(scanner.nextLine());
        }
        //used later to make printing more easy and faster.
        originalWords.addAll(words.stream().collect(Collectors.toList()));
    }

    /**
     * Method sorts words according to char order in the first line of the file.
     */
   public static void sort() {
       //Using collections with overrided comparator to sort words
       Collections.sort(words, new Comparator<String>() {
           @Override
           public int compare(String o1, String o2) {
               int positionFirst = 0;
               int positionSecond = 0;

               //loop used to get position of the word char by char, of chars are the same, controls next one.
               for (int i = 0; i < Math.min(o1.length(), o2.length()) && positionFirst == positionSecond; i++) {
                   //position of the first and second word.
                   positionFirst = order.indexOf(o1.charAt(i));
                   positionSecond = order.indexOf(o2.charAt(i));
               }

               //if same positions are still the same, but length is different, puts smaller first.
               if (positionFirst == positionSecond && o1.length() != o2.length()) {
                   return o1.length() - o2.length();
               }

               //returns where to move the word.
               return positionFirst - positionSecond;
           }
       });
   }
    
}

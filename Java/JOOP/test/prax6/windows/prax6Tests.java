package prax6.windows;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by SamoilovI on 12.10.2016.
 */
public class prax6Tests {

    FrontWindow frontWindow;

    @Before
    public void setUp() {
        frontWindow = new FrontWindow("A");
    }

    @Test
    public void testNewWindowStatus() {
        assertEquals(0, frontWindow.getWindowStatus());
    }

    @Test
    public void getWindowNameAndStatus() {
        assertEquals("Window name: A status: 0", frontWindow.toString());
    }

    @Test
    public void openWindowTest() {
        frontWindow.openWindow();
        assertEquals(10, frontWindow.getWindowStatus());
    }

    @Test
    public void closeWindowTest() {
        frontWindow.closeWindow();
        assertEquals(0, frontWindow.getWindowStatus());
    }

    @Test
    public void closeOpenedWindow20() {
        frontWindow.openWindow();
        frontWindow.close20ofWindow();
        assertEquals(8, frontWindow.getWindowStatus());
    }

    @Test
    public void openClosedWindow20() {
        frontWindow.open20ofWindow();
        assertEquals(2, frontWindow.getWindowStatus());
    }

    @Test
    public void closeAlreadyClosedWindow() {
        frontWindow.close20ofWindow();
        assertEquals(0, frontWindow.getWindowStatus());
    }

    @Test
    public void openAlreadyOpenedWindow() {
        frontWindow.openWindow();
        frontWindow.open20ofWindow();
        assertEquals(10, frontWindow.getWindowStatus());

    }
}

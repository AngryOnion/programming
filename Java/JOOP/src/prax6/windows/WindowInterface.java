package prax6.windows;

/**
 * Created by SamoilovI on 12.10.2016.
 */
public interface WindowInterface {

    int getWindowStatus();
    void openWindow();
    void closeWindow();
    void open20ofWindow();
    void close20ofWindow();

}

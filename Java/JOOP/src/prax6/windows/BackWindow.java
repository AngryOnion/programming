package prax6.windows;

/**
 * Created by SamoilovI on 12.10.2016.
 */
public class BackWindow extends FrontWindow {

    static boolean openingAllowed = true;

    BackWindow(String name) {
        super(name);
    }

    public void allowOpening() {
        openingAllowed = true;
    }

    public void dontAllowOpening() {
        openingAllowed = false;
    }

    @Override
    public void openWindow() {
        super.openWindow();
    }

    @Override
    public void closeWindow() {
        super.closeWindow();
    }

    @Override
    public void open20ofWindow() {
        super.open20ofWindow();
    }

    @Override
    public void close20ofWindow() {
        super.close20ofWindow();
    }
}

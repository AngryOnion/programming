package prax6.windows;

/**
 * Created by SamoilovI on 12.10.2016.
 */
public class FrontWindow implements WindowInterface{

    private static final int FULLCLOSED = 0;
    private static final int WINDOWS20CHANGE = 2;
    private static final int FULLOPENED = 10;
    private final String name;
    private int status = 0;

    FrontWindow(String name) {
        this.name = name;
    }


    private void setStatus(int newStatus) {
        status = newStatus;
    }

    @Override
    public int getWindowStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Window name: " + name + " status: " + status;
    }

    @Override
    public void openWindow() {
        setStatus(FULLOPENED);
        System.out.println("Window is opened, status: " + status);
    }

    @Override
    public void closeWindow() {
        setStatus(FULLCLOSED);
        System.out.println("Window is closed, status: " + status);
    }

    @Override
    public void open20ofWindow() {
        if (isItPossibleToChangeWindowStatus(true)) {
            setStatus(getWindowStatus() + WINDOWS20CHANGE);
            System.out.println("Window is opened for 20%, status: " + status);
        } else {
            System.out.println("You cant open window more");
        }
    }

    @Override
    public void close20ofWindow() {
        if (isItPossibleToChangeWindowStatus(false)) {
            setStatus(getWindowStatus() - WINDOWS20CHANGE);
            System.out.println("Window is closed for 20%, status: " + status);
        } else {
            System.out.println("You cant close window more");
        }
    }

    private boolean isItPossibleToChangeWindowStatus(boolean addOrSubstract) {
        int change = 2;
        if (addOrSubstract) {
            return status + change < 10;
        } else {
            return status - change > 0;
        }

    }
}

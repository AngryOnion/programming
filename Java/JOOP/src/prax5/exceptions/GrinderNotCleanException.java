package prax5.exceptions;

/**
 * Created by Ilja on 5.10.2016.
 */
public class GrinderNotCleanException extends RuntimeException {

    public GrinderNotCleanException(String message) {
        super(message);
    }
}

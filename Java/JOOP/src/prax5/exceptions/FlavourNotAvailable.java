package prax5.exceptions;

/**
 * Created by Ilja on 08.10.2016.
 */
public class FlavourNotAvailable extends RuntimeException {

    public FlavourNotAvailable(String message) {
        super(message);
    }
}

package prax5;

import prax5.coffeeGrinders.CoffeeGrinder;
import prax5.exceptions.FlavourNotAvailable;
import prax5.exceptions.GrinderNotCleanException;

/**
 * Created by Ilja on 5.10.2016.
 */
public class Main {

    public static void main(String[] args) {
        CoffeeGrinder coffeeGrinder = new CoffeeGrinder();

        try {
            coffeeGrinder.grind();
            coffeeGrinder.grind();
            coffeeGrinder.grind();
            coffeeGrinder.grind();


        } catch (GrinderNotCleanException e) {
            System.err.println("Ouch " + e.getMessage());
            coffeeGrinder.clean();
            System.out.println("Cleaning is made!");
        } catch (IllegalArgumentException | FlavourNotAvailable e) {
            System.err.println("Ouch " + e.getMessage());
        }

        try {
            coffeeGrinder.grind();
            coffeeGrinder.addFlavour("Caramel");

        } catch (GrinderNotCleanException e) {
            System.err.println("Ouch " + e.getMessage());
            coffeeGrinder.clean();
            System.out.println("Cleaning is made!");
        } catch (IllegalArgumentException | FlavourNotAvailable e) {
            System.err.println("Ouch " + e.getMessage());
        }
    }
}

package prax5;

import org.junit.Before;
import org.junit.Test;
import prax5.coffeeGrinders.CoffeeGrinder;
import prax5.coffeeGrinders.EspressoGrinder;
import prax5.exceptions.FlavourNotAvailable;
import prax5.exceptions.GrinderNotCleanException;

import java.time.LocalTime;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;

/**
 * Created by Ilja on 5.10.2016.
 */
public class CoffeeGrinderTest {

    CoffeeGrinder coffeeGrinder;
    EspressoGrinder espressoGrinder;

    @Before
    public void prepareTests() {
        coffeeGrinder = new CoffeeGrinder();
        espressoGrinder = new EspressoGrinder();
    }

    @Test
    public void regularGrinderTest() {
        assertEquals(1.0, coffeeGrinder.grind());
    }

    @Test
    public void espressoGrinderTest() {
        assertEquals(0.3, espressoGrinder.grind());
    }

    @Test
    public void countTest() {
        coffeeGrinder.grind();
        coffeeGrinder.grind();
        assertEquals(2, coffeeGrinder.getCount());
    }

    @Test
    public void controlClean() {
        assertEquals(0, coffeeGrinder.clean());
    }

    @Test(expected = GrinderNotCleanException.class)
    public void cleanGrinderTest() {
        for (int i = 0; i < 5; i++) {
            coffeeGrinder.grind();
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void zeroMinutesSavingMode() {
        coffeeGrinder.turnOnEnergySavingMode(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void twoHoursMinutesSavingMode() {
        coffeeGrinder.turnOnEnergySavingMode(120);
    }

    @Test
    public void exceptNoExceptionOnEnergySavingMode() {
        coffeeGrinder.turnOnEnergySavingMode(1);
        coffeeGrinder.turnOnEnergySavingMode(10);
        coffeeGrinder.turnOnEnergySavingMode(60);
    }

    @Test
    public void newGrinderIsNotInEnergySavingMode() {
        assertFalse(coffeeGrinder.isInEnergySavingMode());
    }

    @Test
    public void grinderAfterGrindingInEnergySavingMode() {
        coffeeGrinder.turnOnEnergySavingMode(30);
        coffeeGrinder.grind();
        assertFalse(coffeeGrinder.isInEnergySavingMode());
    }

    @Test
    public void newGrinderIsInEnergySavingMode() {
        coffeeGrinder.turnOnEnergySavingMode(30);
        assertTrue(coffeeGrinder.isInEnergySavingMode());
    }

    @Test
    public void turnOffEnergySavingMode(){
        coffeeGrinder.turnOnEnergySavingMode(30);
        coffeeGrinder.turnOffEnergySavingMode();
        assertFalse(coffeeGrinder.isInEnergySavingMode());
    }

    @Test
    public void getCoffeeBefore6() {
        assertEquals( coffeeGrinder.getClass(), CoffeeGrinder.getRigthCoffee(LocalTime.of(5, 59)).getClass());
    }

    @Test
    public void getCoffeeAt6() {
        assertEquals( espressoGrinder.getClass(), CoffeeGrinder.getRigthCoffee(LocalTime.of(6, 0)).getClass());
    }

    @Test
    public void getCoffeeBefore11() {
        assertEquals(espressoGrinder.getClass(), CoffeeGrinder.getRigthCoffee(LocalTime.of(10, 59)).getClass());
    }

    @Test
    public void getCoffeeAt11() {
        assertEquals( coffeeGrinder.getClass(), CoffeeGrinder.getRigthCoffee(LocalTime.of(11, 1)).getClass());
    }

    @Test
    public void addVanillaToCoffee() {
        coffeeGrinder.addFlavour("Vanilla");
    }

    @Test
    public void addToffeeToCoffee() {
        coffeeGrinder.addFlavour("TOFfee");
    }

    @Test
    public void addMintToCoffee() {
        coffeeGrinder.addFlavour("PEPPERmint");
    }

    @Test(expected = FlavourNotAvailable.class)
    public void addWrongFlavour() {
        coffeeGrinder.addFlavour("Caramel");
    }

}
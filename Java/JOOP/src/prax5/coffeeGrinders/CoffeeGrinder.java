package prax5.coffeeGrinders;

import prax5.exceptions.FlavourNotAvailable;
import prax5.exceptions.GrinderNotCleanException;

import java.time.LocalTime;

/**
 * Created by Ilja on 5.10.2016.
 */
public class CoffeeGrinder {


    private boolean isInEnergySavingMode = false;
    private static final double REGULARGRIND = 1.0;
    private int count = 0;

    public static CoffeeGrinder getRigthCoffee(LocalTime time) {
        if (time.compareTo(LocalTime.of(6,0)) == -1 || time.compareTo(LocalTime.of(11,0)) == 1) {
            return new CoffeeGrinder();
        } else {
            return new EspressoGrinder();
        }
    }

    public double grind() {

        if (getCount() >= 3) {
            throw new GrinderNotCleanException("Too Dirty");
        }
        turnOffEnergySavingMode();
        addCount();
        return REGULARGRIND;
    }

    void addCount() {
        count = count + 1;
    }

    public int getCount() {
        return count;
    }

    public int clean() {
        count = 0;
        return count;
    }

    public void turnOnEnergySavingMode(int minutes) {
        if (minutes < 1 || minutes > 60) {
            throw new IllegalArgumentException();
        } else {
            isInEnergySavingMode = true;
        }
    }

    public boolean isInEnergySavingMode() {
        return isInEnergySavingMode;
    }

    public void turnOffEnergySavingMode() {
        isInEnergySavingMode = false;
    }

    public void addFlavour(String flavour) {
        if (!flavour.equalsIgnoreCase("vanilla") && !flavour.equalsIgnoreCase("toffee") &&
        !flavour.equalsIgnoreCase("peppermint")) {
            throw new FlavourNotAvailable("No such flavour");
            }
    }
}

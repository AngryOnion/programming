package prax5.coffeeGrinders;

/**
 * Created by Ilja on 5.10.2016.
 */
public class EspressoGrinder extends CoffeeGrinder {

    public static final double ESPRESSOGRIND = 0.3;

    @Override
    public double grind() {
        super.grind();
        return ESPRESSOGRIND;
    }
}

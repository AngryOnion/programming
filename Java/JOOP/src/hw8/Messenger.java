package hw8;

import java.io.BufferedWriter;
import java.io.FileWriter;


public class Messenger {

    private String message = "";
    public final static String FILEPATH = "./src/hw8/resources/messages.txt";

    public Messenger() {

    }

    public void addMessage(String newMessage) {
        if (message.length() > 0) {
            message = message + ";";
            message = message + newMessage;
        } else {
            message = message + newMessage;
        }
    }

    public String getMessage() {
        return message;
    }

    public void writeMessage() {
        message = MessagePacker.packMessage(message);
        try {
            FileWriter fstream = new FileWriter(FILEPATH, true);
            BufferedWriter fbw = new BufferedWriter(fstream);
            fbw.write(message);
            message = "";
            fbw.newLine();
            fbw.close();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}

package hw8;

    public class MessagePacker {

        public static String packMessage(String message) {
            message = message.replaceAll("[AEIOUÜÕÄÖaeiouüõäö]", "");
            message = message.replaceAll("  ", " ");
            return message;
        }
}

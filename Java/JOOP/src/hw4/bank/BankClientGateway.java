package hw4.bank;

import hw4.cards.CreditCard;
import hw4.cards.DebitCard;
import hw4.client.Client;
import hw4.client.SeniorClient;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Created by Ilja on 28.09.2016.
 */
public class BankClientGateway {

    private static ArrayList<Client> clients = new ArrayList<>();

    private static Optional<Client> createClient(long iDNumber, LocalDate dateOfBirthday) {
        Optional<Client> op = controlClientAge(iDNumber, dateOfBirthday);
        if (op.isPresent()) {
            DebitCard debitCard = new DebitCard();
            clients.add(op.get());

            op.get().setDebitCard(debitCard);
            if (clients.size() % 3 == 0 ) {
                debitCard.setBalance(debitCard.getBalance().add(new BigDecimal("100")));
            }

            if (iDNumber < 80) {
                op.get().setCreditCard(new CreditCard());
            }
            
        }
        return op;
        
    }

    public static Optional<Client> getClientByID(long iDNumber, LocalDate birthDay) {
        for (Client client: clients) {
            if (client.getIDNumber() == iDNumber) {
                return Optional.ofNullable(client);
            }
        }
        return createClient(iDNumber, birthDay);
    }

    public static BankClientGateway getInstance() {
        return new BankClientGateway();
    }

    private BankClientGateway() {
    }

    private static Optional<Client> controlClientAge(long iDNumber, LocalDate dateOfBirthday) {
        Client client;
        if (ChronoUnit.YEARS.between(dateOfBirthday, LocalDate.now()) < 7) {
            System.out.println("Too young");
            client = null;
        } else {
            if (ChronoUnit.YEARS.between(dateOfBirthday, LocalDate.now()) < 65) {
                client = new SeniorClient(iDNumber, dateOfBirthday);
            } else {
                client = new Client(iDNumber, dateOfBirthday);
            }
        }
        return Optional.ofNullable(client);

    }
}

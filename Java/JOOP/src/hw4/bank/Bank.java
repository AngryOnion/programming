package hw4.bank;

import hw4.cards.CreditCard;
import hw4.client.Client;
import hw4.client.SeniorClient;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by Ilja on 21.09.2016.
 */
public class Bank {


    public static void main(String[] args) {

        List<Optional<Client>> clients = Arrays.asList(
        BankClientGateway.getClientByID(1, LocalDate.of(1991, 1 ,1)),
        BankClientGateway.getClientByID(2, LocalDate.of(2015, 1 ,1)),
        BankClientGateway.getClientByID(15, LocalDate.of(1995, 1 ,1)),
        BankClientGateway.getClientByID(17, LocalDate.of(1997, 1 ,1)),
        BankClientGateway.getClientByID(100, LocalDate.of(1952, 1 ,1)),
        BankClientGateway.getClientByID(165, LocalDate.of(1932, 1 ,1)));


        for (Optional<Client> client: clients) {
            if (client.isPresent()) {
                System.out.println(client.get().getMonthPayment());
                client.get().getDebitCard().makePayment(BigDecimal.TEN);
                CreditCard creditCard = client.get().getCreditCard();
                if (creditCard != null) {
                    client.get().getCreditCard().makePayment(BigDecimal.TEN);
                }
                if (client.get().getCreditCardOptional().isPresent()) {
                    client.get().getCreditCardOptional().get().makePayment(BigDecimal.TEN);

                }
            }
        }

        System.out.println(new SeniorClient(25, LocalDate.of(1942,1,1)).getMonthPayment() + " senti");
        System.out.println(new Client(16, LocalDate.of(1991,1,1)).getMonthPayment() + " euro");

    }
}

package hw4.cards;

import java.math.BigDecimal;

/**
 * Created by Ilja on 24.09.2016.
 */
public class DebitCardNFC extends DebitCard {

    BigDecimal maxNFCPayment = new BigDecimal("20");

    BigDecimal NFCLimit = new BigDecimal("150");

    public DebitCardNFC() {
        super();
    }

    public void makeNFCPayment(BigDecimal amount) {
        if (amount.compareTo(maxNFCPayment) == 0
                || amount.compareTo(maxNFCPayment) == -1) {
            if (NFCLimit.subtract(amount).compareTo(BigDecimal.ZERO) == -1) {
                System.out.println("NFC maksete limiit on ületatud.");
            } else {
                System.out.println("NFC makse");
                NFCLimit = NFCLimit.subtract(amount);
                makePaymentCore(amount);
            }
        } else {
            System.out.println("Liiga suur makse NFC jaoks, palun kasutage tavalist makset");
        }
    }


}

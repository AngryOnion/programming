package hw4.cards;

import hw4.service.Card;

import java.math.BigDecimal;

/**
 * Created by Ilja on 21.09.2016.
 */
public class DebitCard implements Card {

    private BigDecimal balance;

    public DebitCard() {
        balance = new BigDecimal("1000");
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public void makePayment(BigDecimal amount) {
        if (amount.compareTo(balance) == 1) {
            System.out.println("Pole piisavalt vabaid vahendeid");
        } else {
            System.out.println("Debeetkaardi makse " + amount );
            makePaymentCore(amount);
        }

    }

    void makePaymentCore(BigDecimal amount) {
        balance = balance.subtract(amount);
    }
}

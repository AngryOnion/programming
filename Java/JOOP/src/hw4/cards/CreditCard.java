package hw4.cards;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Created by Ilja on 21.09.2016.
 */
public class CreditCard extends DebitCard {

    BigDecimal limit = new BigDecimal("700");

    public CreditCard() {
        super();
    }

    public CreditCard(BigDecimal balance) {
        setBalance(balance);
    }

    @Override
    public void makePayment(BigDecimal amount) {
        if (amount.compareTo(super.getBalance()) == -1
                || amount.compareTo(super.getBalance()) == 0) {
            System.out.println("Krediitkaardi makse");
            makePaymentCore(amount);
        } else if(amount.compareTo(super.getBalance().add(limit)) == -1
                || amount.compareTo(super.getBalance().add(limit)) == 0) {
            limit = limit.subtract(amount.subtract(super.getBalance()));
            System.out.println("Krediitkaardi makse, limiiti on kasutatud " + limit);
            makePaymentCore(amount);
        } else {
            System.out.println("Pole piisavalt vabaid vahendeid.");
        }
    }


    public BigDecimal getBalance(Calendar Cal) {
        return super.getBalance();
    }
}

package hw4.service;

import java.math.BigDecimal;

/**
 * Created by Ilja on 21.09.2016.
 */
public interface Card {

    BigDecimal getBalance ();
    void makePayment(BigDecimal amount);

}

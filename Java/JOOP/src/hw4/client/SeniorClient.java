package hw4.client;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import static java.lang.Math.toIntExact;

/**
 * Created by Ilja on 04.10.2016.
 */
public class SeniorClient extends Client {



    public SeniorClient(long IDNumber, LocalDate dateOfBirthday) {
        super(IDNumber, dateOfBirthday);
    }

    @Override
    public int getMonthPayment() {
        return toIntExact(100 - ChronoUnit.YEARS.between(super.getDateOfBirthday(), LocalDate.now()));
    }
}

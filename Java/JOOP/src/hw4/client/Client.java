package hw4.client;

import hw4.cards.CreditCard;
import hw4.cards.DebitCard;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Created by Ilja on 28.09.2016.
 */
public class Client {

    private long IDNumber;
    private LocalDate dateOfBirthday;
    private DebitCard debitCard;
    private CreditCard creditCard;


    public Client(long IDNumber) {
        this.IDNumber = IDNumber;
        this.debitCard = new DebitCard();
    }

    public Client(long IDNumber, LocalDate dateOfBirthday) {
        this.IDNumber = IDNumber;
        this.dateOfBirthday = dateOfBirthday;
    }

    public long getIDNumber() {
        return IDNumber;
    }

    public void setDebitCard(DebitCard debitCard) {
        this.debitCard = debitCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public DebitCard getDebitCard() {
        return debitCard;
    }

    public Optional<CreditCard> getCreditCardOptional() {
        return Optional.ofNullable(creditCard);
    }

    public CreditCard getCreditCard() {
        if (creditCard == null) {
            return null;
        }
        return creditCard;
    }
    public int getMonthPayment() {
        return 1;
    }

    public LocalDate getDateOfBirthday() {
        return dateOfBirthday;
    }
}

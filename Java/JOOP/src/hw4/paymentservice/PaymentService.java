package hw4.paymentservice;

import hw4.service.Card;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Ilja on 21.09.2016.
 */
public class PaymentService {

    public void process(List<Card> cards) {
        for (Card card : cards) {
            card.makePayment(new BigDecimal("3.0454547"));
            System.out.println("Jaak:" + card.getBalance());
        }

    }
}

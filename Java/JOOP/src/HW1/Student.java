package HW1;

/**
 * Created by Ilja on 8.09.2016.
 */
public class Student {

    private int points;
    private int birthYear;
    private String name;

    public Student(String name) {
        this.name = name;
    }

    public Student(String name, int points) {
        this.name = name;
        this.points = points;
    }

    public Student(String name, int points, int birthYear) {
        this.name = name;
        this.points = points;
        this.birthYear = birthYear;
    }

    public void addPoints(int pointsToAdd) {
        points = pointsToAdd;
    }

    public int getPoints() {
        return points;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public String getName() {
        return name;
    }
}

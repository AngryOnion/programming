package HW1;

/**
 * Created by Ilja on 8.09.2016.
 */
public class Main {

    public static void main(String[] args) {
        Student andres = new Student("Andres", 180);
        Student annika = new Student("Annika", 120);
        Student polina = new Student("Polina", 0);

        andres.setBirthYear(1993);
        annika.setBirthYear(1995);
        polina.setBirthYear(1996);

        System.out.println(andres.getBirthYear());
        System.out.println(annika.getBirthYear());
        System.out.println(polina.getBirthYear());

        Student ilja = new Student("Ilja", 65, 1995);

        System.out.println(ilja.getBirthYear());

    }
}

package hw9;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WineParser {

    public void createVinesFromFile() {
        List<Wine> wines = new ArrayList<>();
        try {
            Stream<String> lines = Files.lines(Paths.get("/src/hw9", "wine.txt"));
            wines = lines.map(str -> str.split(","))
                    .map(array -> new Wine(Integer.parseInt(array[0]),Double.parseDouble(array[1]),
                            Double.parseDouble(array[5]), Double.parseDouble(array[7])))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stream<Wine> wineStream = wines.stream();
        OptionalDouble maxMagnesium = wineStream.mapToDouble(Wine::getMagneesium).max();
        OptionalDouble minMagnesium = wineStream.mapToDouble(Wine::getMagneesium).min();
        OptionalDouble averageMagnesium = wineStream.mapToDouble(Wine::getMagneesium).average();
        System.out.println(String.format("Max magnesium: %s\nMin magnesium: %s\n" +
                "Average magnesium: %s",
                maxMagnesium.getAsDouble(), minMagnesium.getAsDouble(), averageMagnesium.getAsDouble()));

        wineStream.filter(wine -> wine.getMagneesium() > averageMagnesium.getAsDouble())
                .forEach(newWine -> newWine.setAdSentence("Siin veinis on palju magneesiumit!"));

        wineStream.mapToDouble(Wine::getFlavanoids).boxed()
                .sorted().limit(5).forEach(System.out::println);

        wineStream.filter(wine -> wine.getCategory() == 3)
                .forEach(newWine -> newWine.setAdSentence("Alkohol on hea sulane, aga halb peremees!" +
                        "Alkohol kahjustab tervist ja eriti teie aju!"));

        wineStream.forEach(wine -> System.out.println(wine.getTextInfo()));
    }
}

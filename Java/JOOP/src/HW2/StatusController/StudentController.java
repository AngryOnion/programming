package HW2.StatusController;

/**
 * Created by Ilja on 17.09.2016.
 */

/**
 * Interface to control, if student is allowed to open door.
 */
interface StudentController {

    /**
     * Returns boolean if student is allowed to open the door.
     * @param personalCode personal code of Student.
     * @param schoolCode school sign.
     * @return boolean if student is allowed to open the door.
     */
    boolean controlStudent(String personalCode, String schoolCode);
}

package HW2.StatusController;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Ilja on 17.09.2016.
 */
public class SchoolStudentController implements StudentController{


    List<String> schoolList = Arrays.asList("MHG", "KARDLA", "NOO", "REAAL");

    @Override
    public boolean controlStudent(String personalCode, String schoolCode) {
        return schoolList.contains(schoolCode.toUpperCase())
                && (personalCode.charAt(0) == '5'
                || personalCode.charAt(0) == '6');
    }
}

package HW2;

import HW2.Lock.SchoolDoorLock;

/**
 * Class for running all the stuff.
 */
class HW2Main {

    public static void main(String[] args) {
        new SchoolDoorLock("MHG").control("51111111111111111111");
        new SchoolDoorLock("KARLDA").control("4111111111111111111");
        new SchoolDoorLock("KARDLA").control("622222222223333333");
        new SchoolDoorLock("KAPAK,").control("522222222222222222");
        new SchoolDoorLock("NOO").control("523233333333333333");
        new SchoolDoorLock("NOO").control("11111111111111111115");
    }
}

package HW2.Lock;

import HW2.StatusController.SchoolStudentController;

/**
 * Created by Ilja on 17.09.2016.
 */

/**
 * Class for describing school door lock.
 */
public class SchoolDoorLock {

    String schoolCode;

    public SchoolDoorLock(String schoolCode) {
        this.schoolCode = schoolCode;
    }

    public void control(String personalCode) {
        if (new SchoolStudentController().controlStudent(personalCode, schoolCode)) {
            System.out.println("Õpilane isikukoodiga: " + personalCode
                    + " saab " + schoolCode + " kooli sisse");
        } else {
            System.out.println("Õpilane isikukoodiga: " + personalCode
                    + " ei saa " + schoolCode + " kooli sisse");
        }

    }
}

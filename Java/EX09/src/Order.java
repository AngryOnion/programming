import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Ilja on 11.03.2016.
 */
public class Order {

    /** Name of the orderer.*/
    private String name;

    /**Address of the client. */
    private String adress;

    /** Order number to change it. */
    public static int orderNumber = 0;

    /** Real order number. */
    private int realOrderNumber = 0;

    /** Hashmap with quantity of flowers. */
    private HashMap<String, Integer> ordered = new HashMap<>();

    /** Arraylist with all added flowers. */
    private ArrayList<Flower> allFlowers = new ArrayList<>();

    /**
     * Constructor.
      * @param name of the client.
     * @param adress of the client.
     */
    public Order(String name, String adress) {
        this.name = name;
        this.adress = adress;
        ordered.put("Roses", 0);
        ordered.put("Tulips", 0);
        ordered.put("Fair", 0);
    }

    /**
     * Returns order number, if still not paid, returns 0.
     * @return order number.
     */
    public int getOrderNumber() {
        return realOrderNumber;
    }

    /**
     * Address.
     * @return address.
     */
    public String getAddress() {
        return adress;
    }

    /**
     * Client name.
     * @return name.
     */
    public String getClient() {
        return name;
    }

    /***
     * Total price of the order.
     * @return price.
     */
    public double getTotalPrice() {
        double price = 0.00;
        for (Flower flower : allFlowers) {
            if (flower instanceof Rose) {
                if (flower instanceof FairTradeRose) {
                    price += ((FairTradeRose) flower).getPrice();
                } else {
                    price += ((Rose) flower).getPrice(ordered.get("Roses"));
                }
            } else if (flower instanceof Tulip) {
                price += ((Tulip) flower).getPrice(ordered.get("Tulips"));
            }
        }
        return price;
    }


    /**
     * Adds flower.
     * @param flower to add.
     * @return boolean, if flower is added or not.
     */
    public boolean add(Flower flower) {
        if (flower != null) {
            if (flower.getPrice() < 0 || flower.getPrice() == Double.NaN
                    || flower.getReal() < 0) {
                return false;
            }
            if (flower instanceof Rose) {
                if (flower instanceof FairTradeRose) {
                    if (flower.getReal() >= 0) {
                        ordered.put("Fair", ordered.get("Fair") + 1);
                    } else {
                        return false;
                    }
                } else {
                    ordered.put("Roses", ordered.get("Roses") + 1);
                }
            } else if (flower instanceof Tulip) {
                ordered.put("Tulips", ordered.get("Tulips") + 1);
            }
            allFlowers.add(flower);
            return true;
        }
        return false;
    }

    /**
     * Method used to generate next ordernumber.
     * @return order number.
     */
    public static int getNextOrderNumber() {
        orderNumber++;
        return orderNumber;
    }

    /**
     * Method to generate recieve for orderer.
     * @return check string.
     */
    public String pay() {
        realOrderNumber = getNextOrderNumber();
        StringBuilder payment = new StringBuilder();
        payment.append("Order: " + realOrderNumber + "\n"
                + "Client: " + name + "\n"
                + "Address: " + adress + "\n");
        if (ordered.get("Fair") > 0) {
            payment.append("Fair-trade roses: " + ordered.get("Fair") + "\n");
        }
        if (ordered.get("Roses") > 0) {
            payment.append("Roses: " + ordered.get("Roses") + "\n");
        }
        if (ordered.get("Tulips") > 0) {
            payment.append("Tulips: " + ordered.get("Tulips") + "\n");
        }
        payment.append(String.format("%.2f€",
                this.getTotalPrice()).replaceAll(",", "."));
        return payment.toString();
    }

    /**
     * Method used to get access from static method.
     * @return quantity of bought fairtrade roses.
     */
    public int getValue() {
        return ordered.get("Fair");
    }

    /**
     * Method to find person, who have bought the most fair trades, if there is no such, return empty.
     * @param orders orders.
     * @return String with names of the most caring clients.
     */
    public static String findTheMostCaringCustomer(List<Order> orders) {
        if (orders == null) {
            return null;
        }
        StringBuilder caring = new StringBuilder();
        int maxValue = 1;
        caring.append("");
        ArrayList<Order> theMost = new ArrayList<>();
        //Creates Arraylist with the most caring customers.
        for (Order order : orders) {
            if (order != null) {
                if (order.getValue() > 0 && order.getValue() >= maxValue) {
                    if (order.getValue() > maxValue) {
                        theMost.clear();
                        theMost.add(order);
                        maxValue = order.getValue();
                    } else {
                        theMost.add(order);
                    }
                }
            }
        }
        //Produce string for printing
        if (theMost.size() > 0) {
            for (int i = 0; i < theMost.size(); i++) {
                if (theMost.size() > 1) {
                    if (i == 0) {
                        caring.append(theMost.get(i).getClient() + "\n");
                    } else {
                        if (theMost.size() - 1 == i) {
                            caring.append(theMost.get(i).getClient());
                        } else {
                            caring.append(theMost.get(i).getClient());
                            caring.append("\n");
                        }
                    }
                } else {
                    caring.append(theMost.get(i).getClient());
                }
                //flow control if it needs line separator

            }
        }
        return caring.toString();
    }
}

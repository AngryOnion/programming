/**
 * Created by Ilja on 11.03.2016.
 */
public class Flower {

    /**Price of the flower. */
    private double price;

    /**
     * COnstructor.
     * @param price price of the flowers.
     */
    public Flower(double price) {
        this.price = price;
    }

    /**
     * Gets price.
     * @return price.
     */
    public double getPrice() {
        if (price < 0) {
            return 0.0;
        }
        return price;
    }

    /**
     * Method used to know if price is negative.
     * @return real price.
     */
    public double getReal() {
        return price;
    }


}

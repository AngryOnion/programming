/**
 * Created by Ilja on 16.03.2016.
 */
public class FairTradeRose extends Rose {


    /**Price of the flower. */
    private double price;

    /**
     * Constrcutor.
     * @param price of the roses.
     * @param thorns boolean if rose has thorns.
     */
    public FairTradeRose(double price, boolean thorns) {
        super(price, thorns);
        this.price = price * 2;
    }

    /**
     * Gets price.
     * @return price.
     */
    public double getPrice() {
        if (price < 0.0) {
            return 0.0;
        }
        return price;
    }

    /**
     * Method used to get price.
     * @param quantity of the flowers.
     * @return price.
     */
    public double getPrice(int quantity) {
        if (price < 0.0) {
            return 0.0;
        }
        if (quantity < 1) {
            return 0.0;
        }
        return price;
    }

    /**
     * Method to get real price of the roses.
     * @return real price.
     */
    public double getReal() {
        return price;
    }
}

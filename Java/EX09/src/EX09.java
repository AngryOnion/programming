import java.util.Arrays;

/**
 * Created by Ilja on 11.03.2016.
 */
public class EX09 {
    /**
     * Main.
     * @param args input.
     */
    public static void main(String[] args) {
        Order o4 = new Order("Gandalf", "Tallinn");
        o4.add(new Rose(10, true));
        o4.add(new FairTradeRose(8, false));
        o4.add(new FairTradeRose(12, false));
        System.out.println(o4.pay());

        Order o5 = new Order("Guido", "Mõigu");
        o5.add(new Tulip(10, "Red"));
        o5.add(new Tulip(10, null));
        o5.add(new FairTradeRose(4, false));
        o5.add(new FairTradeRose(1, false));
        o5.add(new FairTradeRose(-1, false));
        System.out.println(o5.pay());

        Order o6 = new Order("Mõtus", "Mändra");
        o6.add(new Rose(11, true));

        Order o7 = new Order("Kaunis", "Kungla");
        System.out.println("--");
        System.out.println(Order.findTheMostCaringCustomer(Arrays.asList(o4, o5)));
        System.out.println("--");

        System.out.println(o6.orderNumber);

    }
}

/**
 * Created by Ilja on 11.03.2016.
 */
public class Tulip extends Flower {

    /** Colout of the tulip. */
    private String colour;

    /** Sale procent. */
    private final double sale = 0.1;

    /** Quantity when sale starts.*/
    private final int saleQuantity = 5;

    /**
     * Constructor.
     * @param price price of the flower.
     * @param colour of the tulip.
     */
    public Tulip(double price, String colour) {
        super(price);
        this.colour = colour;
    }

    /**
     * Returns colour of the tulips.
     * @return colour as String.
     */
    public String getColour() {
        return colour;
    }

    /**
     * Returns price of the ordered flowers with sale.
     * @param quantity of the flowers.
     * @return price.
     */
    public double getPrice(int quantity) {
        if (quantity < 1) {
            return 0.0;
        }

        if (quantity >= saleQuantity) {
            return ((super.getPrice() * quantity
                    - (super.getPrice() * sale
                    * quantity)) / quantity);
        }
        return super.getPrice();
    }
}

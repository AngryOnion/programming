/**
 * Created by Ilja on 11.03.2016.
 */
public class Rose extends Flower {

    /** Boolean if rose is thorned or not. */
    private boolean thorns;

    /** Sale procent. */
    private static final double SALE = 0.05;

    /** Quantity when sale starts.*/
    private final int saleQuantity = 3;

    /**
     * COntstructor.
     * @param price price of the flower.
     * @param thorns Boolean if rose is thorned or not.
     */
    public Rose(double price, boolean thorns) {
        super(price);
        this.thorns = thorns;
    }

    /**
     * Return if the roses have thorns or not.
     * @return boolean.
     */
    public boolean getThorns() {
        return thorns;
    }

    /**
     * Returns price of the ordered flowers with sale.
     * @param quantity of the flowers.
     * @return price.
     */
    public double getPrice(int quantity) {
        if (quantity < 1) {
            return 0.0;
        }
        if (quantity >= saleQuantity) {
            return ((super.getPrice() * quantity
                    - (super.getPrice() * sale
                    * quantity)) / quantity);
        }
        return super.getPrice();
    }

}

import javafx.fxml.FXML;
import javafx.scene.image.ImageView;

import java.awt.event.KeyEvent;
import java.beans.EventHandler;

/**
 * Created by Ilja on 16.04.2016.
 */
public class Goat {

    @FXML
    private ImageView goat;
    private int goatX;
    private int goatY;

    public Goat(int x, int y) {
        this.goatX = x;
        this.goatY = y;
        goat.setX(x);
        goat.setX(y);
    }

    public int getGoatX() {
        return goatX;
    }

    public int getGoatY() {
        return goatY;
    }

    public void setGoatX(int goatX) {
        this.goatX = goatX;
    }

    public void setGoatY(int goatY) {
        this.goatY = goatY;
    }

    public void start() {
        
    }
}

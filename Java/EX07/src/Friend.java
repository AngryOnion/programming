import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ilja on 2.03.2016.
 */
public class Friend {
    /** Array with all names of friends.*/
    private ArrayList<String> name;

    /**
     * Gets lastnames of friend.
     * @return lastname of friend.
     */
    public final String getLastName() {
        String sentence = name.toString().replaceAll("\\[|\\]", "").replaceAll(", ", " ");
        String lastWord = sentence.replaceAll("^.*?(\\w+)\\W*$", "$1");
        return lastWord;
    }

    /**
     * Constructs fullname of friend.
     * @return fullname as string.
     */
    public final String getFullName() {
        return String.join(" ", name);
    }

    /**
     * List of firstnames.
     * @return fistnames as list.
     */
    public final List<String> getNames() {
        return name.subList(0, name.size() - 1);
    }

    /**
     * Change names of object, used to create object.
     * @param allNames list of names.
     */
    public final void setAllNames(List<String> allNames) {
        ArrayList<String> result = new ArrayList<>();
        for (String word : allNames) {
            if (!(word.equals("\\s") || word.equals(""))) {
                result.add(word);
            }
        }
        name = result;
    }

    /**private boolean isThereTwoPeoplesWithSameLastname() {
        String lastName = name.get(name.size() - 1);
        List<String> firstNames = name.subList(0, name.size() - 1);
        if (firstNames.contains("ja")) {
            int index = firstNames.indexOf("ja");
        }
    }*/
}

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Ilja on 2.03.2016.
 */
public class EX07 {
    /** List, which contains all friends.
     * At least it does if you have any friend.*/
    private static List<Friend> friends;

    /** Main function.
     * @param args Sisend
     */
    public static void main(String[] args) {
        friends = readFriendsFromFile("example.txt");
        System.out.println(findFriendByLastName("Begins").getFullName());
    }

    /**
     * Method, which is reading file and creating Friend objects.
     * Saves it all into list friends.
     * @param inputFilename name of file.
     * @return returns list which consist of friends.
     */
    public static List<Friend> readFriendsFromFile(String inputFilename) {
        if (inputFilename == null) {
            return null;
        }
        ArrayList<Friend> friendFromFile = new ArrayList<>();
        try {
            Path path = Paths.get(inputFilename);
            Scanner in = new Scanner(path);
            while (in.hasNextLine()) {
                Friend mate = new Friend();
                String names = in.nextLine().trim();
                mate.setAllNames(Arrays.asList(names.split(" ")));
                friendFromFile.add(mate);
            }
        } catch (IOException e) {
            return null;
        }
        friends = friendFromFile;
        return friends;
    }

    /**
     * Finds friend with lastname form array.
     * @param lastName Lastname of friend to find.
     * @return Friend, which is found or not found.
     */
    public static Friend findFriendByLastName(String lastName)   {
        Friend lastFriend = null;
        String compareWith = "~";
        for (Friend mate : friends) {
            if (mate.getLastName().equals(lastName)) {
                String thisFirstNames = String.join(" ", mate.getNames());
                thisFirstNames = thisFirstNames.toLowerCase();
                if (thisFirstNames.compareTo(compareWith) < 0) {
                    lastFriend = mate;
                    compareWith = thisFirstNames;
                }
            }
        }
        return lastFriend;
    }


    // firstname splitida " " jargi ning
    // konstrueerida seda stringbuilderiga uuesti
    /**
     *  List<Friend> FriendList = new ArrayList<>();
     List<String> lines = new ArrayList<>();

     for(int i = 0; i < lines.size(); i++) {
     String line = lines.get(i);
     String[] names = line.split(" ");
     String lastName = names[names.length - 1];
     System.out.println(lastName);
     line = line.replaceAll(" \\S*$", "");
     String[] firstnames = line.split(" ");
     StringBuilder firstName = new StringBuilder();
     for (int j = 0; j < firstnames.length; j++) {
     if (!(firstnames[j].equals(" ") || firstnames[j] == null)) {
     if (j > 0) {
     firstName.append(" " + firstnames[j]);
     } else {
     firstName.append(firstnames[j]);
     }
     }
     }
     Friend newFriend = new Friend(firstName.toString(), lastName);
     FriendList.add(newFriend);
     }
     */
}

package travel;


import java.util.function.ToDoubleBiFunction;

public class Destination implements DestinationModel {


    private Temperature temperature;
    private String name;

    public Destination(String name, Temperature temperature) {
        this.name = name;
        this.temperature = temperature;
    }

    @Override
    public String getName() {
        return name;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    @Override
    public double getAvgWeather(ToDoubleBiFunction<Double, Double> tempHandler) {
        return tempHandler.applyAsDouble(temperature.getMinTemp(), temperature.getMaxTemp());
    }



    public static double getCelsius(Temperature temperature) {
        return (temperature.getMaxTemp() + temperature.getMinTemp()) / 2 - 273.15;
    }

    public static double getFahr(Temperature temperature) {
        return (temperature.getMaxTemp() + temperature.getMinTemp()) / 2 * 9 / 5 - 459.67;
    }
}

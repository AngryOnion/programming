package studentstream;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StudentStreamer {
	
	void studentManager() {
		List<Student> students = IntStream.range(55, 65)
				.mapToObj(i -> new Student(i))
				//.mapToObj(Student::new)
				.collect(Collectors.toList());
		
		students.forEach(s -> s.setGrade(5));
		
		long cnt = students
				.stream()
				.filter(s -> s.getEarnedCreditPoints() >= 60)
				.count();
		
		System.out.println(cnt);
		
		List<Student> filteredStudents = students.stream()
				.filter(s -> s.getGrade() == 5)
				.collect(Collectors.toCollection(LinkedList::new));
		
		long sumOfPoints = students.stream()
				.filter(s -> s.getEarnedCreditPoints() >= 60)
				.mapToInt(s -> s.getEarnedCreditPoints())
				.sum();

	}
	
	public static void main(String[] args) {
		new StudentStreamer().studentManager();
	}
}





package filewriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class MyWriter {
	void writeSomething() {
		FileOutputStream fileStream;
		try {
			fileStream = new FileOutputStream("/home/martin/jtmp/error_log.txt", true);
			System.setErr(new PrintStream(fileStream));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Line 1");
		System.out.println("Line 2");
		System.err.println("Mingi viga");
	}
	
	public static void main(String[] args) {
		new MyWriter().writeSomething();
	}
}

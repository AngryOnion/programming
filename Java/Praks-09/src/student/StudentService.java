package student;

public class StudentService {
	
	private MyLogger logger;
	
	public StudentService(MyLogger logger) {
		this.logger = logger;
	}
	
	void setGrade(int grade) {
		logger.log("Grade set to " + grade);
	}
	
	public static void main(String[] args) {
		new StudentService(new FileLogger()).setGrade(5);
	}
}

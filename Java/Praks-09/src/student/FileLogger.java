package student;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class FileLogger extends MyLogger{
	
	private static final String LOG_FILE = "/home/martin/jtmp/general_log.txt";
	private PrintStream outputStream;
	
	public FileLogger() {
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(LOG_FILE, true);
			outputStream = new PrintStream(fos);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	void log(String line) {
		outputStream.println(line);
	}
}

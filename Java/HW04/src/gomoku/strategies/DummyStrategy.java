package gomoku.strategies;

import gomoku.*;
import sun.java2d.pipe.SpanShapeRenderer;

import java.util.ArrayList;
import java.util.List;

public class DummyStrategy implements ComputerStrategy {

	int[][] b;

	private static int moveCount;

	@Override
	public Location getMove(SimpleBoard board, int player) {
		// let's operate on 2-d array
		b = board.getBoard();
		for (int row = 0; row < b.length; row++) {
			for (int col = 0; col < b[0].length; col++) {
				if (b[row][col] == SimpleBoard.EMPTY) {
					// first empty location
					return new Location(row, col);
				}
			}
		}
		return null;
	}

	@Override
	public String getName() {
		return "Dummy strategy";
	}


	public static List<Location> getPossibleMoves(int[][] board) {
		List<Location> moves = new ArrayList<Location>();
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[0].length; col++) {
				if (board[row][col] == SimpleBoard.EMPTY) {
					moves.add(new Location(row, col));
				}
			}
		}
		return moves;
	}
}

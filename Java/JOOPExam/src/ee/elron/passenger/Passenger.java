package ee.elron.passenger;

import ee.elron.stations.Station;
import ee.elron.trains.Train;

/**
 * Created by Ilja on 15.01.2017.
 */
public class Passenger {

    private static int id;
    private Train train = null;
    private Station station = null;
    private Station destination = null;

    public void exitTrain() {
        this.train = null;
    }

    public void exitStation() {
        this.station = null;
    }

    public Train getTrain() {
        return train;
    }

    public void setTrain(Train train) {
        this.train = train;
        exitStation();
    }


    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
        train.removePassengerFromTrain(this);
        exitTrain();
    }

    public Station getDestination() {
        return destination;
    }

    public void setDestination(Station destination) {
        this.destination = destination;
    }
}

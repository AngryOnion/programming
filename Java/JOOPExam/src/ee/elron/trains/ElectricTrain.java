package ee.elron.trains;

import ee.elron.centralOffice.CentralOffice;
import ee.elron.railways.ElectricRailway;
import ee.elron.railways.Railway;
import ee.elron.schedule.ScheduleOfTrain;

/**
 * Created by Ilja on 15.01.2017.
 */
public class ElectricTrain extends DieselTrain {


    public ElectricTrain(int maxPassengerCount, ScheduleOfTrain schedule, CentralOffice office) {
        super(maxPassengerCount, schedule, office);
    }

    public boolean isAbleToDriveOnRailway(Railway railway) {
        return railway instanceof ElectricRailway;
    }
}

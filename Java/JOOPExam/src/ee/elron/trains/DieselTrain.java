package ee.elron.trains;

import ee.elron.centralOffice.CentralOffice;
import ee.elron.exceptions.TrainIsFullException;
import ee.elron.passenger.Passenger;
import ee.elron.schedule.ScheduleOfTrain;
import ee.elron.stations.RailwayStation;

import java.util.ArrayList;

/**
 * Created by Ilja on 15.01.2017.
 */
public class DieselTrain implements Train, Runnable {

    private int maxPassengerCount;
    private ArrayList<Passenger> passengersInTrain = new ArrayList<>();
    private ScheduleOfTrain schedule;
    private RailwayStation lastVisitedStation;
    private RailwayStation destination;
    private RailwayStation startStation;
    private CentralOffice office;
    private int workedToDayTimes = 0;


    public DieselTrain(int maxPassengerCount, ScheduleOfTrain schedule, CentralOffice office) {
        this.maxPassengerCount = maxPassengerCount;
        this.schedule = schedule;
        this.destination = schedule.getLastRailwayStation();
        this.startStation = schedule.getFirstRailwayStation();
        this.lastVisitedStation = startStation;
        this.office = office;
    }

    @Override
    public int getMaxPassengerCount() {
        return maxPassengerCount;
    }

    @Override
    public void addPassenger(Passenger passenger) {
        if (passengersInTrain.size() < maxPassengerCount) {
            passengersInTrain.add(passenger);
        } else {
            throw new TrainIsFullException();
        }
    }

    @Override
    public void removePassengerFromTrain(Passenger passenger) {
        passengersInTrain.remove(passenger);
    }

    @Override
    public ScheduleOfTrain getSchedule() {
        return schedule;
    }

    @Override
    public void run() {


        while (workedToDayTimes < 7) {
            office.moveTrainFuther(this);
            if (trainArrivedToDestination()) {
                workedToDayTimes++;
                schedule.reverse();
                destination = schedule.getLastRailwayStation();
                startStation = schedule.getFirstRailwayStation();
            }
        }

    }

    public boolean trainArrivedToDestination() {
        return lastVisitedStation.equals(destination);
    }

    @Override
    public RailwayStation getLastVisitedStation() {
        return lastVisitedStation;
    }

    @Override
    public RailwayStation getDestination() {
        return destination;
    }

    public void setLastVisitedStation(RailwayStation lastVisitedStation) {
        this.lastVisitedStation = lastVisitedStation;
    }

    public void letPeopleOutOfTrain() {
        ArrayList<Passenger> passengersNeedToExit = new ArrayList<>();
        for (Passenger passenger: passengersInTrain) {
            if (passenger.getDestination().equals(lastVisitedStation)) {
                passenger.setStation(lastVisitedStation);
                passengersNeedToExit.add(passenger);
            }
        }
        lastVisitedStation.addPassengers(passengersNeedToExit);
    }

    public void letInAsManyPeopleAsPossible(RailwayStation nextRailwayStation) {
        for (Passenger passenger: nextRailwayStation.getPassengers())  {
            if (isPossibleToTakeMorePassengers()) {
                addPassenger(passenger);
                office.helpPassengerToFindWay(passenger,this);
                nextRailwayStation.getPassengers().remove(passenger);
            } else {
                break;
            }
        }
    }

    public boolean isPossibleToTakeMorePassengers() {
        return maxPassengerCount > passengersInTrain.size();
    }

    public ArrayList<RailwayStation> getStationsLeft() {
        ArrayList<RailwayStation> stations = schedule.getStations();
        ArrayList<RailwayStation> stationsLeft = new ArrayList<>();
        for (int i = stations.indexOf(lastVisitedStation); i < stations.size(); i++) {
            stationsLeft.add(stations.get(i));
        }
        return stationsLeft;
    }

    public RailwayStation getStartStation() {
        return startStation;
    }

    public void setStartStation(RailwayStation startStation) {
        this.startStation = startStation;
    }

    @Override
    public void setSchedule(ScheduleOfTrain schedule) {
        this.schedule = schedule;
    }

    public void increaseWorked() {
        workedToDayTimes++;
    }
}

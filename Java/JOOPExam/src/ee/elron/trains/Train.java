package ee.elron.trains;


import ee.elron.passenger.Passenger;
import ee.elron.schedule.ScheduleOfTrain;
import ee.elron.stations.RailwayStation;
import ee.elron.stations.Station;

public interface Train {
    int getMaxPassengerCount();
    RailwayStation getLastVisitedStation();
    RailwayStation getDestination();
    void addPassenger(Passenger passenger);
    void removePassengerFromTrain(Passenger passenger);
    ScheduleOfTrain getSchedule();
    void letPeopleOutOfTrain();
    void letInAsManyPeopleAsPossible(RailwayStation nextRailwayStation);
    void setLastVisitedStation(RailwayStation nextRailwayStation);
    boolean trainArrivedToDestination();
    void increaseWorked();
    void setStartStation(RailwayStation startStation);
    void setSchedule(ScheduleOfTrain schedule);
}

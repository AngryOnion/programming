package ee.elron.stations;

import ee.elron.passenger.Passenger;
import ee.elron.trains.Train;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Ilja on 15.01.2017.
 */
public class RailwayStation implements Station {

    private String name;
    private ArrayList<Train> trainsInStation = new ArrayList<>();
    private ArrayList<Passenger> passengers = new ArrayList<>();

    public RailwayStation(String name) {
        this.name = name;
    }

    public void trainArrived(Train train) {
        trainsInStation.add(train);
    }

    public synchronized void addPassengers(ArrayList<Passenger> newPassengers) {
        passengers.addAll(newPassengers);
    }

    @Override
    public String getName() {
        return name;
    }


    public Collection<? extends Passenger> getPassengers() {
        return passengers;
    }
}

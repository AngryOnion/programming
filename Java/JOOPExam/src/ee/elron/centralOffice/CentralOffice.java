package ee.elron.centralOffice;

import ee.elron.passenger.Passenger;
import ee.elron.railways.ElectricRailway;
import ee.elron.railways.OneTrackRailway;
import ee.elron.railways.Railway;
import ee.elron.schedule.Schedule;
import ee.elron.schedule.ScheduleOfTrain;
import ee.elron.stations.RailwayStation;
import ee.elron.stations.Station;
import ee.elron.trains.DieselTrain;
import ee.elron.trains.ElectricTrain;
import ee.elron.trains.Train;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Ilja on 15.01.2017.
 */
public class CentralOffice {

    private ArrayList<Railway> railways = new ArrayList<>();
    private ArrayList<Station> stations = new ArrayList<>();
    private ArrayList<Schedule> schedules = new ArrayList<>();
    private Random random = new Random();

    public Railway findRailway(Station start, Station end, Train train) {
        for (Railway railway : railways) {
            if ((railway.getFirstStation().equals(start) && railway.getSecondStation().equals(end))
                    || (railway.getFirstStation().equals(end) && railway.getSecondStation().equals(start))) {

                if (train instanceof ElectricTrain) {
                    if (railway instanceof ElectricRailway) {
                        return railway;
                    }
                } else {
                    return railway;
                }
            }
        }
        return null;
    }


    public void moveTrainFuther(Train train) {
        try {
            RailwayStation nextRailwayStation = train.getSchedule().getNextRailwayStation(train.getLastVisitedStation());
            Railway railway = findRailway(train.getLastVisitedStation(), nextRailwayStation, train);
            synchronized (railways) {
                while (railway.isBlocked()) {
                    railways.wait();
                }
            }

            blockRailway(railway, train);
            Thread.sleep(train.getSchedule().getTime(nextRailwayStation));
            train.setLastVisitedStation(nextRailwayStation);
            ScheduleOfTrain scheduleOfTrain = train.getSchedule();
            if (train.trainArrivedToDestination()) {
                train.increaseWorked();
                scheduleOfTrain.reverse();
                train.setSchedule(scheduleOfTrain);
                destination = schedule.getLastRailwayStation();
                train.setDes
                train.setStartStation(scheduleOfTrain.getFirstRailwayStation());
            }
            train.letPeopleOutOfTrain();
            train.letInAsManyPeopleAsPossible(nextRailwayStation);
            unblockRailway(railway, train);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void setRailways(ArrayList<Railway> railways) {
        this.railways = railways;
    }

    public void blockRailway(Railway railway, Train train) {
        synchronized (railways) {
            railway.block(train);
            railways.notifyAll();
        }
    }

    public void unblockRailway(Railway railway, Train train) {
        synchronized (railways) {
            railway.unBlock(train);
            railways.notifyAll();
        }
    }

    public void generateRailways(ArrayList<RailwayStation> stations, boolean isElectric) {
        for (int i = 0; i < stations.size() - 1; i++) {
            railways.add(new OneTrackRailway(stations.get(i), stations.get(i+1)));
        }
    }

    /**
     * Each time passenger need to get somewhere he goes on central office website and plans his way
     */
    public void helpPassengerToFindWay(Passenger passenger, DieselTrain train) {
        ArrayList<RailwayStation> stationsLeft = train.getStationsLeft();
        passenger.setDestination(stationsLeft.get(ThreadLocalRandom.current().nextInt(1, stationsLeft.size())));
    }
}

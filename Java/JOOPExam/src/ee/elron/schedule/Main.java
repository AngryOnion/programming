package ee.elron.schedule;

import ee.elron.centralOffice.CentralOffice;
import ee.elron.stations.RailwayStation;
import ee.elron.trains.DieselTrain;

/**
 * Created by Ilja on 18.01.2017.
 */
public class Main {

    public static void main(String[] args) {
        CentralOffice office = new CentralOffice();
        ScheduleOfTrain scheduleOfTrain = new ScheduleOfTrain();
        RailwayStation station = new RailwayStation("Tartu");
        RailwayStation kul = new RailwayStation("Külitse");
        RailwayStation jog = new RailwayStation("Jõgeva");
        RailwayStation tapa = new RailwayStation("Tapa");
        RailwayStation tallinn = new RailwayStation("Tallinn");
        scheduleOfTrain.getScheduleOfDriving().put( station, (long) 100);
        scheduleOfTrain.getScheduleOfDriving().put( kul, (long) 100);
        scheduleOfTrain.getScheduleOfDriving().put( jog, (long) 100);
        scheduleOfTrain.getScheduleOfDriving().put( tapa, (long) 100);
        scheduleOfTrain.getScheduleOfDriving().put( tallinn, (long) 100);
//
//        System.out.println(scheduleOfTrain.getIndexOfRailwayStation(tallinn));
        DieselTrain dieselTrain = new DieselTrain(100, scheduleOfTrain, office);
        office.generateRailways(scheduleOfTrain.getStations());
        System.out.println(dieselTrain.getLastVisitedStation().getName());
        office.moveTrainFuther(dieselTrain);
        System.out.println(dieselTrain.getLastVisitedStation().getName());
        office.moveTrainFuther(dieselTrain);
        System.out.println(dieselTrain.getLastVisitedStation().getName());
        office.moveTrainFuther(dieselTrain);
        System.out.println(dieselTrain.getLastVisitedStation().getName());
        office.moveTrainFuther(dieselTrain);
        System.out.println(dieselTrain.getLastVisitedStation().getName());
        scheduleOfTrain.reverse();
        office.moveTrainFuther(dieselTrain);
        System.out.println(dieselTrain.getLastVisitedStation().getName());
        office.moveTrainFuther(dieselTrain);
        System.out.println(dieselTrain.getLastVisitedStation().getName());
        office.moveTrainFuther(dieselTrain);
        System.out.println(dieselTrain.getLastVisitedStation().getName());
        office.moveTrainFuther(dieselTrain);
        System.out.println(dieselTrain.getLastVisitedStation().getName());

//        Passenger passenger = new Passenger();
//        office.helpPassengerToFindWay(passenger, dieselTrain);
//        System.out.println(passenger.getDestination().getName());
    }
}

package ee.elron.schedule;

import ee.elron.stations.RailwayStation;

import java.util.*;

/**
 * Created by Ilja on 15.01.2017.
 */
public class ScheduleOfTrain implements Schedule {

    private LinkedHashMap<RailwayStation, Long> scheduleOfDriving = new LinkedHashMap<RailwayStation,Long>();
    private ArrayList<RailwayStation> stations = new ArrayList<>();
    private Random random = new Random();

    public LinkedHashMap<RailwayStation, Long> getScheduleOfDriving() {
        return scheduleOfDriving;
    }

    public void setScheduleOfDriving(LinkedHashMap<RailwayStation, Long> scheduleOfDriving) {
        this.scheduleOfDriving = scheduleOfDriving;
    }

    public Long getTime(RailwayStation stationName) {
        return scheduleOfDriving.get(stationName);
    }

    public RailwayStation getNextRailwayStation(RailwayStation station) {
        int orderNumber = getIndexOfRailwayStation(station);
        if (orderNumber < scheduleOfDriving.size() - 1) {
            return (RailwayStation) scheduleOfDriving.keySet().toArray()[orderNumber + 1];
        }
        return null;
    }

    public RailwayStation getFirstRailwayStation() {
        return (RailwayStation) scheduleOfDriving.keySet().toArray()[0];
    }

    public RailwayStation getLastRailwayStation() {
        return (RailwayStation) scheduleOfDriving.keySet().toArray()[scheduleOfDriving.size() - 1];
    }

    public int getScheduleSize() {
       return scheduleOfDriving.size();
    }

    public int getIndexOfRailwayStation(RailwayStation station) {
        Object[] stations = scheduleOfDriving.keySet().toArray();
        int i = 0;
        for (Object object: stations) {
            if (object.equals(station)) {
                return i;
            }
            i++;
        }
        return Integer.MIN_VALUE;
    }

    public void reverse() {
        LinkedHashMap<RailwayStation, Long> reversedSchedule = new LinkedHashMap<>();
        ListIterator<Map.Entry<RailwayStation, Long>> iter =
                new ArrayList<>(scheduleOfDriving.entrySet()).listIterator(scheduleOfDriving.size());

        while (iter.hasPrevious()) {
            Map.Entry<RailwayStation, Long> entry = iter.previous();
            reversedSchedule.put(entry.getKey(), entry.getValue());
        }
        scheduleOfDriving = reversedSchedule;
    }

    public ArrayList<RailwayStation> getStations() {
            return new ArrayList<>(scheduleOfDriving.keySet());
    }
}

package ee.elron.railways;

import ee.elron.stations.Station;
import ee.elron.trains.Train;

/**
 * Created by Ilja on 15.01.2017.
 */
public class OneTrackElectricRailway extends ElectricRailway {

    private Station firstStation;
    private Station secondStation;
    private Train train = null;
    private boolean blocked = false;

    @Override
    public Station getFirstStation() {
        return firstStation;
    }

    @Override
    public Station getSecondStation() {
        return secondStation;
    }

    @Override
    public String getInformation() {
        return firstStation.getName() + " - " + secondStation.getName();
    }

    @Override
    public synchronized boolean isBlocked() {
        return false;
    }

    @Override
    public void setTrain(Train train) {
        this.train = train;
    }

    public Train getTrain() {
        return train;
    }

    @Override
    public void block(Train train) {
        this.train = train;
        blocked = true;
    }

    @Override
    public void unBlock(Train train) {
        this.train = null;
        blocked = false;
    }
}

package ee.elron.railways;

import ee.elron.stations.Station;
import ee.elron.trains.Train;

/**
 * Created by Ilja on 15.01.2017.
 */
public class TwoTrackRailway implements Railway {

    private Station firstStation;
    private Station secondStation;

    private OneTrackRailway firstRailway;
    private OneTrackRailway secondRailway;

    private boolean blocked = false;
    private Train train;

    @Override
    public String getInformation() {
        return firstStation.getName() + " - " + secondStation.getName();
    }

    @Override
    public boolean isBlocked() {
        return firstRailway.isBlocked() && secondRailway.isBlocked();
    }

    @Override
    public void setTrain(Train train) {
        this.train = train;
    }

    @Override
    public void block(Train train) {
        if (!firstRailway.isBlocked()) {
            firstRailway.block(train);
        } else {
            if (!secondRailway.isBlocked()) {
                secondRailway.block(train);
            }
        }
    }

    @Override
    public void unBlock(Train train) {
        if (firstRailway.getTrain().equals(train)) {
            firstRailway.unBlock(train);
        } else {
            if (secondRailway.getTrain().equals(train)) {
                secondRailway.unBlock(train);
            }
        }
    }

    @Override
    public Station getFirstStation() {
        return firstStation;
    }

    @Override
    public Station getSecondStation() {
        return secondStation;
    }
}

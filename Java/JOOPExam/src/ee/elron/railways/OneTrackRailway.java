package ee.elron.railways;

import ee.elron.stations.Station;
import ee.elron.trains.Train;

/**
 * Created by Ilja on 15.01.2017.
 */
public class OneTrackRailway implements Railway {

    private Station firstStation;
    private Station secondStation;
    private Train train = null;
    private boolean blocked = false;

    public OneTrackRailway(Station firstStation, Station secondStation) {
        this.firstStation = firstStation;
        this.secondStation = secondStation;
    }

    @Override
    public Station getFirstStation() {
        return firstStation;
    }

    @Override
    public Station getSecondStation() {
        return secondStation;
    }

    @Override
    public String getInformation() {
        return firstStation.getName() + " - " + secondStation.getName();
    }

    @Override
    public synchronized boolean isBlocked() {
        return false;
    }

    @Override
    public void setTrain(Train train) {
        this.train = train;
    }

    public Train getTrain() {
        return train;
    }

    @Override
    public void block(Train train) {
        this.train = train;
        blocked = true;
    }

    @Override
    public void unBlock(Train train) {
        this.train = null;
        blocked = false;
    }
}

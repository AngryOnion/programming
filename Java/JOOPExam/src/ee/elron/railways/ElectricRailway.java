package ee.elron.railways;

import ee.elron.stations.Station;

/**
 * Created by Ilja on 15.01.2017.
 */
public abstract class ElectricRailway implements Railway {

    private Station firstStation;
    private Station secondStation;

    @Override
    public String getInformation() {
        return firstStation.getName() + " - " + secondStation.getName();
    }
}

package ee.elron.railways;

import ee.elron.stations.Station;
import ee.elron.trains.Train;

public interface Railway {

    String getInformation();
    boolean isBlocked();
    void setTrain(Train train);
    void block(Train train);
    void unBlock(Train train);
    Station getFirstStation();
    Station getSecondStation();


}

import javafx.animation.*;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.util.Duration;
import java.util.Random;

/**
 * Controller for the game.
 */
public class Controller {

    /**Color. */
    private final int color = 255;

    /**Radius minimum. */
    private final int radMin = 25;

    /**Start time. */
    private final int startTime = 29;

    /**timerClock. */
    @FXML private Text timerClock;

    /**Field of the game as anchorPane. */
    @FXML private AnchorPane pane;

    /**Scorelabel. */
    @FXML private Label scoreLabel;

    /**Standard duration of the game. */
    private int duration = startTime;

    /**Int to store score. */
    private int score = 0;

    /**Width of the map. */
    private final double widthLimit = 798.0;

    /**Height of the map. */
    private final double heightLimit = 513.0;

    /**Timeline of the game. */
    private Timeline timeline = new Timeline();

    /**Button. */
    @FXML private Button startButton;

    /**
     * Function to start the game.
     */
    @FXML private void start() {
        startButton.setDisable(true);
        createCircle();
    }

    /**
     * Function for timer, if time is up, stops game.
     */
    @FXML private void timer() {
        timerClock.setText("30");
        Timeline time = new Timeline();
        time.setCycleCount(Animation.INDEFINITE);
        time.getKeyFrames().add(new KeyFrame(Duration.seconds(1), event ->
            { timerClock.setText(Integer.toString(duration));
                if (duration == 0) {
                    time.stop();
                    timeline.stop();
                    pane.getChildren().clear();
                    timerClock.setText("Game over! Score: " + score);
                    duration = startTime;
                    startButton.setDisable(false);
                    score = 0;
                }
                duration -= 1;
        }));
        time.play();
    }


    /**
     * Method used to add score.
     */
    private void add() {
        score += 1;
        //scoreLabel.setText(Integer.toString(score));
        System.out.println(score);
    }

    /**
     * Function to create random circle.
     */
    @FXML private void createCircle() {
        //create circle
        timer();
        Random random = new Random();
        int radius = random.nextInt(radMin * 2) + radMin;
        Circle circle = new Circle();
        circle.setRadius(radius);
        circle.setCenterX(random.nextInt((int) widthLimit - radius * 2)
                + radius * 2);
        circle.setCenterY(random.nextInt((int) heightLimit - radius * 2)
                + radius * 2);
        System.out.println("Radius: " + radius + " Coordinates: "
                + circle.getCenterX() + " " + circle.getCenterY());
        circle.setFill(Color.rgb(random.nextInt(color),
                random.nextInt(color), random.nextInt(color)));
        circle.setOnMouseClicked(event -> add());
        move(circle);
        pane.getChildren().add(circle);
    }

    /**
     * Recursive function to make circle move.
     * @param circle to move.
     */
    @FXML private void move(Circle circle) {

        timeline.getKeyFrames().removeAll();
        Random random = new Random();
        int radius = (int) circle.getRadius();
        int randomX = 0;
        int randomY = 0;
        while (randomX < radius * 2 || randomX + radius > widthLimit
                || randomY < radius * 2
                || randomY + radius > heightLimit) {
            randomX = random.nextInt(((int) (widthLimit - radius * 2))
                    + radius * 2);
            randomY = random.nextInt(((int) (heightLimit - radius * 2))
                    + radius * 2);
        }
        System.out.println("x: " + randomX + " y: " + randomY);
        timeline = new Timeline(new KeyFrame(Duration.seconds(2),
                new KeyValue(circle.centerXProperty(), randomX,
                        Interpolator.SPLINE(1, 1, 1, 1)),
                new KeyValue(circle.centerYProperty(), randomY,
                        Interpolator.EASE_BOTH)),
                new KeyFrame(Duration.ZERO));
        timeline.play();
        timeline.setOnFinished(event -> move(circle));
    }


}
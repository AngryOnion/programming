import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**EX14. */
public class EX14 extends Application {

    /**Height. */
    private final int height = 600;

    /**Width. */
    private final int width = 800;

    /**
     * App start.
     * @param primaryStage stage of the application.
     * @throws Exception IO.
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("EX14 crazy circle");
        primaryStage.setScene(new Scene(root, width, height));
        primaryStage.setResizable(false);
        primaryStage.show();
    }


    /**
     * EX14 function.
     * @param args cmd.
     */
    public static void main(String[] args) {
        launch(args);
    }
}

/**
 * Caesar Cipher Coder/Decoder made by Ilja Samoilov.
 */
public class EX18 implements EX18Interface {

    /**
     * Constant.
     * Integer limit for array.
     */
    public  final int INTEGER_LIMIT = 255;

    /**
     * Constant.
     * Number of letters in English Alphabet.
     */
    public  final int NUMBER_OF_LETTERS = 26;

    /**
     * Constant.
     * Value in ASCII table of A.
     */
    public  final int A_ASCII = 97;

    /**
     * Constant.
     * Value in ASCII table of Z.
     */
    public  final int Z_ASCII = 122;

    /**
     * Given text and a rotation, encrypts text.
     * @param plainText plain text, readable by humanoids
     *                  with relative ease.
     * @param rotation shift
     * @return encrypted text
     */
    public  String encrypt(String plainText, int rotation) {
        if (plainText == null) {
            return null;
        } else {
            String text = plainText.toLowerCase();
            String result = "";
            if (rotation > 0 ) {
                result = rotate(text, rotation);
            } else {
                result = decrypt(text, -rotation);
            }
            return minimizeText(result);
        }
    }

    /**
     * Finds the most frequently occurring letter in text.
     * @param text either plain or encrypted text.
     * @return the most frequently occurring letter in text.
     */
    public  String findMostFrequentlyOccurringLetter(String text) {
        if (text == null) {
            return null;
        } else {
            String lowText = text.toLowerCase();
            int[] count = new int[INTEGER_LIMIT];
            int max = 0; char result = ' ';
            int maxLetter = Integer.MAX_VALUE;
            for (int i = 0; i < lowText.length(); i++) {
                if (Character.isLetter(lowText.charAt(i))) {
                    count[lowText.charAt(i)]++;
                }
                for (int j = 0; j < lowText.length(); j++) {
                    if (count[lowText.charAt(j)] > 0 && lowText.charAt(j) < maxLetter
                            && count[lowText.charAt(j)] == max) {
                        result = lowText.charAt(j);
                        max = count[lowText.charAt(j)];
                        maxLetter = lowText.charAt(j);
                    }
                    if (max < count[lowText.charAt(j)]
                            && count[lowText.charAt(j)] >= 1) {
                        max = count[lowText.charAt(j)];
                        result = lowText.charAt(j);
                        if (maxLetter > lowText.charAt(j)) maxLetter = lowText.charAt(j);
                    }
                }
            }
            return Character.toString(result).replaceAll(" ", "");
        }
    }


    /**
     * Removes the most prevalent letter from text.
     * @param text either plain or encrypted text.
     * @return text in which the most prevalent letter has been removed.
     */
    public  String minimizeText(String text) {
        if (text == null) {
            return null;
        } else {
            String lowText = text.toLowerCase();
            return lowText.replaceAll(
                    findMostFrequentlyOccurringLetter(lowText), "");
        }
    }

    /**
     * Given the initial rotation and the encrypted text, this method
     * decrypts said text.
     * @param cryptoText Encrypted text.
     * @param rotation How many letters to the right the alphabet was
     *                 shifted in order to encrypt text.
     * @return Decrypted text.
     */
    public String decrypt(String cryptoText, int rotation) {
        if (cryptoText == null) {
            return null;
        } else {
            String text = cryptoText.toLowerCase();
            String result = "";
            if (rotation > 0) {
                for (int i = 0; i < text.length(); i++) {
                    char letter = text.charAt(i);
                    char shiftedLetter = (char) (text.charAt(i)
                            - (rotation % NUMBER_OF_LETTERS));
                    if (letter >= A_ASCII && letter <= Z_ASCII) {
                        if (shiftedLetter < 'a') {
                            result += (char) (text.charAt(i)
                                    + (NUMBER_OF_LETTERS
                                    - (rotation % NUMBER_OF_LETTERS)));
                        } else {
                            result += (char) (text.charAt(i)
                                    - (rotation % NUMBER_OF_LETTERS));
                        }
                    } else {
                        result += letter;
                    }
                }
            } else {
                result = rotate(text, -rotation);
            }
            return result;
        }
    }

    public String rotate(String text, int rotation) {
        String result = "";
        for (int i = 0; i < text.length(); i++) {
            char letter = text.charAt(i);
            char shiftedLetter = (char) (text.charAt(i)
                    + (rotation % NUMBER_OF_LETTERS));
            if (Character.isLetter(letter)) {
                if (shiftedLetter > 'z') {
                    result += (char) (text.charAt(i)
                            - (NUMBER_OF_LETTERS
                            - (rotation % NUMBER_OF_LETTERS)));
                } else {
                    result += shiftedLetter;
                }
            } else {
                result += letter;
            }
        }
        return result;
    }
}

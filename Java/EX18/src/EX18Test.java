import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class EX18Test {
    public EX18Interface ex18Interface;
    static List<Object> manyEX18;

    @Parameterized.Parameters
    public static Collection<Object> instancesToTest() {
        if (manyEX18 == null) return Arrays.asList(new EX18());
        return manyEX18;
    }

    public EX18Test(EX18Interface ex18Interface) {
        this.ex18Interface = ex18Interface;
    }

    // example
    @Test
    public void TestEncrypt() {
        assertEquals("abc", ex18Interface.decrypt("bcd", 1));
    }

    @Test
    public void TestDecrypt() {
        assertEquals("if live gives you onions, try not to cry!",
                ex18Interface.decrypt("Mj pmzi kmziw csy srmsrw, xvc rsx xs gvc!", 4));
    }

    @Test
    public void ZeroRotationEncrypt() {
        assertEquals("i am kiy kiy ca", ex18Interface.encrypt("I am kitty kitty cat", 0));
    }

    @Test
    public void ZeroRotationDecrypt() {
        assertEquals("i am kitty kitty cat", ex18Interface.decrypt("I am kitty kitty cat", 0));
    }

    @Test
    public void DecryptNonLetters() {
        assertEquals("!@#$%^&*(z", ex18Interface.decrypt("!@#$%^&*(a", 1));
    }

    @Test
    public void EncryptNonLetters() {
        assertEquals("!@#$%^&*(c", ex18Interface.encrypt("!@#$%^&*(ba", 1));
    }

    @Test
    public void NegativeRotationEncrypt() {
        assertEquals("bc", ex18Interface.encrypt("bcd", -1));
    }

    @Test
    public void BigRotationEncrypt() {
        assertEquals("o gs qoe qoe ig", ex18Interface.encrypt("I am kitty kitty cat", 110));
    }

    @Test
    public void NegativeBigRotationEncrypt() {
        assertEquals("bc", ex18Interface.encrypt("bcd", -27));
    }

    @Test
    public void NegativeRotationDecrypt() {
        assertEquals("if live gives you onions, try not to cry!",
                ex18Interface.decrypt("Gd jgtc egtcq wms mlgmlq, rpw lmr rm apw!", -2));
    }

    @Test
    public void BigRotationDecrypt() {
        assertEquals("if live gives you onions, try not to cry!",
                ex18Interface.decrypt("Ol robk mobky eua utouty, zxe tuz zu ixe!", 110));
    }

    @Test
    public void Minimize() {
        assertEquals("i am kiy kiy ca!!!!!!",
                ex18Interface.minimizeText("I am kitty kitty cat!!!!!!"));
    }

    @Test
    public void MinimizeSameAmount() {
        assertEquals("bbbb", ex18Interface.minimizeText("bbbbaaaa"));
    }

    @Test
    public void FindMostFrequent() {
        assertEquals("t", ex18Interface.findMostFrequentlyOccurringLetter(
                "I am kitty kitty cat!!!!!!"));
    }

    @Test
    public void FindMostFrequentCase() {
        assertEquals("a", ex18Interface.findMostFrequentlyOccurringLetter(
                "bbbbAaAa"));
    }


    @Test
    public void FindMostFrequentNull() {
        assertEquals(null, ex18Interface.findMostFrequentlyOccurringLetter(null));
    }

    @Test
    public void DecryptNull() {
        assertEquals(null, ex18Interface.decrypt(null, 10));
    }

    @Test
    public void EncryptNull() {
        assertEquals(null, ex18Interface.encrypt(null, 10));
    }

    @Test
    public void MinimizeNull() {
        assertEquals(null, ex18Interface.minimizeText(null));
    }
}
/**
 * Created by Ilja on 16.02.2016.
 */
public class testEX04 {
    /** Guess parameter array index for guess X value*/
    public static final int FIELD_X = 0;
    /** Guess parameter array index for guess Y value*/
    public static final int FIELD_Y = 1;
    /** Settings parameter array index for matrix height value*/
    public static final int FIELD_MATRIX_HEIGHT = 0;
    /** Settings parameter array index for matrix wdith value */
    public static final int FIELD_MATRIX_WIDTH = 1;
    /** Settings parameter array index for matrix target x value */
    public static final int FIELD_TARGET_X = 2;
    /** Settings parameter array index for matrix target y value*/
    public static final int FIELD_TARGET_Y = 3;
    /** The count of settings parameters */
    public static final int INITIAL_PARAMETER_ARRAY_LENGTH = 4;
    /** The count of guess parameters */
    public static final int GUESS_ARRAY_LENGTH = 2;
    /** Maximum matrix dimension value */
    public static final int MAX_DIMENSION = 10;
    /** Precision for double checking */
    public static final double ERROR_BOUND = 0.001;

    public static void main(String[] args) {
        char[][] matrix = createMatrix(10, 10, 0, 0);
        System.out.println(matrix);
        guess(matrix, 2, 2);
        guess(matrix, 2, 2);
        guess(matrix, 3, 3);
        System.out.println(guess(matrix, 0, 0));
        System.out.println(getAsciiMatrix(matrix));

    }
    public static char[][] createMatrix(int n, int m, int x, int y) {
        char[][] matrix = new char[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                matrix[i][j] = '.';
            }
        }
        matrix[x][y] = 'x';
        return matrix;
    }


    public static double guess(char[][] matrix, int x, int y) {
        if (FIELD_X != FIELD_TARGET_X && FIELD_Y != FIELD_TARGET_Y) {
            matrix[x][y] = '*';
            //sd sd
            int xDistance = FIELD_TARGET_X - x;
            int yDistance = FIELD_TARGET_Y - y;
            double distance;
            distance = Math.sqrt((xDistance * xDistance)
                        + (yDistance * yDistance));
            return distance;
        } else {
            return 0.0;
        }
    }

    public static String getAsciiMatrix(char[][] matrix) {
        StringBuilder asciiMatrix = new StringBuilder();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                asciiMatrix.append(matrix[i][j]);
            }
            asciiMatrix.append("\n");
        }

        return asciiMatrix.toString();
    }

}


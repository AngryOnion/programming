package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ArrayList<Double> data = new ArrayList<>();
	    Scanner in = new Scanner(System.in);
        int a = in.nextInt(); int count = 0; double average = 0;
        while (count < a) {
            double number = in.nextDouble();
            data.add(number);
            average += number;
            count++;
        }
        average = Math.floor(100 * average / a ) / 100;
        double kokku = 0;
        System.out.println("Keskmine: " + average);
        for (int i = 0; i < a; i++) {
            double viga = average - data.get(i);
            double squareViga = Math.pow(viga, 2);
            System.out.println(i+1 + ". " + data.get(i) + "   " + viga + "   " + squareViga);
            kokku += squareViga;
        }
        System.out.println("Kokku viga: " + kokku);
    }
}

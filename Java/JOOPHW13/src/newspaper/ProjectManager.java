package newspaper;

public class ProjectManager {

	void putAllToWork() {
		OrderStorage storage = new OrderStorage();
		
		SalesOffice salesOffice = new SalesOffice(storage);
		
		RegularWorker worker1 = new RegularWorker("Praktikant", storage, salesOffice);
		RegularWorker worker2 = new RegularWorker("Homer Simpson", storage, salesOffice);
		RegularWorker worker3 = new RegularWorker("Philip J. Fry", storage, salesOffice);
		RegularWorker worker4 = new RegularWorker("Sipsik", storage, salesOffice);

		Thread salesOfficeThread = new Thread(salesOffice);

		Thread workerThread1 = new Thread(worker1);
		Thread workerThread2 = new Thread(worker2);
		Thread workerThread3 = new Thread(worker3);
		Thread workerThread4 = new Thread(worker4);

		salesOfficeThread.start();
		workerThread1.start();
		workerThread2.start();
		workerThread3.start();
		workerThread4.start();
	}
	
	public static void main(String[] args) {
		new ProjectManager().putAllToWork();
	}
}

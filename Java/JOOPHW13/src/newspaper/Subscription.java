package newspaper;

import java.util.function.IntFunction;

/**
 * Created by Ilja on 06.12.2016.
 */
public interface Subscription {

    int getNumberOfCopiesInWeek();
    double getPrice(IntFunction<Double> handler);

}

package newspaper;

public class RegularWorker implements Runnable {

	private String name;
	private OrderStorage storage;
	private SalesOffice salesOffice;

	public RegularWorker(String name, OrderStorage storage, SalesOffice salesOffice) {
		this.name = name;
		this.storage = storage;
		this.salesOffice = salesOffice;
	}

	private void doSomeWork(Subscription subscription) {
		if (subscription instanceof PrivateSubscription) {
			System.out.println("Worker name: " + name);
			System.out.println("Person name: " + ((PrivateSubscription) subscription).getName());
			System.out.println("Address: " + ((PrivateSubscription) subscription).getAddress());
			System.out.println("Number of copy: " + ((PrivateSubscription) subscription).getCopyNumber());
			System.out.println("Order length: " + ((PrivateSubscription) subscription).getOrderLengthInMonths());
			System.out.println("Order id: " + ((PrivateSubscription) subscription).getOrderNumber());
		}
        if (subscription instanceof BussinessSubscription) {
            System.out.println("Registrycode: " + ((BussinessSubscription) subscription).getRegistryCode());
        }

		System.out.println("Price: " + subscription.getPrice(value -> {
            PrivateSubscription dummy = (PrivateSubscription) subscription;
            if (dummy.getOrderLengthInMonths() > 6) {
                return (value * 0.55)*0.9;
            }
            return value * 0.55;
        }));
		System.out.println();
	}

	private void processNextOrder() throws InterruptedException {
		Subscription subscription = storage.getOrder();
		if (subscription != null) {
			doSomeWork(subscription);
		}
	}



	@Override
	public void run() {
		while (salesOffice.isThereMoreWork()) {
			try {
				processNextOrder();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

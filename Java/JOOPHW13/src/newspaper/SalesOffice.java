package newspaper;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Ilja on 06.12.2016.
 */
public class SalesOffice implements Runnable {

    public AtomicInteger getSequence() {
        return sequence;
    }

    private static Boolean moreWork = true;

    private static AtomicInteger sequence = new AtomicInteger(0);
    private OrderStorage storage;

    public SalesOffice(OrderStorage storage) {
        this.storage = storage;
    }

    public PrivateSubscription createPrivateSubscription() {
        return new PrivateSubscription("John Smith", "Akadeemia tee 15b", sequence.getAndIncrement(), ThreadLocalRandom.current().nextInt(1, 13), ThreadLocalRandom.current().nextInt(1, 10));
    }
    public BussinessSubscription createBussinessSubscription(int months) {
        return new BussinessSubscription("John Smith", "Akadeemia tee 15b", sequence.getAndIncrement(), months, ThreadLocalRandom.current().nextInt(1, 10), "105465465");
    }

    private void generateSubscriptions() throws InterruptedException {
            if (sequence.get() <= 900) {
                storage.addOrder(createPrivateSubscription());
            } else if (sequence.get() > 900 && sequence.get() <= 1350) {
                storage.addOrder(createBussinessSubscription(4));
            } else {
                storage.addOrder(createBussinessSubscription(9));
            }
            Thread.sleep(2);
    }

    public synchronized boolean isThereMoreWork() {
        if (sequence.get() == 1600) {
            this.moreWork = false;
            notifyAll();
        }
        return moreWork;
    }

    public static AtomicInteger getSequenceStatic() {
        return sequence;
    }

    @Override
    public void run() {
        while (isThereMoreWork()) {
            try {
                generateSubscriptions();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}

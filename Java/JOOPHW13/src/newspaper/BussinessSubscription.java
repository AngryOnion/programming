package newspaper;

/**
 * Created by Ilja on 06.12.2016.
 */
public class BussinessSubscription extends PrivateSubscription  {
    private String registryCode;

    public BussinessSubscription(String name, String address, int orderNumber, int orderLengthInMonths, int copyNumber, String registryCode) {
        super(name, address, orderNumber, orderLengthInMonths, copyNumber);
        this.registryCode = registryCode;
    }

    @Override
    public int getNumberOfCopiesInWeek() {
        return super.getCopyNumber() * 5;
    }

    public String getRegistryCode() {
        return registryCode;
    }
}

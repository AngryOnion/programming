package newspaper;

import java.util.ArrayList;
import java.util.List;

public class OrderStorage {

	private List<Subscription> orders = new ArrayList<>();


	public void addOrder(Subscription subscription) {
		synchronized(orders) {
			orders.add(subscription);
			orders.notifyAll();
		}
	}
	
	public Subscription getOrder() throws InterruptedException {
		synchronized (orders) {
			while (orders.isEmpty()) {
				if (SalesOffice.getSequenceStatic().get() == 1600) {
					orders.notifyAll();
					return null;
				} else {
					orders.wait();
				}
			}
			return orders.remove(0);
		}
	}
}

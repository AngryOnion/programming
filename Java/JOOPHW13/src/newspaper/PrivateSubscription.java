package newspaper;

import java.util.function.IntFunction;

/**
 * Created by Ilja on 06.12.2016.
 */
public class PrivateSubscription implements Subscription {
    private String name;
    private String address;
    private int orderNumber;
    private int orderLengthInMonths;
    private int copyNumber;


    public PrivateSubscription(String name, String address, int orderNumber, int orderLengthInMonths, int copyNumber) {
        this.name = name;
        this.address = address;
        this.orderNumber = orderNumber;
        this.orderLengthInMonths = orderLengthInMonths;
        this.copyNumber = copyNumber;
    }

    @Override
    public int getNumberOfCopiesInWeek() {
        return copyNumber * 6;
    }

    @Override
    public double getPrice(IntFunction<Double> handler) {
        return handler.apply(getNumberOfCopiesInWeek());
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public int getOrderLengthInMonths() {
        return orderLengthInMonths;
    }

    public int getCopyNumber() {
        return copyNumber;
    }
}

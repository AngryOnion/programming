import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ilja on 10.05.2016.
 */
public class EX19 {

    public static int[] testDog(String dog) {

        int[] result = new int[2];
        int dead = 0;
        int alive = 0;

        if (dog == null) {
            return result;
        }

        String words[] = dog.split("\\s+");

        Pattern deadDog = Pattern.compile("koer");
        Pattern aliveDog = Pattern.compile("Koer");


        for (int i = 0; i < words.length; i++ ) {

            String word = words[i];
            Matcher findDead = deadDog.matcher(word);
            Matcher findAlive = aliveDog.matcher(word);

            if (findAlive.find()) {
                alive++;
            }
            if (findDead.find()) {
                dead++;
            }
        }
        result[1] = dead; result[0] = alive;

        return result;
    }

    public static String reverse(String original) {
        if (original == null) return null;
        if (original == "") return "";
        String[] words = original.split("\\s+");
        String[] result = new String[words.length];
        for (int i = 0; i < words.length; i++) {

            String resultWord = "";
            String word = words[i];
            String originalWord = words[i];

            StringBuilder reversed = new StringBuilder(word);
            word = reversed.reverse().toString().toLowerCase();

            boolean[] upperCases = new boolean[originalWord.length()];
            for (int j = 0; j < word.length(); j++) {
                char letter  = word.charAt(j);
                if (!(Character.isLetter(letter)) && Character.isUpperCase(originalWord.charAt(j)))  {
                    upperCases[word.length() - 1 - j] = true;
                } else if (Character.isUpperCase(originalWord.charAt(j))) {
                    upperCases[j] = true;
                }
            }

            for (int j = 0; j < word.length(); j++) {
                char letter = word.charAt(j);

                if (Character.isLetter(letter) && upperCases[j]) {
                    resultWord += Character.toUpperCase(letter);
                } else {
                       resultWord += letter;
                }
            }

            original = original.replace(originalWord,resultWord);
            result[i] = resultWord;
        }
        return original;
    }

    public static void main(String[] args) {
        System.out.println(testDog("Koeratoit toidukoeratäpp toidukoor kalamaja TäpikKoer koe")[0]);
        System.out.println(testDog("Koeratoit toidukoeratäpp toidukoor kalamaja TäpikKoer koe")[1]);
        System.out.println(testDog("koerkoerkoer")[1]);
    }
}

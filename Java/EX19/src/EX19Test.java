import static org.junit.Assert.*;

/**
 * Created by Ilja on 10.05.2016.
 */
public class EX19Test {

    @org.junit.Test
    public void testReverse() throws Exception {
        assertEquals("Eret", EX19.reverse("Tere"));
    }
    @org.junit.Test
    public void Notletters() throws Exception {
        assertEquals("!?.", EX19.reverse(".?!"));
    }
    @org.junit.Test
    public void MoreSpaces() throws Exception {
        assertEquals("a    b", EX19.reverse("a    b"));
    }
    @org.junit.Test
    public void Mixed() throws Exception {
        assertEquals("!ereT", EX19.reverse("Tere!"));
    }



}
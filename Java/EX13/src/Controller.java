import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;


/**
 * Controller for FXML.
 */
public class Controller {

    /**Cookie shaped button, click and get score. */
    @FXML private Button cookieButton = new Button();

    /**Label with score count. */
    @FXML
    private Text scoreLabel;

    /**Button to make click better.  */
    @FXML private Button addCursor = new Button();

    /**BUtton to add clickers. */
    @FXML private Button addClicker = new Button();

    /**LAbel with number of cursors. */
    @FXML private Text cursorNumber;

    /**Label with number of clickers. */
    @FXML private Text clickerNumber;

    /** */
    @FXML private Button information = new Button();

    /** Limit. */
    private final int limit = 1000;

    /**Score used to store. */
    private int score = 0;

    /**Score added for click. */
    private int scoreForClick = 1;

    /** Number of clickers. */
    private int clickers = 0;

    /** Startprice. */
    private final int startPrice = 20;

    /**Addiotional price for each cursor. */
    private int cursorPrice = startPrice;

    /** Start. */
    private final int startPriceClikcer = 100;

    /** Each clicker is more expensive. */
    private int clickerPrice = startPriceClikcer;

    /** Start.*/
    private final int start = 5100;

    /**Start seconds. */
    private int seconds = start;

    /**Timer for clickers. */
    private Timeline timeline = new Timeline();

    /**Adder. */
    private final int adder = 100;

    /**Width. */
    private final int width = 500;

    /**Height. */
    private final int height = 150;


    /**
     * Method used to clicker timer.
     */
    @FXML
    private void clikcerTimer() {
        timeline.stop();
        timeline = new Timeline(
                new KeyFrame(Duration.ZERO, event -> clicker()),
                new KeyFrame(Duration.millis(seconds)));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    /**
     * Method, when cookie is clicked.
     */
    @FXML
    private void clikingCookie() {
        score += scoreForClick;
        scoreLabel.setText(Integer.toString(score));
        controlButtons();
    }

    /**
     * Method for addcursor button.
     */
    @FXML
    private void addCursor() {
        if (score >= cursorPrice) {
            scoreForClick += 1;
            score -= cursorPrice;
            cursorPrice += startPrice;
            cursorNumber.setText(Integer.toString(scoreForClick));
            addCursor.setText("Cursor " + cursorPrice);
            controlButtons();
        }
    }

    /**
     * Method to add clicker.
     */
    @FXML private void addClicker() {
        if (seconds > limit && score >= clickerPrice) {
            clickers++;
            score -= clickerPrice;
            clickerPrice += 1;
            seconds -= adder;
            clickerNumber.setText(Integer.toString(clickers));
            addClicker.setText("Clicker " + clickerPrice);
            clikcerTimer();
            controlButtons();
        }
    }

    /**
     * Clicker method.
     */
    @FXML private void clicker() {
        score += 1;
        scoreLabel.setText(Integer.toString(score));
        controlButtons();
    }

    /** Method used to change clicker button style. */
    @FXML private void mouseMovedClicker() {
        if (clickerPrice > score) {
            addClicker.setId("transparent");
        } else {
            addClicker.setId("rich-red");
        }
    }


    /**Method used to change cursor button style. */
    @FXML private void mouseMovedCursor() {
        if (cursorPrice > score) {
            addCursor.setId("transparent");
        } else {
            addCursor.setId("rich-red");
        }
    }

    /**
     * Method used to control, if its needed to show buttons.
     */
    @FXML private void controlButtons() {
        if (clickerPrice > score) {
            addClicker.setId("transparent");
        } else {
            addClicker.setId("rich-blue");
        }
        if (cursorPrice > score) {
            addCursor.setId("transparent");
        } else {
            addCursor.setId("rich-blue");
        }

    }

    /**
     * Changes style of the add cursor button when clicked.
     */
    @FXML private void addCursorClick() {
        if (score >= cursorPrice) {
            addCursor.setId("rich-shadow");
        }

    }

    /**
     * Changes clicker button style when clicked.
     */
    @FXML private void addClickerClick() {
        if (score >= cursorPrice) {
            addClicker.setId("rich-shadow");
        }
    }

    /**
     * Method used to open popup window.
     * @throws IOException reads fxml file.
     */
    @FXML private void openPopup() throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("popup.fxml"));
        stage.setTitle("Information");
        stage.setScene(new Scene(root, width, height));
        stage.show();
    }

    /**
     * Changes information button style, when mouse moved.
     */
    @FXML private void informationMoved() {
        information.setId("information-red");
    }

    /**
     * Changes information button style, when clicked.
     */
    @FXML private void informationClikced() {
        information.setId("information-blue");
    }

    /**
     * Changes information button style back.
     */
    @FXML private void information() {
        information.setId("button-bevel-grey");
    }
}

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**Main. */
public class EX13 extends Application {

    /** Height. */
    private final int height = 600;

    /**Width. */
    private final int width = 500;

    /**
     * Method used to open the stage.
     * @param primaryStage stage.
     * @throws Exception .
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Cookie clicker");
        primaryStage.setScene(new Scene(root, height, width));
        primaryStage.setResizable(false);
        primaryStage.show();
    }


    /**
     * EX13 method.
     * @param args launch.
     */
    public static void main(String[] args) {
        launch(args);
    }
}

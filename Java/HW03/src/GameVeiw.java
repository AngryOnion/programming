package ilja.uss;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Region;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import java.util.ArrayList;
import java.util.Random;
/**
 * Created by Ilja on 22.04.2016.
 */
public class GameVeiw extends View {
    // Path testPath;
    /**Snake path. */
    private Path path;
    /**Paint style of the snake. */
    private Paint paint;
    /**Paint style of treasure. */
    private Paint gold;
    /**Array with positions of the snake, used to draw lines between points. */
    private ArrayList<float[]> position = new ArrayList<>();
    /**Start location. */
    private final float[] startLocation = {50, 50};
    /**Location of the head. */
    private float[] currentLocation = startLocation;
    /**Direction of the snake. */
    private float[] currentDirection = {1, 1};
    /**Height of the screen. */
    private float height;
    /**Width of the screen. */
    private float width;
    /**Boolean if to play or not to play. */
    private boolean play = true;
    /**Start size of the snake.  */
    private final int startSize = 100;
    /**Rectangle of the treasure. */
    private Rect treasure;
    /**Size of the snake. */
    private int snakeSize;
    /**Speed of the snake. */
    private final int speed = 15;
    /**Standard width of the snake. */
    private final int standardWidth = 15;
    /**Fat snake after eaten treasure. */
    private final int fatSnake = 25;
    /**Treasure size. */
    private final int treasureSize = 50;
    /** Treasure location x limit. */
    private final int treasureLocationX = 1000;
    /** Treasure location y limit.*/
    private final int treasureLocationY = 2000;
    /** Variable used to not create treasure somewhere near the
     * border of the screen.*/
    private final int border = 200;
    /**Score. */
    private int score = 0;


    /**
     * Constructor.
     * @param context standard.
     */
    public GameVeiw(Context context) {
        super(context);
        path = new Path();
        paint = new Paint();
        //testPath = new Path();
        treasure = new Rect();
        gold = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.BLUE);
        paint.setStrokeWidth(standardWidth);
        gold.setStyle(Paint.Style.FILL);
        gold.setColor(Color.RED);
        createRunnable();
        snakeSize = startSize;
        createTreasure();
    }

    /**
     * Draws canvases.
     * @param canvas .
     */
    @Override protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        render();
        height = canvas.getHeight();
        width = canvas.getWidth();
        canvas.drawPath(path, paint);
        canvas.drawRect(treasure, gold);
    }


    /**
     * Registers screen touch and changes direction.
     * @param event mouse touch.
     * @return true.
     */
    @Override public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX() - currentLocation[0];
        float y = event.getY() - currentLocation[1];
        float length = (float) Math.sqrt(x * x + y * y);
        x = x / length;
        y = y / length;
        currentDirection = new float[]{x, y};
        return true;
    }

    /**
     * Moves snake to this coordinates.
     * @param x width parameter.
     * @param y height parameter.
     */
    private void move(float x, float y) {
        float[] newLocation = {x, y};

        collisionCheck((int) x, (int) y);
        Region region = new Region();
        Region clip = new Region(0, 0, (int) width, (int) height);
        region.setPath(path, clip);
        //intersection control

        currentLocation = newLocation;
        position.add(newLocation);
        //if treasure
        if (treasure.contains((int) x, (int) y)) {
            eat();
        }
        render();
        invalidate();
    }



    /**
     * Creates treasure in random place of the map.
     */
    private void createTreasure() {
        Random random = new Random();
        int x = 0;
        int y = 0;
        Region region = new Region();
        Region clip = new Region(0, 0, (int) width, (int) height);
        region.setPath(path, clip);
        while (x <= 0 || y <= 0 || region.contains(x, y)) {
            x = random.nextInt(treasureLocationX - border)
                    + border;
            y = random.nextInt(treasureLocationY - border)
                    + border;
        }
        System.out.println(x + "  " + y);
        treasure = new Rect(x, y, x + treasureSize, y + treasureSize);
        invalidate();
    }

    /**
     * Eats treasure and makes snake fatter, like all types of fast foods.
     */
    private void eat() {
        paint.setStrokeWidth(fatSnake);
        snakeSize += speed;
        score += 1;
        createTreasure();
    }


    /**
     * Function used to invalidate changes and draw.
     */
    private void render() {
        //make snake thinner again
        if (position.size() == snakeSize) {
            paint.setStrokeWidth(standardWidth);
        }
        if (position.size() > snakeSize) {
            position.remove(0);
        }
        path.reset();

        path.moveTo(position.get(0)[0], position.get(0)[1]);
        for (int i = 0; i < position.size(); i++) {
            path.lineTo(position.get(i)[0], position.get(i)[1]);
        }
    }

    /**
     * Creates runnable to make moving possible.
     */
    private void createRunnable() {
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                float moveX = currentLocation[0] + currentDirection[0] * speed;
                float moveY = currentLocation[1] + currentDirection[1] * speed;

                if (moveX < 0 || moveX > width) {
                    currentDirection[0] *= -1;
                }
                if (moveY < 0 || moveY > height) {
                    currentDirection[1] *= -1;
                }
                move(currentLocation[0] + currentDirection[0] * speed,
                        currentLocation[1] + currentDirection[1] * speed);
                if (play) {
                    handler.postDelayed(this, 0);
                }
                }
        };
        handler.post(runnable);

    }

    public int getScore() {
        return score;
    }

    /**
     * Checks if there is collision on moving with itself.
     * @param x1 line x.
     * @param y1 line y.
     */
    private void collisionCheck(int x1, int y1) {
        if (position.size() > speed) {

            int x2 = (int) position.get(position.size() - 1)[0];
            int y2 = (int) position.get(position.size() - 1)[1];

            for (int i = 2; i < position.size() - 2; i++) {

                int x3 = (int) position.get(position.size() - i)[0];
                int y3 = (int) position.get(position.size() - i)[1];

                int x4 = (int) position.get(position.size() - i - 1)[0];
                int y4 = (int) position.get(position.size() - i - 1)[1];

                if (collision(x1, y1, x2, y2, x3, y3, x4, y4)) {
                    play = false;
                    break;
                }
            }
        }
    }


    /**
     * Check if there is intersection between two lines.
     * @param x1 line1 start point x coordinate.
     * @param y1 line1 start point y coordinate.
     * @param x2 line1 end point x coordinate.
     * @param y2 line1 end point y coordinate.
     * @param x3 line2 start point x coordinate.
     * @param y3 line2 start point y coordinate.
     * @param x4 line2 end point x coordinate.
     * @param y4 line2 end point y coordinate.
     * @return boolean, if lines collisions.
     */
    private boolean collision(int x1, int y1, int x2, int y2,
                              int x3, int y3, int x4, int y4) {

        // check if it is zero length line.
        if (x1 == x2 && y1 == y2 || x3 == x4 && y3 == y4) {
            return false;
        }
        // compute differences.
        double ax = x2 - x1;
        double ay = y2 - y1;
        double bx = x3 - x4;
        double by = y3 - y4;
        double cx = x1 - x3;
        double cy = y1 - y3;

        double alphaNumerator = by * cx - bx * cy;
        double commonDenominator = ay * bx - ax * by;

        if (commonDenominator > 0) {
            if (alphaNumerator < 0 || alphaNumerator > commonDenominator) {
                return false;
            }
        } else if (commonDenominator < 0) {
            if (alphaNumerator > 0 || alphaNumerator < commonDenominator) {
                return false;
            }
        }
        double betaNumerator = ax * cy - ay * cx;
        if (commonDenominator > 0) {
            if (betaNumerator < 0 || betaNumerator > commonDenominator) {
                return false;
            }
        } else if (commonDenominator < 0) {
            if (betaNumerator > 0 || betaNumerator < commonDenominator) {
                return false;
            }
        }
        if (commonDenominator == 0) {
            // The lines are parallel.
            // Check if they're collinear.
            // Determinant.
            double collinearityTestForP3 = x1 * (y2 - y3)
                    + x2 * (y3 - y1) + x3 * (y1 - y2);
            // If p3 is collinear with p1 and p2 then p4 will also be collinear,
            // since p1-p2 is parallel with p3-p4
            if (collinearityTestForP3 == 0) {
                // The lines are collinear. Now check if they overlap.
                if (x1 >= x3 && x1 <= x4
                        || x1 <= x3 && x1 >= x4
                        || x2 >= x3 && x2 <= x4
                        || x2 <= x3 && x2 >= x4
                        || x3 >= x1 && x3 <= x2
                        || x3 <= x1 && x3 >= x2) {
                    if (y1 >= y3 && y1 <= y4
                            || y1 <= y3  && y1 >= y4
                            || y2 >= y3 && y2 <= y4
                            || y2 <= y3 && y2 >= y4
                            || y3 >= y1 && y3 <= y2
                            || y3 <= y1 && y3 >= y2) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }
}


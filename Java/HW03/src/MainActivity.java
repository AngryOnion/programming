package ilja.uss;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/**
 * Main method.
 */
public class MainActivity extends AppCompatActivity {

    /**Game. */
    private GameVeiw gameVeiw;


    /**
     * Creates view.
     * @param savedInstanceState .
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gameVeiw = new GameVeiw(this);
        setContentView(gameVeiw);
    }

/**
    public void endGame() {

        SharedPreferences prefs = this.getSharedPreferences() ;
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("key", gameVeiw.getScore());
        editor.commit();
    }
*/

    //public void stopClick(View view) {
      //  RelativeLayout relativeLayout = (RelativeLayout)
    // findViewById(R.id.view);
        //relativeLayout.stop
    //}
}

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.stream.IntStream;

/**
 * EX00 solution
 * Created by Ilja Samoilov on 3.02.2016.
 */
public class EX00 {

    static ArrayList<Double> realX = new ArrayList<>();
    static ArrayList<Double> realY = new ArrayList<>();

    /**
     * Add two numbers.
     *
     * @param a first number
     * @param b second number
     * @return sum
     */
    public static int sum(int a, int b) {
        return a + b;
    }

    /**
     * Check whether the number is even.
     *
     * @param number the number to check
     * @return true if even, false if odd
     */
    public static boolean isEven(int number) {
        if (number % 2 == 0) {
            return true;
        }
        if (number % 2 != 0) {
            return false;
        }
        else {
            return false;
        }
    }

    /**
     * Program entry point
     *
     * @param args system arguments
     */
    public static void main(String[] args) {
//        System.out.println(sum(2, 2));
//        System.out.println(isEven(2));
//        int count = 9;
//        String s = "";
//        String str = "";
//        ListIterator<int[]> iter = Arrays.asList(IntStream.range(0, count).toArray()).listIterator();
//        while (iter.hasPrevious()) {
//            str += s;
//        }
//        double[] xS = {6.35, 6.45, 6.55, 6.65, 6.75 , 6.85, 6.95, 7.05};
//        double[] yS = {316.29, 330.36, 344.85, 359.76, 375.09, 390.85, 407.06, 423.70};
//        am1(xS, -0.04, true);
//        am1(yS, 0.8, false);
        byte[] bytes = decodeRaw("U+00DC");
        System.out.println(bytes);
    }

    public String recursive(String str, int countMore) {
        if (countMore > 1) {
            return recursive(str + str, countMore - 1);
        }
        return str;
    }

    public static void am1(double[] array, double add, boolean x) {
        for (double i: array) {
            double chaged= i+add;
            if (x) {
                realX.add(chaged);
            } else {
                realY.add(chaged);
            }
            System.out.println(chaged);
        }
    }

    private static byte[] decodeRaw(String encodedStr) {
        int decoded_byte_length = 0, next_pos = 0, start_pos;
        byte[] decoded_bytes_stream = new byte[encodedStr.length()];
        while (next_pos < encodedStr.length()) {
            char next_char = encodedStr.charAt(next_pos++);
            int next_byte;
            if (next_char == '\\' && (next_pos + 2) < encodedStr.length() && encodedStr.charAt(next_pos) == 'x') {
                start_pos = ++next_pos;
                next_pos += 2;
                String hex_str = encodedStr.substring(start_pos, next_pos);
                next_byte = Integer.parseInt(hex_str, 16);
            } else {
                next_byte = next_char & 0x0ff;
            }
            decoded_bytes_stream[decoded_byte_length++] = (byte) next_byte;
        }
        byte[] result = new byte[decoded_byte_length];
        System.arraycopy(decoded_bytes_stream, 0, result, 0, result.length);
        return result;
    }
}
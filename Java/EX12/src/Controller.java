import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Ilja on 2.04.2016.
 */
public class Controller {

    /** Equation. */
    @FXML
    private TextField formula;


    /** Time. */
    @FXML
    private CategoryAxis xAxis = new CategoryAxis();

    /**Average likes. */
    @FXML
    private NumberAxis yAxis = new NumberAxis();

    /**LineChart. */
    @FXML
    private LineChart<String, Number> lineChart;


    /**
     * Method used by button to change graphic formula.
     * @throws IOException rea dsfile.
     */
    public final void change() throws IOException {
        lineChart.getData().clear();
        XYChart.Series series = new XYChart.Series();
        ArrayList<Double> data = readFile("Average.txt");
        String equation = formula.getText().trim();
        if (equation.equals("x") || equation.equals("")) {
            for (int i = 0; i < data.size(); i++) {
                String time = i + ":00";
                series.getData().add(new XYChart.Data(time, data.get(i)));
            }
        } else {
           for (int j = 0; j < data.size(); j++) {
               String time = j + ":00";
               double average = 0.0;
               if (equation.contains("+") || equation.contains("-")) {
                   String[] parts = equation.split("-|\\+");
                   for (int i = 0; i < parts.length; i++) {
                       if (parts[i].contains("x")) {
                           if (parts[i].contains("^")) {
                               String[] multipliers = parts[i].split("x");
                               if (multipliers.length > 2) {
                                   double multiplier = Double.parseDouble(
                                           multipliers[0].replaceAll(" ", ""));
                                   int power = Integer.parseInt(
                                           multipliers[1].replaceAll(" ", "").substring(1));
                                   average += multiplier
                                           * Math.pow(data.get(j), power);
                               } else {
                                   int power = Integer.parseInt(
                                           multipliers[0].replaceAll(" ", "").substring(1));
                                   average += Math.pow(data.get(j), power);
                               }

                           } else {
                               String[] multipliers = parts[i].split("x");
                               double multiplier = Double.parseDouble(multipliers[0].replaceAll(" ", ""));
                               average += (data.get(j) * multiplier);
                           }
                       } else {
                           average += Double.parseDouble(parts[i]);
                       }
                   }
               } else {
                   if (equation.contains("x")) {
                       if (equation.contains("^")) {
                           String[] multipliers = equation.split("x");
                           if (multipliers.length > 2) {
                               double multiplier = Double.parseDouble(
                                       multipliers[0].replaceAll(" ", ""));
                               int power = Integer.parseInt(
                                       multipliers[1].replaceAll(" ", "").substring(1));
                               average += multiplier
                                       * Math.pow(data.get(j), power);
                           } else {
                               int power = Integer.parseInt(
                                       multipliers[0].replaceAll(" ", "").substring(1));
                               average += Math.pow(data.get(j), power);
                           }
                       } else {
                           String[] multipliers = equation.split("x");
                           double multiplier = Double.parseDouble(
                                   multipliers[0].replaceAll(" ", ""));
                           average += (data.get(j) * multiplier);
                       }
               } else {
                       average += Double.parseDouble(equation);
                   }
               }
               series.getData().add(new XYChart.Data(time, average));
           }

        }

        lineChart.getData().add(series);
    }


    /**
     * Method to read file.
     * @param fileName name of the file.
     * @return List with average likes.
     * @throws IOException reads file.
     */
    public static ArrayList<Double> readFile(String fileName)
            throws IOException {
        ArrayList<Double> readed = new ArrayList<>();
        Path path = Paths.get(fileName);
        Scanner reader = new Scanner(path);
        String line = reader.nextLine();
        String[] times = line.split(";");
        for (int i = 0; i < times.length; i++) {
            double average = Double.parseDouble(times[i]);
            readed.add(average);
        }
        return readed;
    }
}

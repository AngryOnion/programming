
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;



/**
 * EX12 function.
 */
public class EX12 extends Application {
    /** Width. */
    private final int prefWidth = 800;

    /** Height. */
    private final int prefHeight = 600;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("EX12");
        Scene scene = new Scene(root, prefWidth, prefHeight);
        primaryStage.setScene(scene);
        primaryStage.show();

    }


    /**
     * Start.
     * @param args input.
     */
    public static void main(String[] args) {
        launch(args);
    }
}

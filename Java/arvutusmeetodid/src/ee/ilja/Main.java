package ee.ilja;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    static ArrayList<BigDecimal> arguments = new ArrayList<>();
    static ArrayList<BigDecimal> values = new ArrayList<>();
    static double[] doubles = {};

    public static void main(String[] args) {
        generate();


//        Arrays.stream(valuesDouble).forEach(d -> values.add(new BigDecimal(d)));
        System.out.println("-----ARGUMENTS-------");
        System.out.println(" 6 = " + getPowerSum(6).toString());
        System.out.println(" 5 = " + getPowerSum(5).toString());
        System.out.println(" 4 = " + getPowerSum(4).toString());
        System.out.println(" 3 = " + getPowerSum(3).toString());
        System.out.println(" 2 = " + getPowerSum(2).toString());
        System.out.println(" 1 = " + getPowerSum(1).toString());
        System.out.println("-------VALUES--------");
        System.out.println(" 3 = " + getPowerY(3));
        System.out.println(" 2 = " + getPowerY(2));
        System.out.println(" 1 = " + getPowerY(1));
        System.out.println(" 0 = " + getPowerY(0));

        System.out.println(getPowerSum(6).toString() + "a+" + getPowerSum(5).toString() + "b+" + getPowerSum(4).toString() + "c+"
                + getPowerSum(3).toString() + "d=" + getPowerY(3));
        System.out.println(getPowerSum(5).toString() + "a+" + getPowerSum(4).toString() + "b+" + getPowerSum(3).toString() + "c+" + getPowerSum(2).toString() + "d=" + getPowerY(2));
        System.out.println(getPowerSum(4).toString() + "a+" + getPowerSum(3).toString() + "b+" + getPowerSum(2).toString() + "c+" + getPowerSum(1).toString() + "d=" + getPowerY(1));
        System.out.println(getPowerSum(3).toString() + "a+" + getPowerSum(2).toString() + "b+" + getPowerSum(1).toString() + "c+" + "8d=" + getPowerY(0));
        System.out.println("TRAPETS");
        System.out.println(getTrapets());
        System.out.println("Simpson");
        System.out.println(getSimpson());
    }

    private static void generate() {
        arguments.add(new BigDecimal("6.31"));
        arguments.add(new BigDecimal("6.41"));
        arguments.add(new BigDecimal("6.51"));
        arguments.add(new BigDecimal("6.61"));
        arguments.add(new BigDecimal("6.71"));
        arguments.add(new BigDecimal("6.81"));
        arguments.add(new BigDecimal("6.91"));
        arguments.add(new BigDecimal("7.01"));

        double[] valuesDouble = {317.09, 331.16, 345.65, 360.56, 375.89, 391.65, 407.86, 425.5};
        values.add(new BigDecimal("317.09"));
        values.add(new BigDecimal("331.16"));
        values.add(new BigDecimal("345.65"));
        values.add(new BigDecimal("360.56"));
        values.add(new BigDecimal("375.89"));
        values.add(new BigDecimal("391.65"));
        values.add(new BigDecimal("407.86"));
        values.add(new BigDecimal("425.5"));
    }

    static BigDecimal getPowerSum(int power) {
        BigDecimal sum = BigDecimal.ZERO;
        for (BigDecimal d : arguments) {
            sum = sum.add(d.pow(power));
        }
        sum = sum.setScale(2, RoundingMode.HALF_UP);
        return sum;
    }

    static BigDecimal getPowerY(int power) {
        BigDecimal sum = BigDecimal.ZERO;
        for (int i = 0; i < arguments.size(); i++) {
            sum = sum.add(arguments.get(i).pow(power).multiply(values.get(i)));
        }
        sum = sum.setScale(2, RoundingMode.HALF_UP);
        return sum;
    }

    static BigDecimal getTrapets() {
        BigDecimal sum = BigDecimal.ZERO;
        for (int i = 0; i < values.size() - 1; i++) {
            if (i == 0 || i == values.size() - 2) {
                sum = sum.add(values.get(i));
            } else {
                sum = sum.add(values.get(i).multiply(BigDecimal.valueOf(2)));
            }
            System.out.println(sum);
        }
        return sum;
    }

    static BigDecimal getSimpson() {
        BigDecimal sum = BigDecimal.ZERO;
        for (int i = 0; i < values.size() - 1; i++) {
            if (i == 0 || i == values.size() - 2) {
                sum = sum.add(values.get(i));
            } else {
                if (i % 2 != 0) {
                    sum = sum.add(values.get(i).multiply(BigDecimal.valueOf(4)));
                } else {
                    sum = sum.add(values.get(i).multiply(BigDecimal.valueOf(2)));
                }
            }
            System.out.println(sum);
        }
        return sum;
    }
}

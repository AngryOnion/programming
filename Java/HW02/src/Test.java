/**
 * Created by Ilja on 18.03.2016.
 */
public class Test {

    public static int[][] grid;

    public static void main(String[] args) {
        grid = new int[20][10];
        fill();
        System.out.println(getAsciiMatrix(grid));
    }

    public static void fill() {
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < 10; j++) {
                grid[i][j] = 0;
            }
        }
    }

    public static String getAsciiMatrix(int[][] matrix) {
        StringBuilder asciiMatrix = new StringBuilder();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < 10; j++) {
                asciiMatrix.append(matrix[i][j]);
            }
            asciiMatrix.append("\n");
        }

        return asciiMatrix.toString();
    }

}

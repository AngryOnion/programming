/**
 * Created by Ilja on 18.03.2016.
 */
import java.io.IOException;

/**
 * Homework 02 - Droptris AI.
 * https://courses.cs.ttu.ee/pages/ITI0011:HW02_Droptris
 */
public class HW02 {
    /**
     * The main method. You can use this to initialize the game.
     * Tester will not execute the main method.
     * @param args Arguments from command line
     */
    public static void main(String[] args) {
        Configuration c = new Configuration(1, 1, 1);
        run(c.toString());
    }

    /**
     * Deivder for counter.
     */
    public final int devider = 3;

    /**
     * Optional setup. This method will be called
     * before the game is started. You can do some
     * precalculations here if needed.
     *
     * If you don't need to precalculate anything,
     * just leave it empty.
     */
    public static void setup() {
    }

    /**
     * The method to execute your AI.
     * @param connectionString JSON-formatted connection string.
     *                         If you implement Socket connection yourself
     *                         you should use this string directly when connecting.
     *                         If you use DroptrisConnection, you can ignore that.
     * @return The final score. You should read the score from the server.
     */
    public static int run(String connectionString) {
        try {
            // false - only "O" blocks are sent
            DroptrisConnection conn = new DroptrisConnection("ilsamo", true);
            String line;
            // read "welcome" line from connection
            line = conn.readLine();
            System.out.println(line);
            // read block sadad

            System.out.println(line);
            // parse line.., get the block
            // send column index and rotation
            int count = 1;
            line = conn.readLine();
            while (line != null) {
                System.out.println(line);
                if (line.equals("{\"block\": \"O\"}")) {
                    if (count == 1 || (count - 1) % devider == 0) {
                        conn.sendAction("{\"column\": 0, \"rotation\": 0}");
                    } else if (count == 2 || (count - 2) % devider == 0) {
                        conn.sendAction("{\"column\": 2, \"rotation\": 0}");
                    } else {
                        conn.sendAction("{\"column\": 4, \"rotation\": 0}");
                    }
                    count++;
                } else {
                    conn.sendAction("{\"column\": 6, \"rotation\": 1}");
                }
                line = conn.readLine();
            }

            System.out.println(conn.readScoreData());
            System.out.println("game over!");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }
}

public class Main {

    private static String rec(String string) {
        if (string.length() < 3) return string;
        return string.charAt(2)
                + rec(string.substring(3))
                + string.charAt(1);
    }

    public static void main(String[] args) {
        int sum = -1;
        for (int i = 1; i <= 5; i++) {
            if (i % 7 == i + i) {
                System.out.println("i broke " + i);
                break;
            } else if (i % 3 == 0) {
                sum += 3;
                System.out.println("I am upper " + sum);
            } else if (i % 5 == 0 || i % 3 == 0) {
                sum += 2;
                System.out.println(sum);
            }
            sum = sum + 1;
            System.out.println(i + " " + sum);
        }

        System.out.println(sum);

    }
}

class Human {
    int hands = 2;
    int getHands() {
        return hands;
    }
}
class Ton extends Human {
    @Override
    int getHands() {
        return 1;
    };
}



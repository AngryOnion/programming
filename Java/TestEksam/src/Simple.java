/**
 * Created by Ilja on 25.05.2016.
 */
public class Simple {
    public static int y = 10, x = 100;
    public static void main(String[] args) {
        int x = 0;
        int z = x + 0;
        System.out.println("main x,y,z: " + x + " " + y + " " + z);
        z = world(hello(x), y);
        System.out.println("main x,y,z: " + x + " " + y + " " + z);
        for (x = 8; x++ < 60; x += 3) {
            x += x / 2;
        }
        System.out.println("x:" + x);
        String go = "pistre";
        for (int i = 0; i < 3; i++) {
            go = go.substring(0, i) + go.charAt(i + 2) +
                    go.charAt(i + 1) + go.charAt(i) + go.substring(i + 3);
        }
        System.out.println(go);
    }
    public static void hello(int x, int z) {
        x = z + 100;
        System.out.println("hello(world(x, z))");
    }
    public static int hello(int x) {
        int y = 0;
        ++x;
        while (x < 100) {
            if (y > 2)
                continue;
            break;
        }
        System.out.println("hello x,y: " + (x - 2) + " " + y);
        return x - 2;
    }
    public static int world(int x, int z) {
        int y = 0;
        y--;
        while (y++ <= 3)
            z += y;
        System.out.println("world x,z++,z: " + x + " " + z + " " + z);
        return z;
    }
}
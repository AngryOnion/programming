/**
 * Created by Ilja on 25.05.2016.
 */
public class ReadMe {
    public static int a = 5, b = 6;
    public static void main(String[] args) {
        int c = bar(foo(a), foo(a++, b));
        System.out.println(c);
        System.out.println("main a: " + a + " b: " + b);
        for (int i = b, k = 0; i + k++ < 8; i++) {
            while (k-- > 0) {
                System.out.println(i + " ja k+i" + " ja " + k);
            }
            k++;
        }
        String error = "kraud";
        for (int err = 0; err < 3; err++) {
            error = error.substring(err, err + 2)
                    + error.charAt(err * 2 + 1) + error.substring(err + 2);
        }
        System.out.println(error);
    }
    public static int foo(int a) {
        System.out.println("foo a:" + a);
        if (a-- <= 1) {
            return 1;
        }
        return foo(a - 3) + a;
    }
    public static int foo(int b, int c) {
        System.out.println("foo a:" + (++a) + " b:" + b);
        return a + b / 2;
    }
    public static int bar(int b, int x) {
        int y = a * x + b;
        int a = 1;
        return y - (--a);
    }
}

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * Main Class.
 */
public class EX11 extends Application {

    /**
     * Label with changed text.
     */
    @FXML
    private Label changed;
    /**
     * Textfield where from text is changed.
     */
    @FXML
    private TextField text;

    /**
     * Height.
     */
    private final int prefHeight = 300;

    /**
     * Width.
     */
    private final int prefWidth = 300;

    /**
     * Method to change label with text.
     * @param actionEvent makes action.
     */
    public final void changeText(ActionEvent actionEvent) {
        changed.setText(text.getText());
    }

    @Override
    /**
     * Starts the application.
     */
    public final void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("EX11");
        primaryStage.setScene(new Scene(root, prefWidth, prefHeight));
        primaryStage.show();
    }


    /**
     * Main method.
     * @param args args.
     */
    public static void main(String[] args) {
        launch(args);
    }
}

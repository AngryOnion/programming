import sun.misc.BASE64Decoder;
import sun.misc.IOUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.zip.GZIPInputStream;

/**
 * Caesar Cipher Coder/Decoder made by Ilja Samoilov.
 */
public class EX03 {

     /**
      * Constant.
      * Integer limit for array.
      */
     public static final int INTEGER_LIMIT = 255;

     /**
     * Constant.
     * Number of letters in English Alphabet.
     */
    public static final int NUMBER_OF_LETTERS = 26;

     /**
     * Constant.
     * Value in ASCII table of A.
     */
    public static final int A_ASCII = 97;

     /**
     * Constant.
         * Value in ASCII table of Z.
     */
    public static final int Z_ASCII = 122;

        /**
     * Given text and a rotation, encrypts text.
     * @param plainText plain text, readable by humanoids
     *                  with relative ease.
     * @param rotation shift
     * @return encrypted text
     */
    public static String encrypt(String plainText, int rotation) {
        if (plainText == null) {
            return null;
        } else {
            String text = plainText.toLowerCase();
            String result = "";
            if (rotation > 0) {
                result = rotate(text, rotation);
            } else {
                result = decrypt(text, -rotation);
            }
            return minimizeText(result);
        }
    }

    /**
     * Finds the most frequently occurring letter in text.
     * @param text either plain or encrypted text.
     * @return the most frequently occurring letter in text.
     */
    public static String findMostFrequentlyOccurringLetter(String text) {
        if (text == null) {
            return null;
        } else {
            String lowText = text.toLowerCase();
            int[] count = new int[INTEGER_LIMIT];
            int max = 0; char result = ' ';
            int maxLetter = Integer.MAX_VALUE;
            for (int i = 0; i < lowText.length(); i++) {
                if (Character.isLetter(lowText.charAt(i))) {
                    count[lowText.charAt(i)]++;
                }
                for (int j = 0; j < lowText.length(); j++) {
                    if (max > 1 && lowText.charAt(j) < maxLetter
                            && count[lowText.charAt(j)] == max) {
                        result = lowText.charAt(j);
                        max = count[lowText.charAt(j)];
                        maxLetter = lowText.charAt(j);
                    }
                    if (max < count[lowText.charAt(j)]
                            && count[lowText.charAt(j)] >= 1) {
                        max = count[lowText.charAt(j)];
                        result = lowText.charAt(j);
                    }
                }
            }
            return Character.toString(result).replaceAll(" ", "");
        }
    }


    /**
     * Removes the most prevalent letter from text.
     * @param text either plain or encrypted text.
     * @return text in which the most prevalent letter has been removed.
     */
    public static String minimizeText(String text) {
        if (text == null) {
            return null;
        } else {
            String lowText = text.toLowerCase();
            return lowText.replaceAll(
                    findMostFrequentlyOccurringLetter(lowText), "");
        }
    }

    /**
     * Given the initial rotation and the encrypted text, this method
     * decrypts said text.
     * @param cryptoText Encrypted text.
     * @param rotation How many letters to the right the alphabet was
     *                 shifted in order to encrypt text.
     * @return Decrypted text.
     */
    public static String decrypt(String cryptoText, int rotation) {
        if (cryptoText == null) {
            return null;
        } else {
            String text = cryptoText.toLowerCase();
            String result = "";
            if (rotation > 0) {
                for (int i = 0; i < text.length(); i++) {
                    char letter = text.charAt(i);
                    char shiftedLetter = (char) (text.charAt(i)
                            - (rotation % NUMBER_OF_LETTERS));
                    if (letter >= A_ASCII && letter <= Z_ASCII) {
                        if (shiftedLetter < 'a') {
                            result += (char) (text.charAt(i)
                                    + (NUMBER_OF_LETTERS
                                    - (rotation % NUMBER_OF_LETTERS)));
                        } else {
                            result += (char) (text.charAt(i)
                                    - (rotation % NUMBER_OF_LETTERS));
                        }
                    } else {
                        result += letter;
                    }
                }
            } else {
                result = rotate(text, -rotation);
            }
            return result;
        }
    }

     public static String rotate(String text, int rotation) {
         String result = "";
         for (int i = 0; i < text.length(); i++) {
             char letter = text.charAt(i);
             char shiftedLetter = (char) (text.charAt(i)
                     + (rotation % NUMBER_OF_LETTERS));
             if (Character.isLetter(letter)) {
                 if (shiftedLetter > 'z') {
                     result += (char) (text.charAt(i)
                             - (NUMBER_OF_LETTERS
                             - (rotation % NUMBER_OF_LETTERS)));
                 } else {
                     result += shiftedLetter;
                 }
             } else {
                 result += letter;
             }
         }
         return result;
     }

     public static String decompress(final byte[] compressed) throws IOException {
         String outStr = "";
         if ((compressed == null) || (compressed.length == 0)) {
             return "";
         }
         if (isCompressed(compressed)) {
             GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(compressed));
             BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(gis, "UTF-8"));

             String line;
             while ((line = bufferedReader.readLine()) != null) {
                 outStr += line;
             }
         } else {
             outStr = new String(compressed);
         }
         return outStr;
     }

    public static boolean isCompressed(final byte[] compressed) {
        return (compressed[0] == (byte) (GZIPInputStream.GZIP_MAGIC)) && (compressed[1] == (byte) (GZIPInputStream.GZIP_MAGIC >> 8));
    }

    /**
     * The main method, which is the entry point of the program.
     * @param args Arguments from the command line
     */
    public static void main(String[] args) throws IOException {
//        System.out.println(encrypt("I am kitty kitty cat", 0)); // => i am kiy kiy ca
//        System.out.println(decrypt("Mj pmzi kmziw csy srmsrw, xvc rsx xs gvc!", 4)); // => if live gives you onions, try not to cry!
//        System.out.println(findMostFrequentlyOccurringLetter("")); // => ""
//        System.out.println(minimizeText("I am kitty kitty cat")); // i am kiy kiy ca
//        System.out.println(rotate("If live gives you onions, try not to cry!", 4)); //Mj pmzi kmziw csy srmsrw, xvc rsx xs gvc!
//        System.out.println("keytool -import -noprompt -trustcacerts -alias ERGOSAPDEV -file C:/ERGOEUP/CERT1.crt -keystore C:/ERGOEUP/keystore-devcgi.jks -storepass changeit".replaceAll("\\\\", "/"));
        String example = "H4sICJBQzFYAA3dhdAClUm1LwzAQ/i2mwtJbWyrKpJZ+kI3BEMVSFhdWnW3XLTW1gTGxdFj/uslWxeE2EcPl7sm95SG5yOvYHTfxEHJfWZanODJNfZV4wXKRFXNrthDPXRYtumKa4hP7ASXzKi1ruOE1Px7EvAxfbLlmooYQcZtpcA5zXkFaFoAjhwyueiahQc9QyPWdcYgINgjWG6vMGn8Bcy27Uj7xJuO7y9gq2Bn/5ZLtRvs5HKb5TxL7bvl7lx9E9rznwXgDqMscwxXOmJLepE+IiNWHBkoNC6X9C+aw9rB4JCMakAl1hXWf535xK898zKh0rSvlzuNNGrlWYj0J2WFEhcqYUAzJFKr3mtZlfwqWHKgsLoFrdojydFaU0kEYZBC2zpqYHLkjFrZOPc3ONA38O2AaspJmXi+XONL1dvL2AYxCY57kAgAA";
        BASE64Decoder decoder = new BASE64Decoder();

        byte[] imageByte = decoder.decodeBuffer(example);

//        System.out.println(decompress(imageByte).replaceAll(";", ";\n"));

        String hex = "cafebabe0000003400760a00200038090039003a07003b08003c0a003d00\n" +
                "3e0a0039003f0800400800410800420a003d004308004408004508004607\n" +
                "00470800480a000e00490b004a004b0b004a004c0a004d004e08004f0700\n" +
                "50037fffffff0a005100520a005100530a004d00540800550b004a00560a\n" +
                "0057005803800000000a0057004b0a0057004c07005901000a4649525354\n" +
                "5f48494e540100124c6a6176612f6c616e672f537472696e673b01000d43\n" +
                "6f6e7374616e7456616c756501000b5345434f4e445f48494e5401000a54\n" +
                "484952445f48494e540100063c696e69743e010003282956010004436f64\n" +
                "6501000f4c696e654e756d6265725461626c650100046d61696e01001628\n" +
                "5b4c6a6176612f6c616e672f537472696e673b295601000d537461636b4d\n" +
                "61705461626c650100166765744c6576656e73687465696e44697374616e\n" +
                "6365010033284c6a6176612f6c616e672f4368617253657175656e63653b\n" +
                "4c6a6176612f6c616e672f4368617253657175656e63653b294907005a07\n" +
                "005b010034284c6a6176612f6c616e672f4368617253657175656e63653b\n" +
                "4c6a6176612f6c616e672f4368617253657175656e63653b492949010010\n" +
                "67657446757a7a7944697374616e6365010045284c6a6176612f6c616e67\n" +
                "2f4368617253657175656e63653b4c6a6176612f6c616e672f4368617253\n" +
                "657175656e63653b4c6a6176612f7574696c2f4c6f63616c653b29490700\n" +
                "5c07005d01000a536f7572636546696c6501000b50757a7a6c652e6a6176\n" +
                "610c0026002707005e0c005f006001000650757a7a6c650100e94120636f\n" +
                "6d706f6e656e74206f6620736f66747761726520636f6e66696775726174\n" +
                "696f6e206d616e6167656d656e742c2076657273696f6e20636f6e74726f\n" +
                "6c2c20616c736f206b6e6f776e206173207265766973696f6e20636f6e74\n" +
                "726f6c206f7220736f7572636520636f6e74726f6c2c2069732074686520\n" +
                "6d616e6167656d656e74206f66206368616e67657320746f20646f63756d\n" +
                "656e74732c20636f6d70757465722070726f6772616d732c206c61726765\n" +
                "207765622073697465732c20616e64206f7468657220636f6c6c65637469\n" +
                "6f6e73206f6620696e666f726d6174696f6e2e0700610c006200630c0064\n" +
                "0065010078496e20636f6d707574696e672c207468652064696666207574\n" +
                "696c6974792069732061206461746120636f6d70617269736f6e20746f6f\n" +
                "6c20746861742063616c63756c6174657320616e6420646973706c617973\n" +
                "2074686520646966666572656e636573206265747765656e2074776f2066\n" +
                "696c65732e01002454686973206973206120474954207265706f7369746f\n" +
                "727920796f752061726520696e2101004553747564792074686520706173\n" +
                "7420696620796f7520776f756c6420646566696e65207468652066757475\n" +
                "72652e20476f206261636b20616e642074727920616761696e210c006200\n" +
                "27010024202a2052756e2077697468206120706172616d6574657220666f\n" +
                "72207468652068696e7401002e202a2052756e20776974682074776f2070\n" +
                "6172616d657465727320666f72207468652068696e74206e6f2074776f01\n" +
                "002e202a205468657265206a757374206d69676874206265206f6e65206d\n" +
                "6f72652068696e7420736f6d6577686572650100226a6176612f6c616e67\n" +
                "2f496c6c6567616c417267756d656e74457863657074696f6e0100185374\n" +
                "72696e6773206d757374206e6f74206265206e756c6c0c0026006307005a\n" +
                "0c006600670c0068006907006a0c006b006c01001e5468726573686f6c64\n" +
                "206d757374206e6f74206265206e656761746976650100116a6176612f6c\n" +
                "616e672f496e746567657207006d0c006e006f0c006e00700c0071006c01\n" +
                "00174c6f63616c65206d757374206e6f74206265206e756c6c0c00720073\n" +
                "07005d0c007400750100106a6176612f6c616e672f4f626a656374010016\n" +
                "6a6176612f6c616e672f4368617253657175656e63650100025b49010010\n" +
                "6a6176612f7574696c2f4c6f63616c650100106a6176612f6c616e672f53\n" +
                "7472696e670100106a6176612f6c616e672f53797374656d0100036f7574\n" +
                "0100154c6a6176612f696f2f5072696e7453747265616d3b0100136a6176\n" +
                "612f696f2f5072696e7453747265616d0100077072696e746c6e01001528\n" +
                "4c6a6176612f6c616e672f537472696e673b295601000465786974010004\n" +
                "284929560100066c656e6774680100032829490100066368617241740100\n" +
                "042849294301000e6a6176612f6c616e672f4d6174680100036d696e0100\n" +
                "0528494929490100106a6176612f7574696c2f4172726179730100046669\n" +
                "6c6c010008285b494949492956010006285b494929560100036d61780100\n" +
                "08746f537472696e6701001428294c6a6176612f6c616e672f537472696e\n" +
                "673b01000b746f4c6f77657243617365010026284c6a6176612f7574696c\n" +
                "2f4c6f63616c653b294c6a6176612f6c616e672f537472696e673b002100\n" +
                "03002000000003001a0021002200010023000000020004001a0024002200\n" +
                "010023000000020007001a00250022000100230000000200080005000100\n" +
                "260027000100280000001d00010001000000052ab70001b1000000010029\n" +
                "000000060001000000040009002a002b00010028000000b9000200010000\n" +
                "005e2abe04a0000fb200021204b6000503b800062abe05a0000fb2000212\n" +
                "07b6000503b800062abe1032a0000fb200021208b6000503b80006b20002\n" +
                "1209b60005b20002b6000ab20002120bb60005b20002120cb60005b20002\n" +
                "120db60005b10000000200290000003e000f0000000b0006000c000e000d\n" +
                "00120010001800110020001200240015002b0016003300170037001a003f\n" +
                "001b0045001c004d001d0055001e005d001f002c00000005000312111200\n" +
                "09002d002e00010028000001aa0006000b000000e22ac600072bc7000dbb\n" +
                "000e59120fb70010bf2ab9001101003d2bb9001101003e1c9a00051dac1d\n" +
                "9a00051cac1c1da400142a3a042b4b19044c1d3d2bb9001101003e1c0460\n" +
                "bc0a3a041c0460bc0a3a0503360715071ca300101904150715074f840701\n" +
                "a7fff004360815081da300712b15080464b900120200360919050315084f\n" +
                "04360715071ca300442a15070464b9001202001509a0000703a700040436\n" +
                "0a190515071905150704642e0460190415072e0460b80013190415070464\n" +
                "2e150a60b800134f840701a7ffbc19043a0619053a0419063a05840801a7\n" +
                "ff8f19041c2eac0000000200290000007e001f0000002200080023001200\n" +
                "2600190027002000290024002a0026002c002a002d002c00300031003200\n" +
                "3400330036003400390035003b0036004200390049003a00500045005900\n" +
                "460060004500660049006f004a007b004b0081004d008a004e00a0005000\n" +
                "c5004d00cb005400cf005500d3005600d7004900dd005b002c0000003200\n" +
                "0d0809fd001301010515ff0010000807002f07002f010107003007003000\n" +
                "01000012fc000201fc001a011840012cfa00110009002d00310001002800\n" +
                "00029d0007000e0000016f2ac600072bc7000dbb000e59120fb70010bf1c\n" +
                "9c000dbb000e591214b70010bf2ab9001101003e2bb90011010036041d9a\n" +
                "001015041ca300081504a7000402ac15049a000e1d1ca300071da7000402\n" +
                "ac1d1504a400162a3a052b4b19054c15043e2bb90011010036041d0460bc\n" +
                "0a3a051d0460bc0a3a061d1cb800130460360803360915091508a2001019\n" +
                "05150915094f840901a7ffef190515081905be1216b8001719061216b800\n" +
                "1804360915091504a300af2b15090464b900120200360a19060315094f04\n" +
                "15091c64b80019360b150912161c64a400071da7000b1d15091c60b80013\n" +
                "360c150b150ca4000502ac150b04a4000c1906150b046412164f150b360d\n" +
                "150d150ca300472a150d0464b900120200150aa000121906150d1905150d\n" +
                "04642e4fa700231906150d041906150d04642e1905150d2eb80013190515\n" +
                "0d04642eb80013604f840d01a7ffb819053a0719063a0519073a06840901\n" +
                "a7ff5019051d2e1ca3000819051d2eac02ac000000020029000000b6002d\n" +
                "0000005f0008006000120062001600630020006600270067002f006a0033\n" +
                "006b0040006d0045006e005000710056007300590074005b0075005e0076\n" +
                "006100770069007a0070007b0077007f00800080008a0081009100800097\n" +
                "008500a3008600aa008900b4008a00c0008b00c6008e00d0008f00e70092\n" +
                "00ee009300f0009700f6009800ff009c010a009d0119009f012800a30148\n" +
                "009c014e00a8015200a9015600aa015a0089016000af016800b0016d00b2\n" +
                "002c00000060001708090dfd001d01014001000d40010018ff0019000a07\n" +
                "002f07002f0101010700300700300001010000fa0013fc001501fd002f01\n" +
                "014701fc000a010efc000301241ffa0005ff0011000907002f07002f0101\n" +
                "01070030070030000100000c00090032003300010028000001430003000c\n" +
                "0000009c2ac600072bc7000dbb000e59120fb70010bf2cc7000dbb000e59\n" +
                "121ab70010bf2ab9001b01002cb6001c4e2bb9001b01002cb6001c3a0403\n" +
                "3605033606121d360703360815081904b6001ea2004e19041508b6001f36\n" +
                "0903360a15062db6001ea20033150a9a002e2d1506b6001f360b1509150b\n" +
                "a00019840501150704601506a000068405021506360704360a840601a7ff\n" +
                "ca840801a7ffae1505ac0000000200290000005e0017000000b6000800b7\n" +
                "001200b9001600ba002000c1002b00c2003700c5003a00c9003d00cc0041\n" +
                "00ce004e00cf005700d1005a00d2006800d3007000d5007700d7007a00db\n" +
                "008300dc008600df008a00e3008d00d2009300ce009900e8002c00000031\n" +
                "000908090dff0023000907002f07002f0700340700350700350101010100\n" +
                "00fd00150101fc002b01fa0006f90005fa000500010036000000020037\n";
        hex = hex.replaceAll("\n", "");
//        StringBuilder output = new StringBuilder();
//        for (int i = 0; i < hex.length(); i+=2) {
//            String str = hex.substring(i, i+2);
//            output.append((char)Integer.parseInt(str, 16));
//        }
//        System.out.println(output);
//        try(  PrintWriter out = new PrintWriter( "puzzle.class" )  ){
//            out.println(output);
//        }
        String out = readFile("Puzzle", StandardCharsets.UTF_8);
        FileOutputStream fos = new FileOutputStream("puzzle.class");
        fos.write(hexStringToByteArray(hex));
        fos.close();

    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s .length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
}
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ilja on 7.03.2016.
 */
public class Team {

    /**Codename of team. */
    private String codename;

    /** List of soldiers in team. */
    private List<SoldierOfFortune> members = new ArrayList<>();

    /** List of completed missions. */
    private ArrayList<Mission> missions = new ArrayList<>();

    /** List of failed missions.*/
    private ArrayList<Mission> failed = new ArrayList<>();

    /** Constructor. */
    public Team() {
    }
    /**
     * Constructor for team.
      * @param codeName of the team.
     * @param members list of team members.
     */
    public Team(String codeName, List<SoldierOfFortune> members) {
        this.codename = codeName;
        this.members = members;

    }

    /**
     * Sets codename of the team.
     * @param name of the team.
     */
    public void setCodeName(String name) {
        this.codename = name;
    }

    /**
     * Sets members of the team.
     * @param members of the team to set.
     */
    public void setMembers(List<SoldierOfFortune> members) {
        this.members = new ArrayList<>();
        if (!(members == this.members) || this.members.isEmpty()
                || this.members == null) {
            for (SoldierOfFortune soldier : members) {
                this.addSoldierToTeam(soldier);
            }
        }
    }

    /**
     * Members of the team.
     * @return soldiers.
     */
    public List<SoldierOfFortune> getMembers() {
        return members;
    }

    /**
     * Codename.
     * @return codename.
     */
    public String getCodeName() {
        return codename;
    }

    /**
     * Adds soldiers to list.
     * @param soldier soldier to add.
     */
    public void addSoldierToTeam(SoldierOfFortune soldier) {
        if (!(soldier == null)) {
            if (!(members.contains(soldier))) {
                members.add(soldier);
            }
        }
    }

    /**
     * Returns size of the team.
     * @return size.
     */
    public int getNumberOfSoldiers() {
        if (members == null) {
            return 0;
        }
        return members.size();
    }

    /**
     * Makes team to the string.
     * @return formatted string.
     */
    public String toString() {
        if (members.size() == 0 || members == null) {
            return codename + ": ";
        } else {
            ArrayList<String> teamCodenames = new ArrayList<>();
            for (SoldierOfFortune soldier : members) {
                teamCodenames.add(soldier.getCodename());
            }
            if (codename == null) {
                return String.join(", ", teamCodenames);
            } else {
                String start = codename + ": ";
                return start + String.join(", ", teamCodenames);
            }
        }
    }

    /**
     * How many missions are completed.
     * @return average of completed missions.
     */
    public double averageMissionsCompleted() {
        if (this.members == null || members.size() == 0) {
            return 0.0;
        }
        double completed = 0;
        double number = 0;
        for (SoldierOfFortune soldier : members) {
            if (soldier != null) {
                completed += soldier.getNumberOfMissionsCompleted();
                number++;
            }
        }
        return  (completed / number);
    }

    /**
     * Tries to send soldiers to mission, uses recieveteam method.
     * @param mission where to send soldiers.
     * @return if mission is completed or not.
     */
    public  boolean sendToMission(Mission mission) {
        if (mission == null || members == null
                || members.size() == 0) {
            return false;
        }
        if (mission.receiveTeam(this)) {
            for (SoldierOfFortune soldier : members) {
                soldier.setNumberOfMissionsCompleted(1);
            }
            missions.add(mission);
            return true;
        } else {
            failed.add(mission);
            return false;
        }
    }

    /**
     * Returns list on completed missions.
     * @return completed missions.
     */
    public List<Mission> getCompletedMissions() {
        return missions;
    }

}

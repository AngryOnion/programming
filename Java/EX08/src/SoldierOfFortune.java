/**
 * Created by Ilja on 7.03.2016.
 */
public class SoldierOfFortune {

    /** Firstname of the soldier.*/
    private String firstName;

    /** Firstname of the soldier.*/
    private String lastName;

    /** Firstname of the soldier.*/
    private String codeName;

    /** how many missions completed. */
    private int missionsCompleted = 0;

    /** Constructor. */
    public SoldierOfFortune() {
    }

    /**
     * Constructor.
     * @param firstName of the soldier.
     * @param lastName of the soldier.
     * @param codeName of the soldier.
     */
    public SoldierOfFortune(String firstName, String lastName, String codeName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.codeName = codeName;
    }

    /**
     * Sets firstname of soldier.
     * @param name of the soldier.
     */
    public void setFirstName(String name) {
        this.firstName = name;
    }

    /**
     * Sets lastname of soldier.
     * @param name of the soldier.
     */
    public void setLastName(String name) {
        this.lastName = name;
    }

    /**
     * Sets codename of soldier.
     * @param name ofthesoldier.
     */
    public void setCodeName(String name) {
        this.codeName = name;
    }

    /**
     * Returns name.
     * @return name.
     */
    public String getCodeName() {
        return codeName;
    }

    /**
     * Returns name.
     * @return name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Returns name.
     * @return name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Changes soldier to string.
     * @return string wiht full name of the soldier
     */
    public String toString() {
        if (firstName == null && lastName == null && codeName == null) {
            return "";
        }
        StringBuilder fullname = new StringBuilder();
        fullname.append(firstName + " \"" + codeName + "\" " + lastName);
        return fullname.toString();
    }

    /**
     * How many missions soldier completed.
     * @return number of missions completed.
     */
    public int getNumberOfMissionsCompleted() {
        return missionsCompleted;
    }

    /**
     * Sets number of missions completed.
     * @param completed missions.
     */
    public void setNumberOfMissionsCompleted(int completed) {
        if (!(completed < 0)) {
            missionsCompleted += completed;
        }
    }

    /**
     * Codename of the mission.
     * @return codename.
     */
    public String getCodename() {
        return codeName;
    }
}

/**
 * Created by Ilja on 7.03.2016.
 */
public class Mission {
    /** Is mission is completed. */
    private boolean situation;

    /**Codename of the mission. */
    private String codeName;

    /**Requieres missions to complete. */
    private int requiresMissions;

    /** Constructor. */
    public Mission() {
    }

    /**
     * Constructor.
     * @param codeName of the mission.
     */
    public Mission(String codeName) {
        this.codeName = codeName;
    }

    /**
     * Sets codename of the mission.
     * @param name of the mission.
     */
    public void setCodeName(String name) {
        this.codeName = name;
    }

    /**
     * checks if mission is completed.
     * @return boolean.
     */
    public boolean isCompleted() {
        return situation;
    }

    /**
     * Sets requierement to complete this mission.
     * @param completed missions required.
     */
    public void setRequiredMissionsCompleted(int completed) {
        if (completed > 0) {
            requiresMissions = completed;
        }
    }

    /**
     * Controls if this team can pass this mission.
     * @param team to complete it.
     * @return boolean.
     */
    public boolean receiveTeam(Team team) {
        if (team == null || team.getNumberOfSoldiers() < 1) {
            return false;
        }
        if (requiresMissions <= team.averageMissionsCompleted()) {
            situation = true;
            return true;
        }
        return false;
    }

    /**
     * Mission decription.
     * @return name of the mission.
     */
    public String toString() {
        return "Operation " + codeName;
    }
}

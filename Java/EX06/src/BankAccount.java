/**
 * Created by Ilja on 28.02.2016.
 */
public class BankAccount {
    /** Amount of money on account.*/
    private double balance;
    /** tax.*/
    private final double tax = 0.01;

    /**
     * Method what returns amount of money on account.
     * @return balance on account.
     */
    public final double getBalance() {
        if (balance == Double.NaN) {
            return Double.NaN;
        } else {
            return balance;
        }
    }

    /**
     * Withdraws money from account.
     * @param amount how much money to withdraw from account.
     * @return how much money left on balance(if it is possible).
     */
    public final double withdrawMoney(double amount) {
        if (amount == Double.NaN) {
            return Double.NaN;
        }
        if (amount > balance) {
            return Double.NaN;
        } else {
            balance -= amount;
            return balance;
        }
    }

    /**
     * Adds money to account.
     * @param amount to add to balance.
     */
    public final void addMoney(double amount) {
        if (amount != Double.NaN && amount > 0) {
            balance += amount;
        }
    }

    /**
     * Transfer money from one account to another.
     * @param targetAccount where to transfer money.
     * @param amount how much money to transfer.
     * @return boolean .
     */
    public final boolean transferMoneyTo(BankAccount targetAccount, double amount) {
        if (targetAccount == null || amount == Double.NaN
                || amount <= 0 || balance == Double.NaN) {
            return false;
        }
        double procent = amount * tax;
        if (amount + procent > balance) {
            return false;
        }
        balance -= (amount + procent);
        targetAccount.balance += amount;
        return true;
    }
}
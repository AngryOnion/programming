
/**
 *import java.util.Scanner;
 * Created by Ilja on 7.02.2016.
 */
public class EX02 {

    /**
     * Constant.
     * Every 3 days, feed worms.
     */
    public static final int WORM_FEEDING_DAY = 3;

    /**
     * Constant.
     * Every 5 days, bathe in sand.
     */
    public static final int BATHING_DAY = 5;

    /**
     * Constant.
     * Total number of days for which instructions are needed.
     */
    public static final int NUMBER_OF_DAYS = 30;

    /**
     * Entry point of the program.
     * @param args Arguments from command line.
     */
    public static void main(String[] args) {
        // call and print getInstructionForCurrentDay inside a loop here
        //Scanner.in = new Scanner(System.in)
        //int i = in.nextint();
        for (int currentDay = 1; currentDay <= NUMBER_OF_DAYS; currentDay++) {
                System.out.println(getInstructionForCurrentDay(currentDay));
            if (currentDay < 1) {
                break;
            }
        }
    }
        /**Scanner in = new Scanner(System.in);
        * int inputedDay = 0;
        * for (int i = 0; i < NUMBER_OF_DAYS; i++) {
        *    int day = 0;
        *    while (day > NUMBER_OF_DAYS || day < 1) {
        *        day = in.nextInt();
            }
            if (day <= inputedDay) {
                System.out.println("Cant fly back in time");
            } else if (day > inputedDay) {
                System.out.println(getInstructionForCurrentDay(day));
            } else if (day == NUMBER_OF_DAYS) {
                System.exit(0);
            }
            inputedDay = day;
        }

    }
    */

    /**
     * @param currentDay number of day to print instructions for.
     * @return instruction for given day
     */
    public static String getInstructionForCurrentDay(int currentDay) {
        String instruction;
        if (currentDay < 1) {
            instruction = "Can't fly back in time";
        } else if (currentDay % (WORM_FEEDING_DAY * BATHING_DAY) == 0) {
            instruction = "Day " + currentDay + " : " + "glide in wind";
        } else if (currentDay % BATHING_DAY == 0) {
            instruction = "Day " + currentDay + " : " + "bathe in sand";
        } else if (currentDay % WORM_FEEDING_DAY == 0) {
            instruction = "Day " + currentDay + " : " + "feed worms";
        } else {
            instruction = "Day " + currentDay + " : " + "give fruit and water";
        }
        return instruction;
    }
}
import java.util.*;

/**
 * Created by Ilja on 30.04.2016. sds
 */
public class Child {

    private String name;

    private ArrayList<Child> friends = new ArrayList<>();

    private static List<Child> children = new ArrayList<>();

    public Child(String name) {
        this.name = name;
    }
    public void playsWith(Child... children) {
        if (!(children == null)) {
            for (Child c : children) {
                if (c.getName() != null) {
                    friends.add(c);
                    c.friends.add(this);
                }
            }
        }
    }

    public static List<Child> getSandbox(Child child) {
        int lenght = children.size();
        int order = 0;
        if (!(children.contains(child))) children.add(child);
        for (Child c : child.friends) {
            if (!(children.contains(c))) {
                children.add(c);
            }
        }
        if (children.size() == lenght) return children;
        order++;
        return getSandbox(children.get(order));
    }

    public String getName() {
        return this.name;
    }
}

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ilja on 30.04.2016.
 */
public class EX17 {

    public static void main(String[] args) {
        Child marta = new Child("Marta");
        Child mati = new Child("Mati");
        Child kati = new Child("Kati");

        marta.playsWith(mati);
        mati.playsWith(kati);

        List<Child> children = Child.getSandbox(marta);
        for(Child c : children) {
            if (c == null) System.out.println("its null");
            System.out.println(c.getName());
        }
    }
}

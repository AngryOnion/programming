/**
 * Created by Ilja on 21.04.2016.
 */
public class EX16 {


    /** 9. */
    public static final int NINE = 9;
    /** 8. */
    public static final int EIGHT = 8;
    /** 10. */
    public static final int TEN = 10;


    /**
     * Recursive method to find occurrence of 9s and 8s.
     * @param n number.
     * @return  occurrence of 9 and 8s.
     */
    public static int count98(int n) {
        //IMPLEMENTATION HERE
        int count = 0;
        if (n == 0) {
            return count;
        } else if (n % TEN == NINE || n % TEN == EIGHT) {
            count++;
            count += count98(n / TEN);
        } else {
            count += count98(n / TEN);
        }
        return count;
    }

    /**
     * Recursive method to find occurrence of 9s and 8s.
     * If 2 numbers before 9 or 8 sum is 9 or 8 add 1 to occurrence rate.
     * @param n number.
     * @return occurrence.
     */
    public static int count98Harder(int n) {
        int count = 0;
        int length = String.valueOf(n).length();
        if (n == 0) {
            return count;
        } else if (n % TEN == NINE || n % TEN == EIGHT) {
            if (String.valueOf(n).length() > 2) {
                int firstDigit = Integer.parseInt(Integer.toString(n).
                        substring(length - 2, length - 1));
                int secondDigit = Integer.parseInt(Integer.toString(n).
                        substring(length - 2 - 1, length - 2));
                if (firstDigit + secondDigit == EIGHT
                        || firstDigit + secondDigit == NINE) {
                    count++;
                }
            }
            count++;
            count += count98Harder(n / TEN);
        } else {
            count += count98Harder(n / TEN);
        }
        return count;
    }

    /**
     * Main.
     * @param args cmd.
     */
    public static void main(String[] args) {
    }
}

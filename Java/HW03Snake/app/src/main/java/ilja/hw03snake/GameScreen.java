package ilja.hw03snake;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;

import java.util.Timer;
import java.util.TimerTask;

public class GameScreen extends AppCompatActivity {

    Timer timer = new Timer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Game game = new Game(this);
        setContentView(game);
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (!Game.getPlay()) {
                    Intent over = new Intent(GameScreen.this, GameOver.class);
                    startActivityForResult(over, 100);
                    timer.cancel();
                }
            }
        },0, 1000);
    }
    
}

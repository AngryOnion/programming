package ilja.hw03snake;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuScreen extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_screen);
        final Button StartButton = (Button) findViewById(R.id.StartButton);
        if (StartButton != null) {
            StartButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent game = new Intent(MenuScreen.this, GameScreen.class);
                    startActivityForResult(game,0);
                }
            });
        }
        final Button settingsButton = (Button) findViewById(R.id.settingsButton);
        assert settingsButton != null;
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent settings = new Intent(MenuScreen.this, Settings.class);
                startActivityForResult(settings,0);
            }
        });

    }


}

package ilja.hw03snake;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class GameOver extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);
        TextView score = (TextView) findViewById(R.id.score);
        assert score != null;
        score.setText(Integer.toString(Game.getScore()));
        ImageView gameover = (ImageView) findViewById(R.id.imagegameover);
        assert gameover != null;
        gameover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent start = new Intent(GameOver.this, MenuScreen.class);
                startActivityForResult(start,100);
            }
        });
    }
}

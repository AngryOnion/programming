package hw2;


import java.util.NoSuchElementException;

/**
 * Created by Ilja on 06.11.2016.
 */
public class EmptyStackException extends NoSuchElementException {
    protected EmptyStackException(String s) {
        super(s);
    }
}

package hw2;


import java.util.ArrayList;
import java.util.List;

public class Stack {
    List<Object> list = new ArrayList<Object>();

    public boolean empty() {
        return list.size()==0;
    }

    public Object peek() {
        if (empty()) {
            throw new EmptyStackException("Stack is empty");
        }
        return list.get(0);
    }

    public Object pop() {
        if (empty()) {
            throw new EmptyStackException("Stack is empty");
        }
        Object deletable = list.get(list.size()-1);
        list.remove(list.size()-1);
        return deletable;
    }

    public Object push(Object object) {
        list.add(0, object);
        return object;
    }
}

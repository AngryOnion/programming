package hw2;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * Created by Ilja on 06.11.2016.
 */
public class StackTest {

    private Stack stack;

    @Before
    public void setUp() throws Exception {
        stack = new Stack();
    }

    @Test
    public void empty() throws Exception {
        assertTrue(stack.empty());
    }

    @Test
    public void notEmpty() {
        stack.push(123);
        assertFalse(stack.empty());
    }

    @Test
    public void peek() throws Exception {
        stack.push(123);
        stack.push(321);
        assertEquals(321, stack.peek());
    }

    @Test(expected = EmptyStackException.class)
    public void peekEmptyStack() {
        stack.peek();
    }

    @Test
    public void pop() throws Exception {
        stack.push(123);
        stack.push(321);
        assertEquals(123, stack.pop());
    }

    @Test(expected = EmptyStackException.class)
    public void popEmptyStack() {
        stack.pop();
    }

    @Test
    public void push() throws Exception {
        assertEquals("Hello world", stack.push("Hello world"));
    }

}
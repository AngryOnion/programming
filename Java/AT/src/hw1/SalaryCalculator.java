package hw1;

/**
 * Created by Ilja on 04.10.2016.
 */
public class SalaryCalculator {

    private static final int JUNIOR = 10;
    private static final int SENIOR = 15;
    private static final int SPECIALIST = 22;


    private static final int JUNIOR_AND_SENIOR_NORMHOURS = 8;
    private static final int SPECIALIST_NORMHOURS = 9;



    private static final int DOUBLE = 2;
    private static final int TRIPLE = 3;

    public static void main(final String[] args) {
        SalaryCalculator calculator = new SalaryCalculator();
        System.err.println(calculator.getSalary(SPECIALIST, 3) + " should be 66");
    }

    private int getSalary(final int workerType, final int workHours) {
        int salary = 0;
        if (workerType == JUNIOR) {
            salary = getWorkerSalary(JUNIOR_AND_SENIOR_NORMHOURS, JUNIOR, workHours, DOUBLE);
        }
        if (workerType == SENIOR) {
            salary = getWorkerSalary(JUNIOR_AND_SENIOR_NORMHOURS, SENIOR, workHours, DOUBLE);
        }
        if (workerType == SPECIALIST) {
            salary = getWorkerSalary(SPECIALIST_NORMHOURS, SPECIALIST, workHours, TRIPLE);
        }

        if (workHours > 20) {
            if (workerType == JUNIOR) {
                    salary += 10;
            }
            if (workerType == SENIOR) {
                salary += 20;
            }
            if (workerType == SPECIALIST) {
                salary += 30;
            }
        }
        return salary;
    }

    private int getWorkerSalary(int normHours, int workerSalary, int workHours, int multiplier) {
        int salary = 0;
        if (workHours > normHours) {
                salary = workerSalary * (workHours - normHours) * multiplier;
                salary += workerSalary * normHours;
        } else {
            salary += workerSalary * workHours;
        }
        return salary;
    }
}
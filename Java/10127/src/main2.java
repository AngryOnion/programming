import java.util.Scanner;

/**
 * Created by Ilja on 16.02.2016.
 */
public class main2 {
    public static void main(String args[]) {

        Scanner in = new Scanner(System.in);

        while (in.hasNextLine()) {
            String EnteredValue = in.nextLine();
            String correctOne = EnteredValue.replaceAll("/r/n", "");
            if (correctOne.equals("#")) {
                System.out.println("");
                break;
            }
            else {
                byte[] bytes = correctOne.getBytes();
                byte m = bytes[0];
                System.out.println(m);
            }
        }
    }
}

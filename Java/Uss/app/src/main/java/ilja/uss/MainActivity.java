package ilja.uss;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Main method.
 */
public class MainActivity extends AppCompatActivity {

    /**Game. */
    private GameVeiw gameVeiw;

    private TextView textView;

    Button settings;


    /**
     * Creates view.
     * @param savedInstanceState .
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gameVeiw = new GameVeiw(this);
        setContentView(gameVeiw);
        settings = new Button(getApplicationContext());
        PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);

        textView = new TextView(this);
        textView.setText(Integer.toString(gameVeiw.getScore()));
    }

    public void changeText(String text) {
        textView.setText(text);
        gameVeiw.invalidate();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
                return true;

/**
    public void endGame() {

        SharedPreferences prefs = this.getSharedPreferences() ;
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("key", gameVeiw.getScore());
        editor.commit();
    }
*/

    //public void stopClick(View view) {
      //  RelativeLayout relativeLayout = (RelativeLayout)
    // findViewById(R.id.view);
        //relativeLayout.stop
    //}
}

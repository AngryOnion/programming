package travel;

public class Traveller {

    void travel() {
        Destination dest = new Destination(new Name("Rooma", "Roma", "Itaalia", "Euroopa"), new Temperature(300, 270));


        double celsius = dest.getAvgWeather((doub, doub2) -> (doub + doub2)/2.0 -273.15);
        double fahr = dest.getAvgWeather((doub, doub2) -> (doub + doub2) *9.0 / 5.0 - 459.67);
        System.out.println(celsius);
        System.out.println(fahr);
        System.out.println(Destination.getFahr(dest.getTemperature()));

        System.out.println(dest.print(name -> String.format("%s (%s), %s, %s",
                name.getName(), name.getOriginalName(), name.getCountry(), name.getContinent())));
        System.out.println(dest.print(name -> String.format("%s, %s",
                name.getName(), name.getOriginalName())));
        System.out.println(dest.print(Name::getName));
        System.out.println(dest.print(name -> name.getName().toUpperCase()));
        System.out.println(dest.print(name -> String.format("%s, %s",
                name.getName(), name.getCountry().toUpperCase())));

    }

    public static void main(String[] args) {
        new Traveller().travel();
    }
}

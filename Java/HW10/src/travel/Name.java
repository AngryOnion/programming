package travel;

/**
 * Created by Ilja on 14.11.2016.
 */
public class Name {

    private final String name;
    private final String originalName;
    private final String country;
    private final String continent;

    public Name(String name, String originalName, String country, String continent) {
        this.name = name;
        this.originalName = originalName;
        this.country = country;
        this.continent = continent;
    }

    public String getName() {
        return name;
    }

    public String getOriginalName() {
        return originalName;
    }

    public String getCountry() {
        return country;
    }

    public String getContinent() {
        return continent;
    }
}

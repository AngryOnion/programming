package travel;


import java.util.function.Function;
import java.util.function.ToDoubleBiFunction;

public class Destination implements DestinationModel {


    private Temperature temperature;
    private Name name;


    public Destination(Name name, Temperature temperature) {
        this.name = name;
        this.temperature = temperature;
    }

    @Override
    public Name getName() {
        return name;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    @Override
    public double getAvgWeather(ToDoubleBiFunction<Double, Double> tempHandler) {
        return tempHandler.applyAsDouble(temperature.getMinTemp(), temperature.getMaxTemp());
    }

    public String print(Function<Name, String> handler) {
        return handler.apply(name);
    }

    public static double getCelsius(Temperature temperature) {
        return (temperature.getMaxTemp() + temperature.getMinTemp()) / 2 - 273.15;
    }

    public static double getFahr(Temperature temperature) {
        return (temperature.getMaxTemp() + temperature.getMinTemp()) / 2 * 9 / 5 - 459.67;
    }
}

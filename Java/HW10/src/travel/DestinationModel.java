package travel;

import java.util.function.ToDoubleBiFunction;

public interface DestinationModel {
    // return name
    public Name getName();
    // return temp in Kelvins
    public Temperature getTemperature();
    // return temp using provided converter method
    public double getAvgWeather(ToDoubleBiFunction<Double, Double> tempHandler);
}

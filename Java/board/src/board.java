import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ilja on 20.05.2016.
 */
public class board {

    private static final int OPENFOUR = 100000;
    private static final int HALFCLOSEDFOUR = 10000;
    private static final int OPENTHREE = 500;
    private static final int HALFCLOSEDTHREE = 30;
    private static final int OPENTWO = 25;
    private static final int CLOSEDTWO = 10;
    
    public static int[][] board = new int[10][10];
    
    
    public static void main(String[] args) {


        board[1][0] = 1;
        board[0][0] = 1;

        board[5][5] = -1;
        board[6][6] = -1;

        findMoves(board);

        evaluateScore(board, 1);
        
    }

    public static List<Object> findMoves(int[][] board) {

        List<Object> moves = new ArrayList<Object>();
        // loops all cells
        for (int row = board.length - 1; row >= 0; row--) {
            for (int col = board[0].length - 1; col >= 0; col--) {
                if (!(board[row][col] == 0)) {
                    // loops around not empty cells and adds all unique empty cells in list
                    for (int i = -1; i <= 1; i++) {
                        for (int j = -1; j <= 1; j++) {
                            if (row + i >= 0 && row + i < board.length) {
                                if (col + j >= 0 && col + j < board.length) {


                                }
                            }
                        }
                    }
                }
            }
        }
        return moves;
    }


    public static int evaluateScore(int[][] board, int player) {


        int score = 0;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (i < (board.length - 1)) {
                    if (board[i][j] == player && board[i + 1][j] == player) {
                        if (i < (board.length - 2)) {
                            if (board[i + 2][j] == player) {
                                //4
                                if (i < (board.length - 3)) {
                                    if (board[i + 3][j] == player) {
                                        //WIN
                                        if (i < (board.length - 4)) {
                                            if (board[i + 4][j] == player) {
                                                return Integer.MAX_VALUE;
                                            } else {
                                                if (board[i + 4][j] == 0) {
                                                    if (i - 1 >= 0) {
                                                        if (board[i - 1][j] == 0) {
                                                            score += OPENFOUR;
                                                        } else {
                                                            if (board[i - 1][j] != player) {
                                                                score += HALFCLOSEDFOUR;
                                                            }
                                                        }
                                                    } else {
                                                        score += HALFCLOSEDFOUR;
                                                    }
                                                } else {
                                                    if (i - 1 >= 0) {
                                                        if (board[i - 1][j] == 0) {
                                                            score += HALFCLOSEDFOUR;
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            score += HALFCLOSEDFOUR;
                                        }
                                    } else {
                                        if (board[i + 3][j] == 0) {
                                            if (i - 1 >= 0) {
                                                if (board[i - 1][j] == 0) {
                                                    score += OPENTHREE;
                                                } else {
                                                    if (board[i - 1][j] != player) {
                                                        score += HALFCLOSEDTHREE;
                                                    }
                                                }
                                            } else {
                                                score += HALFCLOSEDTHREE;
                                            }
                                        } else {
                                            if (i - 1 >= 0) {
                                                if (board[i - 1][j] == 0) {
                                                    score += HALFCLOSEDTHREE;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (board[i - 1][j] != player) {
                                        score += HALFCLOSEDTHREE;
                                    }
                                }
                            } else {
                                if (board[i + 2][j] == 0) {
                                    if (i - 1 >= 0) {
                                        if (board[i - 1][j] == 0) {
                                            score += OPENTWO;
                                        } else {
                                            if (board[i - 1][j] != player) {
                                                score += CLOSEDTWO;
                                            }
                                        }
                                    } else {
                                        score += CLOSEDTWO;
                                    }
                                } else {
                                    if (i - 1 >= 0) {
                                        if (board[i - 1][j] == 0) {
                                            score += CLOSEDTWO;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (board[i - 1][j] != player) {
                                score += CLOSEDTWO;
                            }
                        }
                    }
                }

                if (j < (board[0].length - 1)) {
                    if (board[i][j] == player && board[i][j + 1] == player) {
                        if (j < (board[0].length - 2)) {
                            if (board[i][j + 2] == player) {
                                //4
                                if (j < (board[0].length - 3)) {
                                    if (board[i][j + 3] == player) {
                                        //WIN
                                        if (j < (board[0].length - 4)) {
                                            if (board[i][j + 4] == player) {
                                                return Integer.MAX_VALUE;
                                            } else {
                                                if (board[i][j + 4] == 0) {
                                                    if (j - 1 >= 0) {
                                                        if (board[i][j - 1] == 0) {
                                                            score += OPENFOUR;
                                                        } else {
                                                            if (board[i][j - 1] != player) {
                                                                score += HALFCLOSEDFOUR;
                                                            }
                                                        }
                                                    } else {
                                                        score += HALFCLOSEDFOUR;
                                                    }
                                                } else {
                                                    if (j - 1 >= 0) {
                                                        if (board[i][j - 1] == 0) {
                                                            score += HALFCLOSEDFOUR;
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            score += HALFCLOSEDFOUR;
                                        }
                                    } else {
                                        if (board[i][j + 3] == 0) {
                                            if (j - 1 >= 0) {
                                                if (board[i][j - 1] == 0) {
                                                    score += OPENTHREE;
                                                } else {
                                                    if (board[i][j - 1] != player) {
                                                        score += HALFCLOSEDTHREE;
                                                    }
                                                }
                                            } else {
                                                score += HALFCLOSEDTHREE;
                                            }
                                        } else {
                                            if (j - 1 >= 0) {
                                                if (board[i][j - 1] == 0) {
                                                    score += HALFCLOSEDTHREE;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (board[i][j - 1] != player) {
                                        score += HALFCLOSEDTHREE;
                                    }
                                }
                            } else {
                                if (board[i][j + 2] == 0) {
                                    if (j - 1 >= 0) {
                                        if (board[i][j - 1] == 0) {
                                            score += OPENTWO;
                                        } else {
                                            if (board[i][j - 1] != player) {
                                                score += CLOSEDTWO;
                                            }
                                        }
                                    } else {
                                        score += CLOSEDTWO;
                                    }
                                } else {
                                    if (j - 1 >= 0) {
                                        if (board[i][j - 1] == 0) {
                                            score += CLOSEDTWO;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (board[i][j - 1] != player) {
                                score += CLOSEDTWO;
                            }
                        }
                    }
                }

                if (j < (board[0].length - 1) && (i < board.length - 1)) {
                    if (board[i][j] == player && board[i + 1][j + 1] == player) {
                        if (j < (board[0].length - 2) && (i < board.length - 2)) {
                            if (board[i + 2][j + 2] == player) {
                                //4
                                if (j < (board[0].length - 3)  && (i < board.length - 3)) {
                                    if (board[i + 3][j + 3] == player) {
                                        //WIN
                                        if (j < (board[0].length - 4)  && (i < board.length - 4)) {
                                            if (board[i + 4][j + 4] == player) {
                                                return Integer.MAX_VALUE;
                                            } else {
                                                if (board[i + 4][j + 4] == 0) {
                                                    if (j - 1 >= 0 && i - 1 >= 0) {
                                                        if (board[i - 1][j - 1] == 0) {
                                                            score += OPENFOUR;
                                                        } else {
                                                            if (board[i - 1][j - 1] != player) {
                                                                score += HALFCLOSEDFOUR;
                                                            }
                                                        }
                                                    } else {
                                                        score += HALFCLOSEDFOUR;
                                                    }
                                                } else {
                                                    if (j - 1 >= 0 && i - 1 >= 0) {
                                                        if (board[i - 1][j - 1] == 0) {
                                                            score += HALFCLOSEDFOUR;
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            score += HALFCLOSEDFOUR;
                                        }
                                    } else {
                                        if (board[i + 3][j + 3] == 0) {
                                            if (j - 1 >= 0 && i - 1 >= 0) {
                                                if (board[i - 1][j - 1] == 0) {
                                                    score += OPENTHREE;
                                                } else {
                                                    if (board[i - 1][j - 1] != player) {
                                                        score += HALFCLOSEDTHREE;
                                                    }
                                                }
                                            } else {
                                                score += HALFCLOSEDTHREE;
                                            }
                                        } else {
                                            if (j - 1 >= 0 && i - 1 >= 0) {
                                                if (board[i - 1][j - 1] == 0) {
                                                    score += HALFCLOSEDTHREE;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (board[i - 1][j - 1] != player) {
                                        score += HALFCLOSEDTHREE;
                                    }
                                }
                            } else {
                                if (board[i + 2][j + 2] == 0) {
                                    if (j - 1 >= 0 && i - 1 >= 0) {
                                        if (board[i - 1][j - 1] == 0) {
                                            score += OPENTWO;
                                        } else {
                                            if (board[i - 1][j - 1] != player) {
                                                score += CLOSEDTWO;
                                            }
                                        }
                                    } else {
                                        score += CLOSEDTWO;
                                    }
                                } else {
                                    if (j - 1 >= 0 && i - 1 >= 0) {
                                        if (board[i - 1][j - 1] == 0) {
                                            score += CLOSEDTWO;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (board[i - 1][j - 1] != player) {
                                score += CLOSEDTWO;
                            }
                        }
                    }
                }

                if (j >= 1 && (i < board.length - 1)) {
                    if (board[i][j] == player && board[i + 1][j - 1] == player) {
                        if (j >= 2 && (i < board.length - 2)) {
                            if (board[i + 2][j - 2] == player) {
                                //4
                                if (j >= 3 && (i < board.length - 3)) {
                                    if (board[i + 3][j - 3] == player) {
                                        //WIN
                                        if (j >= 4  && (i < board.length - 4)) {
                                            if (board[i + 4][j - 4] == player) {
                                                return Integer.MAX_VALUE;
                                            } else {
                                                if (board[i + 4][j - 4] == 0) {
                                                    if (j < (board.length - 1) && i - 1 >= 0) {
                                                        if (board[i - 1][j + 1] == 0) {
                                                            score += OPENFOUR;
                                                        } else {
                                                            if (board[i - 1][j + 1] != player) {
                                                                score += HALFCLOSEDFOUR;
                                                            }
                                                        }
                                                    } else {
                                                        score += HALFCLOSEDFOUR;
                                                    }
                                                } else {
                                                    if (j < (board.length - 1) && i - 1 >= 0) {
                                                        if (board[i - 1][j + 1] == 0) {
                                                            score += HALFCLOSEDFOUR;
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            score += HALFCLOSEDFOUR;
                                        }
                                    } else {
                                        if (board[i + 3][j - 3] == 0) {
                                            if (j < (board.length - 1) && i - 1 >= 0) {
                                                if (board[i - 1][j + 1] == 0) {
                                                    score += OPENTHREE;
                                                } else {
                                                    if (board[i - 1][j + 1] != player) {
                                                        score += HALFCLOSEDTHREE;
                                                    }
                                                }
                                            } else {
                                                score += HALFCLOSEDTHREE;
                                            }
                                        } else {
                                            if (j < (board.length - 1) && i - 1 >= 0) {
                                                if (board[i - 1][j + 1] == 0) {
                                                    score += HALFCLOSEDTHREE;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (j < (board.length - 1)) {
                                        if (board[i - 1][j + 1] != player) {
                                            score += HALFCLOSEDTHREE;
                                        }
                                    }
                                }
                            } else {
                                if (board[i + 2][j - 2] == 0) {
                                    if (j < (board.length - 1) && i - 1 >= 0) {
                                        if (board[i - 1][j + 1] == 0) {
                                            score += OPENTWO;
                                        } else {
                                            if (board[i - 1][j + 1] != player) {
                                                score += CLOSEDTWO;
                                            }
                                        }
                                    } else {
                                        score += CLOSEDTWO;
                                    }
                                } else {
                                    if (j < (board.length - 1) && i - 1 >= 0) {
                                        if (board[i - 1][j + 1] == 0) {
                                            score += CLOSEDTWO;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (j < (board.length - 1)) {
                                if (board[i - 1][j + 1] != player) {
                                    score += CLOSEDTWO;
                                }
                            }
                        }
                    }
                }


            }
        }

        System.out.println(score);
        return score;    }
    
}

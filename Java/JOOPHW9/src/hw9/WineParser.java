package hw9;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class WineParser {

    private static final int CATEGORY = 0;
    private static final int ALCOHOL = 1;
    private static final int MAGNESIUM = 5;
    private static final int FLAVANOIDS = 7;

    void createVinesFromFile() {
        List<Wine> wines = new ArrayList<>();
        try {
            Stream<String> lines = Files.lines(Paths.get("src/hw9", "wine.txt"));
            wines = lines.map(str -> str.split(","))
                    .map(array -> new Wine(Integer.parseInt(array[CATEGORY]),Double.parseDouble(array[ALCOHOL]),
                            Double.parseDouble(array[MAGNESIUM]), Double.parseDouble(array[FLAVANOIDS])))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        OptionalDouble maxMagnesium = wines.stream().mapToDouble(Wine::getMagneesium).max();
        OptionalDouble minMagnesium = wines.stream().mapToDouble(Wine::getMagneesium).min();
        OptionalDouble averageMagnesium = wines.stream().mapToDouble(Wine::getMagneesium).average();
        System.out.println(String.format("Max magnesium: %s\nMin magnesium: %s\n" +
                        "Average magnesium: %s",
                maxMagnesium.getAsDouble(), minMagnesium.getAsDouble(), averageMagnesium.getAsDouble()));

        wines.stream().filter(wine -> wine.getMagneesium() > averageMagnesium.getAsDouble())
                .forEach(newWine -> newWine.setAdSentence("Siin veinis on palju magneesiumit!"));

        wines.stream().mapToDouble(Wine::getFlavanoids).boxed()
                .sorted().limit(5).forEach(System.out::println);

        wines.stream().filter(wine -> wine.getCategory() == 3)
                .forEach(newWine -> newWine.setAdSentence("Alkohol on hea sulane, aga halb peremees!" +
                        "Alkohol kahjustab tervist ja eriti teie aju!"));

        wines.stream().forEach(wine -> System.out.println(wine.getTextInfo()));
    }
}

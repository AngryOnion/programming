package hw9;

class Wine {

    private int category;
    private Double alcohol;
    private Double magneesium;
    private Double flavanoids;
    private String adSentence;

    Wine(Integer category, Double alcohol, Double magneesium, Double flavanoids) {
        this.category = category;
        this.alcohol = alcohol;
        this.magneesium = magneesium;
        this.flavanoids = flavanoids;
        adSentence = "";
    }

    int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public Double getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(Double alcohol) {
        this.alcohol = alcohol;
    }

    Double getMagneesium() {
        return magneesium;
    }

    public void setMagneesium(Double magneesium) {
        this.magneesium = magneesium;
    }

    Double getFlavanoids() {
        return flavanoids;
    }

    public void setFlavanoids(Double flavanoids) {
        this.flavanoids = flavanoids;
    }

    public String getAdSentence() {
        return adSentence;
    }

    void setAdSentence(String adSentence) {
        this.adSentence = adSentence;
    }

    String getTextInfo() {
        return String.format("Category: %s\nAlcohol: %s\nMagnesium: %s\n" +
                        "flavanoids: %s\nadSentence: %s",
                category, alcohol, magneesium, flavanoids, adSentence);
    }

}

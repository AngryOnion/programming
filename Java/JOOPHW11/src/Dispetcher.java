import java.util.ArrayList;
import java.util.List;

public class Dispetcher implements Runnable {

    private List<Plane> planes = new ArrayList<>();

    private CommandBase base;

    public Dispetcher(List<Plane> planes, CommandBase base) {
        this.planes = planes;
        this.base = base;
    }

    public void addPlane(Plane plane) {
        planes.add(plane);
    }

    public void landAllPlanes() throws InterruptedException {
        for (Plane plane: planes) {
            base.add(plane.getName(), Boolean.TRUE);
            Thread.sleep(2000);
        }
    }

    @Override
    public void run() {
        try {
            landAllPlanes();
        } catch (InterruptedException e) {
            System.err.println("Thread error, bad job");
        }
    }
}

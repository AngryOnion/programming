import java.util.Optional;

public class Plane implements Runnable {

    private String name;
    private CommandBase commandBase;

    public Plane(String name, CommandBase commandBase) {
        this.name = name;
        this.commandBase = commandBase;
    }

    private void fly() throws InterruptedException {
        while (true) {
            if (!checkIfPlaneNeedsToLand()) {
                System.out.println(String.format("%s is in the air", name));
                Thread.sleep(1500);
            } else {
                break;
            }
        }
        System.out.println(String.format("!!!!%s is on ground", name));
    }

    private boolean checkIfPlaneNeedsToLand() {
        Optional<Boolean> check = commandBase.needsToLand(this.name);
        if (check.isPresent()) {
            return check.get();
        }
        return false;
    }

    @Override
    public void run() {
        try {
            fly();
        } catch (InterruptedException e) {
            System.err.println("Error in threads");
        }
    }


    public String getName() {
        return name;
    }
}

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Airport {

    public static void main(String[] args) {


        CommandBase commandBase = new CommandBase();

        Plane plane1 = new Plane("PhpIsFromHell", commandBase);
        Plane plane2 = new Plane("Cat", commandBase);
        Plane plane3 = new Plane("Jenkins", commandBase);
        Plane plane4 = new Plane("Lazy", commandBase);
        Plane plane5 = new Plane("DevNull", commandBase);
        Plane plane6 = new Plane("NullPointerException", commandBase);

        Dispetcher dispetcher = new Dispetcher(Arrays.asList(plane1, plane2, plane3, plane4, plane5, plane6), commandBase);

        Thread process1 = new Thread(plane1);
        Thread process2 = new Thread(plane2);
        Thread process3 = new Thread(plane3);
        Thread process4 = new Thread(plane4);
        Thread process5 = new Thread(plane5);
        Thread process6 = new Thread(plane6);
        List<Thread> processes = Arrays.asList(process1, process2, process3, process4,process5, process6);
        Thread leader = new Thread(dispetcher);

        processes.forEach(Thread::start);

        leader.start();

    }
}

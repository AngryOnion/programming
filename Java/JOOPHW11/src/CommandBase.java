import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class CommandBase {

    private Map<String, Boolean> base = new HashMap<>();

    synchronized void add(String planeName, Boolean isNeededToLand) {
        base.put(planeName, isNeededToLand);
    }

    synchronized Optional<Boolean> needsToLand(String planeName) {
        return Optional.ofNullable(base.get(planeName));
    }
}
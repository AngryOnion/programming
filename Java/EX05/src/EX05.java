import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by Ilja on 26.02.2016.
 */
public class EX05 {
    /**Array used to split input. */
    public static final int ARRAY_LENGHT = 4;
    /**Array to getmovie realease date. */
    public static final int DATE = 0;
    /**Array to get moviename. */
    public static final int MOVIENAME = 1;
    /**Array to get movie description. */
    public static final int DESCRIPTION = 2;
    /**Array to get movie rating. */
    public static final int RATING = 3;

    /**
     * Main function.
     * @param args input arguments.
     */
    public static void main(String[] args) {
        System.out.println(getNicelyFormattedMovie("2016-05-29|Movie| Decription"));
    }

    /**
     * Function reads oneliner content of file about movies.
     * Converts content to exact form and writes it into the output file.
     * Form:
     * Moviename
     * Release date: 24/02/2016
     * Description: description
     * Average rating: 8.0
     * @param inputFilename file, where from function is reading input
     * @param outputFilename file, where to write
     * @return number of films
     */
    public static int convert(String inputFilename, String outputFilename) {
        int movieCount = 0;
        try {
            FileWriter writer  = new FileWriter(outputFilename);
            List<String> lines = readSmallFile(inputFilename);
            for (int i = 0; i < lines.size(); i++) {
                String formatted = getNicelyFormattedMovie(lines.get(i));
                if (formatted != null) {
                    movieCount++;
                    if (movieCount > 1) {
                        writer.write("\n");
                    }
                    writer.write(formatted + "\n");
                    }
                }
            writer.close();
        } catch (IOException | NullPointerException ex) {
        }
        return movieCount;
    }

    /**
     * Function to read file and make it to String Array.
     * @param filename name of file to read.
     * @return Array of lines
     * @throws IOException because of file paths.
     */
    public static List<String> readSmallFile(String filename)
            throws IOException {
        Path path = Paths.get(filename);
        List<String> lines = Files.readAllLines(path);
        return lines;
    }

    /**
     * Function reads user oneliner input, converts it to nice form
     * and return it.
     * @param movieLine oneliner input form user
     * @return nicely formated input about movie.
     */
    public static String getNicelyFormattedMovie(String movieLine) {
        String[] splitted =  splitInputToArray(movieLine);
        if (validateMovieFormat(splitted)) {
            String[] date = splitted[DATE].split("-");
            String rightDate = date[2] + "/" + date[1] + "/" + date[0];
            String formatted = splitted[MOVIENAME] + "\n"
                    + "Release date: " + rightDate + "\n"
                    + "Description: " + splitted[DESCRIPTION] + "\n"
                    + "Average rating: " + splitted[RATING];
            return formatted;
        } else {
            return null;
        }
    }

    /**
     * Splints inputted string to parts.
     * @param input line form file.
     * @return splitted array.
     */
    public static String[] splitInputToArray(String input) {
        if (input == null) {
            return null;
        }
        String[] stringParts = input.split("\\|");
        String[] splitted = new String[stringParts.length];
        for (int i = 0; i < stringParts.length; i++) {
            splitted[i] = stringParts[i];
        }
        return splitted;
    }

    /**
     * Method to validate if line is correct or not.
     * @param splitted Array to control.
     * @return whether string is correct format or not.
     */
    public static boolean validateMovieFormat(String[] splitted) {
        try {
            return splitted.length == ARRAY_LENGHT;
        } catch (NullPointerException e) {
            return false;
        }
    }

// && splitted[MOVIENAME].matches("^(?=\\s*\\S).*$")
// && splitted[DESCRIPTION].matches("^(?=\\s*\\S).*$"));
    // return (splitted[DATE].matches("\\d{4}-\\d{2}-\\d{2}")
    //&& splitted[RATING].matches("\\d.\\d{1,2}")
    //);

    /**
     * writer.write(formatted + "\n" + "\n");
     */
    /**
     *  while (scanner.hasNextLine()) {
     String line = "";
     // "\n" -> newline
     line += scanner.nextLine();
     String formatted = getNicelyFormattedMovie(line);
     if (formatted != null) {
     movieCount++;
     String[] spitted = formatted.split("\n");
     if (movieCount > 1) writer.write("\n");
     for (int i = 0; i < spitted.length; i++) {
     writer.write(spitted[i]);
     writer.write("\n");
     writer.flush();
     }
     }
     }
     writer.close();
     scanner.close();
     */
    //Scanner scanner = new Scanner(readingPath);
    //Path readingPath = Paths.get(inputFilename);
    //BufferedWriter writer = Files.newBufferedWriter(writingPath);
}

:- dynamic ruut/3.
%--------------------------------
kaigu_variandid(X,Y,Suund,X1,Y1):-
    vota_tammiga(X,Y,Suund,X1,Y1),
     !.
kaigu_variandid(X,Y,Suund,X1,Y1):-
    votmine(X,Y,Suund,X1,Y1),
     !.
kaigu_variandid(X,Y,Suund,X1,Y1):-
    kaimine(X,Y,Suund,X1,Y1),
     !.
kaigu_variandid(X,Y,Suund,X1,Y1):-
    kaimine_tammiga(X,Y,Suund,X1,Y1),
     !.
%--------------------------------
votmine(X,Y,Suund,X1,Y1):-
    ruut(X, Y, MyColor),
    kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2,MyColor),
    vota(X,Y,Suund,X1,Y1,X2,Y2).

vota_tammiga(X, Y, Suund, X1, Y1):-
	ruut(X, Y, MyColor), 
	kas_saab_votta_tammiga_vasakule(X, Y, Suund, X1, Y1, X2, Y2, MyColor),
	vota(X, Y, Suund, X1, Y1, X2, Y2).

vota_tammiga(X, Y,Suund, X1, Y1):-
	ruut(X, Y, MyColor),
	kas_saab_votta_tammiga_paremale(X, Y, Suund, X1, Y1, X2, Y2, MyColor),
	vota(X, Y, Suund, X1, Y1, X2, Y2).
	
vota_tammiga(X, Y,Suund, X1, Y1):-
	ruut(X, Y, MyColor),
	kas_saab_votta_tammiga_vasakule_tagasi(X, Y, Suund, X1, Y1, X2, Y2, MyColor),
	vota(X, Y, Suund, X1, Y1, X2, Y2).

vota_tammiga(X, Y,Suund, X1, Y1):-
	ruut(X, Y, MyColor),
	kas_saab_votta_tammiga_paremale_tagasi(X, Y, Suund, X1, Y1, X2, Y2, MyColor),
	vota(X, Y, Suund, X1, Y1, X2, Y2).
%--------
kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2, MyColor):-  % Votmine edasi paremale
    X1 is X + Suund,
    Y1 is Y + 1,
    ruut(X1,Y1, Color),
    Color =\= MyColor, Color =\= 0,
    X2 is X1 + Suund,
    Y2 is Y1 + 1,
    ruut(X2,Y2, 0).

kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2, MyColor):-  % Votmine edasi vasakule
    X1 is X + Suund,
    Y1 is Y - 1,
    ruut(X1,Y1, Color),
    Color =\= MyColor, Color =\= 0,
    X2 is X1 + Suund,
    Y2 is Y1 - 1,
    ruut(X2,Y2, 0).

kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2, MyColor):-  % Votmine tagasi paremale
    X1 is X + Suund * -1,
    Y1 is Y + 1,
    ruut(X1,Y1, Color),
    Color =\= MyColor, Color =\= 0,
    X2 is X1 + Suund * -1,
    Y2 is Y1 + 1,
    ruut(X2,Y2, 0).

kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2, MyColor):-  % Votmine tagasi vasakule
    X1 is X + Suund * -1,
    Y1 is Y - 1,
    ruut(X1,Y1, Color),
    Color =\= MyColor, Color =\= 0,
    X2 is X1 + Suund * -1,
    Y2 is Y1 - 1,
    ruut(X2,Y2, 0).

kas_saab_votta_tammiga_vasakule(X, Y, Suund, X1, Y1, X2, Y2, Tamm):-
	%saan tammi oiget värvi
	RegularColor is Tamm / 10,
	%kontrollin et see pole tyhi ruut
	RegularColor >= 1,
	X1 is X + Suund,
    Y1 is Y - 1,
    %v6tan ruudu varvi
	ruut(X1, Y1, Color),
	%kontrollin et see pole sama varvi ja pole tyhi
	Color =\= RegularColor, Color =\= 0,
	%kontrollin kas selle nuppu taga on ruumi, et seda saaks ara suua
	X2 is X1 + Suund,
    Y2 is Y1 - 1,
    ruut(X2, Y2, 0).

kas_saab_votta_tammiga_vasakule(X, Y, Suund, X1, Y1, X2, Y2, Tamm):-
	%rekursiivselt vaatan kas saab syya minnes jarjest labi diagonaali
	NewX is X + Suund,
    NewY is Y - 1,
	ruut(NewX, NewY, 0), 
	kas_saab_votta_tammiga_vasakule(NewX, NewY, Suund, X1, Y1, X2, Y2, Tamm).

kas_saab_votta_tammiga_paremale(X,Y,Suund,X1,Y1,X2,Y2, Tamm):-
	%saan tammi oiget värvi
	RegularColor is Tamm / 10,
	%kontrollin et see pole tyhi ruut
	RegularColor >= 1,
    X1 is X + Suund,
    Y1 is Y + 1,
    %v6tan ruudu varvi
    ruut(X1,Y1, Color),
    %kontrollin et see pole sama varvi ja pole tyhi
    Color =\= RegularColor, Color =\= 0,
    %kontrollin kas selle nuppu taga on ruumi, et seda saaks ara suua
    X2 is X1 + Suund,
    Y2 is Y1 + 1,
    ruut(X2,Y2, 0).

kas_saab_votta_tammiga_paremale(X, Y, Suund, X1, Y1, X2, Y2, Tamm):-
	%rekursiivselt vaatan kas saab syya minnes jarjest labi diagonaali
	NewX is X + Suund,
    NewY is Y + 1,
	ruut(NewX, NewY, 0), 
	kas_saab_votta_tammiga_paremale(NewX, NewY, Suund, X1, Y1, X2, Y2, Tamm).
	
kas_saab_votta_tammiga_paremale_tagasi(X,Y,Suund,X1,Y1,X2,Y2, Tamm):-
    %saan tammi oiget värvi
    RegularColor is Tamm / 10,
    %kontrollin et see pole tyhi ruut
    RegularColor >= 1,
	X1 is X + Suund * - 1,
    Y1 is Y + 1,
    ruut(X1,Y1, Color),
    %kontrollin et see pole sama varvi ja pole tyhi
    Color =\= RegularColor, Color =\= 0,
    %kontrollin kas selle nuppu taga on ruumi, et seda saaks ara suua
    X2 is X1 + Suund * -1,
    Y2 is Y1 + 1,
    ruut(X2,Y2, 0).
	
kas_saab_votta_tammiga_paremale_tagasi(X, Y, Suund, X1, Y1, X2, Y2, Tamm):-
	%rekursiivselt vaatan kas saab syya minnes jarjest labi diagonaali
	NewX is X + Suund * -1,
    NewY is Y + 1,
	ruut(NewX, NewY, 0), 
	kas_saab_votta_tammiga_paremale_tagasi(NewX, NewY, Suund, X1, Y1, X2, Y2, Tamm).
	
kas_saab_votta_tammiga_vasakule_tagasi(X,Y,Suund,X1,Y1,X2,Y2, Tamm):-
    %saan tammi oiget värvi
    RegularColor is Tamm / 10,
    %kontrollin et see pole tyhi ruut
    RegularColor >= 1,
	X1 is X + Suund * -1,
    Y1 is Y - 1,
    ruut(X1,Y1, Color),
    %kontrollin et see pole sama varvi ja pole tyhi
    Color =\= RegularColor, Color =\= 0,
    %kontrollin kas selle nuppu taga on ruumi, et seda saaks ara suua
    X2 is X1 + Suund * -1,
    Y2 is Y1 - 1,
    ruut(X2,Y2, 0).
	
kas_saab_votta_tammiga_vasakule_tagasi(X, Y, Suund, X1, Y1, X2, Y2, Tamm):-
	%rekursiivselt vaatan kas saab syya minnes jarjest labi diagonaali
	NewX is X + Suund * -1,
    NewY is Y - 1,
	ruut(NewX, NewY, 0),
	kas_saab_votta_tammiga_paremale_tagasi(NewX, NewY, Suund, X1, Y1, X2, Y2, Tamm).

vota(X, Y, _, X1, Y1, X2, Y2):-
    %tyhjenda ruutu
    retract(ruut(X, Y, Nr)),
    assert(ruut(X, Y, 0)),
    %kustutan vastasse oma
    retract(ruut(X1, Y1, _)),
    assert(ruut(X1, Y1, 0)),
    %asetan enda nuppu
    retract(ruut(X2, Y2, _)),
    assert(ruut(X2, Y2, Nr)),
    %kontrollin kas on tamm
    muutub_tammiks(X2, Y2, Nr).


tee_kaik(X, Y, X1, Y1):-
    %tyhjenda ruutu
    retract(ruut(X, Y, Nr)),
    assert(ruut(X, Y, 0)),
    %asetan enda nuppu
    retract(ruut(X1, Y1, _)),
    assert(ruut(X1, Y1, Nr)),
    %kontrollin kas on tamm
    muutub_tammiks(X1, Y1, Nr).

%vaatan kas nupp nyyd asub aares
muutub_tammiks(8, Y, 1):- 
    retract(ruut(8, Y, _)), 
    assert(ruut(8, Y, 10)).

muutub_tammiks(1, Y, 2):- 
    retract(ruut(1, Y, _)), 
    assert(ruut(1, Y, 20)).

%siis kui eelmised kaks feilivad
muutub_tammiks(_,_,_).

%--------------------------------
kaimine(X,Y,Suund,X1,Y1):-
    kas_naaber_vaba(X,Y,Suund,X1,Y1),
    tee_kaik(X,Y,X1,Y1),
    write([' kaib ', X1,Y1]).

%pidin eraldi tegema, kuna tamm saab ka tagasi minna
kaimine_tammiga(X, Y, Suund, X1, Y1):-
	kas_naaber_vaba_tammi_jaoks(X, Y, Suund, X1, Y1),
	tee_kaik(X, Y, X1, Y1),
	write([' kaib ', X1,Y1]).

kas_naaber_vaba(X,Y,Suund,X1,Y1):-
    X1 is X +Suund,
    Y1 is Y + 1,
    ruut(X1,Y1, 0).

kas_naaber_vaba(X,Y,Suund,X1,Y1):-
    X1 is X +Suund,
    Y1 is Y - 1,
    ruut(X1,Y1, 0), write(' voi ').

% paremale
kas_naaber_vaba_tammi_jaoks(X,Y,Suund,X1,Y1):-
    X1 is X +Suund,
    Y1 is Y + 1,
    ruut(X1,Y1, 0).

% vasakule
kas_naaber_vaba_tammi_jaoks(X,Y,Suund,X1,Y1):-
    X1 is X +Suund,
    Y1 is Y - 1,
    ruut(X1,Y1, 0), write(' voi ').

% vasakule tagasi
kas_naaber_vaba_tammi_jaoks(X,Y,Suund,X1,Y1):-
    X1 is X +Suund * -1,
    Y1 is Y - 1,
    ruut(X1,Y1, 0), write(' voi ').

% vasakule tagasi
kas_naaber_vaba_tammi_jaoks(X,Y,Suund,X1,Y1):-
    X1 is X +Suund * -1,
    Y1 is Y + 1,
    ruut(X1,Y1, 0), write(' voi ').

%---------MÄNGU ALGSEIS-------------
% Valged
ruut(1,1,1).
ruut(1,3,1).
ruut(1,5,1).
ruut(1,7,1).
ruut(2,2,1).
ruut(2,4,1).
ruut(2,6,1).
ruut(2,8,1).
ruut(3,1,10).
ruut(3,3,1).
ruut(3,5,1).
ruut(3,7,1).
% Tühjad ruudud
ruut(4,2,2).
ruut(4,4,0).
ruut(4,6,0).
ruut(4,8,0).
ruut(5,1,0).
ruut(5,3,0).
ruut(5,5,0).
ruut(5,7,0).
% Mustad
ruut(6,2,2).
ruut(6,4,0).
ruut(6,6,2).
ruut(6,8,2).
ruut(7,1,2).
ruut(7,3,2).
ruut(7,5,2).
ruut(7,7,2).
ruut(8,2,2).
ruut(8,4,2).
ruut(8,6,2).
ruut(8,8,2).

/*
ruut(X,Y, Status).  %   kus X, Y [1,8]
Status = 0      %  tühi
Status = 1      %  valge
Status = 2      %  must
Status = 10     %  valge tamm
Status = 20     %  must tamm
*/

%=================== Print checkers board - Start ==================
print_board :-
	print_squares(8).

print_squares(Row) :-
	between(1, 8, Row),
	write('|'), print_row_squares(Row, 1), write('|'), nl,
	NewRow is Row - 1,
	print_squares(NewRow), !.
print_squares(_) :- !.


print_row_squares(Row, Col) :-
	between(1, 8, Col),
	ruut(Col, Row, Status), write(' '), write(Status), write(' '),
	NewCol is Col + 1,
	print_row_squares(Row, NewCol), !.
print_row_squares(_, _) :- !.

%=================== Print checkers board - End ====================

%=================== Print checkers board v2 - Start ==================
status_sq(ROW,COL):-
	(	ruut(ROW,COL,COLOR),
		write(COLOR)
	);(
		write(' ')
	).
status_row(ROW):-
	write('row # '),write(ROW), write('   '),
	status_sq(ROW,1),
	status_sq(ROW,2),
	status_sq(ROW,3),
	status_sq(ROW,4),
	status_sq(ROW,5),
	status_sq(ROW,6),
	status_sq(ROW,7),
	status_sq(ROW,8),
	nl.
% print the entire checkers board..
status:-
	nl,
	status_row(8),
	status_row(7),
	status_row(6),
	status_row(5),
	status_row(4),
	status_row(3),
	status_row(2),
	status_row(1).

%=================== Print checkers board v2 - End ====================
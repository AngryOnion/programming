%Naised
female(anna).
female(natalja).
female(galina).
female(julja).
female(alisa).
female(sveta).
female(nastja).
female(alena).
female(maria).
%Mehed
male(ilja).
male(vladislav).
male(vlad).
male(anatolij).
male(nikolai).
male(richard).
male(roman).
male(martin).
male(sasha).
%emad
mother(anna, ilja).
mother(anna, martin).
mother(anna, maria).
mother(anna, sasha).
mother(natalja, vladislav).
mother(natalja, julja).
mother(galina, anna).
mother(galina, sveta).
mother(julja, alisa).
mother(sveta, vlad).
mother(sveta, artem).
mother(alena, nastja).
%abielus
married(anna, vladislav).
married(sveta, anatolij).
married(julja, richard).
married(natalja, nikolai).
married(alena, vlad).
married(galina, roman).

ema_lapsed(Mina):-
            mother(Ema, Mina),
            mother(Ema, Teine_Laps),
            Teine_Laps \= Mina, write(Teine_Laps),
            nl,
            fail.
ema_lapsed(_):-
            write('minu emal ei ole rohkem lapsi').

father(Child, Father):-
            mother(Child, Mother),
            married(Mother, Father),
            male(Father).
brother(Child, Brother):-
            mother(Child, Mother),
            mother(Brother, Mother),
            male(Brother),
            Brother \= Child.

sister(Child, Sister):-
            mother(Child, Mother),
            mother(Sister, Mother),
            female(Sister),
            Sister \= Child.

aunt(Child, Aunt):-
            (
            mother(Child, Mother),
            sister(Mother, Aunt)
            );(
            father(Child, Father),
            sister(Father, Aunt)
            ).
uncle(Child, Uncle):-
            (
            mother(Child, Mother),
            brother(Mother, Uncle)
            );(
            father(Child, Father),
            brother(Father, Uncle)
            ).
grandfather(Child, Grandfather):-
            (
            mother(Child, Mother),
            father(Mother, Grandfather)
            );(
            father(Child, Father),
            father(Father, Grandfather)
            ).

grandmother(Child, Grandmother):-
            (
            mother(Child, Mother),
            mother(Mother, Grandmother)
            );(
            father(Child, Father),
            mother(Father, Grandmother)
            ).
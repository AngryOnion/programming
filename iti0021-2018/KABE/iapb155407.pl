:- module('Ilja Samoilov', [iapb155407/3]).

iapb155407(Color, X, Y):-
    (
    Color < 10,
    ruut(X2,Y2, Color),
    nl,
    write([Color, 'Nupp ', ruudul, X2,Y2]),
    leia_suund(Color,Suund),
    votmine(X2,Y2,Suund,X1,Y1),
    !)
    ;
    (
    Tamm is Color * 10,
    ruut(X2,Y2, Tamm),
    nl, write([Tamm, 'Nupp ', ruudul, X2,Y2]),
    write('1'),
    leia_suund(Tamm,Suund),
    vota_tammiga(X2, Y2, Suund, X1, Y1),
    !
    )
    ;
    (
    Tamm is Color * 10,
    ruut(X2,Y2, Tamm),
    nl,
    write([Tamm, 'Nupp ', ruudul, X2 ,Y2]),
    leia_suund(Tamm,Suund),
    kaimine_tammiga(X2, Y2, Suund, X1, Y1),
    !
    )
    ;
    (
    Color < 10,
    ruut(X2,Y2, Color),
    nl,
    write([Color, 'Nupp ', ruudul, X2, Y2]),
    leia_suund(Color,Suund),
    kaigu_variandid(X2,Y2,Suund,X1,Y1),
    !
    )
    .
iapb155407(_, _, _).


%iapb155407(Color, X, Y):-
%     X =\= 0, Y =\= 0,
%     %kohustuslik osa
%     ruut(X, Y, Color),
%     nl,
%     write([Color, 'Nupp ', ruudul, X, Y]),
%     leia_suund(Color, Suund),
%     kaigu_variandid(X, Y, Suund, X1, Y1),
%     !
%
%     ;
%
%    nl, write([Color, 'Nupp ', ruudul, X, Y]),
%    leia_suund(Color, Suund),
%    (%tava nupp
%    otsi_voimalusi(Color, Suund, X2, Y2)
%    ;
%    TammColor is Color * 10,
%    otsi_voimalusi(TammColor, Suund, X2, Y2)),
%    kaigu_variandid(X2, Y2, Suund, X1, Y1),
%    !.
%
%
%
%otsi_voimalusi(Color, Suund, X, Y):-
%
%	%tava nupp
%	ruut(X, Y, Color),
%	kas_saab_votta(X, Y, Suund, X1, Y1, X2, Y2, Color);
%	ruut(X, Y, Color),
%	kas_naaber_vaba(X, Y, Suund, X1, Y1);
%
%    %tamm
%    %votmine
%	(Color = 10;Color = 20),
%    ruut(X, Y, Color),
%    kas_tamm_saab_votta(X, Y, Suund, X1, Y1, X2, Y2, Color);
%    %kaimine
%    (Color = 10; Color = 20),
%    ruut(X, Y, Color),
%    kas_naaber_vaba_tammi_jaoks(X, Y, Suund, X1, Y1).

leia_suund(1, 1).
leia_suund(2,-1).

leia_suund(10,-1).
leia_suund(10, 1).

leia_suund(20,-1).
leia_suund(20, 1).


%--------------------------------
kaigu_variandid(X,Y,Suund,X1,Y1):-
    vota_tammiga(X,Y,Suund,X1,Y1),
     !.
kaigu_variandid(X,Y,Suund,X1,Y1):-
    votmine(X,Y,Suund,X1,Y1),
     !.
kaigu_variandid(X,Y,Suund,X1,Y1):-
    kaimine(X,Y,Suund,X1,Y1),
     !.
kaigu_variandid(X,Y,Suund,X1,Y1):-
    kaimine_tammiga(X,Y,Suund,X1,Y1),
     !.
%--------------------------------
votmine(X,Y,Suund,X1,Y1):-
    ruut(X, Y, MyColor),
    kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2,MyColor),
    vota(X,Y,Suund,X1,Y1,X2,Y2).

vota_tammiga(X, Y, Suund, X1, Y1):-
	ruut(X, Y, MyColor),
	kas_saab_votta_tammiga_vasakule(X, Y, Suund, X1, Y1, X2, Y2, MyColor),
	vota(X, Y, Suund, X1, Y1, X2, Y2).

vota_tammiga(X, Y,Suund, X1, Y1):-
	ruut(X, Y, MyColor),
	kas_saab_votta_tammiga_paremale(X, Y, Suund, X1, Y1, X2, Y2, MyColor),
	vota(X, Y, Suund, X1, Y1, X2, Y2).

vota_tammiga(X, Y,Suund, X1, Y1):-
	ruut(X, Y, MyColor),
	kas_saab_votta_tammiga_vasakule_tagasi(X, Y, Suund, X1, Y1, X2, Y2, MyColor),
	vota(X, Y, Suund, X1, Y1, X2, Y2).

vota_tammiga(X, Y,Suund, X1, Y1):-
	ruut(X, Y, MyColor),
	kas_saab_votta_tammiga_paremale_tagasi(X, Y, Suund, X1, Y1, X2, Y2, MyColor),
	vota(X, Y, Suund, X1, Y1, X2, Y2).
%--------
kas_tamm_saab_votta(X, Y, Suund, X1, Y1, X2, Y2, Color):-
    	kas_saab_votta_tammiga_vasakule(X, Y, Suund, X1, Y1, X2, Y2, Color);
    	kas_saab_votta_tammiga_paremale(X, Y, Suund, X1, Y1, X2, Y2, Color);
    	kas_saab_votta_tammiga_paremale_tagasi(X, Y, Suund, X1, Y1, X2, Y2, Color);
    	kas_saab_votta_tammiga_vasakule_tagasi(X, Y, Suund, X1, Y1, X2, Y2, Color).

kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2, MyColor):-  % Votmine edasi paremale
    X1 is X + Suund,
    Y1 is Y + 1,
    ruut(X1,Y1, Color),
    Color =\= MyColor, Color =\= 0,
    X2 is X1 + Suund,
    Y2 is Y1 + 1,
    ruut(X2,Y2, 0).

kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2, MyColor):-  % Votmine edasi vasakule
    X1 is X + Suund,
    Y1 is Y - 1,
    ruut(X1,Y1, Color),
    Color =\= MyColor, Color =\= 0,
    X2 is X1 + Suund,
    Y2 is Y1 - 1,
    ruut(X2,Y2, 0).

kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2, MyColor):-  % Votmine tagasi paremale
    X1 is X + Suund * -1,
    Y1 is Y + 1,
    ruut(X1,Y1, Color),
    Color =\= MyColor, Color =\= 0,
    X2 is X1 + Suund * -1,
    Y2 is Y1 + 1,
    ruut(X2,Y2, 0).

kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2, MyColor):-  % Votmine tagasi vasakule
    X1 is X + Suund * -1,
    Y1 is Y - 1,
    ruut(X1,Y1, Color),
    Color =\= MyColor, Color =\= 0,
    X2 is X1 + Suund * -1,
    Y2 is Y1 - 1,
    ruut(X2,Y2, 0).

kas_saab_votta_tammiga_vasakule(X, Y, Suund, X1, Y1, X2, Y2, Tamm):-
	%saan tammi oiget värvi
	RegularColor is Tamm / 10,
	%kontrollin et see pole tyhi ruut
	RegularColor >= 1,
	X1 is X + Suund,
    Y1 is Y - 1,
    %v6tan ruudu varvi
	ruut(X1, Y1, Color),
	%kontrollin et see pole sama varvi ja pole tyhi
	Color =\= RegularColor, Color =\= 0,
	%kontrollin kas selle nuppu taga on ruumi, et seda saaks ara suua
	X2 is X1 + Suund,
    Y2 is Y1 - 1,
    ruut(X2, Y2, 0).

kas_saab_votta_tammiga_vasakule(X, Y, Suund, X1, Y1, X2, Y2, Tamm):-
	%rekursiivselt vaatan kas saab syya minnes jarjest labi diagonaali
	NewX is X + Suund,
    NewY is Y - 1,
	ruut(NewX, NewY, 0),
	kas_saab_votta_tammiga_vasakule(NewX, NewY, Suund, X1, Y1, X2, Y2, Tamm).

kas_saab_votta_tammiga_paremale(X,Y,Suund,X1,Y1,X2,Y2, Tamm):-
	%saan tammi oiget värvi
	RegularColor is Tamm / 10,
	%kontrollin et see pole tyhi ruut
	RegularColor >= 1,
    X1 is X + Suund,
    Y1 is Y + 1,
    %v6tan ruudu varvi
    ruut(X1,Y1, Color),
    %kontrollin et see pole sama varvi ja pole tyhi
    Color =\= RegularColor, Color =\= 0,
    %kontrollin kas selle nuppu taga on ruumi, et seda saaks ara suua
    X2 is X1 + Suund,
    Y2 is Y1 + 1,
    ruut(X2,Y2, 0).

kas_saab_votta_tammiga_paremale(X, Y, Suund, X1, Y1, X2, Y2, Tamm):-
	%rekursiivselt vaatan kas saab syya minnes jarjest labi diagonaali
	NewX is X + Suund,
    NewY is Y + 1,
	ruut(NewX, NewY, 0),
	kas_saab_votta_tammiga_paremale(NewX, NewY, Suund, X1, Y1, X2, Y2, Tamm).

kas_saab_votta_tammiga_paremale_tagasi(X,Y,Suund,X1,Y1,X2,Y2, Tamm):-
    %saan tammi oiget värvi
    RegularColor is Tamm / 10,
    %kontrollin et see pole tyhi ruut
    RegularColor >= 1,
	X1 is X + Suund * - 1,
    Y1 is Y + 1,
    ruut(X1,Y1, Color),
    %kontrollin et see pole sama varvi ja pole tyhi
    Color =\= RegularColor, Color =\= 0,
    %kontrollin kas selle nuppu taga on ruumi, et seda saaks ara suua
    X2 is X1 + Suund * -1,
    Y2 is Y1 + 1,
    ruut(X2,Y2, 0).

kas_saab_votta_tammiga_paremale_tagasi(X, Y, Suund, X1, Y1, X2, Y2, Tamm):-
	%rekursiivselt vaatan kas saab syya minnes jarjest labi diagonaali
	NewX is X + Suund * -1,
    NewY is Y + 1,
	ruut(NewX, NewY, 0),
	kas_saab_votta_tammiga_paremale_tagasi(NewX, NewY, Suund, X1, Y1, X2, Y2, Tamm).

kas_saab_votta_tammiga_vasakule_tagasi(X,Y,Suund,X1,Y1,X2,Y2, Tamm):-
    %saan tammi oiget värvi
    RegularColor is Tamm / 10,
    %kontrollin et see pole tyhi ruut
    RegularColor >= 1,
	X1 is X + Suund * -1,
    Y1 is Y - 1,
    ruut(X1,Y1, Color),
    %kontrollin et see pole sama varvi ja pole tyhi
    Color =\= RegularColor, Color =\= 0,
    %kontrollin kas selle nuppu taga on ruumi, et seda saaks ara suua
    X2 is X1 + Suund * -1,
    Y2 is Y1 - 1,
    ruut(X2,Y2, 0).

kas_saab_votta_tammiga_vasakule_tagasi(X, Y, Suund, X1, Y1, X2, Y2, Tamm):-
	%rekursiivselt vaatan kas saab syya minnes jarjest labi diagonaali
	NewX is X + Suund * -1,
    NewY is Y - 1,
	ruut(NewX, NewY, 0),
	kas_saab_votta_tammiga_paremale_tagasi(NewX, NewY, Suund, X1, Y1, X2, Y2, Tamm).

vota(X, Y, _, X1, Y1, X2, Y2):-
    %tyhjenda ruutu
    retract(ruut(X, Y, Nr)),
    assert(ruut(X, Y, 0)),
    %kustutan vastasse oma
    retract(ruut(X1, Y1, _)),
    assert(ruut(X1, Y1, 0)),
    %asetan enda nuppu
    retract(ruut(X2, Y2, _)),
    assert(ruut(X2, Y2, Nr)),
    %kontrollin kas on tamm
    muutub_tammiks(X2, Y2, Nr).


tee_kaik(X, Y, X1, Y1):-
    %tyhjenda ruutu
    retract(ruut(X, Y, Nr)),
    assert(ruut(X, Y, 0)),
    %asetan enda nuppu
    retract(ruut(X1, Y1, _)),
    assert(ruut(X1, Y1, Nr)),
    %kontrollin kas on tamm
    muutub_tammiks(X1, Y1, Nr).

%vaatan kas nupp nyyd asub aares
muutub_tammiks(8, Y, 1):-
    retract(ruut(8, Y, _)),
    assert(ruut(8, Y, 10)).

muutub_tammiks(1, Y, 2):-
    retract(ruut(1, Y, _)),
    assert(ruut(1, Y, 20)).

%siis kui eelmised kaks feilivad
muutub_tammiks(_,_,_).

%--------------------------------
kaimine(X,Y,Suund,X1,Y1):-
    kas_naaber_vaba(X,Y,Suund,X1,Y1),
    tee_kaik(X,Y,X1,Y1),
    write([' kaib ', X1,Y1]).

%pidin eraldi tegema, kuna tamm saab ka tagasi minna
kaimine_tammiga(X, Y, Suund, X1, Y1):-
	kas_naaber_vaba_tammi_jaoks(X, Y, Suund, X1, Y1),
	tee_kaik(X, Y, X1, Y1),
	write([' kaib tammiga ', X1,Y1]).

kas_naaber_vaba(X,Y,Suund,X1,Y1):-
    X1 is X +Suund,
    Y1 is Y + 1,
    ruut(X1,Y1, 0).

kas_naaber_vaba(X,Y,Suund,X1,Y1):-
    X1 is X +Suund,
    Y1 is Y - 1,
    ruut(X1,Y1, 0), write(' voi ').

% paremale
kas_naaber_vaba_tammi_jaoks(X,Y,Suund,X1,Y1):-
    X1 is X +Suund,
    Y1 is Y + 1,
    ruut(X1,Y1, 0).

% vasakule
kas_naaber_vaba_tammi_jaoks(X,Y,Suund,X1,Y1):-
    X1 is X +Suund,
    Y1 is Y - 1,
    ruut(X1,Y1, 0), write(' voi ').

% vasakule tagasi
kas_naaber_vaba_tammi_jaoks(X,Y,Suund,X1,Y1):-
    X1 is X +Suund * -1,
    Y1 is Y - 1,
    ruut(X1,Y1, 0), write(' voi ').

% vasakule tagasi
kas_naaber_vaba_tammi_jaoks(X,Y,Suund,X1,Y1):-
    X1 is X +Suund * -1,
    Y1 is Y + 1,
    ruut(X1,Y1, 0), write(' voi ').

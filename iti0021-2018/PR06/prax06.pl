yhisosa([], _, []).

yhisosa([H1|T1], L2, [H1|Res]):-
    member(H1, L2),
    yhisosa(T1, L2, Res),
    !.

yhisosa([_|T1], L2, Res):-
    yhisosa(T1, L2, Res).


%yhend([],[],[]).
%
%yhend([H1/T1], L2, Res):-
%    not(member(H1, Res)),
%    yhend(T1, L2, Res).
%
%yhend([H1/T1], L2, [H1/Res]):-
%    yhend(T1, L2, Res).
%
%yhend([_/T1], L2, Res):-
%    yhend(T1, L2, Res).


yhend(List1, List2, X):-
    merge(List1, List2, Vahe),
    remove_duplicates(Vahe,X).

merge([A|As], [B|Bs], [A,B|Rs]) :-
    merge(As, Bs, Rs).
merge([], Bs, Bs) :- !.
merge(As, [], As).

remove_duplicates([],[]).

remove_duplicates([H | T], List) :-
     member(H, T),
     remove_duplicates( T, List).

remove_duplicates([H | T], [H|T1]) :-
      \+member(H, T),
      remove_duplicates( T, T1).

vahe([],_,[]).

vahe([H1|T1], L2, [H1|Res]):-
     \+ member(H1, L2),
     vahe(T1, L2, Res).

vahe([_|T1], L2, Res):-
    vahe(T1, L2, Res).

ristkorrutis(A, B, X):-
    findall([X,Y],(member(X,A),member(Y,B)),X).

%ristkorrutis([1,2,3], [a], X).

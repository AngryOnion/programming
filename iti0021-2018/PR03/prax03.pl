%Naised
female(anna).
female(natalja).
female(galina).
female(julja).
female(alisa).
female(sveta).
female(nastja).
female(alena).
female(maria).
female(vanavanaema).
female(vanavanaema2).
female(vanavanaema3).
female(vanavanaema4).
%Mehed
male(ilja).
male(vladislav).
male(vlad).
male(anatolij).
male(nikolai).
male(richard).
male(roman).
male(martin).
male(sasha).
male(vanavanaisa).
male(vanavanaisa2).
male(vanavanaisa3).
male(vanavanaisa4).
%emad
mother(ilja, anna).
mother(martin, anna).
mother(maria, anna).
mother(sasha, anna).
mother(vladislav, natalja).
mother(julja, natalja).
mother(anna, galina).
mother(sveta, galina).
mother(alisa, julja).
mother(vlad, sveta).
mother(artem, sveta).
mother(nastja, alena).
mother(galina, vanavanaema).
mother(natalja, vanavanaema2).
mother(nikolai, vanavanaema3).
mother(roman, vanavanaema4).
%abielus
married(anna, vladislav).
married(sveta, anatolij).
married(julja, richard).
married(natalja, nikolai).
married(alena, vlad).
married(galina, roman).
married(vanavanaema, vanavanaisa).
married(vanavanaema2, vanavanaisa2).
married(vanavanaema3, vanavanaisa3).
married(vanavanaema4, vanavanaisa4).

father(Child, Father):-
            mother(Child, Mother),
            married(Mother, Father),
            male(Father).
brother(Child, Brother):-
            mother(Child, Mother),
            mother(Brother, Mother),
            male(Brother),
            Brother \= Child.

sister(Child, Sister):-
            mother(Child, Mother),
            mother(Sister, Mother),
            female(Sister),
            Sister \= Child.

aunt(Child, Aunt):-
            (
            mother(Child, Mother),
            sister(Mother, Aunt)
            );(
            father(Child, Father),
            sister(Father, Aunt)
            ).
uncle(Child, Uncle):-
            (
            mother(Child, Mother),
            brother(Mother, Uncle)
            );(
            father(Child, Father),
            brother(Father, Uncle)
            ).
grandfather(Child, Grandfather):-
            (
            mother(Child, Mother),
            father(Mother, Grandfather)
            );(
            father(Child, Father),
            father(Father, Grandfather)
            ).

grandmother(Child, Grandmother):-
            (
            mother(Child, Mother),
            mother(Mother, Grandmother)
            );(
            father(Child, Father),
            mother(Father, Grandmother)
            ).
ancestor(Child, Parent) :- mother(Child, Parent) ; father(Child, Parent).
ancestor(Child, Parent) :-
            (mother(Child, Mother),
            ancestor(Mother, Parent))
            ; (father(Child, Father),
            ancestor(Father, Parent)).
male_ancestor(Child, Parent) :- father(Child, Parent).
male_ancestor(Child, Parent) :-
            (mother(Child, Mother),
            male_ancestor(Mother, Parent))
            ;(father(Child, Father),
            male_ancestor(Father, Parent)).
female_ancestor(Child, Parent) :- mother(Child, Parent).
female_ancestor(Child, Parent) :-
            (mother(Child, Mother),
            female_ancestor(Mother, Parent))
            ;(father(Child, Father),
            female_ancestor(Father, Parent)).

ancestor1(Child, Parent, N) :- N =:= 1, (mother(Child, Parent) ; father(Child, Parent)).
ancestor1(Child, Parent, N) :-
        (mother(Child, Mother),
         ancestor1(Mother, Parent, N - 1))
         ;(father(Child, Father),
         ancestor1(Father, Parent, N - 1)).

%ancestor2(Child, Parent, X) :- (mother(Child, Parent) ; father(Child, Parent)).
ancestor2(Child, Parent, X) :-
        ancestor(Child, Parent),
        count(Parent, Count),
        X < Count.
count(Parent, Count) :-
   aggregate_all(count, get_child(Child, Parent), Count).
%    count(Child, Parent, X, M).

get_child(Child, Parent) :-
    mother(Child, Parent) ; father(Child, Parent).




liitlause --> lihtlause, sidesona, lihtlause.
liitlause --> lihtlause, sidesona, liitlause.
%liitlause --> liitlause, sidesona, lihtlause.
%liitlause --> liitlause, sidesona, liitlause.
lihtlause
--> nimisonafraas, tegusonafraas.
%lisasin juurde
lihtlause --> tegusonafraas.
nimisonafraas --> nimisona, omadussonafraas, nimisona.
%lisatud
nimisonafraas --> omadussona, nimisona.
nimisonafraas
--> nimisona,nimisonafraas ;[].
nimisona
-->[pakapiku];[habe];[tema];[sobimatuse];[jouluvanaks] ; [kivile] ; [sammal] ; [uhkus] ; [raha] ; [volad].
% terminalsümbolid esinevad reeglis paremal pool ühiklistidena
omadussonafraas
--> maarsona, omadussona.
maarsona
--> [liiga] ; [ei] ; [upakile].
omadussona
--> [lyhike] ; [must] ; [veerevale].
sidesona --> [,] ; [ja].
tegusonafraas
--> tegusona, nimisonafraas.
%lisatud
tegusonafraas --> nimisona, maarsona, tegusona.
%lisatud
tegusonafraas --> nimisona, tegusona, maarsona.
tegusonafraas --> nimisona, tegusona.
tegusona
--> [tingib];[pohjustab];[kasva] ; [ajab] ; [tuleb] ; [jaavad] ; [laheb].
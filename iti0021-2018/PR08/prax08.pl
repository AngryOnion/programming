on_eriliik(taim,elusolend).

on_eriliik(loom,elusolend).

on_eriliik(selgroogne,loom).

on_eriliik(lehm, selgroogne).
on_eriliik(konn,selgroogne).
on_eriliik(madu,selgroogne).
on_eriliik(rott,selgroogne).

on_eriliik(selgrootu,loom).

on_eriliik(amblik,selgrootu).
on_eriliik(sipelgas,selgrootu).

on_eriliik(oistaimed,taim).

on_eriliik(lillia,oistaimed).
on_eriliik(tulp,oistaimed).

on_eriliik(paljasseemnetaimed,taim).

on_eriliik(kuusk,paljasseemnetaimed).
on_eriliik(mand,paljasseemnetaimed).
%on_eriliik(s,d).
%on_eriliik(t,d).
%on_eriliik(s1,s).
%on_eriliik(s2,s).
%on_eriliik(s11,s1).
%on_eriliik(s12,s1).
%on_eriliik(s21,s2).
%on_eriliik(s22,s2).
%on_eriliik(t1,t).
%on_eriliik(t2,t).
%on_eriliik(t21,t2).
%on_eriliik(t22,t2).
%
%eats(t1,t2).
%eats(s2,t1).
%eats(s1,s2).
eats(madu, rott).
eats(rott, amblik).
eats(lehm, tulp).
eats(konn, sipelgas).
eats(sipelgas, kuusk).

%count_terminals(Node,Terminals,Count):- count(Node,Terminals,0,Count).

count(Node,Terminals,Num,Count):- on_eriliik(X, Node), count(X,Terminals,Num,Count).
count(Node,Terminals,Num,Count):-
    \+ on_eriliik(_, Node), !,
    Terminals = Node,
    Count is Num + 1,
    !,
    count(Node, Terminals, Num, Count).
%count(Node,Terminals,Count):- on_eriliik(X, Node), count(X,Terminals,Count).
%count(Node,Terminals,Count + 1):- \+ on_eriliik(_, Node), !, Terminals = Node.
find_terminal(R, R) :-
    \+ on_eriliik(_, R).
find_terminal(R, X) :-
    on_eriliik(C, R),
    find_terminal(C, X).
count_terminals(Node,Terminals,Count):-
    findall(X, find_terminal(Node, X), Terminals),
    length(Terminals, Count).

find_other(R, R) :-
    \+ eats(_, R).
find_other(R, C) :-
    eats(C, R).

% eats(Kes, Keda)
find_eat_roots(R, [R]):-
    \+ eats(_, R),
    !.
find_eat_roots(R, [R|Res]):-
    eats(C, R),
    find_eat_roots(C, Res).


eats_pred(R, R):-
   \+ eats(_, R).

eats_pred(R, X):-
   eats(X, R),
   eats_pred(X, R).

find_eat_terminals([],[], _).
find_eat_terminals([El|Tail], List, Res):-
    findall(X, find_terminal(El, X), T),
    append(List, T, L),
    find_eat_terminals(Tail, L, Res).

find_eat_terminals([El], List, Res):-
    findall(X, find_terminal(El, X), T),
    append(List, T, Res),
    !.

extinction(Who,What_spieces,How_many):-
    find_eat_roots(Who, Roots),
    find_eat_terminals(Roots, [],What_spieces),
    length(What_spieces, How_many),
    !.

find_liik(Liik):-
    findall(X, eats(C, X), L),
    writeln(L),
    find_liik(_, L, 0, Liik).

find_liik(Res, [_|[]], _, Res):-
    !.

find_liik(Liik, [El|Tail], N, Res):-
    write(N),
    extinction(El, _, M),
    write(M),
    writeln(Liik),
    (M > N
    -> find_liik(El, Tail, M, Res)
    ;  find_liik(Liik, Tail, N, Res)).




find_most_sensitive_species(Liik, C, R):-
    find_liik(Liik),
    extinction(Liik, R, C).
viimane_element(X,[X]).
viimane_element(X,[_|Z]):-
    viimane_element(X,Z),
    !.

suurim([], []).
suurim([A|[]], X):-
    X = [A].
suurim([A|[B|C]], X):-
    suurim([B|C], N),
    (A > B ->
    append([A], N, X);
    append([B], N, X)),
    !.

paki([], []).
paki([Head, Head|Tail], Res):-
    !,
    paki([Head|Tail], Res).
paki([Head|Tail], [Head|Res]):-
    paki(Tail, Res).

duplikeeri([],[]).
duplikeeri([Head|Tail], [Head, Head| N]):-
    duplikeeri(Tail, N).

kordista([], _, []).
kordista([Head], 1, [Head]) :-
    !.
kordista([Head], N, [Head|A]) :-
    M is N - 1,
    M > 0,
    kordista([Head], M, A).
kordista([Head|Tail], N, A) :-
    kordista([Head], N, Y),
    kordista(Tail, N, Z),
    !,
    append(Y, Z, A).

paaritu_arv(A):-
    B is A mod 2,
    B == 1.

paaris_arv(A):-
    B is A mod 2,
    B == 0.

suurem_kui(A, B):-
    A > B.

vordle_predikaadiga([], [Reegel], []).
vordle_predikaadiga([], [Reegel], [A]).
vordle_predikaadiga([Head|Tail], [Reegel], [Head|Res]):-
    Term=.. [Reegel, Head],
    Term,
    vordle_predikaadiga(Tail, [Reegel], Res).
vordle_predikaadiga([Head|Tail], [Reegel], Res):-
    vordle_predikaadiga(Tail, [Reegel], Res).

vordle_predikaadiga([], [Reegel, Arv], []).
vordle_predikaadiga([], [Reegel, Arv], [A]).
vordle_predikaadiga([Head|Tail], [Reegel, Arv], [Head|Res]):-
    Term=.. [Reegel, Head, Arv],
    Term,
    vordle_predikaadiga(Tail, [Reegel, Arv], Res).
vordle_predikaadiga([Head|Tail], [Reegel, Arv], Res):-
    vordle_predikaadiga(Tail, [Reegel, Arv], Res).
package ram0620.homework6;

import java.util.Scanner;
import java.util.StringJoiner;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Kirill on 8.02.2018.
 */
public class HomeWork6 {

    private static Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) {

        while (true) {
            System.out.println("To play guess the number enter 1\nTo play Bulls and Cows enter 2\nTo exit enter 3");
            //get game mode from user input
            int gameMode = scanner.nextInt();
            if (gameMode == 1) {
                guessTheNumber();
            } else if (gameMode == 2) {
                bullsAndCows();
            } else if (gameMode == 3) {
                System.out.println("Goodbye");
                break;
            }
        }
    }

    private static void guessTheNumber() {
        int secretNumber = ThreadLocalRandom.current().nextInt(0, 11);
        System.out.println("Guess the number from 0 to 10");
        int attempts = 0;
        while (true) {
            //get console input
            int enteredNumber = scanner.nextInt();
            //increment attempts number
            attempts++;
            if (enteredNumber == secretNumber) {
                System.out.printf("You won. Attempts: %s \n", attempts);
                break;
            }
            if (enteredNumber > secretNumber) {
                System.out.println("Your number is bigger than guessed one");
            } else {
                System.out.println("Your number is smaller than guessed one");
            }
        }
    }

    private static void bullsAndCows() {
        //generate random 4 digit number
        int secretNumber = ThreadLocalRandom.current().nextInt(1000, 10000);
        //change type of number to string to make it easier for checking
        String secretNumberString = String.valueOf(secretNumber);
        System.out.println("Enter 4 digit number:");
        int playerNumber = scanner.nextInt();
        String playerNumberString = String.valueOf(playerNumber);
        if (playerNumberString.length() == 4) {
            int bulls = 0;
            int cows = 0;
            for (int i = 0; i < playerNumberString.length(); i++) {
                //check if secret number position is the same
                if (playerNumberString.charAt(i) == secretNumberString.charAt(i)) {
                    bulls++;
                    //check if secret number contains number from entered digit (bulls not included)
                } else if (secretNumberString.indexOf(playerNumberString.charAt(i)) >= 0) {
                    cows++;
                }
            }
            System.out.printf("Number was %s \nCows: %s Bulls: %s \n", secretNumber, cows, bulls);
        } else {
            bullsAndCows();
        }

    }
}

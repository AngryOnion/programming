package ram0620.homework2;

import java.text.DecimalFormat;
import java.util.Scanner;

/**
 * Created by Kirill on 14.02.2018.
 */
public class KehaMassiIndeks {

    public static void main(String[] args) {
        // сканнер дляполучения ввода из консоли
        Scanner scanner = new Scanner(System.in);

        // Спрашиваем полчеловека
        System.out.println("Your sex (M-male, F-female):");

        if (scanner.hasNext("m") || scanner.hasNext("M") || scanner.hasNext("f") || scanner.hasNext("F")) {
            String sex = scanner.nextLine();

            System.out.println("Your age");
            //Есть ли в и/о число
            if (scanner.hasNextInt()) {
                double age = scanner.nextInt();
                System.out.println("Your height (cm)");
                //Есть ли в и/о число
                if (scanner.hasNextDouble() ) {
                    double height = scanner.nextDouble();
                    System.out.println("Your weight");

                   //Есть ли в и/о число
                    if (scanner.hasNextDouble()) {
                        double weight = scanner.nextDouble();

                        //иницилизируем переменные тут, потому что дальше идет control flow
                        double idealWeight; double fatPercent;
                        //вычисления для женщин и мужчин разные
                        if (sex.equalsIgnoreCase("m")) {
                            //вычисляем идеальный вес
                            idealWeight = (3 * height - 450 + age) * 0.25 + 45;
                            //и процент жира
                            fatPercent = ((weight - idealWeight) / weight) * 100 + 15;
                        } else {
                            //вычисляем идеальный вес
                            idealWeight = (3 * height - 450 + age) * 0.225 + 40.5;
                            //и процент жира
                            fatPercent = ((weight - idealWeight) / weight) * 100 + 22;
                        }
                        //вычисления оставшихся переменных
                        double bodyMassIndex = weight / Math.pow((height / 100), 2);
                        double volume = (1000 * weight) / (8.9 * fatPercent + 11 * (100 - fatPercent));
                        double y = (35.75 - Math.log10(weight)) / 53.2;
                        double area = (Math.pow(1000 * weight, y) * Math.pow(height, 0.3)) / 3318.2;
                        String mark;

                        //оценка индексу массы тела
                        if (bodyMassIndex < 18) {
                            mark = "skinny";
                        } else if (bodyMassIndex >= 18 && bodyMassIndex <= 25) {
                            mark = "normal";
                        } else if (bodyMassIndex > 25 && bodyMassIndex <= 30) {
                            mark = "overweight";
                        } else {
                            mark = "very overweight";
                        }

                        //делаем числа красивей с помо      щью форматирования
                        DecimalFormat f = new DecimalFormat("##.00");

                        System.out.printf("Ideal weight: %s\n", f.format(idealWeight));
                        System.out.printf("Fat percent: %s\n",f.format(fatPercent));
                        System.out.printf("Body volume: %s\n", f.format(volume));
                        System.out.printf("Area: %s \n", f.format(area));
                        System.out.printf("Body mass index: %s\n", f.format(bodyMassIndex));
                        System.out.printf("Evaluation: %s", mark);


                    //выведение ошибки в случае неверного ввода полей
                    } else System.out.println("Error");
                } else System.out.println("Error");
            } else System.out.println("Error");
        } else System.out.println("Error");
    }
}


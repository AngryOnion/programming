package ram0620.homework8;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Kirill on 10.02.2018.
 */
public class Task2 {


    public static void main(String[] args) {
        int[] statistics = new int[11];

        for (int i = 1; i <= 100000; i++) {
            int dice1 = rollDice();
            int dice2 = rollDice();

            int sum = dice1+dice2;
            //2 -> 0 position
            statistics[sum - 2]++;
        }

        for (int i = 0; i < statistics.length; i++){
            System.out.printf("Статистика %d -> %d\n", i+2,statistics[i]);
        }
    }

    private static int rollDice(){
        return ThreadLocalRandom.current().nextInt(1, 7);
    }
}

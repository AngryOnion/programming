package ram0620.homework8;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;


public class Task1 {

    public static void main(String[] args) {
        int[] array = generateArray();
        getAllEvenElementsNumbers(array);
        int max = findMaxValuesOfArray(array);
        getAverage(array, findMinimum(array), max);
        isAriseSequence(array);
        findSecondMinimum(array);
        System.out.println();
    }

    private static double getAverage(int[] array, int minimum, int max) {
        int sum = 0;
        int count = 0;
        for (int i :array) {
            if (i != minimum && i != max) {
                count++;
                sum += i;
            }
        }
        double average = (double) sum / (double) count;
        System.out.println("Cреднее арифметическое: " + average);
        return average;
    }

    private static int[] generateArray() {
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            //случайное число из диапазона 1..20
            array[i] = ThreadLocalRandom.current().nextInt(1, 21);
        }
        return array;
    }

    private static void getAllEvenElementsNumbers(int[] array) {
        System.out.println("Четные номера элементов массива");
        for (int i = 0; i < array.length; i++) {
            //четное ли число
            if (array[i] % 2 == 0) {
                System.out.printf("Номер: %s Элемент: %s\n", i, array[i]);
            }
        }
    }

    private static int findMaxValuesOfArray(int[] array) {
        int max = 0;
        ArrayList<Integer> elementNumbers = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            if (array[i] == max) {
                elementNumbers.add(i);
            }
            if (array[i] > max) {
                max = array[i];
                elementNumbers = new ArrayList<>();
                elementNumbers.add(i);
            }
        }
        if (elementNumbers.size() == 1) {
            System.out.printf("Максимальный элемент встречается один раз, номер %s, значение %s\n",
                    elementNumbers.get(0), max);
        } else {
            System.out.println("Максимальные элементы");
            for (int i: elementNumbers) {
                System.out.printf("Номер: %s Элемент: %s\n", i, max);
            }
        }
        return max;
    }

    private static int findMinimum(int[] array) {
        int min = Integer.MAX_VALUE;
        for (int i : array) {
            if (i < min) {
                min = i;
            }
        }
        return min;
    }

    private static boolean isAriseSequence(int[] array) {
        int lastValue = 0;
        boolean arise = true;
        for (int i : array) {
            if (i < lastValue) {
                arise = false;
                break;
            }
            lastValue = i;
        }
        if (arise) {
            System.out.println("Последовательность возрастает");
        } else {
            System.out.println("Последовательность не возрастает");
        }
        return arise;
    }

    private static void findSecondMinimum(int[] array) {
        int min = findMinimum(array);
        int secondMin = Integer.MAX_VALUE;
        for (int i : array) {
            if (i < secondMin && i !=min ) {
                secondMin = i;
            }
        }
        System.out.println("Второй минимум: " + secondMin);
    }
}

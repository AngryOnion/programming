package ram0620.homework11;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class Task2 {

    static int[][] months = new int[12][];
    static String maxTemperatureString = "";
    static String minTemperatureString = "";
    static int maxTemp = Integer.MIN_VALUE;
    static int minTemp = Integer.MAX_VALUE;

    public static void main(String[] args) {
        createArrays();
        fillWithTemperatures();
        print();
        getMonthMaxAndMin();
        System.out.printf("Minimum temperature: %s. Date: (%s)\n", minTemp, minTemperatureString);
        System.out.printf("Maximum temperature: %s. Date: (%s)", maxTemp, maxTemperatureString);
    }

    /**
     * Create empty arrays
     */
    public static void createArrays() {
        months[0] = new int[31];
        months[1] = new int[28];
        months[2] = new int[31];
        months[3] = new int[30];
        months[4] = new int[31];
        months[5] = new int[30];
        months[6] = new int[31];
        months[7] = new int[31];
        months[8] = new int[30];
        months[9] = new int[31];
        months[10] = new int[30];
        months[11] = new int[31];
    }


    /**
     * Add random temperatures to array
     */
    public static void fillWithTemperatures() {
        for (int i = 0; i < months.length; i++) {
            //winter
            if (i == 0 || i == 1 || i == 11) {
                for (int j = 0; j < months[i].length; j++) {
                    months[i][j] = ThreadLocalRandom.current().nextInt(-30,4);
                }
            }
            //spring
            if (i == 2 || i == 3 || i == 4) {
                for (int j = 0; j < months[i].length; j++) {
                    //may is warmer than other spring months
                    if (i == 4) {
                        months[i][j] = ThreadLocalRandom.current().nextInt(5,26);
                    } else {
                        months[i][j] = ThreadLocalRandom.current().nextInt(-2,16);
                    }
                }
            }
            //summer
            if (i == 5 || i == 6 || i == 7) {
                for (int j = 0; j < months[i].length; j++) {
                    months[i][j] = ThreadLocalRandom.current().nextInt(10,35);
                }
            }
            //autumn
            if (i == 8 || i == 9 || i == 10) {
                for (int j = 0; j < months[i].length; j++) {
                    if (i == 8) {
                        months[i][j] = ThreadLocalRandom.current().nextInt(5,21);
                    } else if (i == 9) {
                        months[i][j] = ThreadLocalRandom.current().nextInt(0,13);
                    } else {
                        months[i][j] = ThreadLocalRandom.current().nextInt(-10,8);
                    }
                }
            }
        }
    }

    /**
     * Get average temperature of month
     * @param month array of month temperature
     * @return average of month temperature
     */
    public static BigDecimal getAverage(int[] month) {
        int sum = 0;
        for (int dayTemperature: month){
            sum += dayTemperature;
        }
        return new BigDecimal(sum).divide(new BigDecimal(month.length), new MathContext(3));
    }

    /**
     * Print array of month temperature
     */
    public static void print() {
        System.out.println("            1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16"
                +  "  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31   Average");
        System.out.print("January  ");
        printMonth(months[0]);
        System.out.print("February ");
        printMonth(months[1]);
        System.out.print("March    ");
        printMonth(months[2]);
        System.out.print("April    ");
        printMonth(months[3]);
        System.out.print("May      ");
        printMonth(months[4]);
        System.out.print("June     ");
        printMonth(months[5]);
        System.out.print("July     ");
        printMonth(months[6]);
        System.out.print("August   ");
        printMonth(months[7]);
        System.out.print("September");
        printMonth(months[8]);
        System.out.print("October  ");
        printMonth(months[9]);
        System.out.print("November ");
        printMonth(months[10]);
        System.out.print("December ");
        printMonth(months[11]);
    }

    /**
     * Print month array to line
     * @param month month temperature array
     */
    public static void printMonth(int[] month) {
        for (int i = 0; i < month.length; i++) {
            int spaces = 4 - String.valueOf(month[i]).length();
            System.out.print(repeat(spaces));
            System.out.print(month[i]);
        }
        System.out.print(repeat((31 - month.length) * 4 + 3));
        System.out.println(getAverage(month));
    }

    /**
     * Repeat spaces
     * @param count of spaces
     * @return String with spaces
     */
    public static String repeat(int count) {
        return new String(new char[count]).replace("\0", " ");
    }

    /**
     * Calculate month maximum and minimum
     */
    public static void getMonthMaxAndMin() {
        ArrayList<String> max = new ArrayList<>();
        ArrayList<String> min = new ArrayList<>();
        for (int i = 0; i < months.length; i++) {
            for (int j = 0; j < months[i].length; j++) {
                final int dayTemperature = months[i][j];
                if (dayTemperature > maxTemp) {
                    maxTemp = dayTemperature;
                    max.clear();
                } else if (dayTemperature < minTemp) {
                    minTemp = dayTemperature;
                    min.clear();
                }
                if (dayTemperature == maxTemp) {
                    //String format to fix int size to 2 and add leading zero before number
                    max.add(j + 1 + "." + (String.format("%02d", i + 1)));
                }
                if (dayTemperature == minTemp) {
                    min.add(j + 1 + "." + (String.format("%02d", i + 1)));
                }
            }
        }
        //Format array to string with space between them
        maxTemperatureString = String.join(" ", max);
        minTemperatureString = String.join(" ", min);
    }
}

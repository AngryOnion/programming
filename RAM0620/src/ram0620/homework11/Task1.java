package ram0620.homework11;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Task1 {
    //

    static int[][] placCarriage = new int[2][54];
    static int[][] coopCarriage = new int[2][36];
    static int[] svCarriage = new int[18];
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        fillArrays();
        printCarriages();
        System.out.println("Введите номер вагона:");
        int carriageNumber = scanner.nextInt();
        int[] carriage = getCarriageByNumber(carriageNumber);
        System.out.print("Свободные места: ");
        printFreePlaces(carriage);
        if (carriageNumber != 3) {
            System.out.println("Желаете только нижние места? (y/n): ");
            String lowPlaces = scanner.next();
            if (lowPlaces.trim().contains("y")) {
                System.out.print("Свободные места: ");
                printLowerPlaces(carriage);
            }
            if (carriageNumber == 1 || carriageNumber == 5) {
                System.out.println("Желаете только боковые места? (y/n): ");
                lowPlaces = scanner.next();
                if (lowPlaces.trim().contains("y")) {
                    System.out.print("Свободные места: ");
                    printSidePlaces(carriage);
                }
            }
        }
        System.out.println("Выберите место: ");
        int place = scanner.nextInt();
        if (checkIfPlaceIsFree(carriageNumber, place)) {
            buyTicket(carriageNumber, place);
            System.out.println("Спасибо за покупку");
            System.out.println("Ваш билет: Вагон " + carriageNumber + ",место " + place);
            System.out.println("Счастливого пути");
        }
    }

    /**
     * Fill carriage arrays with 0 and 1.
     * 0 - free, 1 - already bought by someone
     */
    public static void fillArrays() {
        for (int i = 0; i < placCarriage.length; i++) {
            for (int j = 0; j < placCarriage[i].length; j++) {
                placCarriage[i][j] = ThreadLocalRandom.current().nextInt(0,2);
            }
        }
        for (int i = 0; i < coopCarriage.length; i++) {
            for (int j = 0; j < coopCarriage[i].length; j++) {
                coopCarriage[i][j] = ThreadLocalRandom.current().nextInt(0,2);
            }
        }
        for (int i = 0; i < svCarriage.length; i++) {
            svCarriage[i] = ThreadLocalRandom.current().nextInt(0,2);
        }
    }

    /**
     * Buy ticket, change its value from 0 to 1
     * @param carriage number of carriage
     * @param place number of place
     */
    public static void buyTicket(int carriage, int place) {
        if (carriage == 1 || carriage == 5) {
            placCarriage[carriage][place] = 1;
        } else if (carriage == 2 || carriage == 4) {
            coopCarriage[carriage][place] = 1;
        } else if (carriage == 3){
            svCarriage[place] = 1;
        }
    }

    /**
     * @param carriage number of carriage
     * @param place number of palce
     * @return if place is free
     */
    public static boolean checkIfPlaceIsFree(int carriage, int place) {
        boolean isFree = false;
        if (carriage == 1 || carriage == 5) {
            if (placCarriage[carriage][place - 1] == 0) {
                isFree = true;
            }
        } else if (carriage == 2 || carriage == 4) {
            if (coopCarriage[carriage][place - 1] == 0) {
                isFree = true;
            }
        } else if (carriage == 3){
            if (svCarriage[place - 1] == 0) {
                isFree = true;
            }
        }
        return isFree;
    }

    /**
     * @param carriageNumber number of carriage
     * @return right carriage array by number
     */
    public static int[] getCarriageByNumber(int carriageNumber) {
        if (carriageNumber == 1) {
            return placCarriage[0];
        } else if (carriageNumber == 2) {
            return coopCarriage[0];
        } else if (carriageNumber == 3) {
            return svCarriage;
        } else if (carriageNumber == 4) {
            return coopCarriage[1];
        } else if (carriageNumber == 5) {
            return placCarriage[1];
        }
        return new int[10];
    }

    /**
     * Method to print each carraige array to one line
     */
    public static void printCarriages() {
        printArray(placCarriage[0]);
        printArray(coopCarriage[0]);
        printArray(svCarriage);
        printArray(coopCarriage[1]);
        printArray(placCarriage[1]);
    }

    /**
     * Prints array to one line
     * @param array array with integers
     */
    public static void printArray(int[] array) {
        for (int anArray : array) {
            System.out.print(anArray + " ");
        }
        System.out.println();
    }

    /**
     * Prints only free places to one line
     * @param array
     */
    public static void printFreePlaces(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                System.out.print(i + 1 + " ");
            }
        }
        System.out.println();
    }

    /**
     * Prints only places that are under
     * @param array
     */
    public static void printLowerPlaces(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0 && (i +1) % 2 == 0) {
                System.out.print(i + 1 + " ");
            }
        }
        System.out.println();
    }

    /**
     * Prints only side places in carriage
     * @param array
     */
    public static void printSidePlaces(int[] array) {
        for (int i = 36; i < array.length; i++) {
            if (array[i] == 0) {
                System.out.print(i + 1 + " ");
            }
        }
        System.out.println();
    }
}

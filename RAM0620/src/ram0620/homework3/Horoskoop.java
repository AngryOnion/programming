package ram0620.homework3;

import java.util.Scanner;

/**
 * Created by Ilja on 8.02.2018.
 */
public class Horoskoop {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter day:");
        int day = scanner.nextInt();
        System.out.println("Enter month:");
        int month = scanner.nextInt();
        String sign = "";
        switch (month) {
            case 1:
                if (day > 19) {
                    sign = "Водолей";
                } else {
                    sign = "Козерог";
                }
                break;
            case 2:
                if (day > 18) {
                    sign = "Рыбы";
                } else {
                    sign = "Водолей";
                }
                break;
            case 3:
                if (day > 20) {
                    sign = "Овен";
                } else {
                    sign = "Рыбы";
                }
                break;
            case 4:
                if (day > 19) {
                    sign = "Телец";
                } else {
                    sign = "Овен";
                }
                break;
            case 5:
                if (day > 20) {
                    sign = "Близнецы";
                } else {
                    sign = "Телец";
                }
                break;
            case 6:
                if (day > 21) {
                    sign = "Рак";
                } else {
                    sign = "Близнецы";
                }
                break;
            case 7:
                if (day > 22) {
                    sign = "Лев";
                } else {
                    sign = "Рак";
                }
                break;
            case 8:
                if (day > 22) {
                    sign = "Дева";
                } else {
                    sign = "Лев";
                }
                break;
            case 9:
                if (day > 22) {
                    sign = "Весы";
                } else {
                    sign = "Дева";
                }
                break;
            case 10:
                if (day > 22) {
                    sign = "Скорпион";
                } else {
                    sign = "Весы";
                }
                break;
            case 11:
                if (day > 22) {
                    sign = "Стрелец";
                } else {
                    sign = "Скорпион";
                }
                break;
            case 12:
                if (day > 21) {
                    sign = "Козерог";
                } else {
                    sign = "Стрелец";
                }
                break;
            default:
                sign = "Ошибка";
        }
        System.out.printf("Знак зодиака: %s", sign);
    }
}

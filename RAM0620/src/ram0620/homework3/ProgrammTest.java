package ram0620.homework3;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Ilja on 7.02.2018.
 */
public class ProgrammTest {
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";

    public static void main(String[] args) {
        int score = 0;
        Scanner scanner = new Scanner(System.in);
        //Game loop
        for (int i = 0; i < 5; i++) {
            //Generate 2 random numbers from 1 to 10, max number is not included
            int firstNumber = ThreadLocalRandom.current().nextInt(1, 11);
            int secondNumber = ThreadLocalRandom.current().nextInt(1, 11);
            System.out.printf("%s * %s = ", firstNumber, secondNumber);
            int answer = scanner.nextInt();
            if (answer == firstNumber * secondNumber) {
                //if answer is right increment score
                score++;
                System.out.println(ANSI_GREEN + "Правильно" + ANSI_RESET);
            } else {
                System.out.println(ANSI_RED + "Ошибка" + ANSI_RESET);
            }
        }
        switch (score) {
            case 5:
                System.out.println("Молодец! Правильных ответов " + score);
                break;
            case 4:
            case 3:
                System.out.println("Надо бы еще поучить! Правильных ответов " + score);
                break;
            case 2:
            case 1:
            case 0:
                System.out.println("Срочно нужно учить таблицу умножения! Правильных ответов " + score);
                break;
        }
    }
}

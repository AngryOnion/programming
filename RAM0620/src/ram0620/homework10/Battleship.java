package ram0620.homework10;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

/**
 *      1 корабль — ряд из 4 клеток («четырёхпалубные»)
 2 корабля — ряд из 3 клеток («трёхпалубные»)
 3 корабля — ряд из 2 клеток («двухпалубные»)
 4 корабля — 1 клетка («однопалубные»)
 */
public class Battleship {

    public static final char EMPTY = '.';
    public static final char SHIP = '+';
    public static final char FIRED = 'x';
    private static char[][] playerField = new char[10][10];
    private static char[][] opponentField = new char[10][10];
    private static boolean playerTurn = true;
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        /**initField(playerField);
        placeShips(playerField, 4, 1);
        placeShips(playerField, 3, 2);
        placeShips(playerField, 2, 3);
        placeShips(playerField, 1, 4);
        **/
        initField(opponentField);
        placeShips(opponentField, 4, 1);
        placeShips(opponentField, 3, 2);
        placeShips(opponentField, 2, 3);
        placeShips(opponentField, 1, 4);
        //gameloop
        while(true) {
//            System.out.println("Your field");
//            printField(playerField);
            System.out.println("Computer field");
            printOpponentField(opponentField);
            printField(opponentField);
            System.out.println("Fire! Enter x:");
            int x = scanner.nextInt();
            System.out.println("Enter y:");
            int y = scanner.nextInt();
            if (x >= 0 && x < 10 && y >= 0 && y < 10) {
                if (fire(opponentField, x, y)) {
                    System.out.println("Попал");
                } else {
                    System.out.println("Мимо");
                }
            } else {
                continue;
            }
        }
    }


    /**
     * Create field
     * @param field
     */
    private static void initField(char[][] field) {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                field[i][j] = EMPTY;
            }
        }
    }

    /**
     * Places ships to the field
     * @param field game field
     * @param size size of the ship
     * @param count how many ships to place
     */
    private static void placeShips(char[][] field, int size, int count) {
        //place 4ship
        for (int i = 0; i < count; i++) {
            placeShip(field, size);
        }
    }

    /**
     * Place ship to the field
     * @param field game field
     * @param size size of the ship
     */
    private static void placeShip(char[][] field, int size) {
        int firstCellX = ThreadLocalRandom.current().nextInt(0, 10);
        int firstCellY = ThreadLocalRandom.current().nextInt(0, 10);
        if(isFreeAroundCell(field, firstCellX, firstCellY)) {
            if (size > 1) {
                boolean up = false;
                boolean down = false;
                boolean left = false;
                boolean right = false;
                if (firstCellX - size - 1 >= 0) {
                    left = true;
                }
                if (firstCellY - size - 1 >= 0) {
                    up = true;
                }
                if (firstCellX + size - 1 < 10) {
                    right = true;
                }
                if (firstCellX + size - 1 < 10) {
                    down = true;
                }

                boolean[] directions = {up, down, left, right};
                if (!up && !down && !left && !right) {
                    //generate new cells
                    placeShip(field, size);
                } else {
                    placeShipCells(field, size, firstCellX, firstCellY, directions);
                }
            } else {
                field[firstCellX][firstCellY] = SHIP;
            }
        } else {
            placeShip(field, size);
        }
    }

    /**
     * Places all cells of the ship if ship is bigger than 1 cell
     * @param field to place
     * @param size of the ship
     * @param firstCellX initial x coordinate of ship
     * @param firstCellY initial y coordinate of ship
     * @param directions direction where to place ship
     */
    private static void placeShipCells(char[][] field, int size, int firstCellX, int firstCellY, boolean[] directions) {
        if (!directions[0] && !directions[1] && !directions[2] && !directions[3]) {
            //generate new cells
            placeShip(field, size);
            return;
        }
        int direction = 0;
        try {
            direction = getDirection(directions);
        } catch (StackOverflowError e) {
            System.out.println();
        }
        switch (direction) {
            case 0:
                for (int i = 0; i < size; i++) {
                    if (!isFreeAroundCell(field, firstCellX, firstCellY - i)) {
                        directions[direction] = false;
                        placeShipCells(field, size, firstCellX, firstCellY, directions);
                    }
                }
                for (int i = 0; i < size; i++) {
                    field[firstCellX][firstCellY - i] = SHIP;
                }
                break;
            case 1:
                for (int i = 0; i < size; i++) {
                    if (!isFreeAroundCell(field, firstCellX, firstCellY + i)) {
                        directions[direction] = false;
                        placeShipCells(field, size, firstCellX, firstCellY, directions);
                    }
                }
                for (int i = 0; i < size; i++) {
                    field[firstCellX][firstCellY + i] = SHIP;
                }
                break;
            case 2:
                for (int i = 0; i < size; i++) {
                    if (!isFreeAroundCell(field, firstCellX-i, firstCellY)) {
                        directions[direction] = false;
                        placeShipCells(field, size, firstCellX, firstCellY, directions);
                    }
                }
                for (int i = 0; i < size; i++) {
                    field[firstCellX - i][firstCellY] = SHIP;
                }
                break;
            case 3:
                for (int i = 0; i < size; i++) {
                    if (!isFreeAroundCell(field, firstCellX+i, firstCellY)) {
                        directions[direction] = false;
                        placeShipCells(field, size, firstCellX, firstCellY, directions);
                    }
                }
                for (int i = 0; i < size; i++) {
                    field[firstCellX + i][firstCellY] = SHIP;
                }
                break;
        }
    }

    /**
     * Choose random direction where to place ship
     * @param directions array with all the possible directions
     * @return direction number
     */
    private static int getDirection(boolean[] directions) {
        int direction = ThreadLocalRandom.current().nextInt(0, 4);
        if (directions[direction]) {
            return direction;
        } else {
            return getDirection(directions);
        }
    }

    /**
     * Check if cells around are free
     * @param field of game
     * @param x coordinate
     * @param y coordinate
     * @return boolean if cells are free around
     */
    private static boolean isFreeAroundCell(char[][] field, int x, int y) {
        for (int i = -1; i <= 1 ; i++) {
            for (int j = -1; j <= 1; j++) {
                try {
                    if (x + i >= 0 && x + i < field.length && y + j >= 0 && y + j < field.length) {
                        if (field[x + i][y + j] != EMPTY) {
                            return false;
                        }
                    }
                }catch (ArrayIndexOutOfBoundsException e) {
                    System.out.println("error");
                }
            }
        }
        return true;
    }

    private static boolean isFreeAroundCellAndNotOutOfBounds(char[][] field, int x, int y) {
        for (int i = -1; i <= 1 ; i++) {
            for (int j = -1; j <= 1; j++) {
                try {
                    if (x + i >= 0 && x + i < field.length && y + j >= 0 && y + j < field.length) {
                        if (field[x + i][y + j] != EMPTY) {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }catch (ArrayIndexOutOfBoundsException e) {
                    System.out.println("error");
                }
            }
        }
        return true;
    }
    private static boolean checkCellEmptiness(char[][] field, int x, int y) {
        return field[x][y] == EMPTY;
    }


    /**
     * Prints field to console
     * @param field
     */
    private static void printField(char[][] field) {
        System.out.print("  ");
        for (int i = 0; i < 10; i++) {
            System.out.print(i + " ");
            if (i == 9) {
                System.out.print("\n");
            }
        }
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (j==0) {
                    System.out.print(i + " ");
                }
                System.out.print(field[i][j] + " ");
            }
            System.out.println();
        }
    }

    /**
     * Prints opponent fields, ships are not visible
     * @param field
     */
    private static void printOpponentField(char[][] field) {
        System.out.print("  ");
        for (int i = 0; i < 10; i++) {
            System.out.print(i + " ");
            if (i == 9) {
                System.out.print("\n");
            }
        }
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (j==0) {
                    System.out.print(i + " ");
                }
                if (field[i][j] != SHIP) {
                    System.out.print(field[i][j] + " ");
                } else {
                    System.out.print(EMPTY + " ");
                }
            }
            System.out.println();
        }
    }

    /**
     * Kills ship if it is where
     * @param field
     * @param x coordinate
     * @param y coordinate
     * @return
     */
    private static boolean fire(char[][] field, int x, int y) {
        if (field[x][y] == SHIP) {
            field[x][y] = FIRED;
            killCellsAround(field,x,y);
            return true;
        }
        field[x][y] = FIRED;
        return false;
    }

    /**
     * Kill cells around of the ship if its killed
     * @param field
     * @param x coordinate
     * @param y coordinate
     */
    private static void killCellsAround(char[][] field, int x, int y) {
        if (checkIfShipsAround(field, x, y)) return;
            for (int i = -1; i <= 1 ; i++) {
                for (int j = -1; j <= 1; j++) {
                    if (x + i >= 0 && x + i < field.length && y + j >= 0 && y + j < field.length) {
                        field[x+i][y+j] = FIRED;
                    }
                }
            }
        System.out.println("Убил");
    }

    /**
     * Checks if ships are around cells
     * @param field
     * @param x
     * @param y
     * @return
     */
    private static boolean checkIfShipsAround(char[][] field, int x, int y) {
        if (checkIfOutOfBorders(x, 1)) {
            if (field[x+1][y] == SHIP) {
                return true;
            }
        }
        if (checkIfOutOfBorders(x, -1)) {
            if (field[x-1][y] == SHIP) {
                return true;
            }
        }
        if (checkIfOutOfBorders(y, 1)) {
            if (field[x][y+1] == SHIP) {
                return true;
            }
        }
        if (checkIfOutOfBorders(y, -1)) {
            if (field[x][y-1] == SHIP) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if cell is out of bounds
     * @param cell
     * @param changer
     * @return
     */
    private static boolean checkIfOutOfBorders(int cell, int changer) {
        return cell + changer >= 0 && cell + changer < 10;
    }
}

package ram0620.homework5;

/**
 * Created by Kirill on 8.02.2018.
 */
public class Task1 {
    public static void main(String[] args) {
        int number = 100000000;
        double nextNumber;
        double pi = 0;

        for (int i = 0; i <= number; i++) {
            nextNumber = 4* ((Math.pow(-1, i))/((2*i)+1));
            pi = pi + nextNumber;

        }

        System.out.printf("PI=%.15f%n",pi);
    }
}

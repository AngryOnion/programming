package ram0620.homework5;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;

/**
 * Created by Kirill on 8.02.2018.
 */
public class Task2 {
    public static DecimalFormat df = new DecimalFormat("#,##");

    public static void main(String[] args) {
        getThis(12, 12, 100000);
    }

    public static void getThis(int month, double percent, double credit) {
        double last = credit;
        double monthIntress = percent/12/100;
        double monthlyPayment = (credit * monthIntress)/ (1 - (1/Math.pow(1+monthIntress, month)));
        BigDecimal payment = new BigDecimal(monthlyPayment);
        monthlyPayment = payment.round(MathContext.DECIMAL32).doubleValue();
        for (int i = 0; i < month; i++) {
            double remaining = new BigDecimal((1 + monthIntress) * last - monthlyPayment).round(MathContext.DECIMAL32).doubleValue();
            last = remaining;
            System.out.println(remaining);
        }
        System.out.println(monthlyPayment);
    }
}

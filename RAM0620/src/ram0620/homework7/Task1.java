package ram0620.homework7;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Kirill on 8.02.2018.
 */
public class Task1 {

    public static void main(String[] args) {
        ArrayList<Integer> perfectNumbers = getPerfectNumbers();
        HashMap<Integer, Integer> amicableNumbers = getAmicableNumbers();
        printNumbers(perfectNumbers, amicableNumbers);
    }

    /**
     * Print numbers what we calculated before
     * @param perfectNumbers list of perfect numbers
     * @param amicableNumbers pairs of amicable numbers
     */
    private static void printNumbers(ArrayList<Integer> perfectNumbers, HashMap<Integer, Integer> amicableNumbers) {
        System.out.println("Perfect numbers:");
        perfectNumbers.forEach(System.out::println);
        if (perfectNumbers.size() > amicableNumbers.size()) {
            System.out.println("More perfect numbers");
        } else if (perfectNumbers.size() == amicableNumbers.size()) {
            System.out.println("Same amount of amicable and perfect numbers");
        } else {
            System.out.println("More amicable numbers");
        }
    }

    /**
     * Get all amicable numbres from 1 to 10000
     * @return amicable number pairs
     */
    private static HashMap<Integer, Integer> getAmicableNumbers() {
        HashMap<Integer, Integer> amicablePairs = new HashMap<>();
        for(int i = 1; i < 10000; i++ ) {
            int a = sum(divisors(i));
            int b = sum(divisors(a));
            if(i == b && i != a){
                if(!amicablePairs.containsKey(i) && !amicablePairs.containsValue(a)
                        && !amicablePairs.containsKey(a) && !amicablePairs.containsValue(i)) {
                    amicablePairs.put(i,a);
                }
            }
        }
        return amicablePairs;
    }

    /**
     * @param n number what divisors is needed to get
     * @return list with divisors of number
     */
    public static ArrayList<Integer> divisors(int n) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(1);
        //We need iterate only through half of number, because if divisor > number it is always smaller than 2
        for(int i = 2; i < (n/2+1); i++){
            if(n % i == 0){
                list.add(i);
            }
        }
        return list;
    }

    /**
     * Sum all numbers from list
     * @param numbers list with divisors
     * @return sum of numbers
     */
    public static int sum(ArrayList<Integer> numbers){
        int sum = 0;
        for(Integer e : numbers)
            sum += e;
        return sum;
    }

    /**
     * Get all perfect numbers from 1 to 10000
     * @return list of perfect integers
     */
    private static ArrayList<Integer> getPerfectNumbers() {
        ArrayList<Integer> perfectNumbers = new ArrayList<>();
        perfectNumbers.add(1);
        for (int i = 2; i < 10000; i++) {
            if (isNumPerfect(i)) {
                perfectNumbers.add(i);
            }
        }
        return perfectNumbers;
    }

    /**
     * @param number to check
     * @return whatever number is perfect or not
     */
    private static boolean isNumPerfect(int number) {
        int sum = 1;
        for (int i = 2; i <= number / 2; i++) {
            if (number % i == 0) {
                sum += i;
            }
        }
        return sum == number;
    }
}

package ram0620.homework7;

/**
 * Created by Kirill on 22.04.2018.
 */
public class Task2 {
    public static void main(String[] args) {
        getSyracuseNumbers();
    }

    /**
     * Calculate and print all Syracuse numbers
     */
    public static void getSyracuseNumbers() {
        for (int i = 2; i <= 30; i++) {
            //length of calculation
            int count= 0;
            //biggest number
            int max = 0;
            System.out.print(i + " : ");
            int x = i;
            //calculate till number is bigger than 0 and not 1
            while (x != 1 && x > 0) {
                count++;
                if (x % 2 == 0) {
                    x =x/2;
                } else {
                    x = (x*3 + 1) / 2;
                }
                if (x > max) {
                    max = x;
                }
                System.out.print(x + " ");
            }
            System.out.println();
            System.out.printf("длительность: %s высота: %s", count, max);
            System.out.println();
        }
    }
}

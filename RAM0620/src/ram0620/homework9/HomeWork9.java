package ram0620.homework9;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Kirill on 11.02.2018.
 */
public class HomeWork9 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите предложение");
        programLoop();
    }


    /**
     * Loop to get all functions of the program
     */
    private static void programLoop() {
        String sentence = scanner.nextLine();
        //make possible breaking menu loop
        outer:
        while (true) {
            showMenu();
            int menu = scanner.nextInt();
            switch (menu) {
                case 1:
                    System.out.println("Количество слов " + countWordsInSentence(sentence));
                    break;
                case 2:
                    getShortestAndLongestWord(sentence);
                    break;
                case 3:
                    System.out.println("Отсортированное предложение:");
                    System.out.println(sortWordsInSentence(sentence));
                    break;
                case 4:
                    findWordInSentence(sentence);
                    break;
                case 5:
                    getWordsWhereFirstAndLastLettersSame(sentence);
                    break;
                case 6:
                    findAllPalindromsAndAnagrams(sentence);
                    break;
                case 7:
                    changeCaseOfTheSentence(sentence, true);
                    break;
                case 8:
                    break outer;
            }
        }
    }

    /**
     * Prints all palindroms and anagrams from sentence
     * @param sentence to analyze
     */
    private static void findAllPalindromsAndAnagrams(String sentence) {
        System.out.println("Палиндромы:");
        String[] words = getWords(sentence);
        for (String word: words) {
            if (isPalindrome(word)) System.out.println(word);
        }
        System.out.println("Анаграмы");
        for (String word: words) {
            for (String word2: words) {
                if (!word.equals(word2)) {
                    if (isAnagram(word, word2)) {
                        System.out.println(word + " - " + word2);
                    }
                }
            }
        }
    }

    /**
     * Prints all words from sentence where first and last letters are the same
     * @param sentence to analyze
     */
    private static void getWordsWhereFirstAndLastLettersSame(String sentence) {
        System.out.println("Слова, у которых первая и последняя буквы одинаковые:");
        for (String word : getWords(sentence)) {
            if (word.length() > 1 && word.charAt(0) == word.charAt(word.length() - 1)) {
                System.out.println(word);
            }
        }
    }

    /**
     * Sort all words in sentence in alphabetical order
     * @param sentence to analyze
     * @return sorted sentence
     */
    private static String sortWordsInSentence(String sentence) {
        String[] words = getWords(sentence);
        Arrays.sort(words);
        return String.join(",", words);
    }

    /**
     * @param sentence where to find word
     * @return if words is present in sentence
     */
    private static boolean findWordInSentence(String sentence) {
        System.out.println("Введите слово для поиска");
        String word = scanner.nextLine();
        word = word.trim();
        boolean contains = sentence.contains(word);
        if (contains) {
            System.out.println("Слово есть в предложение");
        } else {
            System.out.println("Слово нету в предложение");
        }
        return contains;
    }

    /**
     * Prints shortest and longest word in sentence
     * @param sentence to analyze
     */
    private static void getShortestAndLongestWord(String sentence) {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        String minWord = "";
        String maxWord = "";
        for (String word : getWords(sentence)) {
            if (word.length() < min) {
                min = word.length();
                minWord = word;
            }
            if (word.length() > max) {
                max = word.length();
                maxWord = word;
            }
        }
        System.out.println("Самое длинное слово" + maxWord);
        System.out.println("Самое короткое слово" + minWord);
    }

    /**
     * Split sentence to word array, delete all non alfranumerical characters
     * @param sentence to split
     * @return array with words
     */
    private static String[] getWords(String sentence) {
        return sentence.trim().split("\\W+");
    }

    /**
     * Count all words in sentence
     * @param sentence to analyze
     * @return number of words
     */
    private static int countWordsInSentence(String sentence) {
        return getWords(sentence).length; // возвращает количество слов разделенные пробелом
    }

    /**
     * Change case of the whole sentence
     * @param sentence to change
     * @param textCase true - upper case, false - lower case
     * @return new sentence
     */
    private static String changeCaseOfTheSentence(String sentence, boolean textCase) {
        if (textCase) {
            sentence = sentence.toUpperCase();
        } else {
            sentence = sentence.toLowerCase();
        }
        System.out.println(sentence);
        return sentence;
    }

    /**
     * Check if words are anagram or not
     * @param firstWord to check
     * @param secondWord to check
     * @return if words are anagrams to each other
     */
    private static boolean isAnagram(String firstWord, String secondWord) {
        char[] word1 = firstWord.toLowerCase().replaceAll("[\\s]", "").toCharArray();
        char[] word2 = secondWord.toLowerCase().replaceAll("[\\s]", "").toCharArray();
        Arrays.sort(word1);
        Arrays.sort(word2);
        return Arrays.equals(word1, word2);
    }

    /**
     * Checks if word is palindrome
     * @param word to check
     * @return if word is palindrome
     */
    public static boolean isPalindrome(String word){
        int start = 0;
        int end = word.length() - 1;
        while (end > start) {
            if (word.charAt(start) != word.charAt(end)) {
                return false;
            }
            start++;
            end--;
        }
        return true;
    }

    /**
     * Shows menu for program loop
     */
    private static void showMenu() {
        System.out.println("Меню (для доступа введите желаемую цифру)");
        System.out.println("1.Количество слов в предложении\n" +
                "2.Вывод самого короткого и самого длинного слова\n" +
                "3.Вывод отсортированных слов в порядке возрастания или убывания (вид вывода передавать с помощью аргумента функции, используйте тип bool)\n" +
                "4.Поиск слова в предложении\n" +
                "5.Вывести все слова, у которых первая и последняя буквы одинаковые\n" +
                "6.Вывести слова, которые являются анаграммами, палиндромами\n" +
                "7.Вывод предложения в оригинале, в верхнем или нижнем регистре (вид вывода передавать с помощью аргумента функции)\n" +
                "8.Выход");
    }
}

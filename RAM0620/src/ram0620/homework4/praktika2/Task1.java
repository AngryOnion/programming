package ram0620.homework4.praktika2;

import java.util.Scanner;

public class Task1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество секунд с начала суток:");
        int seconds = scanner.nextInt();
        int hours = getHours(seconds);
        int minutes = getMinutes(seconds, hours);
        int secondsLeft = getSecondsLeft(seconds, hours);
        System.out.println(getRealTime(hours, minutes, secondsLeft));
    }

    /**
     * Get seconds left after getting hours and minutes
     * @param seconds
     * @param hours
     * @return
     */
    private static int getSecondsLeft(int seconds, int hours) {
        return (seconds - (hours * 3600)) % 60;
    }

    /**
     * Get whole count of minutes
     * @param seconds
     * @param hours
     * @return minutes count
     */
    private static int getMinutes(int seconds, int hours) {
        return (seconds - (hours * 3600)) / 60;
    }

    /**
     * Get whole count of hours
     * @param seconds
     * @return hours count
     */
    private static int getHours(int seconds) {
        return seconds / 3600;
    }

    /**
     * Get time string
     * @param hours
     * @param minutes
     * @param seconds
     * @return
     */
    private static String getRealTime(int hours, int minutes, int seconds) {
        return String.format("%s:%s:%s", hours, minutes, seconds);
    }
}

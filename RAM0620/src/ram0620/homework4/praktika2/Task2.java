package ram0620.homework4.praktika2;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Task2 {

    private static List<Integer> monthsWith31Days = Arrays.asList(1,3,5,7,8,10,12);
    private static List<Integer> monthsWith30Days = Arrays.asList(4,6,9,11);

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter day: ");
        int day = scanner.nextInt();
        System.out.print("Enter month: ");
        int month=scanner.nextInt();
        System.out.print("Enter year: ");
        int year = scanner.nextInt();

        if (checkDate(day, month, year)) {
            System.out.println("Date is correct");
            System.out.println("Next day: " + getNextDate(day,month,year));
            System.out.printf("Days before New Year: %s\n",getDaysBeforeNewYear(day,month,year));
        }
        else System.out.println("Date is not correct");
    }

    /**
     * Finds next date using getDaysInMonth function
     * @param day
     * @param month
     * @param year
     * @return returns next date in String format
     */
    public static String getNextDate(int day, int month, int year) {
        int daysInMonth = getDaysInMonth(month, year);
        if (day != daysInMonth) {
            return String.format("%s.%s.%s", day + 1, month, year);
        } else {
            if (month == 12) {
                return String.format("1.1.%s", year + 1);
            } else {
                return String.format("1.%s.%s", month + 1, year);
            }
        }
    }

    /**
     * Checks date
     * @param day
     * @param month
     * @param year
     * @return boolean if date either is correct
     */
    public static boolean checkDate(int day, int month, int year){
        int daysInMonth = getDaysInMonth(month,year);
        if (day > 0 && day <= daysInMonth) {
            if (month > 0 && month <= 12) {
                if (year > 0) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Finds days count in month
     * @param month
     * @param year
     * @return days count in month
     */
    public static int getDaysInMonth(int month, int year){
        if (monthsWith31Days.contains(month)) {
            return 31;
        } else if (month == 2) {
            if (((year % 4 == 0) && !(year % 100 == 0) || (year % 400 == 0))) return 29;
            else return 28;
        } else {
            return 30;
        }
    }

    /**
     * Finds days before new year, uses getDaysInMonth method
     * @param day
     * @param month
     * @param year
     * @return days count before new year
     */
    public static int getDaysBeforeNewYear(int day, int month, int year) {
        int daysBeforeNewYear = 0;
        daysBeforeNewYear += getDaysInMonth(month, year) - day;
        for (int i = month + 1; i <= 12; i++) {
            daysBeforeNewYear += getDaysInMonth(i, year);
            if (i == 12) {
                daysBeforeNewYear += 1;
            }
        }
        if (month == 12 && day == 31) {
            daysBeforeNewYear += 1;
        }
        return daysBeforeNewYear;
    }
}

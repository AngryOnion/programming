package ram0620.homework4.praktika1;

public class Task {

    /**
     * Finds triangle line segment length
     * @param xA A abscissa coordinate
     * @param yA A ordinate coordinate
     * @param xB B abscissa coordinate
     * @param yB B ordinate coordinate
     * @return size in double of line segment
     */
    public static double Leng(double xA, double yA, double xB, double yB) {
        double xPow = Math.pow(xA - xB, 2);
        double yPow = Math.pow(yA - yB, 2);
        return Math.abs(Math.sqrt(xPow + yPow));
    }

    /**
     * Finds perimeter of triangle
     * @param xA A abscissa coordinate
     * @param yA A ordinate coordinate
     * @param xB B abscissa coordinate
     * @param yB B ordinate coordinate
     * @param xC C abscissa coordinate
     * @param yC C ordinate coordinate
     * @return size in double of triangle perimeter
     */
    public static double Perim(double xA, double yA, double xB, double yB, double xC, double yC) {
        double ab = Leng(xA, yA, xB, yB);
        double bc = Leng(xB, yB, xC, yC);
        double ca = Leng(xC, yC, xA, yA);
        return ab + bc + ca;
    }

    /**
     * Finds triangle area using perimeter and line segment lengths
     * @param xA A abscissa coordinate
     * @param yA A ordinate coordinate
     * @param xB B abscissa coordinate
     * @param yB B ordinate coordinate
     * @param xC C abscissa coordinate
     * @param yC C ordinate coordinate
     * @return area in double of triangle
     */
    public static double Area(double xA, double yA, double xB, double yB, double xC, double yC) {
        double halfPerim = Perim(xA, yA, xB, yB, xC, yC) / 2.0;
        final double perimAB = halfPerim - Math.abs(Leng(xA, yA, xB, yB));
        final double perimBC = halfPerim - Math.abs(Leng(xB, yB, xC, yC));
        final double perimAC = halfPerim - Math.abs(Leng(xC, yC, xA, yA));
        double area = Math.sqrt(halfPerim * perimAB * perimBC * perimAC);
        return area;
    }
}


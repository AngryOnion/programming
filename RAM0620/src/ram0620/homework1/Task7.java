package ram0620.homework1;

import java.util.Scanner;

/**
 * Created by Kirill on 7.02.2018.
 */
public class Task7 {
    //init Scanner
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        //get userinput
        System.out.println("Enter three-digit number: ");
        int number = scanner.nextInt();
        // dividing by 10 with / gives first number, because it lefts only integer value
        // dividing by 10 with leftover gives second number
        int firstNumber = number / 100;
        int secondNumber = number % 100 / 10;
        int thirdNumber = number % 100 % 10;
        System.out.printf("First number: %s\nSecond number: %s\nThird number: %s",
                firstNumber, secondNumber, thirdNumber);
    }
}

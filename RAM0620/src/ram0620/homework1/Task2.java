package ram0620.homework1;

import java.util.Scanner;

/**
 * Created by Kirill on 7.02.2018.
 */
public class Task2 {
    //init pi, it never changes so it must be final
    private static final double PI = 3.14;
    //init Scanner
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter the diameter of the circle:");
        //get diameter from user input
        double d = scanner.nextDouble();
        System.out.printf("Circumference: %s", d*PI);
    }
}

package ram0620.homework1;

import java.util.Scanner;

/**
 * Created by Kirill on 7.02.2018.
 */
public class Task3 {

    //init Scanner
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        //get user input
        System.out.println("Enter A:");
        int a = scanner.nextInt();
        System.out.println("Enter B:");
        int b = scanner.nextInt();
        System.out.println("Enter C:");
        int c = scanner.nextInt();

        //get section length
        int ac = c - a;
        int bc = c - b;

        //print results
        System.out.printf("Length of a section AC = %s\n\r", ac);
        System.out.printf("Length of a section BC = %s\n\r", bc);
        System.out.printf("Sum = %s\n\r", ac + bc);
    }
}

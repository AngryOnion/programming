package ram0620.homework1;

import java.util.Scanner;

/**
 * Created by Kirill on 7.02.2018.
 */
public class Task6 {
    //init Scanner
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter two-digit number: ");
        int number = scanner.nextInt();
        // dividing by 10 with / gives first number, because it lefts only integer value
        // dividing by 10 with leftover gives second number
        System.out.printf("Left number: %s\n Right number: %s", number / 10, number % 10);
    }
}

package ram0620.homework1;

import java.util.Scanner;

/**
 * Created by Kirill on 7.02.2018.
 */
public class Task4 {

    //init Scanner
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter angle in degrees:");
        double angle = scanner.nextDouble();
        //Get radians using Java library Math
        System.out.printf("Radians: %s", Math.toRadians(angle));
    }
}

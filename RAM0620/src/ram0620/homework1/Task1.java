package ram0620.homework1;

import java.util.Scanner;

/**
 * Created by Kirill on 7.02.2018.
 */
public class Task1 {
    //init Scanner for output from console
    private static Scanner in = new Scanner(System.in);

    /**
     * @param args from command line
     */
    public static void main(String[] args) {
        System.out.println("Enter A:");
        //get a thought scanner
        double a = in.nextDouble();
        System.out.println("Enter B:");
        //get b
        double b = in.nextDouble();
        //print formatted string, %s means string
        System.out.printf("Area: %s\nPerimeter: %s", a*b, (a+b)*2);
    }

}

package ram0620.homework1;

import java.util.Scanner;

/**
 * Created by Kirill on 7.02.2018.
 */
public class Task5 {

    //init Scanner
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        //get user input
        System.out.println("Enter temperature in celsius:");
        double temperatureCelsius = scanner.nextDouble();
        double temperatureFahrenheit = temperatureCelsius * 9/5 + 32;

        System.out.printf("Temperature in Celsius: %s \n Temperature in Fahrenheit: %s",
                temperatureCelsius, temperatureFahrenheit);
    }
}

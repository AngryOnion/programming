package ee.ttu.algoritmid.scoreboard;

import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class ScoreBoard {

    private TreeSet<Participant> scoreBoard = new TreeSet<Participant>(Comparator.comparingInt(Participant::getTime));

    /**
     * Adds a participant's time to the checkpoint scoreboard
     */
    public void add(Participant participant) {
        scoreBoard.add(participant);
    }

    /**
     * Returns top n number of participants in the checkpoint to be displayed on the scoreboard
     * This method will be queried by the tests every time a new participant is added
     */
    public List<Participant> get(int n) {
        if (scoreBoard.size() == 0) return null;
        return scoreBoard.stream().limit(n).collect(Collectors.toList());
//        ArrayList<Participant> participants = new ArrayList<>();
//        Iterator<Participant> iterator = scoreBoard.iterator();
//        int counter = 0;
//        while (counter < n && iterator.hasNext()) {
//            participants.add(iterator.next());
//            counter++;
//        }
//        return participants;
//        for (Map.Entry<Integer, Participant> entry : scoreBoard.entrySet()) {
//            if (n == 0) {
//                return participants;
//            }
//            participants.add(entry.getValue());
//            n--;
//        }
//        return participants;
    }
}
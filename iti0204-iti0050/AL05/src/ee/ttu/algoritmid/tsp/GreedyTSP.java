package ee.ttu.algoritmid.tsp;

import java.util.ArrayList;
import java.util.List;

public class GreedyTSP {

    static Town startTown = new Town(0,0);
    static List<Integer> visitedTowns = new ArrayList<>();
    static int[] allTowns;
    static int[] path;
    /* Greedy search */
    public static int[] greedySolution(int[][] adjacencyMatrix) {
        path = new int[adjacencyMatrix.length + 1];
        path[0] = 0;
        if (adjacencyMatrix.length == 1) {
            return path;
        }
//        if (adjacencyMatrix.length == 2) {
//            path[1] = 1;
//            path[2] = 0;
//            return path;
//        }
        findNextTown(adjacencyMatrix);
        return path;
    }

    private static void findNextTown(int[][] adjacencyMatrix) {
        int lastTown = 0;
        while (true) {
            int minValue = Integer.MAX_VALUE;
            int minTown = 0;
            for (int i = 0; i < adjacencyMatrix[lastTown].length; i++) {
                if (adjacencyMatrix[lastTown][i] < minValue && !visitedTowns.contains(i) && adjacencyMatrix[lastTown][i] != 0) {
                    minValue = adjacencyMatrix[lastTown][i];
                    minTown = i;
                }
            }
            visitedTowns.add(minTown);
            lastTown = minTown;
            path[visitedTowns.size()] = minTown;
            if (visitedTowns.size() == adjacencyMatrix.length) {
                path[adjacencyMatrix.length] = 0;
                break;
            }
        }
    }
}
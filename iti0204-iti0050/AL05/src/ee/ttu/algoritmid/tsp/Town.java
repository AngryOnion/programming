package ee.ttu.algoritmid.tsp;

import java.util.Objects;

public class Town {
    int x;
    int y;

    public Town(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public boolean townEquals(int x, int y) {
        return x == this.x &&
                y == this.y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Town town = (Town) o;
        return x == town.x &&
                y == town.y;
    }


    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}

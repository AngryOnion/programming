package ee.ttu.algoritmid.fibonacci;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class AL01B {

    private long MILLISECONDS_IN_YEAR = 3153600000L;

    public static void main(String[] args) {
        AL01B al01B = new AL01B();
        System.out.println(al01B.timeToComputeRecursiveFibonacci(69));
//        System.out.println(al01B.recursiveF(30).multiply(BigInteger.valueOf(3)).subtract(BigInteger.valueOf(2)));
//        BigDecimal one = BigDecimal.valueOf(1.6180339887).pow(31);
//        System.out.println(one);
//        BigDecimal two = BigDecimal.valueOf(1.6180339887).pow(30);
//        System.out.println(two);
//        System.out.println(one.divide(two));
    }
    /**
     * Estimate or find the exact time required to compute the n-th Fibonacci number.
     * @param n The n-th number to compute.
     * @return The time estimate or exact time in YEARS.
     */
    public String timeToComputeRecursiveFibonacci(int n) {
        long start = System.currentTimeMillis();
        recursiveF(35);
        long end = System.currentTimeMillis();
        long time = end - start;
//        System.out.println(time);
        BigDecimal timeForRow = BigDecimal.valueOf(time).divide(getRows(35), 15, RoundingMode.HALF_UP);
//        BigDecimal timeForRow = BigDecimal.valueOf(time).divide(BigDecimal.valueOf(30), 15, RoundingMode.FLOOR);
        BigDecimal rows = getRows(n);
        return rows.multiply(timeForRow).divide(BigDecimal.valueOf(MILLISECONDS_IN_YEAR), 15, RoundingMode.HALF_EVEN).toString();
//        return String.valueOf(timeGoldenRatio.divide(BigDecimal.valueOf(MILLISECONDS_IN_YEAR), 15, BigDecimal.ROUND_HALF_UP));
//        return Integer.toString(calendar.get(Calendar.YEAR));
    }

    /**
     * Compute the Fibonacci sequence number recursively.
     * (You need this in the timeToComputeRecursiveFibonacci(int n) function!)
     * @param n The n-th number to compute.
     * @return The n-th Fibonacci number as a string.
     */
    public BigInteger recursiveF(int n) {
        if (n <= 1)
            return BigInteger.valueOf(n);
        return recursiveF(n - 1).add(recursiveF(n - 2));
    }

    public BigInteger iterativeF(int n) {
        if (n < 1) {
            return BigInteger.ZERO;
        }
        if (n == 1 || n == 2) return BigInteger.ONE;
        BigInteger number[] = new BigInteger[n+1];
        number[1] = number[2] = BigInteger.valueOf(1);
        for (int i = 3; i <= n; i++) {
            number[i] = number[i - 1].add(number[i - 2]);
        }
        return number[n];
    }

    private BigDecimal getRows(int n) {
        return BigDecimal.valueOf(iterativeF(n).longValue()).multiply(BigDecimal.valueOf(3)).subtract(BigDecimal.valueOf(2));
    }
}



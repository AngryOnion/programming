package ee.ttu.algoritmid.stamps;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        System.out.println(Stamps.findStamps(13, Arrays.asList(7,3,2,6)));
        System.out.println(Stamps.findStamps(100, Arrays.asList(10, 24, 30, 33, 36)));
    }
}

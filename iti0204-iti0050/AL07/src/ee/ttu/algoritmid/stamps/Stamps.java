package ee.ttu.algoritmid.stamps;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Stamps {

    public static List<Integer> findStamps(int sum, List<Integer> stampOptions) throws IllegalArgumentException {
//        if (sum == 0) throw new IllegalArgumentException();
        if (stampOptions.isEmpty()) throw new IllegalArgumentException();
        int[] minArray = new int[sum + 1];
        int[] result = new int[sum+1];

        Arrays.fill(minArray, Integer.MAX_VALUE - 1);
        minArray[0] = 0;

        Arrays.fill(result, -1);
        List<Integer> usedStamps = new ArrayList<>();
        for (int j = 0; j < stampOptions.size(); j++) {
            for (int i = 0; i <= sum; i++) {
                Integer stamp = stampOptions.get(j);
                if (i >= stamp) {
                    if (minArray[i - stamp] + 1 < minArray[i]) {
                        minArray[i] = 1 + minArray[i - stamp];
                        result[i] = j;
                    }
                }
            }
        }

        if (result[sum] == -1) {
            return new ArrayList<>();
        }
        int start = sum;
        while (start != 0) {
            Integer stamp = stampOptions.get(result[start]);
            usedStamps.add(stamp);
            start = start - stamp;
        }
        return usedStamps;
    }
}
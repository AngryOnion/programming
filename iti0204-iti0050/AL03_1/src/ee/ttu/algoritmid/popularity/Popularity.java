package ee.ttu.algoritmid.popularity;

import java.util.HashMap;

public class Popularity {

    private HashMap<String, Integer> points;
    private int max = 0;

    public Popularity(int maxCoordinates) {
        points = new HashMap<>(maxCoordinates + 1);
    }
    /**
     * @param x, y - coordinates
     */
    void addPoint(Integer x, Integer y) {
        String point = x + " " + y;
        if (points.containsKey(point)) {
            int popularity = points.get(point) + 1;
            points.put(point, popularity);
            if (popularity > max) {
                max = popularity;
            }
        } else {
            points.put(point, 1);
            if (max < 1) {
                max = 1;
            }
        }
    }

    /**
     * @param x, y - coordinates
     * @return the number of occurrennces of the point
     */

    int pointPopularity(Integer x, Integer y) {
        return points.getOrDefault(x + " " + y, 0);
    }


    /**
     * @return the number of occurrennces of the most popular point
     */

    int maxPopularity() {
        return max;
    }
}

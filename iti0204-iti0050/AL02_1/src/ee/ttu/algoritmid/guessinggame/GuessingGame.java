package ee.ttu.algoritmid.guessinggame;

import java.lang.reflect.Field;
import java.util.Arrays;

public class GuessingGame {

    Oracle oracle;
    private Boolean heavier = null;

    public GuessingGame(Oracle oracle) {
        this.oracle = oracle;
    }

    /**
     * @param fruitArray - All the possible fruits.
     * @return the name of the fruit.
     */
    public String play(Fruit[] fruitArray)  {
        Arrays.sort(fruitArray, new FruitComparator());
        return tryToGuess(fruitArray);
    }

    private String tryToGuess(Fruit[] fruitArray) {
        if (fruitArray.length < 3 && heavier != null) {
            return heavier ? fruitArray[1].getName() : fruitArray[0].getName();
        }
        String status = oracle.isIt(fruitArray[fruitArray.length/2+1]);
        if (status.equals(Constants.CORRECT)) {
            return fruitArray[fruitArray.length/2+1].getName();
        } else if (status.equals(Constants.HEAVIER)) {
            heavier = true;
            Fruit[] splitArray = Arrays.copyOfRange(fruitArray, fruitArray.length/2+1, fruitArray.length);
            return tryToGuess(splitArray);
        } else {
            heavier = false;
            Fruit[] splitArray = Arrays.copyOfRange(fruitArray, 0, fruitArray.length/2+1);
            return tryToGuess(splitArray);
        }
    }

    private boolean guessOnce(Fruit fruit) {
       return oracle.isIt(fruit).equals(Constants.CORRECT);
    }
}


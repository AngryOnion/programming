package ee.ttu.algoritmid.guessinggame;

public class Constants {
    public static final String HEAVIER = "heavier";
    public static final String LIGHTER = "lighter";
    public static final String CORRECT = "correct!";

}

package ee.ttu.algoritmid.dancers;

public class DancerNode {

    private DancerNode left = null;
    private DancerNode right = null;
    private DancerNode parent = null;
    private String color = "";
    private Dancer key;

    public static final String BLACK = "Black";
    public static final String RED = "Red";

    public DancerNode(Dancer key) {
        color = BLACK;
        this.key = key;
    }

    public DancerNode getLeft() {
        return left;
    }

    public void setLeft(DancerNode left) {
        if (left != null) left.setParent(this);
        this.left = left;
    }

    public DancerNode getRight() {
        return right;
    }

    public void setRight(DancerNode right) {
        if (right != null) right.setParent(this);
        this.right = right;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public DancerNode getParent() {
        return parent;
    }

    public void setParent(DancerNode parent) {
        this.parent = parent;
    }

    public boolean isRed() {
        return color.equals(RED);
    }

    public boolean isBlack() {
        return color.equals(BLACK);
    }

    public Dancer getKey() {
        return key;
    }

    public void setKey(Dancer key) {
        this.key = key;
    }

    public DancerNode getGrandParent() {
        return parent != null ? parent.getParent() : null;
    }

    public DancerNode getSibling() {
        if (parent == null) {
            return null;
        }
        if (isOnLeft()) {
            return parent.getRight();
        }
        return parent.getLeft();
    }

    public DancerNode getUncle() {
        if (parent == null || parent.getParent() == null) {
            return null;
        }
        if (parent.isOnLeft()) {
            return parent.getParent().getRight();
        } else {
            return parent.getParent().getLeft();
        }
    }

    public void moveDown(DancerNode changable) {
        if (parent != null) {
            if (isOnLeft()) {
                parent.setLeft(changable);
            } else {
                parent.setRight(changable);
            }
        }
        changable.setParent(parent);
        parent = changable;
    }

    public void rotateLeft() {
        DancerNode initialRight = this.getRight();
        DancerNode initialParent = this.getParent();
        if (initialRight != null) {
            this.setRight(initialRight.getLeft());
            initialRight.setLeft(this);
            this.setParent(initialRight);
            if (this.getRight() != null) {
                this.getRight().setParent(this);
            }
            //this can be root
            commonRotation(initialRight, initialParent);
        }
        initialRight.setParent(initialParent);
    }

    private void commonRotation(DancerNode initialRight, DancerNode initialParent) {
        if (initialParent != null) {
            if (this.equals(initialParent.getLeft())) {
                initialParent.setLeft(initialRight);
            } else if (this.equals(initialParent.getRight())) {
                initialParent.setRight(initialRight);
            }
        }
    }

    public void rotateRight() {
        DancerNode initialLeft = this.getLeft();
        DancerNode initialParent = this.getParent();
        if (initialLeft != null) {
            this.setLeft(initialLeft.getRight());
            initialLeft.setRight(this);
            this.setParent(initialLeft);
            if (this.getLeft() != null) {
                this.getLeft().setParent(this);
            }
            //this can be root
            commonRotation(initialLeft, initialParent);
        }
        initialLeft.setParent(initialParent);
    }

    public boolean isOnLeft() {
        if (parent ==null) return false;
        return this == parent.getLeft();
    }

    public boolean hasRedChild() {
        return  (left !=null && left.isRed())
            || (right != null && right.isRed());
    }

    public Integer getHeight() {
        return this.key.getHeight();
    }

    public static DancerNode createLeaf(DancerNode parent) {
        DancerNode dancerNode = new DancerNode(null);
        dancerNode.setParent(parent);
        return dancerNode;
    }

    @Override
    public String toString() {
        return String.format("Key: %s, left: %s, right %s",getHeight(),
                left != null ? left.getHeight() : "",
                right != null ? right.getHeight() : "");
    }
}

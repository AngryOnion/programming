package ee.ttu.algoritmid.dancers;

import ee.ttu.algoritmid.dancers.trees.FemaleTree;
import ee.ttu.algoritmid.dancers.trees.MaleTree;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;

public class HW01 implements Dancers {

    private FemaleTree femaleTree = new FemaleTree();
    private MaleTree maleTree = new MaleTree();

    //<Male, Female>
    @Override
    public SimpleEntry<Dancer, Dancer> findPartnerFor(Dancer candidate) throws IllegalArgumentException {
        if (candidate == null) throw new IllegalArgumentException();
        if (candidate.getGender().equals(Dancer.Gender.MALE)) {
            return femaleTree.findDancer(candidate, maleTree);
        } else {
            return maleTree.findPairToMale(candidate, femaleTree);
        }
    }

    @Override
    public List<Dancer> returnWaitingList() {
        ArrayList<Dancer> dancers = (ArrayList<Dancer>) maleTree.getMales().clone();
        dancers.addAll(femaleTree.getFemales());
        dancers.sort((o1, o2) -> {
            if (o1.getHeight() == o2.getHeight()) {
                return o1.getGender().equals(Dancer.Gender.FEMALE) ? -1 : 1;
            }
            return o1.getHeight() - o2.getHeight();
        });
        return dancers;
    }
}
package ee.ttu.algoritmid.dancers.trees;

import ee.ttu.algoritmid.dancers.Dancer;
import ee.ttu.algoritmid.dancers.DancerNode;

/**
 * Not racist implementation of red black tree
 */
public class NativeAfroAmericanTree {

    DancerNode root = null;


    private void repairTreeAfterInsertion(DancerNode node) {
        if (node.getParent() == null) {
            insertCase1(node);
        } else if (node.getParent().isBlack()) {
            insertCase2(node);
        } else if (node.getUncle() != null && node.getUncle().isRed()) {
            insertCase3(node);
        } else {
            insertCase4(node);
        }
    }

    private void insertCase1(DancerNode node) {
        if (node.getParent() == null) {
            node.setColor(DancerNode.BLACK);
        }
    }

    private void insertCase2(DancerNode node) {
        return;
    }

    private void insertCase3(DancerNode node) {
        node.getParent().setColor(DancerNode.BLACK);
        node.getUncle().setColor(DancerNode.BLACK);
        node.getGrandParent().setColor(DancerNode.RED);
        repairTreeAfterInsertion(node.getGrandParent());
    }

    private void insertCase4(DancerNode node) {
        DancerNode parent = node.getParent();
        DancerNode grandDad = parent.getParent();
        if (grandDad != null) {
            if (grandDad.getLeft() != null) {
                if (node.equals(grandDad.getLeft().getRight())) {
//                    rotateLeft(parent);
                    parent.rotateLeft();
                    node = node.getLeft();
                }
            } else if (grandDad.getRight() != null) {
                if (node.equals(grandDad.getRight().getLeft())) {
//                    rotateRight(parent);
                    parent.rotateRight();
                    node = node.getRight();
                }
            }
        }


        insertCase4additional(node);
    }

    private void insertCase4additional(DancerNode node) {
        if (node != null) {
            DancerNode parent = node.getParent();
            DancerNode grandDad = parent.getParent();
            if (node.equals(parent.getLeft())) {
//                rotateRight(grandDad);
                grandDad.rotateRight();
            } else {
//                rotateLeft(grandDad);
                grandDad.rotateLeft();
            }
            parent.setColor(DancerNode.BLACK);
            grandDad.setColor(DancerNode.RED);
        }
    }


    //          bool uvBlack = ((u == NULL or u->color == BLACK) and (v->color == BLACK));
    public void deleteNode(DancerNode v) {
        DancerNode replacement = getSuccessor(v);
        boolean bothBlack = (replacement == null || replacement.isBlack()) && v.isBlack();
        DancerNode parent = v.getParent();

        if (replacement == null) {
            if (v.equals(root)) {
                root = null;
            } else {
                if (bothBlack) {
                    fixDoubleBlack(v);
                } else {
                    if (v.getSibling() != null) {
                        v.getSibling().setColor(DancerNode.RED);
                    }

                }
                if (parent != null) {
                    if (v.isOnLeft()) {
                        parent.setLeft(null);
                    } else {
                        parent.setRight(null);
                    }
                }
            }
            return;
        }

        if (v.getLeft() == null || v.getRight() == null) {
            if (v.equals(root)) {
                v.setKey(replacement.getKey());
                v.setLeft(null);
                v.setRight(null);
            } else {
                if (v.isOnLeft()) {
                    parent.setLeft(replacement);
                } else {
                    parent.setRight(replacement);
                }
                replacement.setParent(parent);
                if (bothBlack) {
                    fixDoubleBlack(v);
                } else {
                    replacement.setColor(DancerNode.BLACK);
                }
            }
            return;
        }
        swapValues(replacement, v);
        deleteNode(replacement);
    }

    private void fixDoubleBlack(DancerNode node) {
        if (node.equals(root)) {
            return;
        }
        DancerNode sibling = node.getSibling();
        DancerNode parent = node.getParent();
//      Double black is in upper level
        if (sibling == null) {
            fixDoubleBlack(parent);
        } else {
            if (sibling.isRed()) {
                parent.setColor(DancerNode.RED);
                sibling.setColor(DancerNode.BLACK);
                if (sibling.isOnLeft()) {
                    rotateRight(parent);
                } else {
                    rotateLeft(parent);
                }
                fixDoubleBlack(node);
            } else {
                if (sibling.hasRedChild()) {
                    if (sibling.getLeft() != null && sibling.getLeft().isRed()) {
                        if (sibling.isOnLeft()) {
                            sibling.getLeft().setColor(sibling.getColor());
                            sibling.setColor(parent.getColor());
                            rotateRight(parent);
                        } else {
                            sibling.getLeft().setColor(parent.getColor());
                            rotateRight(sibling);
                            rotateLeft(parent);
                        }
                    } else {
                        if (sibling.isOnLeft()) {
                            sibling.getRight().setColor(parent.getColor());
                            rotateLeft(sibling);
                            rotateRight(parent);
                        } else {
                            sibling.getRight().setColor(sibling.getColor());
                            sibling.setColor(parent.getColor());
                            rotateLeft(parent);
                        }
                    }
                    parent.setColor(DancerNode.BLACK);
                } else {
                    sibling.setColor(DancerNode.RED);
                    if (parent.isBlack()) {
                        fixDoubleBlack(parent);
                    } else {
                        parent.setColor(DancerNode.BLACK);
                    }
                }
            }
        }
    }

    private void rotateLeft(DancerNode node) {
        //new parent will be node right child
        DancerNode newParent = node.getRight();
        if (node.equals(root)) {
            root = newParent;
        }
        node.moveDown(newParent);
        node.setRight(newParent.getLeft());
        if (newParent.getLeft() != null) {
            newParent.getLeft().setParent(node);
        }
        newParent.setLeft(node);
    }

    private void rotateRight(DancerNode node) {
        //new parent will be node right child
        DancerNode newParent = node.getLeft();
        if (node.equals(root)) {
            root = newParent;
        }
        node.moveDown(newParent);
        node.setLeft(newParent.getRight());
        if (newParent.getRight() != null) {
            newParent.getRight().setParent(node);
        }
        newParent.setRight(node);
    }

    private void swapValues(DancerNode n1, DancerNode n2) {
        Dancer temp = n1.getKey();
        n1.setKey(n2.getKey());
        n2.setKey(temp);
    }

    private void swapColors(DancerNode n1, DancerNode n2) {
        String temp = n1.getColor();
        n1.setColor(n2.getColor());
        n2.setColor(temp);
    }

    public DancerNode getRoot() {
        return root;
    }

    public void newInsert(Dancer dancer) {
        DancerNode node = new DancerNode(dancer);
        if (root == null) {
            node.setColor(DancerNode.BLACK);
            root = node;
        } else {
            DancerNode temp = search(node.getHeight());
            if (node.getHeight() < temp.getHeight()) {
                temp.setLeft(node);
            } else {
                temp.setRight(node);
            }
            fixDoubleRed(node);
        }
    }

    private void fixDoubleRed(DancerNode node) {
        if (node.equals(root)) {
            node.setColor(DancerNode.BLACK);
            return;
        }

        DancerNode parent = node.getParent();
        DancerNode grandParent = node.getParent().getParent();
        DancerNode uncle = node.getUncle();

        if (parent.isRed()) {
            if (uncle != null && uncle.isRed()) {
                parent.setColor(DancerNode.BLACK);
                uncle.setColor(DancerNode.BLACK);
                grandParent.setColor(DancerNode.RED);
                fixDoubleRed(grandParent);
            } else {
                if (parent.isOnLeft()) {
                    if (node.isOnLeft()) {
                        swapColors(parent, grandParent);
                    } else {
                        rotateLeft(parent);
                        swapColors(node, grandParent);
                    }
                    rotateRight(grandParent);
                } else {
                    if (node.isOnLeft()) {
                        rotateRight(parent);
                        swapColors(node, grandParent);
                    } else {
                        swapColors(parent, grandParent);
                    }
                    rotateLeft(grandParent);
                }
            }
        }
    }

    private DancerNode search(Integer height) {
        DancerNode temp = root;
        while (temp != null) {
            if (height < temp.getHeight()) {
                if (temp.getLeft() == null) {
                    break;
                } else {
                    temp = temp.getLeft();
                }
            } else {
                if (temp.getRight() == null) {
                    break;
                } else {
                    temp = temp.getRight();
                }
            }
        }
        return temp;
    }


    private DancerNode getSuccessor(DancerNode node) {
        if (node.getLeft() != null && node.getRight() != null) {
            return getMostLeftWithoutChild(node.getRight());
        }
        if (node.getLeft() == null && node.getRight() == null) {
            return null;
        }
        if (node.getLeft() != null) {
            return node.getLeft();
        } else {
            return node.getRight();
        }
    }

    private DancerNode getMostLeftWithoutChild(DancerNode node) {
        while (true) {
            if (node.getLeft() == null) {
                return node;
            }
            node = node.getLeft();
        }
    }

    public DancerNode findPair(int height) {
        DancerNode node = root;
        while (true) {
            Integer nodeHeight = node.getHeight();
            if (nodeHeight == height) return node;
            if (height > nodeHeight) {
                if (node.getRight() != null) {
                    node = node.getRight();
                } else {
                    return null;
                }
            } else {
                if (node.getLeft() != null) {
                    node = node.getLeft();
                } else {
                    return null;
                }
            }
        }
    }
}


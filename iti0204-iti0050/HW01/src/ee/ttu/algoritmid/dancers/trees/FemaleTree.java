package ee.ttu.algoritmid.dancers.trees;

import ee.ttu.algoritmid.dancers.Dancer;
import ee.ttu.algoritmid.dancers.DancerNode;

import java.util.AbstractMap;
import java.util.ArrayList;

public class FemaleTree extends NativeAfroAmericanTree {

    private ArrayList<Dancer> females = new ArrayList<>();

    public AbstractMap.SimpleEntry<Dancer, Dancer> findDancer(Dancer dancer, MaleTree maleTree) {
        DancerNode node = root;
        if (node == null) {
            maleTree.newInsert(dancer);
            maleTree.getMales().add(dancer);
            return null;
        }
        DancerNode pair = null;
        for (int i = 5; i <= 9; i++) {
            pair = findPair(dancer.getHeight() - i);
            if (pair != null) {
                Dancer female = pair.getKey();
                deleteNode(pair);
                females.remove(female);
                return new AbstractMap.SimpleEntry<>(dancer, female);
            }
        }
        maleTree.newInsert(dancer);
        maleTree.getMales().add(dancer);
        return null;

    }



    public ArrayList<Dancer> getFemales() {
        return females;
    }
}

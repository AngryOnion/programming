package ee.ttu.algoritmid.dancers.trees;

import ee.ttu.algoritmid.dancers.Dancer;
import ee.ttu.algoritmid.dancers.DancerNode;

import java.util.AbstractMap;
import java.util.ArrayList;

public class MaleTree extends NativeAfroAmericanTree {

    private ArrayList<Dancer> males = new ArrayList<>();

    public AbstractMap.SimpleEntry<Dancer, Dancer> findPairToMale(Dancer dancer, FemaleTree femaleTree) {
        DancerNode node = root;
        if (node == null) {
            femaleTree.newInsert(dancer);
            femaleTree.getFemales().add(dancer);
            return null;
        }
        DancerNode pair = null;
        for (int i = 5; i <= 9; i++) {
            pair = findPair(dancer.getHeight() + i);
            if (pair != null) {
                Dancer male = pair.getKey();
                deleteNode(pair);
                males.remove(male);
                return new AbstractMap.SimpleEntry<>(male, dancer);
            }
        }
        femaleTree.newInsert(dancer);
        femaleTree.getFemales().add(dancer);
        return null;

    }

    public ArrayList<Dancer> getMales() {
        return males;
    }
}

package ee.ttu.algoritmid.dancers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Searcher {
    private List<Dancer> dancers;

    public Searcher() {
        dancers = new ArrayList<>();
    }

    void addDancer(Dancer dancer) {
        dancers.add(dancer);
        dancers.sort(Comparator.comparingInt(Dancer::getHeight));
    }

    Dancer getPair(Dancer dancer) {
        if (dancers.isEmpty()) {
            dancers.add(dancer);
            return null;
        }
        if (dancer.getGender().equals(Dancer.Gender.MALE)) {
            for (Dancer candidate: dancers) {

            }
        } else {

        }

        dancers.add(dancer);
        return null;
    }
}

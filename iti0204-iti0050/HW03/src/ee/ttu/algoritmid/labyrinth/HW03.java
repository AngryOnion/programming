package ee.ttu.algoritmid.labyrinth;

import ee.ttu.algoritmid.labyrinth.solvers.PathFinder;
import ee.ttu.algoritmid.labyrinth.solvers.Visitor;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class HW03 {
    private MazeRunner mazeRunner;

    public HW03(String fileName) throws IOException, URISyntaxException {
        mazeRunner = new MazeRunner(fileName);
    }

    public MazeRunner getMazeRunner() {
        return mazeRunner;
    }

    /**
     * Returns the list of steps to take to get from beginning ("B") to
     * the treasure ("T").
     *
     * @return return the steps taken as a list of strings (e.g., ["E", "E", "E"])
     * return null if there is no path (there is no way to get to the treasure).
     */
    public List<String> solve() {
        final long start = System.currentTimeMillis();
        Visitor visitor = new Visitor(mazeRunner);
        if (visitor.getTreasure() == null) {
            return null;
        }
        System.out.println("=======MAP===============");
        System.out.println(visitor.mapToString(visitor.getMap()));
        PathFinder pathFinder = new PathFinder(visitor);
        final long stop = System.currentTimeMillis();
        List<String> path = pathFinder.findPath();
        System.out.println("=======PATH==============");
        System.out.println(path);
        System.out.println("=======TIME(MILLIS)======");
        System.out.println(stop - start);
        return path;
    }
}

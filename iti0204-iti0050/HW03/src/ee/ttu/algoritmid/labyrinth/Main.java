package ee.ttu.algoritmid.labyrinth;

import java.io.IOException;
import java.net.URISyntaxException;

public class Main {
    public static void main(String[] args) throws IOException, URISyntaxException {
        HW03 hw03 = new HW03("maze.txt");
        hw03.solve();
    }
}

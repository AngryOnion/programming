package ee.ttu.algoritmid.labyrinth.pojos;

public enum Direction {
    NORTH("N"), SOUTH("S"), EAST("E"), WEST("W");

    private final String directionLetter;

    Direction(String directionLetter) {
        this.directionLetter = directionLetter;
    }

    public String getDirectionLetter() {
        return directionLetter;
    }

    public Direction getOtherwise() {
       switch (this) {
           case EAST:
               return WEST;
           case WEST:
               return EAST;
           case NORTH:
               return SOUTH;
           case SOUTH:
               return NORTH;
       }
       return NORTH;
    }
}

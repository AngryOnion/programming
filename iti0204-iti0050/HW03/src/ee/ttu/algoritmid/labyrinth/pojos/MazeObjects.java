package ee.ttu.algoritmid.labyrinth.pojos;

public enum MazeObjects {
    TREASURE(-2), WALL(-1), START(0), UNKNOWN(-5), VISITED(-6);

    private final int value;

    MazeObjects(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}

package ee.ttu.algoritmid.labyrinth.pojos;

import java.util.AbstractMap.SimpleEntry;
import java.util.List;

public class Intersection {

    private SimpleEntry<Integer, Integer> location;
    private List<Direction> wayBack;

    public Intersection(SimpleEntry<Integer, Integer> location, List<Direction> wayBack) {
        this.location = location;
        this.wayBack = wayBack;
    }

    public List<Direction> getWayBack() {
        return wayBack;
    }

    public void setWayBack(List<Direction> wayBack) {
        this.wayBack = wayBack;
    }

    public SimpleEntry<Integer, Integer> getLocation() {
        return location;
    }
}

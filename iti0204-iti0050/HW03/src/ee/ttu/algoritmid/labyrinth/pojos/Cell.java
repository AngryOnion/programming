package ee.ttu.algoritmid.labyrinth.pojos;

import java.util.AbstractMap.SimpleEntry;
import java.util.List;

public class Cell {

    private int cost;
    private SimpleEntry<Integer, Integer> coordinates;
    private List<Cell> adjacentCells;

    public Cell(int cost, SimpleEntry<Integer, Integer> coordinates) {
        this.cost = cost;
        this.coordinates = coordinates;
    }

    public List<Cell> getAdjacentCells() {
        return adjacentCells;
    }

    public void setAdjacentCells(List<Cell> adjacentCells) {
        this.adjacentCells = adjacentCells;
    }

    public int getCost() {
        return cost;
    }

    public SimpleEntry<Integer, Integer> getCoordinates() {
        return coordinates;
    }

}

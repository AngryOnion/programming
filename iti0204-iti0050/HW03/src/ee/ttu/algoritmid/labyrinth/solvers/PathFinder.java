package ee.ttu.algoritmid.labyrinth.solvers;

import ee.ttu.algoritmid.labyrinth.helper.Helper;
import ee.ttu.algoritmid.labyrinth.pojos.Cell;

import java.util.AbstractMap.SimpleEntry;
import java.util.*;

public class PathFinder {

    private List<List<Integer>> map;

    private Cell startPoint;

    private Cell treasure;

    private Map<SimpleEntry<Integer, Integer>, Cell> allCells;

    private Visitor visitor;

    private Set<Cell> unsettledCells = new HashSet<>();
    private Map<Cell, Cell> predecessors;
    private Map<Cell, Integer> distance;

    public PathFinder(Visitor visitor) {
        this.visitor = visitor;
        this.map = visitor.getMap();
        this.allCells = visitor.getAllCells();
    }

    public List<String> findPath() {
        allCells.values().forEach(cell -> cell.setAdjacentCells(findAllAdjacentCells(cell.getCoordinates())));
        startPoint = allCells.get(visitor.getStartPoint());
        treasure = allCells.get(visitor.getTreasure());
        dijkstra();

        return createDirectionsStringList();
    }

    private void dijkstra() {

        unsettledCells = new HashSet<>();
        distance = new HashMap<>();
        predecessors = new HashMap<>();

        distance.put(startPoint, 0);
        unsettledCells.add(startPoint);

        while (unsettledCells.size() > 0) {
            Cell cell = getMinimum(unsettledCells);
            unsettledCells.remove(cell);
            findMinimalDistances(cell);
        }
    }

    private void findMinimalDistances(Cell cell) {
        List<Cell> adjacentNodes = cell.getAdjacentCells();
        for (Cell target : adjacentNodes) {
            int cost = target.getCost();
            //fix for treasure having -2 cost
            if (cost < 0) cost = 0;
            if (getShortestDistance(target) > getShortestDistance(cell) + cost) {
                distance.put(target, getShortestDistance(cell) + cost);
                predecessors.put(target, cell);
                unsettledCells.add(target);
            }
        }

    }

    private Cell getMinimum(Set<Cell> unsettledCells) {
        Cell min = null;
        for (Cell unsettledCell : unsettledCells) {
            if (min == null) {
                min = unsettledCell;
            } else {
                if (getShortestDistance(unsettledCell) < getShortestDistance(min)) {
                    min = unsettledCell;
                }
            }
        }
        return min;
    }

    private int getShortestDistance(Cell destination) {
        Integer distance = this.distance.get(destination);
        if (distance == null) {
            return Integer.MAX_VALUE;
        } else {
            return distance;
        }
    }

    private LinkedList<Cell> getPath() {
        LinkedList<Cell> path = new LinkedList<>();
        Cell step = treasure;
        // check if a path to the treasure exists
        if (predecessors.get(step) == null) {
            return null;
        }
        path.add(step);
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }
        // Reverse it to get right order
        Collections.reverse(path);
        return path;
    }


    private List<Cell> findAllAdjacentCells(SimpleEntry<Integer, Integer> pos) {
        List<Cell> adjacentCells = new ArrayList<>();
        int x = pos.getKey();
        int y = pos.getValue();
        if (Helper.checkNorth(x, y, map)) {
            adjacentCells.add(findCell(x, y - 1));
        }
        if (Helper.checkEast(x, y, map)) {
            adjacentCells.add(findCell(x + 1, y));
        }
        if (Helper.checkSouth(x, y, map)) {
            adjacentCells.add(findCell(x, y + 1));
        }
        if (Helper.checkWest(x, y, map)) {
            adjacentCells.add(findCell(x - 1, y));
        }
        return adjacentCells;
    }

    private Cell findCell(int x, int y) {
        return allCells.get(new SimpleEntry<>(x, y));
    }

    private List<String> createDirectionsStringList() {
        List<Cell> path = getPath();
        List<String> result = new ArrayList<>();

        if (path != null) {

            for (int i = 0; i < path.size(); i++) {
                if (i + 1 < path.size()) {
                    result.add(Helper.getDirection(path.get(i), path.get(i+1)).getDirectionLetter());
                }
            }
        } else {
            return null;
        }
        return result;
    }

}

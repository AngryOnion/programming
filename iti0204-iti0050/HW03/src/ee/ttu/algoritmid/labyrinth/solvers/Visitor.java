package ee.ttu.algoritmid.labyrinth.solvers;

import ee.ttu.algoritmid.labyrinth.MazeRunner;
import ee.ttu.algoritmid.labyrinth.helper.Helper;
import ee.ttu.algoritmid.labyrinth.pojos.*;

import java.util.*;
import java.util.AbstractMap.SimpleEntry;

public class Visitor {

    private final MazeRunner mazeRunner;

    private List<List<Integer>> map;

    private List<List<Integer>> visitedMap;

    private SimpleEntry<Integer, Integer> startPoint;

    private SimpleEntry<Integer, Integer> treasure = null;

    private HashMap<SimpleEntry<Integer, Integer>, Cell> allCells = new HashMap<>();

    private List<Direction> wayBack = new ArrayList<>();

    private List<Intersection> intersections = new ArrayList<>();


    public Visitor(MazeRunner mazeRunner) {
        this.mazeRunner = mazeRunner;
        startPoint = mazeRunner.getPosition();
        allCells.put(startPoint, new Cell(0, startPoint));
        map = createEmptyMap(MazeObjects.UNKNOWN);
        updateMap();
        visitedMap = createEmptyMap(MazeObjects.UNKNOWN);
        visitedMap.get(startPoint.getValue()).set(startPoint.getKey(), MazeObjects.VISITED.getValue());
        start();
    }

    /**
     * Use depth search to visit all cells in maze, if intersection with more than 2 available not visited ways present
     * save it and go back when no more available unvisited ways present
     */
    private void start() {
        List<Direction> directions = searchAvailableNotVisitedTurns(mazeRunner.getPosition());
        Random rand = new Random();
        while (directions.size() > 0) {
            Direction direction = directions.get(rand.nextInt(directions.size()));
            findIntersections(directions);
            wayBack.add(direction.getOtherwise());
            move(direction);
            directions = searchAvailableNotVisitedTurns(mazeRunner.getPosition());
            if (directions.size() == 0) {
                directions = successor(directions);
                if (directions == null) return;
            }

        }

    }

    /**
     * If no unvisited cells present, go back to last intersection or stop if no intersections present
     *
     * @param directions current directions
     * @return directions available
     */
    private List<Direction> successor(List<Direction> directions) {
        while (directions.size() == 0 && intersections.size() > 0) {
            if (wayBack.isEmpty()) {
                Intersection intersection = intersections.get(intersections.size() - 1);
                if (intersection.getLocation().equals(mazeRunner.getPosition())) {
                    intersections.remove(intersection);
                }
                if (intersections.size() == 0) return null;
                intersection = intersections.get(intersections.size() - 1);
                if (searchAvailableNotVisitedTurns(intersection.getLocation()).size() == 0) {
                    intersections.remove(intersection);
                }
                wayBack = intersection.getWayBack();
            }
            goBack(wayBack);
            wayBack.clear();
            directions = searchAvailableNotVisitedTurns(mazeRunner.getPosition());
        }
        return directions;
    }

    private void findIntersections(List<Direction> directions) {
        if (directions.size() > 1) {
            if (intersections.size() > 0) {
                intersections.get(intersections.size() - 1).setWayBack(new ArrayList<>(wayBack));
            }
            intersections.add(new Intersection(mazeRunner.getPosition(), new ArrayList<>()));
            wayBack.clear();
        }
    }

    private void goBack(List<Direction> directions) {
        Collections.reverse(directions);
        for (Direction direction : directions) {
            mazeRunner.move(direction.getDirectionLetter());
        }
    }

    private List<Direction> searchAvailableNotVisitedTurns(SimpleEntry<Integer, Integer> pos) {
        List<Direction> directions = new ArrayList<>();
        int x = pos.getKey();
        int y = pos.getValue();
        if (Helper.checkNorth(x, y, this.map)
                && visitedMap.get(y - 1).get(x) != MazeObjects.VISITED.getValue()) {
            directions.add(Direction.NORTH);
        }
        if (Helper.checkEast(x, y, this.map)
                && visitedMap.get(y).get(x + 1) != MazeObjects.VISITED.getValue()) {
            directions.add(Direction.EAST);
        }
        if (Helper.checkSouth(x, y, this.map)
                && visitedMap.get(y + 1).get(x) != MazeObjects.VISITED.getValue()) {
            directions.add(Direction.SOUTH);
        }
        if (Helper.checkWest(x, y, this.map)
                && visitedMap.get(y).get(x - 1) != MazeObjects.VISITED.getValue()) {
            directions.add(Direction.WEST);
        }
        return directions;
    }


    private List<List<Integer>> createEmptyMap(MazeObjects filler) {
        List<List<Integer>> map = new ArrayList<>();
        SimpleEntry<Integer, Integer> size = mazeRunner.getSize();
        for (int x = 0; x < size.getKey(); x++) {
            List<Integer> newRow = new ArrayList<>();
            for (int y = 0; y < size.getValue(); y++) {
                newRow.add(filler.getValue());
            }
            map.add(x, newRow);
        }
        return map;
    }

    private void move(Direction direction) {
        mazeRunner.move(direction.getDirectionLetter());
        updateMap();
        final SimpleEntry<Integer, Integer> resultPosition = mazeRunner.getPosition();
        allCells.put(resultPosition, new Cell(getCost(resultPosition), resultPosition));
        visitedMap.get(resultPosition.getValue()).set(resultPosition.getKey(), MazeObjects.VISITED.getValue());
//        System.out.println("=======VISITED======");
//        System.out.println(mapToString(visitedMap));
        if (getCost(resultPosition) == MazeObjects.TREASURE.getValue()) {
            treasure = resultPosition;
        }
    }

    private Integer getCost(SimpleEntry<Integer, Integer> position) {
        return map.get(position.getValue()).get(position.getKey());
    }

    private void updateMap() {
        List<List<Integer>> square = mazeRunner.scan();
        SimpleEntry<Integer, Integer> position = mazeRunner.getPosition();
        for (int x = 0; x < square.size(); x++) {

            List<Integer> column = square.get(x);
            int xChange = Helper.squareCoordinates.get(x);

            for (int y = 0; y < column.size(); y++) {
                int yChange = Helper.squareCoordinates.get(y);

                SimpleEntry<Integer, Integer> cell = new SimpleEntry<>(position.getKey() + xChange,
                        position.getValue() + yChange);
                if (checkIfInMapBounds(cell)) {
                    map.get(cell.getValue()).set(cell.getKey(), column.get(y));
                }
            }
        }
//        System.out.println("=======MAP======");
//        System.out.println(mapToString(map));
    }

    private boolean checkIfInMapBounds(SimpleEntry<Integer, Integer> cell) {
        return cell.getValue() >= 0 && cell.getValue() < mazeRunner.getSize().getValue()
                && cell.getKey() >= 0 && cell.getKey() < mazeRunner.getSize().getKey();
    }

    public String mapToString(List<List<Integer>> map) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < map.size(); i++) {
            for (int j = 0; j < map.get(i).size(); j++) {
                Integer value = map.get(i).get(j);

                if (value == MazeObjects.WALL.getValue()) {
                    result.append("#");
                } else if (value == MazeObjects.UNKNOWN.getValue()) {
                    result.append("*");
                } else if (value == MazeObjects.TREASURE.getValue()) {
                    result.append("T");
                } else if (value == MazeObjects.VISITED.getValue()) {
                    result.append("X");
                } else if (value == 10) {
                    result.append("$");
                } else {
                    SimpleEntry<Integer, Integer> pos = mazeRunner.getPosition();
                    if (pos.getKey() == j && pos.getValue() == i) {
                        result.append("!");
                    } else {
                        result.append(value);
                    }
                }
                result.append(" ");
            }
            result.append("\n");
        }
        return result.toString();
    }

    public SimpleEntry<Integer, Integer> getTreasure() {
        return treasure;
    }

    public List<List<Integer>> getMap() {
        return map;
    }

    SimpleEntry<Integer, Integer> getStartPoint() {
        return startPoint;
    }

    HashMap<SimpleEntry<Integer, Integer>, Cell> getAllCells() {
        return allCells;
    }
}

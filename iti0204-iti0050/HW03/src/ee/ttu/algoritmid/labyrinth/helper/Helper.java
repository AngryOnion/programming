package ee.ttu.algoritmid.labyrinth.helper;

import ee.ttu.algoritmid.labyrinth.pojos.Cell;
import ee.ttu.algoritmid.labyrinth.pojos.Direction;

import java.util.HashMap;
import java.util.List;

public class Helper {

    public static final HashMap<Integer, Integer> squareCoordinates;

    static {
        squareCoordinates = new HashMap<>();
        squareCoordinates.put(0, -1);
        squareCoordinates.put(1, 0);
        squareCoordinates.put(2, 1);
    }

    public static boolean checkWest(int x, int y, List<List<Integer>> map) {
        return (x > 0) && ((map.get(y).get(x - 1) >= 0) || (map.get(y).get(x - 1) == -2));
    }

    public static boolean checkSouth(int x, int y, List<List<Integer>> map) {
        return (y < map.size() - 1) && ((map.get(y + 1).get(x) >= 0) || (map.get(y + 1).get(x) == -2));
    }

    public static boolean checkEast(int x, int y, List<List<Integer>> map) {
        return (x < map.get(y).size() - 1) && ((map.get(y).get(x + 1) >= 0) || (map.get(y).get(x + 1) == -2));
    }

    public static boolean checkNorth(int x, int y, List<List<Integer>> map) {
        return y > 0 && ((map.get(y - 1).get(x) >= 0) || (map.get(y - 1).get(x) == -2));
    }

    public static Direction getDirection(Cell source, Cell target) {
       int x = source.getCoordinates().getKey();
       int y = source.getCoordinates().getValue();
       int x2 = target.getCoordinates().getKey();
       int y2 = target.getCoordinates().getValue();
       if (x == x2 + 1) return Direction.WEST;
       else if (x == x2 - 1) return Direction.EAST;
       else if (y == y2 + 1) return Direction.NORTH;
       else return Direction.SOUTH;
    }
}

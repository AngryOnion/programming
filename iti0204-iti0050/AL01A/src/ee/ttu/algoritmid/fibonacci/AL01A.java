package ee.ttu.algoritmid.fibonacci;

import java.math.BigInteger;

public class AL01A {

    /**
     * Compute the Fibonacci sequence number.
     * @param n The number of the sequence to compute.
     * @return The n-th number in Fibonacci series.
     * asdsd
     */
    public String iterativeF(int n) {
        if (n < 1) {
            return Integer.toString(0);
        }
        if (n == 1 || n == 2) return "1";
        BigInteger number[] = new BigInteger[n+1];
        number[1] = number[2] = BigInteger.valueOf(1);
        for (int i = 3; i <= n; i++) {
            number[i] = number[i - 1].add(number[i - 2]);
        }
        return number[n].toString();
    }

    public static void main(String[] args) {
        System.out.println(new AL01A().iterativeF(5000));
    }
}
package chairs;

public class BussinesClassChair extends Chair {

    private int limit = 35;

    BussinesClassChair(String workerName) {
        super(workerName);
    }


    @Override
    public void lowerAngle() {
        if (isLimitExceeded(limit)) {
            super.changeAngle();
        }
    }
}

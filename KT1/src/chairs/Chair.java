package chairs;

import exceptions.SeatLoweringLimitExceeded;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Chair implements ChairInterface {

    private static int ANGLE_CHANGE = 7;
    private boolean heatingIsOn = false;
    private int angle = 0;
    private int limit = 28;
    private String workerName = null;
    private static List<String> possibleBusinessChairNames = Arrays.asList("Tiina", "Riina", "Miina", "Tiit");

    public static Chair createChair(String workerName) {
        if (possibleBusinessChairNames.contains(workerName)) {
            return new BussinesClassChair(workerName);
        } else if (workerName == null || workerName.equals("")) {
            return new SleepingChair();
        } else {
            return new Chair(workerName);
        }
    }


    Chair(String workerName) {
        this.workerName = workerName;
    }

    Chair(int angle) {
        this.angle = angle;
    }

    @Override
    public void turnOnHeating() {
        heatingIsOn = true;
    }

    @Override
    public void increaseAngle() {
        if (angle >= ANGLE_CHANGE) {
            angle -= ANGLE_CHANGE;
        } else {
            throw new SeatLoweringLimitExceeded("Ei saa rohkem liigutada");
        }
    }

    @Override
    public void lowerAngle() {
        if (isLimitExceeded(limit)) {
            changeAngle();
        }
    }

    @Override
    public Optional<String> getWorker() {
        return Optional.ofNullable(workerName);
    }

    @Override
    public String toString() {
        if (workerName == null) {
            return "See on tavaline tool, nurk :" + angle + " stuardess/stuard puudub";
        }
        return "See on tavaline tool, nurk :" + angle + " stuardessi/stuardi nimi: " + workerName;
    }

    @Override
    public void setUpToZeroAngle() {
        angle = 0;
    }

    boolean isLimitExceeded(int limit) {
        if (angle + ANGLE_CHANGE > limit) {
            throw new SeatLoweringLimitExceeded("Ei saa rohkem liigutada");
        } else {
            return true;
        }
    }

    void changeAngle() {
        angle += ANGLE_CHANGE;
    }

    public boolean isHeatingIsOn() {
        return heatingIsOn;
    }
}

package chairs;

import java.util.Optional;

public interface ChairInterface {

    public void turnOnHeating();
    public void increaseAngle();
    public void lowerAngle();
    public Optional<String> getWorker();
    public String toString();
    public void setUpToZeroAngle();
}

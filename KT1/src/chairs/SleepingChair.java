package chairs;


public class SleepingChair extends Chair {


    private final int limit = 42;


    SleepingChair() {
        super(42);
    }

    @Override
    public void lowerAngle() {
        if (isLimitExceeded(limit)) {
            super.changeAngle();
            super.turnOnHeating();
        }
    }
}

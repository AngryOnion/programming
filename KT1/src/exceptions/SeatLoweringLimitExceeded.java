package exceptions;

public class SeatLoweringLimitExceeded extends RuntimeException {

    public SeatLoweringLimitExceeded(String message) {
        super(message);
    }
}

import chairs.Chair;
import controllers.Controller;

public class Main {

    public static void main(String[] args) {

        Chair regularWithStuard = Chair.createChair("Masha");
        Chair sleepingChair = Chair.createChair(null);
        Chair bussinessClassChair = Chair.createChair("Tiina");

        Controller controller = new Controller();

        controller.addChair(regularWithStuard);
        controller.addChair(sleepingChair);
        controller.addChair(bussinessClassChair);

        regularWithStuard.lowerAngle();
        regularWithStuard.lowerAngle();

        sleepingChair.increaseAngle();
        sleepingChair.increaseAngle();

        bussinessClassChair.lowerAngle();
        bussinessClassChair.lowerAngle();

        controller.putAllChairsZeroDegree();
    }
}

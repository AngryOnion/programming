package controllers;

import chairs.Chair;

import java.util.ArrayList;

public class Controller {



    private ArrayList<Chair> chairs = new ArrayList<>();

    public void addChair(Chair chair) {
        chairs.add(chair);
    }

    public void putAllChairsZeroDegree() {
        if (chairs.size() > 0) {
            for (Chair chair : chairs) {
                chair.setUpToZeroAngle();
            }
        }
    }

    public int getChairsCount() {
        return chairs.size();
    }
}

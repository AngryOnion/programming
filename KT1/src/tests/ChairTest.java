package tests;

import chairs.Chair;
import exceptions.SeatLoweringLimitExceeded;
import org.junit.Test;

import static org.junit.Assert.*;

public class ChairTest {

    private Chair regularWithStuard;
    private Chair sleepingChair;
    private Chair bussinessClassChair;

    @org.junit.Before
    public void setUp() throws Exception {
        regularWithStuard = Chair.createChair("Masha");
        sleepingChair = Chair.createChair(null);
        bussinessClassChair = Chair.createChair("Tiina");
    }

    @org.junit.Test
    public void turnOnHeating() throws Exception {
        regularWithStuard.turnOnHeating();
        assertTrue(regularWithStuard.isHeatingIsOn());
    }

    @org.junit.Test(expected = SeatLoweringLimitExceeded.class)
    public void increaseAngle() throws Exception {
        regularWithStuard.increaseAngle();
    }

    @org.junit.Test(expected = SeatLoweringLimitExceeded.class)
    public void lowerAngle() throws Exception {
        sleepingChair.lowerAngle();
    }

    @org.junit.Test
    public void getWorker(){
        assertEquals("Masha", regularWithStuard.getWorker().get());
        assertEquals("Tiina", bussinessClassChair.getWorker().get());
    }

    @org.junit.Test
    public void toStringTest() throws Exception {
        System.out.println(regularWithStuard.toString());
        System.out.println(bussinessClassChair.toString());
        System.out.println(sleepingChair.toString());
    }

    @Test
    public void sleepingChairWorkerTest() {
        assertFalse(sleepingChair.getWorker().isPresent());
    }

}
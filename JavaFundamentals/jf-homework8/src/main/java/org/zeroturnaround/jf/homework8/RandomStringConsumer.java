package org.zeroturnaround.jf.homework8;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeroturnaround.jf.homework8.data.Hash;
import org.zeroturnaround.jf.homework8.data.Salt;
import org.zeroturnaround.jf.homework8.util.BramHash;

import java.util.*;

/**
 * Consumer class, will keep reading in new random words, feeding them to the hashing algorithm,
 * and checking the result for collisions with any of the given hashes.
 * The method {@link #run()} should never return, until a proper shutdown is initiated.
 */
@SuppressWarnings("unused")
public class RandomStringConsumer implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(RandomStringConsumer.class);

    /**
     * This should contain the actual hash testing + result recording logic.
     */

    @Override
    public void run() {
        boolean interrupted = false;
        try {
            while (!CollisionFinder.isReady) {
                try {
                    checkPassword(CollisionFinder.generatedStrings.take());
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                    interrupted = true;
                }
            }
        } finally {
            if (interrupted)
                Thread.currentThread().interrupt();
        }
    }

    private void checkPassword(String password) {
        Optional<Map.Entry<String, String>> optional = CollisionFinder.hashesAndSalts.entrySet().stream()
                .filter(entry -> entry.getKey().equals(BramHash.hash(entry.getValue() + password))).findAny();
        optional.ifPresent(entry -> CollisionFinder.hashesAndSalts.remove(entry.getKey()));
        optional.ifPresent(entry -> CollisionFinder.addPassword(Hash.hashes.indexOf(entry.getKey()),password));
    }

}

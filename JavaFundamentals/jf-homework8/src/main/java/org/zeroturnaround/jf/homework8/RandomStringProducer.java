package org.zeroturnaround.jf.homework8;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

/**
 * Producer class, will keep generating new random words to serve as the input for the hashing algorithm.
 * The method {@link #run()} should never return, until a proper shutdown is initiated.
 */
@SuppressWarnings("unused")
public class RandomStringProducer implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(RandomStringProducer.class);

    /**
     * This should contain the actual String generation logic.
     */
    @Override
    public void run() {
        boolean interrupted = false;
        try {
            while (!CollisionFinder.isReady) {
                try {
                    CollisionFinder.generatedStrings.put(generateString());
                } catch (InterruptedException e) {
                    interrupted = true;
                    log.error(e.getMessage());
                }
            }
        } finally {
            if (interrupted) {
                log.error(Thread.currentThread().getName() + " is interrupted");
                Thread.currentThread().interrupt();
            }
        }

    }

    private String generateString() {
        return randomAlphabetic(0, 75);
    }


}

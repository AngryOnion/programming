package org.zeroturnaround.jf.homework8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeroturnaround.jf.homework8.data.Hash;
import org.zeroturnaround.jf.homework8.data.Password;
import org.zeroturnaround.jf.homework8.data.Salt;

/**
 * Sets up the required number of producer and consumer threads,
 * coordinates their information exchange,
 * keeps track of the discovered collisions (and only stores the first one found),
 * and takes care of a !!clean!! shutdown once all passwords have a found collision.
 */
public class CollisionFinder {

    @SuppressWarnings("unused")
    private static final Logger log = LoggerFactory.getLogger(CollisionFinder.class);

    static LinkedBlockingQueue<String> generatedStrings = new LinkedBlockingQueue<>();
    public static ConcurrentHashMap<String, String> hashesAndSalts = new ConcurrentHashMap<>();
    public static List<String> passwords = Collections.synchronizedList(new ArrayList<>());
    public static boolean isReady = false;
    /**
     * Read the data provided by the {@link Salt} and {@link Hash} classes, and for each find a collision.
     * A collision happens when 2 different inputs generate the same hash.
     * This is dangerous in password-validation systems that use hash-codes (and in many other applications),
     * as the system will wrongfully assume the user to have supplied the correct password, allowing an attacker to login.
     * <p>
     * N.B. 1: If by chance your randomly generate the actual original password, then this is fine too of course!
     * N.B. 2: As you by now, no doubt have seen, the elements in {@link Password}, {@link Salt} and {@link Hash} are all in the same order,
     * <t><t>meaning the fir    st password with the first salt results in the first hash.
     * <t><t>When returning from this method, make sure your password guesses are in the same order.
     */
    public List<String> findCollidingPasswords() {
        // STEP 1: create and start the needed numbers of producers and consumers using a proper Executor(s)
        // STEP 2: do a clean wait (do not waste CPU resources waiting) until all passwords are cracked
        // STEP 3: signal all producers and consumers that work is done, and that they should finish
        // STEP 4: do another clean wait until all of them are shut down, and until your Executor(s) have shut down properly
        // STEP 5: only now return the result
        IntStream.range(0, 10).forEach(a -> passwords.add(a,""));
        initializeHashMap();
        initializeThreads();
        return passwords; // Temporary placeholder
    }

    private void initializeHashMap() {
        for (int i = 0; i < Hash.hashes.size(); i++) {
            hashesAndSalts.put(Hash.hashes.get(i), Salt.salts.get(i));
        }
    }

    static void addPassword(int index, String password) {
        passwords.set(index, password);
        if (hashesAndSalts.size() == 0) {
            isReady = true;
        }
    }
    
    private void initializeThreads() {
        Thread consumer1 = new Thread(new RandomStringConsumer());
        Thread consumer2 = new Thread(new RandomStringConsumer());
        Thread consumer3 = new Thread(new RandomStringConsumer());
        Thread consumer4 = new Thread(new RandomStringConsumer());

        Thread producer1 = new Thread(new RandomStringProducer());
        Thread producer2 = new Thread(new RandomStringProducer());

        consumer1.setName("Consumer 1");
        consumer2.setName("Consumer 2");
        consumer3.setName("Consumer 3");
        consumer4.setName("Consumer 4");

        producer1.setName("Producer 1");
        producer2.setName("Producer 2");
        List<Thread> threads = Arrays.asList(producer1, producer2, consumer1, consumer2, consumer3, consumer4);
        threads.forEach(Thread::start);
        threads.forEach(thread -> {
            try {
                thread.join();
            } catch (InterruptedException e) {
                log.error(e.getMessage());
                Thread.currentThread().interrupt();
            }
        });

    }
}

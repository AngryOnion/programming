import requests
import json

guid = []

def saveSession():
    global guid
    payload = {"authenticationGuid": "d9ca681e-ee57-4b4c-9d59-115d88d8f3ce","owner": "EUP","id": "39505290255","firstName": "Ilja","lastName": "Samoilov"}
    r = requests.post("http://localhost:8090/saveSession", data=json.dumps(payload), headers={'content-type':"application/json;charset=UTF-8"})
    print(r.status_code, r.reason)
    guid.append(r.text)

for i in range(10000):
    saveSession()
print(guid)

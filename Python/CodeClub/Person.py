from datetime import date

__author__ = 'Ilja'


class Person:
    def __init__(self, first_name, last_name, birth_date):
        self.first_name = first_name
        self.last_name = last_name
        self.birth_date = birth_date

    def full_name(self):
        return self.first_name + " " + self.last_name

    def age(self):
        today = date.today()
        return (
            today.year - self.birth_date.year - (
            (today.month, today.day) < (self.birth_date.month, self.birth_date.day)))

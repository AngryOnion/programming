__author__ = 'Ilja'
def elements_count(*lists):
    d = {}
    l = []
    for list in lists:
        for x in list:
            if x in l:
                if x in d:
                    number = d[x] + 1
                    d[x] = number
                else:
                    d[x] = 1
            else:
                l.append(x)
    return d

print(elements_count([1, 3, 5, 7, 9, 11], [1, 3]))
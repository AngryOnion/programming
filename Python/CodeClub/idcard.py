import re
def is_person_code_valid(s):
    g = [1,2,3,4,5,6,7,8,9,1]
    m = [3,4,5,6,7,8,9,1,2,3]
    if re.match("^([1-6][0-9]{2}[0-1][0-9][0-2][0-9][0-9]{4})$",s):
        if (int(s[0]) == 5 or 6) and (int(s[1:3]) > 17):
            return False
        if s[3:5] == "02" and int(s[5:7]) > 29:
            return False
        f = 0
        for i in range(10):
            f += int(s[i]) * g[i]
        if f % 11 == 10:
            f = 0
            for i in range(10):
                f += int(s[i]) * m[i]
            if f % 11 == 10 and int(s[10])==0:
                return True
            elif f % 11 == int(s[10]):
                return True
            else:
                return False
        elif f % 11 == int(s[10]):
            return True
        else:
            return False
    else:
        return False
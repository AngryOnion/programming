__author__ = 'Ilja'
import math

def fib(n):

    numberOne = 1
    numberTwo = 1
    sum = 0


    for i in range(n):

        if i == 0:
            sum += numberTwo

        newNumber = numberOne + numberTwo
        numberOne = numberTwo
        numberTwo = newNumber
        sum = sum + numberTwo

    return numberTwo


def fib2(n):

    return math.floor((((1 + math.sqrt(5))/2)**n - ((1 - math.sqrt(5))/2)**n)/math.sqrt(5))


print(fib(0))
print()
def decoder(word):
    if word == None or len(word) == 0:
        return ""
    resultString = ""
    tempString = ""
    tempNumber = ""
    for i in range(len(word)):
        char = word[i]
        if char.isalpha():
            tempString += char
        if char.isdigit():
            if len(tempString) > 0:
                if tempNumber == "":
                    resultString += tempString
                else:
                    resultString += tempString * int(tempNumber)
                tempString = ""
                tempNumber = ""
            tempNumber += char
        if i == len(word) - 1:
            if tempNumber == "":
                resultString += tempString
            else:
                resultString += tempString * int(tempNumber)
    return resultString


print(decoder("100fu"))
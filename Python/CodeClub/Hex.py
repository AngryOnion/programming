__author__ = 'Ilja'

import re

def hex_to_rgb(string):
    if not len(string) == 6:
        return []
    elif re.search("[g-z]", string):
        return []
    elif re.search("[G-Z]", string):
        return []
    elif not string.isalnum():
        return []
    else:
        l = list()
        l.append(int(string[:2], 16))
        l.append(int(string[2:4], 16))
        l.append(int(string[4:6], 16))
        return l

print(hex_to_rgb("ffffff"))
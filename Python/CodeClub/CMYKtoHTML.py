def cmyk_to_hex(a):
    c = float(a[0]) / 100.0
    m = float(a[1]) / 100.0
    y = float(a[2]) / 100.0
    k = float(a[3]) / 100.0
    r = round(255.0 - ((min(1.0, c * (1.0 - k) + k)) * 255.0))
    g = round(255.0 - ((min(1.0, m * (1.0 - k) + k)) * 255.0))
    b = round(255.0 - ((min(1.0, y * (1.0 - k) + k)) * 255.0))
    return '#%02x%02x%02x' % (r, g, b)
print(cmyk_to_hex([0, 75, 75, 20]))
print(cmyk_to_hex([96,63,0,2]   ))
print(cmyk_to_hex([0,100,100,0]))
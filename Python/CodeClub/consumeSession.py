import saveSessions
import requests
import json

for i in range(0,10000,2):
    payload = {"authenticationGuid": "d9ca681e-ee57-4b4c-9d59-115d88d8f3ce", "token": saveSessions.guid[i]}
    r = requests.post("http://localhost:8090/getSession", data=json.dumps(payload),
                      headers={'content-type': "application/json;charset=UTF-8"})
    print(r.status_code, r.reason)

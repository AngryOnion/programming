import parser
import re
def solve_freds_homework(e):
    e = e.replace(" ", "")
    if len(e)== 0 or re.match("[^0-9xy+-=*/]+",e):
        return None
    if 'x' or 'y' in e:
        if '=' not in e:
            e += "=0"
        e1,e2 = e.split("=")
        c = parser.expr(e1).compile()
        d = parser.expr(e2).compile()
        for x in range(-100, 100):
            for y in range(-100, 100):
                try:
                    if eval(c) == eval(d):
                        return x, y
                except ZeroDivisionError:
                    continue
    else:
        return None

print(solve_freds_homework("9"))
print(solve_freds_homework(""))
print(solve_freds_homework("50 = x - x"))
print(solve_freds_homework(" y / -y * x * x = -y / y - x * -6 - -10"))
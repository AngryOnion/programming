class TicTacToe:
    def __init__(self):
        self.f = [[0 for x in range(3)] for y in range(3)]
        for i in range(3):
            for j in range(3):
                self.f[i][j] = 2
    def add_move(self, r, v, p):
        if self.f[r - 1][v - 1] == 2:
            self.f[r - 1][v - 1] = p

    def get_state(self):
        c = 0
        for i in range(3):
            for j in range(3):
                if self.f[i][j] != 2:
                    c += 1
        if(self.d(0)):
            return 0
        elif(self.d(1)):
            return 1
        elif(c == 9):
            return 2
        else:
            return 3
    def d(self, p):
        if (self.f[0][0] == p and self.f[0][1] == p and self.f[0][2] == p) or\
            (self.f[1][0] == p and self.f[1][1] == p and self.f[1][2] == p) or\
            (self.f[2][0] == p and self.f[2][1] == p and self.f[2][2] == p) or\
            (self.f[0][0] == p and self.f[1][0] == p and self.f[2][0] == p) or\
            (self.f[0][1] == p and self.f[1][1] == p and self.f[2][1] == p) or\
            (self.f[0][0] == p and self.f[1][1] == p and self.f[2][2] == p) or\
            (self.f[0][2] == p and self.f[1][1] == p and self.f[2][0] == p):
                return True


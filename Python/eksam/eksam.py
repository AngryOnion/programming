# coding: utf-8
__author__ = 'Ilja'
import re
import random
import sys

start_amount = 100
amount = 80


def Start():
    '''Alustab programmi tööd ning küsib ja jattab globaalseks kasutaja poolt valitud marja liiki,
    funktsiooni check abil kontrollib, kas selline liik on failis olemas ning Checkpoisoni abil
    kontrollib, kas mari on mürgine'''
    global marjad
    marjad = str(input('Mida soovid metsast korjata?'))
    if Check(marjad) == True:
        if CheckForPoison(marjad) == True:
            Walk(marjad)
        elif CheckForPoison(marjad) == False:
            print('Seda ei tohi korjata!')
            Start()
    else:
        print('Antud liiki ei leidu!')
        Start()


def Walk(marjad):
    '''Kasutab Create funktsiooni selleks, et leida juhuslikku marja, ning kui see on sama, mis kasutaja sissestas,
    läheb edasi korjamise funktsioonile, kui mitte sama, mis kasutaja sissestas siis genereerib uue marja
    :arg
    marjad - globaalne ühik, mida Start funktsioonis sissesatas kasutaja
    '''
    m = Create()
    number = random.randint(0, 5)
    random_list = m[number]
    random_berrie = random_list[0]
    print('Metsas leiti "%s!"' % random_berrie)
    Pick(marjad, random_berrie, random_list)


def Pick(marjad, random_berrie, random_list):
    '''Kui kasutaja leiab marja, mida ta tahtis, siis ta saab seda üleskorjata, ning funktsiooni BucketRoom abil
    näitab palju on ämbris veel ruumi.
    :arg
    marjad - globaalne ühik, mida Start funktsioonis sissesatas kasutaja
    random_berrie - juhuslik arv, mida genereeris walk funktsioon
    random_list - list, kuhu kuulub marjad, infoga kas ta on mürgine ja palju ühikut leiad
    '''
    if marjad == random_berrie:
        berries_amount = int(random_list[-1].strip())
        room_in_bucket = BucketRoom(berries_amount)
        print('Lisati ämbrisse (ämbris on vaba ruumi veel %s ühikut)!' % room_in_bucket)
        confirmation = input('„Kas soovid edasi otsida (Jah/Ei)?')
        if confirmation.lower() == 'jah':
            Walk(marjad)
        elif confirmation.lower() == 'ei':
            print('Marjal käik lõppenud – ämbris on %s ühikut marju(%s)!' % (amount, marjad))
            sys.exit(0)
    else:
        print('Jalutatakse mööda!')
        confirmation = str(input('Kas soovid edasi otsida (Jah/Ei)?'))
        if confirmation.lower() == 'jah':
            Walk(marjad)
        elif confirmation.lower() == 'ei':
            print('Marjal käik lõppenud – ämbris on %s ühikut marju(%s)!' % (amount, marjad))
        else:
            Walk(marjad)


def BucketRoom(berries_amount):
    '''Loeb palju ruumi on jaanud ämbris ning kui ämber on täis '''
    global amount
    if amount == start_amount:
        print('Ämber sai täis\n', 'Marjal käik lõppenud – ämbris on 100 ühikut marju(%s)!' % marjad)
        confirmation = str(input('„Kas soovite teostada uut marjal käiku (Jah/Ei)?'))
        if confirmation.lower() == 'jah':
            print('Ämbrisse mahub 100 ühikut metsmarju!')
            amount = 0
            Start()
        else:
            sys.exit(0)
    elif amount < 100:
        amount = + berries_amount
        room = 100 - amount
        return room


def CheckForPoison(marjad):
    '''Kontrollib listides positsioonide listis abil, kas mari on mürgine, 1 mürginem 0 ei ole
    '''

    data = Create()
    if marjad in (x[0] for x in data):
        number = [(i, colour.index(marjad)) for i, colour in enumerate(data) if marjad in colour] #tuple, kus
        #esimene arv on positsioon globaalses listis ning teine arv positsioon listis listis
        berrie_list = [x[0] for x in number]
        berrie = berrie_list[0]
        right_list = data[berrie]
        if int(right_list[1]) == 0:
            return True
        else:
            return False
    else:
        return False


def Check(marjad):
    '''Kontrollib regexi abil, kas mari on olemas failis v mitte'''
    with open('marjad.txt', 'r') as file:
        data = file.read()
        if re.search(marjad, data, re.IGNORECASE):
            return True
        else:
            return False


def Create():
    '''Loeb faili, ning loob list of list, sisestes listides on 1 väärtus mari nimi, teine kas ta on mürgine v mitte
    ja kolmas palju marju on kui leiad'''
    with open('marjad.txt', 'r', ) as file:
        reader = file.readlines()
        list = [x.split("\t") for x in reader]
        return list


print('Ämbrisse mahub 100 ühikut metsmarju!')
Create()
Start()

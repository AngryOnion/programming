import tkinter as tk
import random as rd

# Konstandid
BLOKI_SUURUS = 5
AEG = 25
ounX = 0
ounY = 0
oun = 0
score = 0


# Ussi loomise funktsioon
def loo_uss(canvas, uss):
    uss_blokid = []
    for x, y in uss:
        x1 = x * BLOKI_SUURUS
        y1 = y * BLOKI_SUURUS
        x2 = x1 + BLOKI_SUURUS
        y2 = y1 + BLOKI_SUURUS
        blokid = canvas.create_rectangle(x1, y1, x2, y2, fill='red')
        uss_blokid.append(blokid)
    # tsukli lopp
    return uss_blokid


def liikumine(canvas, uss, ussi_blokid, kustuta=True):
    # leiame ussi pea
    x, y = uss[0]
    global ounX
    global ounY
    global oun
    global score

    # kuhu pea liigub
    if suund == "up":
        y = y - 1
    elif suund == "down":
        y = y + 1
    elif suund == "left":
        x = x - 1
    elif suund == "right":
        x = x + 1

        # lisame nö uut pea, sõltuvalt sellest
    # kuhu mängija tahab liikuda
    uss.insert(0, [x, y])
    # muudame seda punkti ruuduks
    x1 = x * BLOKI_SUURUS
    y1 = y * BLOKI_SUURUS
    x2 = x1 + BLOKI_SUURUS
    y2 = y1 + BLOKI_SUURUS
    blokid = canvas.create_rectangle(x1, y1, x2, y2, fill='red')

    ussi_blokid.insert(0, blokid)
    # kustatab ussi lõpu ära
    if ((ounX >= x1 and ounX <= x2)
        or (ounX + BLOKI_SUURUS >= x1
            and ounX + BLOKI_SUURUS <= x2)):
        if ((ounY >= y1 and ounY <= y2)
            or (ounY + BLOKI_SUURUS >= y1
                and ounY + BLOKI_SUURUS <= y2)):
            score = score + 1
            kustuta = False
            print("ate")
            canvas.delete(oun)
            ouna_genereerimine()

    if (kustuta):
        del uss[-1]
        canvas.delete(ussi_blokid[-1])
        del ussi_blokid[-1]

    if kaotamine(uss) or x < 0 or y < 0:
        print("kaotus")
    elif (x > 400 / BLOKI_SUURUS or y > 400 / BLOKI_SUURUS):
        print("kaotus")
    else:
        root.after(AEG, liikumine, canvas, uss, ussi_blokid)


def muuda_suunda(uus_suund):
    global suund

    if uus_suund == "left":
        # != tähendab ei võrdu
        if suund != "right":
            suund = uus_suund

    elif uus_suund == "right":
        if suund != "left":
            suund = uus_suund

    elif uus_suund == "up":
        if suund != "down":
            suund = uus_suund

    elif uus_suund == "down":
        if suund != "up":
            suund = uus_suund


def ouna_genereerimine():
    global ounX
    global ounY
    global oun
    ounX = rd.randint(100, 200)
    ounY = rd.randint(100, 200)

    x1 = ounX + BLOKI_SUURUS
    y1 = ounY + BLOKI_SUURUS
    oun = canvas.create_rectangle(ounX, ounY, x1, y1, fill='green')


def kaotamine(ussi_blokid):
    pea = ussi_blokid[0]
    x = pea[0]
    y = pea[1]
    for i in range(1, len(ussi_blokid)):
        blokk = ussi_blokid[i]
        x1 = blokk[0]
        y1 = blokk[1]
        if (x == x1):
            if (y == y1):
                print("GG")
                return True
    return False


# -----------------
# defineerimi algset suunda
suund = "up"
# -----------------


root = tk.Tk()
canvas = tk.Canvas(root)
canvas.pack()
root.resizable(width=False, height=False)
root.geometry('400x400')
text = tk.Text(root)
text.insert(tk.INSERT, "Ussimäng\n")
text.insert(tk.INSERT, "Score:")
text.pack()
# loo ussi
uss = [[x, 25] for x in range(10, 35)]
# uss += [[35,y] for y in range(25,1,-1)]
# uss += [[x,1] for x in range(35,1,-1)]
# http://pastebin.com/Gvfi6QWs
ussi_blokid = loo_uss(canvas, uss)
root.after(AEG, liikumine, canvas, uss, ussi_blokid)
# Anname programmile teada, mis nuppu vajutamised teevad
root.bind('<Left>', lambda event: muuda_suunda("left"))
root.bind('<Right>', lambda event: muuda_suunda("right"))
root.bind('<Up>', lambda event: muuda_suunda("up"))
root.bind('<Down>', lambda event: muuda_suunda("down"))
ouna_genereerimine()
root.mainloop()
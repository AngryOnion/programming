import tkinter as tk
import random as rd
 
field = [[0,0,0,0],
         [0,0,0,0],
         [0,0,0,0],
         [0,0,0,0]]
field_array = []
lose = False
#Tagastab kas on kaotatud või mitte
def kontrolli():
    global field
    find = False
    saab_edasi_mangida = True
    for i in range(0,4):
        if (find == True):
            break
            return True
        for j in range(0,4):
            if (field[i][j] != 0):
                if (i == 0):
                    if(j==0):
                        saab_edasi_mangida = kontrolli_ruutu(False,False,True,True,i,j)
                    elif(j==3):
                        saab_edasi_mangida = kontrolli_ruutu(True,False,False,True,i,j)
                    else:
                        saab_edasi_mangida = kontrolli_ruutu(True,False,True,True,i,j)
                elif(i == 3):
                    if(j==0):
                        saab_edasi_mangida = kontrolli_ruutu(False,True,True,False,i,j)
                    elif(j==3):
                        saab_edasi_mangida = kontrolli_ruutu(True,True,False,False,i,j)
                    else:
                        saab_edasi_mangida = kontrolli_ruutu(True,True,True,False,i,j)
                else:
                    if(j==0):
                        saab_edasi_mangida = kontrolli_ruutu(False,True,True,True,i,j)
                    elif(j==3):
                        saab_edasi_mangida = kontrolli_ruutu(True,True,False,True,i,j)
                    else:
                        saab_edasi_mangida = kontrolli_ruutu(True,True,True,True,i,j)
                if(saab_edasi_mangida):
                    return True
            elif (field[i][j] == 0):
                return True
    return saab_edasi_mangida
 
def kontrolli_ruutu(vasak, ylemine, parem, alumine, i, j):
    global field
    if(vasak):
        if(field[i][j-1] == 0 or field[i][j-1] == field[i][j]):
            return True
    if(ylemine):
        if(field[i-1][j] == 0 or field[i-1][j] == field[i][j]):
            return True
    if(parem):
        if(field[i][j+1] == 0 or field[i][j+1] == field[i][j]):
            return True
    if(alumine):
        if(field[i+1][j] == 0 or field[i+1][j] == field[i][j]):
            return True
    return False
 
def place():
    global field
    global lose
    #genereerime suvalised koordinatid
    count = 0
    for i in range(0,4):
        for j in range(0,4):
            if (field[i][j] != 0):
                 count = count + 1
    if (count == 16):
        kontrolli()
    x = rd.randint(0,3)
    y = rd.randint(0,3)
    try:
        if (field[x][y] == 0):
            #kui on vaba, siis pane sinna ruudu 1 voi 2
            field[x][y] = rd.randint(1,2)
        else:
            place()
    except:
        quit()
 
def draw():
    global field
    global field_array
    #kustutame mänguplatsist vana väärtused
    for i in range(len(field_array)):
   
        canvas.delete(field_array[i])
    #kustutame vana väärtused mälust
    del field_array[:]
   
    for i in range(0,400,100):
        canvas.create_line([(0, i),(400, i)])
       
    for i in range(0,400,100):
        canvas.create_line([(i, 0),(i, 400)])    
   
    for x in range(0,4):
        for y in range(0,4):
            text = field[x][y]
            i=""
            if(text == 0):
                text = " "
            elif(text == 1):
                i = canvas.create_text(50+100*x, 50+100*y,fill="green",
                               font="Times 32 bold",text="%s"%text)
            elif(text == 2):
                i = canvas.create_text(50+100*x, 50+100*y,fill="red",
                               font="Times 32 bold",text="%s"%text)
            elif(text == 4):
                i = canvas.create_text(50+100*x, 50+100*y,fill="blue",
                               font="Times 32 bold",text="%s"%text)
            elif(text == 8):
                i = canvas.create_text(50+100*x, 50+100*y,fill="orange",
                               font="Times 32 bold",text="%s"%text)
            elif(text == 16):
                i = canvas.create_text(50+100*x, 50+100*y,fill="dark orchid",
                               font="Times 32 bold",text="%s"%text)
            elif(text == 32):
                i = canvas.create_text(50+100*x, 50+100*y,fill="goldenrod",
                               font="Times 32 bold",text="%s"%text)    
            elif(text == 64):
                i = canvas.create_text(50+100*x, 50+100*y,fill="khaki",
                               font="Times 32 bold",text="%s"%text)
            elif(text == 128):
                i = canvas.create_text(50+100*x, 50+100*y,fill="azure",
                               font="Times 32 bold",text="%s"%text)
            elif(text == 256):
                i = canvas.create_text(50+100*x, 50+100*y,fill="aquamarine",
                               font="Times 32 bold",text="%s"%text)
            elif(text == 512):
                i = canvas.create_text(50+100*x, 50+100*y,fill="navy",
                               font="Times 32 bold",text="%s"%text)
            elif(text == 1024):
                i = canvas.create_text(50+100*x, 50+100*y,fill="saddle brown",
                               font="Times 32 bold",text="%s"%text)
            elif(text == 2048):
                i = canvas.create_text(50+100*x, 50+100*y,fill="gold",
                               font="Times 32 bold",text="%s"%text)
            else:
                i = canvas.create_text(50+100*x, 50+100*y,fill="black",
                               font="Times 32 bold",text="%s"%text)
            field_array.append(i)
 
def kaik(suund):
    global field
    if (suund== "up"):
        for i in range(0,4):
            for j in range(1,4):
                if (field[i][j] != 0):
                    if (field[i][j-1] != 0):
                        if(field[i][j]== field[i][j-1]):
                            field[i][j-1]= field[i][j] + field[i][j-1]
                            field[i][j] = 0
                    else:
                        field[i][j-1] = field[i][j]
                        field[i][j] = 0
    if (suund== "down"):
        for i in range(3,-1,-1):
            for j in range(2,-1,-1):
               
                if (field[i][j] != 0):
                    if (field[i][j+1] != 0):
                        if(field[i][j]== field[i][j+1]):
                            field[i][j+1]= field[i][j] + field[i][j+1]
                            field[i][j] = 0
                    else:
                        field[i][j+1] = field[i][j]
                        field[i][j] = 0
    if (suund== "right"):
        for j in range(3,-1,-1):
            for i in range(2,-1,-1):
                if (field[i][j] != 0):
                    if (field[i+1][j] != 0):
                        if(field[i][j]== field[i+1][j]):
                            field[i+1][j]= field[i][j] + field[i+1][j]
                            field[i][j] = 0
                    else:
                        field[i+1][j] = field[i][j]
                        field[i][j] = 0
    if (suund== "left"):
        for j in range(0,4):
            for i in range(1,4):
                if (field[i][j] != 0):
                    if (field[i-1][j] != 0):
                        if(field[i][j]== field[i-1][j]):
                            field[i-1][j]= field[i][j] + field[i-1][j]
                            field[i][j] = 0
                    else:
                        field[i-1][j] = field[i][j]
                        field[i][j] = 0
    place()
    draw()
 
def quit():
    global root
    root.destroy()
 
root = tk.Tk()
root.title('2048')
canvas = tk.Canvas(root, width=400, height=400, bg='white')
canvas.pack()
root.resizable(width=False, height=False)
root.geometry('400x400')
#Anname programmile teada, mis nuppu vajutamised teevad
root.bind('<Left>', lambda event:kaik("left"))
root.bind('<Right>', lambda event:kaik("right"))
root.bind('<Up>', lambda event:kaik("up"))
root.bind('<Down>', lambda event:kaik("down"))
draw()
place()
draw()

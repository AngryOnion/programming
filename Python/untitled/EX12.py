__author__ = 'Ilja'

import re


class Heatmap(object):


    def read_file(self, file):
        """Loeb faili ja tagastab listi sonadega selles tekstifailis"""
        lines = [line.rstrip('\n') for line in open(file)]
        content = str(lines)
        word_list = re.sub('[^ABCDEFGHIJKLMNOPQRSŠZŽTUVWÕÄÖÜXY]', ' ', content, 0, re.IGNORECASE).split()
        word_list = [element.lower() for element in word_list]
        return word_list


    def pair_frequency(self, word_list):
        """kasutab eelmise funktsiooni poolt loodud listi ja muudab seda sõnastikuks,
        kus võtmeks on kõik esinevad tähepaarid ja väärtuseks nende esinemise sagedus"""
        pair_dictionary = {}


    def create_heatmap(self, filename, pair_dictionary):
        pass


if __name__ == '__main__':
    pass

d = Heatmap()
c = Heatmap()
print(d.pair_frequency(c.read_file('testfile.txt')))
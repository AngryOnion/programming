__author__ = 'Madis'

class CallCentre(object):
    def __init__(self):
        self.nouns = ['koer', 'porgand', 'madis', 'kurk', 'tomat']
        self.targets = ['koera', 'porgandit', 'madist', 'kurki', 'tomatit']
        self.verbs = ['sööb', 'lööb', 'jagab', 'tahab', 'ei taha']
        self.adjectives = ['ilus', 'kole', 'pahane', 'magus', 'sinu']
        self.target_adjectives = ['ilusat', 'koledat', 'pahast', 'magusat', 'sinu']
        self.sentence = 'noun verb target'
        self.twosentences = 'sentence sentence'
        self.beautifulsentence = 'adjective noun verb targetadjective target .'
        self.count = [0, 0, 0, 0, 0]

    def create_sentence(self, instr):
        dict = ''
        for w in instr.split(' '):
            if w != 'noun' and \
                w != 'target' and \
                w != 'verb' and \
                w != 'adjective' \
                and w != 'targetadjective' \
                and w != 'sentence' and w != 'twosentences' \
                and w != 'beautifulsentence':
                dict += w

            else:
                if w == 'sentence':
                    dict += self.create_sentence(self.sentence)
                if w == 'twosentences':
                    dict += self.create_sentence(self.sentence) * 2
                if w == 'beautifulsentence':
                    dict += self.create_sentence(self.beautifulsentence)
                if self.count[0] == len(self.nouns) - 1:
                    self.count[0] = 0 # check that in bounds
                if w == 'noun':
                    dict += ' ' + self.nouns[self.count[0]]
                    self.count[0] += 1

                if self.count[1] == len(self.nouns) - 1:
                    self.count[1] = 0
                if w == 'target':
                    dict += ' ' + self.targets[self.count[1]]
                    self.count[1] += 1

                if self.count[2] == len(self.verbs) - 1:
                    self.count[2] = 0
                if w == 'verb':
                    dict += ' ' + self.verbs[self.count[2]]
                    self.count[2] += 1

                if self.count[3] == len(self.nouns) - 1:
                    self.count[3] = 0
                if w == 'adjective':
                    dict += ' ' + self.adjectives[self.count[0]]
                    self.count[3] += 1

                if self.count[4] == len(self.nouns) - 1:
                    self.count[4] = 0
                if w == 'targetadjective':
                    dict += ' ' + self.target_adjectives[self.count[4]]
                    self.count[4] += 1
        return dict

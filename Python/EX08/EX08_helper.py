"""
Helper module to load call centre instance.

Your tests should use this module to get the call centre instance:

centre = EX08_helper.get_callcentre()
sentence = centre.create_sentence("noun !")
...

"""
import callcentre


def get_callcentre():
    """Return new instance of call centre."""
    return callcentre.CallCentre()

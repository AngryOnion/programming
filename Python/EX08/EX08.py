#!/usr/bin/env python
# coding: utf-8
__author__ = 'Ilja'

""" CallCenter klassi testide mall. NB! Fail peab olema UTF-8 kodeeringus.

    Teste saab käivitada järgmise käsureaga:
    python -m unittest EX08.py
"""

import EX08_helper
import unittest


class Tests(unittest.TestCase):
    """ CallCenter klassi testide klass 

        Käesolev klass pärib sisaldab testilugusid (test case), mis testib
        CallCentre koodi."""

    def setup(self):
        """ CallCenteri objekti loomine EX08_helper.get_callcentre() abil.

        Loome värske CallCenter klassist objekti ja tagastame selle.
        Objekti loomiseks kasutame abimoodulit EX08_helper ja sealt
        funktsiooni get_callcentre(). Abifunktsioon on testides kasutamiseks
        mugavam."""
        call_centre_instance = EX08_helper.get_callcentre()
        return call_centre_instance

    def test_uks_nimisona(self):
        "Testime, kas kõnekeskus tagastab esimese nimisõna nimisõnade hulgast."
        cc = self.setup()
        create = cc.create_sentence
        info = create('noun')
        self.assertEqual(info, 'koer')

    def test_all_nouns(self):
        """
        Testime, kas programm tagastab koike nimisonasi
        """
        cc = self.setup()
        create = cc.create_sentence
        for i in range(0, 50):
            info = create('noun')
            self.assertEqual(info, self.nouns[i % 5])

    def test_all_verbs(self):
        """
        Testime, kas programm tagastab koike tegusonasi
        """
        cc = self.setup()
        create = cc.create_sentence
        for i in range(0, 50):
            info = create('verb')
            self.assertEqual(info, self.verbs[i % 5])

    def test_all_targets(self):
        """
        Testime, kas programm tagastab koike targetˇid
        """
        cc = self.setup()
        create = cc.create_sentence
        for i in range(0, 50):
            info = create('targets')
            self.assertEqual(info, self.targets[i % 5])

    def test_all_adjectives(self):
        """
        Testime, kas programm tagastab koike omadussonad
        """
        cc = self.setup()
        create = cc.create_sentence
        for i in range(0, 50):
            info = create('adjectives')
            self.assertEqual(info, self.adjectives[i % 5])

    def test_all_target_adjectives(self):
        """
        Testime, kas programm tagastab koike target omadussonad
        """
        cc = self.setup()
        create = cc.create_sentence
        for i in range(0, 50):
            info = create('target_adjectives')
            self.assertEqual(info, self.target_adjectives[i % 5])

    def test_sentence(self):
        """
        Testime, kas lause on oige
        """
        cc = self.setup()
        create = cc.create_sentence
        for i in range(0, 50):
            sentence = (self.nouns[i % 5] + ' ' + self.verbs[i % 5] + ' ' + self.targets[i % 5] + ' .')
            info = create('sentence')
            self.assertEqual(info, sentence)

    def test_two_sentences(self):
        """
        Testime, kas kaks lause on oiged
        """
        cc = self.setup()
        create = cc.create_sentence
        for i in range(0, 50):
            first_sentence = (self.nouns[i % 5] + ' ' + self.verbs[i % 5] + ' ' + self.targets[i % 5] + ' .')
            second_sentence = (
                self.nouns[(i + 1) % 5] + ' ' + self.verbs[(i + 1) % 5] + ' ' + self.targets[(i + 1) % 5] + ' .')
            info = create('twosentences')
            self.assertEqual(info, first_sentence + ' ' + second_sentence)

    def beautiful_sentence(self):
        """
        Testime, kas ilusalause on oige
        """
        cc = self.setup()
        create = cc.create_sentence
        for i in range(0, 50):
            sentence = (self.adjectives[i % 5] + ' ' + self.nouns[i % 5] + ' ' + self.verbs[i % 5] + ' '
                        + self.targetadjectives[i % 5] + ' ' + self.targets[i % 5] + ' .')
            info = create('beautifulsentence')
            self.assertEqual(info, sentence)

    def test_for_random_words(self):
        """
        Testime, kas programm tagastab suvalisi asju
        """
        cc = self.setup()
        create = cc.create_sentence
        for i in range(0, 50):
            info = create(self.random_words[i % 5])
            self.assertEqual(info, self.random_words[i % 5])

# coding: utf-8
import sys
import random

__author__ = 'Ilja'

global rohk
rohk = 1.80
global arv
arv = 2


def start():
    confirmation = str(input('Süsteem on ülesse seatud – kas käivitada süsteem (jah/ei)'))
    if confirmation.lower() == 'jah':
        pump(rohk)
    elif confirmation.lower() == 'ei':
        print('Süsteemi ei võeta tööle!')
        sys.exit(0)  # SEDA PEAB ÄRA MUUTMA
    else:
        start()


def kraan():
    confirmation = str(input("Kas keerata kraanid lahti (jah/ei)?"))
    if confirmation.lower() == 'jah':
        logsystem(arv, rohk)
    elif confirmation.lower() == "ei":
        print('Kraane lahti ei keeratud!')
        sys.exit(0)


def logsystem(arv, rohk):
    if arv % 2 == 0:
            while 1.85<=rohk:
                veeTarbimine= random.choice(("[Tarbitakse]", "[Ei tarbita]"))
                if veeTarbimine=="[Tarbitakse]":rohuSuund = "[Alaneb]"
                if veeTarbimine== "[Tarbitakse]"and 1.80<=rohk: rohk -= 0.05
                rohk = round(rohk,2)
                if veeTarbimine== "[Ei tarbita]":rohuSuund = "[Seisab]"
                print("\t[Seisab]", veeTarbimine, rohuSuund,": %.2f bar"%rohk)
    if arv % 2 != 0:
        while rohk <= 2.75:
            veeTarbimine = random.choice(("[Tarbitakse]", "[Ei tarbita]"))
            if veeTarbimine== '[Ei tarbita]' and rohk <= 2.70 : rohk += 0.15
            if veeTarbimine== '[Tarbitakse]'and rohk <= 2.75: rohk += 0.10
            rohk = round(rohk,2)
            print("\t[Töötab]", veeTarbimine,"[Tõuseb] : %.2f bar"%rohk)
    arv += 1
    kasKraanid = input("\nKas soovite jätkata (jah/ei)? ")
    if kasKraanid != "jah":
        print("Süsteem suleti!")
    else:
        kraan()


def pump(rohk):
    print('Suvaveepump käivitus!')
    print('Hudrofoori rõhk:')
    while rohk < 2.85:
        print(round(rohk, 2), 'bar')
        if 2.75 <= rohk <= 2.85:
            print("Süvaveepump seiskus!")
            kraan()
        rohk += 0.15


start()

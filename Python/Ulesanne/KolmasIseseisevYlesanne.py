# coding: utf-8
__author__ = 'Ilja'
import re
import datetime
import sys
import string
from time import localtime, strftime



print("""L - Sademete lisamine andmebaasi.
M - Andmebaasis oleva sademe muutmine.
P - Andmebaasis asuvate sademete kuvamine.
E - Programmi sulgemine.""")


def choice():
    """Asks user what function of that program to use
    L - add to database, M - change in database, P - show what is in database, E - exit programm"""

    letter = input('\nSisestage soovitud valik (L, M, P, E):')
    if letter.lower() == 'l':
        Lfunction()
    elif letter.lower() == 'm':
        Mfunction()
    elif letter.lower() == 'p':
        Pfunction()
    elif letter.lower() == 'e':
        Efunction()
    else:
        choice()


def Lfunction():
    '''Lisa andmebaasi indifikaatori ning sademete hulka'''
    indificator = input('\tFikseerige sade kujul [Identifikaator] [20] (C):')
    if re.match('\[[\w+]*\]\s\[\d*\]', indificator, re.IGNORECASE):
        with open('andmebaas.txt', 'a') as file:
            perception_time = strftime("%d.%m.%Y %H:%M:%S", localtime())
            file.write(('%s - %s\n' % (perception_time, indificator)))
        print('\tSade fikseeritud!')
        file.close()
        choice()
    elif indificator.lower() == 'c':
        choice()
    else:
        Lfunction()

def Mfunction():
    '''Kuvab andmebaasi sisu ilma aega ning annab voimaluse muuta sisut'''
    with open('andmebaas.txt', 'r') as file:
        data = file.read()
        file_content = re.findall('\[[\w+]*\]\s\[\d*\]', data, re.IGNORECASE)
        print('\tAndmebaasis olevad andmed:')
        for x in file_content:
            print('\t\t%s.' % (file_content.index(x)+1), x)
        file.close()
    index_of_precipation = input('\tSisestage sademe indeks, mida soovite muuta (C):')
    if int(index_of_precipation) in range(len(file_content)):
        precipation = input('\tFikseerige sade kujul [Identifikaator] [20] (C):')
        index = int(index_of_precipation) - 1
        if re.match('\[[\w+]*\]\s\[\d*\]', precipation, re.IGNORECASE):
            replace(index, precipation)
        else:
            Mfunction()
    elif index_of_precipation.lower() == 'c':
            choice()
    else:
        print("\t\tSisestatud sademe indeksit ei eksisteeri andmebaasis!")
        Mfunction()


def replace(index_of_precipation, subs):
    """Vahetab sademe indeksi jargi sadet andmebaasis"""
    perception_time = strftime("%d.%m.%Y %H:%M:%S", localtime())
    substandtime = str(('%s - %s\n' % (perception_time, subs)))
    file = open('andmebaas.txt', 'r')
    content = file.readlines()
    content.pop(index_of_precipation)
    content.insert(index_of_precipation, substandtime)
    content = ''.join(content)
    file.close()
    file = open('andmebaas.txt', 'w')
    file.write(content)
    print('\tSademe kirje on uuendatud!')
    file.close()
    choice()

def Pfunction():
    """Kuvab andmebaasi sisu koos sissestamise ajaga"""
    file = open('andmebaas.txt', 'r')
    print('\tAndmebaasi sisu:')
    content = file.readlines()
    for i in content:
        print("\t\t"+i[:-1])
    file.close()
    return choice()


def Efunction():
    """Paneb programmi kinni"""
    confirmation = input("\tKas olete kindel, et soovite programmi sulgeda ('jah')?")
    if confirmation.lower() == 'jah':
        print('\tProgramm läks kinni')
        sys.exit(0)
    else:
        Efunction()


choice()


""" Mitte tootavad variandid replace funktsiooni
def replace(pattern, subs):
    substandtime = str(('%s - %s.' % (datetime.datetime.now(), subs)))
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open('andmebaas.txt') as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, substandtime))

    close(fh)
    #Remove original file
    remove('andmebaas.txt')
    #Move new file
    move(abs_path, 'andmebaas.txt')


def replace2(pattern, subs):
    with open('andmebaas.txt', "r+" ) as file:
        fileContents = file.read()
        textPattern = re.compile(re.escape(pattern))
        substandtime = str(('%s - %s.' % (datetime.datetime.now(), subs)))
        fileContents = textPattern.sub(substandtime, fileContents)
        file.seek(0)
        file.truncate()
        file.write(fileContents)

def replace3(pattern, subs):
    substandtime = str(('%s - %s.' % (datetime.datetime.now(), subs)))
    lines = filter(lambda x:x[0:-1]!=pattern, open("andmebaas.txt", "r"))
    open("andmebaas.txt", "w").write(substandtime.join(lines))"""



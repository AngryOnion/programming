# coding: utf-8
__author__ = 'Ilja'
import random

pressure = 1.65
number = 2

Start = str(input("Süsteem on ülesse seatud - kas käivitada süsteem (jah/ei)? "))
if Start.lower() != "jah":
    print("Süsteemi ei võeta tööle!")

elif Start.lower() == "jah":
    print("\tSüvaveepump käivitus!\n\t\tHüdrofoori rõhk:")
    while pressure <= 2.80:
        pressure += 0.15
        pressure = "%.2f"%pressure
        print("\t\t\t{0} bar".format(pressure))
        pressure = float(pressure)
    print("\tSüvaveepump seiskus!")

    Kraan = str(input("\nKas keerata kraanid lahti (jah/ei)? "))

    if Kraan.lower() != "jah":
        print("Kraane lahti ei keeratud!") # \t

    while Kraan.lower() == "jah":

        if number % 2 == 0:
            while 1.85 <= pressure:
                consumption = random.choice(("[Tarbitakse]", "[Ei tarbita]"))
                if consumption == "[Tarbitakse]" : pressureway = "[Alaneb]"
                if consumption == "[Tarbitakse]"and 1.80 <= pressure : pressure -= 0.05
                pressure = round(pressure, 2)
                if consumption == "[Ei tarbita]":pressureway = "[Seisab]"
                print("\t[Seisab]", consumption, pressureway, ": %.2f bar"%pressure)

        if number % 2 != 0:
            while pressure <= 2.75:
                consumption = random.choice(("[Tarbitakse]", "[Ei tarbita]"))
                if consumption == '[Ei tarbita]' and pressure <= 2.70 : pressure += 0.15
                if consumption == '[Tarbitakse]'and pressure <= 2.75: pressure += 0.10
                pressure = round(pressure, 2)
                print("\t[Töötab]", consumption,"[Tõuseb] : %.2f bar"%pressure)
        number +=1
        Kraan = str(input("\nKas soovite jätkata (jah/ei)? "))
        if Kraan.lower() != "jah":
            print("Süsteem suleti!")


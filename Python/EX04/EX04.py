"""
Find mines using a mine detecting goat named Miina.

@author: Ilja Samoilov IAPB15
"""


def create_mine_map(mines, layers):
    """
    Arguments:
    mines -- Expects an multidimentional array whereas each element contains 2 ints.
    layers -- Expects an int. Which will determine the ammount of layers that the final array will have.

    Returns:
    An array
    """
    polar_map = [[0 for x in range(4)] for x in range(layers + 1)]  # Creates array and fills it's elements with zeroes
    list_of_mines = list(set(mines))  # Creates a duplicate free list out of mines
    for i in range(len(list_of_mines)):
        if list_of_mines[i][0] <= layers and list_of_mines[i][1] <= 3 and layers > 0:
            first = False
            if list_of_mines[i][0] == 0:  # If it is the first layer
                start = 0
                end = 2
                first = True
            elif list_of_mines[i][0] == layers:  # If it is the last layer
                start = -1
                end = 0
            else:  # If it is a middle layer
                start = -1
                end = 2
            for j in range(start, end, 1):
                for k in range(4):
                    if first is False and (k == list_of_mines[i][1] + 2 or k == list_of_mines[i][1] - 2):
                        continue
                    polar_map[list_of_mines[i][0] + j][k] += 1
                first = False
        else:
            pass
    for i in range(len(list_of_mines)):  # Adds all the x's
        if list_of_mines[i][0] <= layers and list_of_mines[i][1] <= 3 and layers > 0:
            polar_map[list_of_mines[i][0]][list_of_mines[i][1]] = 'X'
        else:
            pass
    return polar_map[0:layers]

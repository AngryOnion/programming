import math
ab = float(input())
bc = float(input())
hypotenuse = math.sqrt(math.pow(ab, 2) + math.pow(bc, 2))
mc = hypotenuse / 2
angle = math.acos(mc / ab)
degrees = math.degrees(angle)
s = str(int(round(degrees)))
print(s)
__author__ = 'Ilja'
import random


def decide(sensor_data, current_state):
    """
    Decide in which direction should robot move using sensor_data and current_state
    Args:
        sensor_data -- List of blocking objects in specific direction.
        e.g. [-1, -1, 0, 0, 0, 0, 0, 0]
        where
        0  - Free
        -1  - Object
        current_state -- current robot direction ie. N, NE, E, SE, S, SW, W, NW
    Return:
    Random direction or None.
    """
    states = {'N': 0, 'NE': 1, 'E': 2, 'SE': 3, 'S': 4, 'SW': 5, 'W': 6, 'NW': 7}  # all posible states
    direction = None
    if current_state in states and all(i in [-1, 0] for i in sensor_data):
        for i in range(-2, 3, 1):
            if sensor_data[(states[current_state] + i) % 8] == 0:
                direction = i
    return direction

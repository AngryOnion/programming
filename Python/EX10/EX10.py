__author__ = 'Ilja'
import re

class CallCentre(object):
    """Class what is creating sentences."""
    def noun(self):
        """It generates nouns."""
        while True:
            yield "koer"
            yield "porgand"
            yield "madis"
            yield "kurk"
            yield "tomat"
 
    def target(self):
        """It generates targets."""
        while True:
            yield "koera"
            yield "porgandit"
            yield "madist"
            yield "kurki"
            yield "tomatit"
 
    def verb(self):
        """It generates verbs."""
        while True:
            yield "sööb"
            yield "lööb"
            yield "jagab"
            yield "tahab"
            yield "ei taha"
 
    def adjective(self):
        """It generates adjectives."""
        while True:
            yield "ilus"
            yield "kole"
            yield "pahane"
            yield "magus"
            yield "sinu"
 
    def targetadjective(self):
        """It generates targetadjectives."""
        while True:
            yield "ilusat"
            yield "koledat"
            yield "pahast"
            yield "magusat"
            yield "sinu"
 
    def __init__(self):
        """ Generating values """
        self.noun = self.noun()
        self.target = self.target()
        self.verb = self.verb()
        self.adjective = self.adjective()
        self.targetadjective = self.targetadjective()
 
    def create_sentence(self, words):
        """Creates complete sentences."""
        dict = ''
        strings = re.split(r'(\s+)', words)
        for element in strings:
            if element not in ['targetadjective',
                               'adjective',
                               'verb',
                               'target',
                               'noun',
                               'sentence',
                               'beautifulsentence',
                               'twosentences']:
                    dict += element
            else:
                if element == 'noun':
                    dict += next(self.noun)
                elif element == 'target':
                    dict += next(self.target)
                elif element == 'verb':
                    dict += next(self.verb)
                elif element == 'adjective':
                    dict += next(self.adjective)
                elif element == 'targetadjective':
                    dict += next(self.targetadjective)
                elif element == 'sentence':
                    dict += next(self.noun) + " " + next(self.verb) + " " + next(self.target) + " ."
                elif element == 'beautifulsentence':
                    dict += next(self.adjective) + " " + next(self.noun) + " " + next(self.verb) + \
                            " " + next(self.targetadjective) + " " + next(self.target) + " ."
                elif element == 'twosentences':
                    dict += next(self.noun) + " " + next(self.verb) + " " + next(self.target) + \
                            " . " + next(self.noun) + " " + next(self.verb) + " " + next(self.target) + " ."
        return dict

__author__ = 'Ilja'
def reorient_cars(list_of_wagons, depot_length):
    train_len=len(list_of_wagons)#train length
    train=[]
    if train_len<=depot_length and depot_length>0:
        train=list_of_wagons[::-1]#just reverse it
    elif train_len>depot_length and depot_length>0:
        in_dep=train_len//depot_length
        out_dep=train_len%depot_length
        start=depot_length
        for a in range(in_dep):
            for i in range(start,start-depot_length,-1):
                train.append(list_of_wagons[i-1])
            start+=depot_length
        for i in range(train_len,train_len-out_dep,-1):
            train.append(list_of_wagons[i-1])
    else:
        train=list_of_wagons
    train=reorient_cars2(train)
    return train
def reorient_cars2(list_of_wagons):
    flipped_cars=[]
    for i in range(0,len(list_of_wagons),1):
        element_len=len(list_of_wagons[i])
        if element_len>1:
            word=list_of_wagons[i]
            flipped_cars.append(word[::-1])
        else:
            flipped_cars.append(list_of_wagons[i])
    return flipped_cars
reorient_cars(["s", "b"], 2)
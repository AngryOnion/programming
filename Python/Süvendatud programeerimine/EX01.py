def convert_dates_to_coconuts(count):#funktsiooni defineerimine
    if count > 0:
        pap = int(count//7)#jagamine // kuna harjutuse jargi me saame 7 dalti eest papaia, ja 8 datli me ei saa 1 papaia ja 1/8 sellest.
        ban = int(pap//5)#kuna defineerimises me ei saa panna korrutamist, siis see laheb jargmisesse tehingusse.
        coc = int(ban*2//3)
        res = int(pap + ban + coc)#tulemus= on papaiade ja banaanide ja kookoste kokku arv, kuna see ongi tehingute arv
        return res
    else:
        return 0

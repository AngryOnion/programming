__author__ = 'Ilja'
def F(n):
    if n == 0: return 1
    elif n == 1: return 1
    else: return F(n-1)+F(n-2)

print(F(3))
print(F(2))
print(F(0))
import re
__author__ = 'Ilja'


def ex1_solution(input_string):
    """Kontrollib kas ette antud sone on kahendarv"""
    if re.match('[01]*$', input_string):
        return True
    else:
        return False


def ex2_solution(input_string):
    """ KOntrollib kas kahendarv sonena jagub kahega"""
    if re.match('[01]*$', input_string):
        if int(input_string) % 2 == 0:
            return True
        else:
            return False
    else:
        return False


def ex3_solution(input_string):
    """Kontrollib kas kahendarvu sone pikkus on paarisarv """
    if re.match('[01]*$', input_string):
        if len(input_string) % 2 == 0:
            return True
        else:
            return False
    else:
        return False


def ex4_solution(input_string):
    """Kontrollib, kas kahendarvu sone sisaldab mustrit 0110 või 1001 """
    if re.search("0110", input_string) or re.search('1001', input_string):
        return True
    else:
        return False


def ex5_solution(input_string):
    """Kontrollib, kas kahendarv sõnena sisaldab mustrit 0110 ja 1001 """
    if re.search('(1001 + 0110)', input_string):
        return True
    else:
        return False


def ex6_solution(input_string):
    """Kontrollib, kas sõne on "kastiauto ratas" või "kasti auto ratas" või "kasti-auto ratas" """
    if re.match('kasti(-|\s)?auto\sratas', input_string):
        return True
    else:
        return False


def ex7_solution(input_string):
    """KOntrollib, kas sõne koosneb 3 või 4 sõnast """
    if re.match('\w*\s\w*\s\w*$|\w*\s\w*\s\w*\s\w*$', input_string):
        return True
    else:
        return False

print(ex7_solution('Word'))
print('space')

def ex8_solution(input_string):
    """KOntrollib, kas sõne "kass" ja "koer" vahel on maksimaalselt 2 sõna  """
    if re.search('kass', input_string) and re.search('koer', input_string):
        if re.match('kass*\s?\w*\s?\w*\skoer', input_string):  # S tühik,w sõna
            return True
        else:
            return False
    else:
        return False


def ex9_solution(input_string):
    """KOntrollib, kas sõne on korrektne kellaaeg """
    if re.match('[2][0-3]:[0-5][0-9]$|[1][0-9]:[0-5][0-9]$|[0-9]:[0-5][0-9]$|[0][0]:[0][0]$', input_string):
        return True
    else:
        return False


def ex10_solution(input_string):  # VOIB OLLA SUVALISES KOHAS
    # peab olema A C G T sees, min 3 tripleti, algab ATG ja lopeb TAA, TAG, TGA
    """Kontrollib ,kas DNA ahel on korrektne v mitte"""
    if re.match('[ACGT]*', input_string):
        if re.search('ATG(\W\W\W)+(TAA|TAG|TGA)', input_string):
            return True
        else:
            return False
    else:
        return False


def ex11_solution(input_string):  # peab olema $ ees, koma tuhandeliste vahel ja kümmnendkohad punktiga
    """KOntrollib kas rahasumma on korrektne USA süsteemi järgi"""
    if re.match('\$(\D+\.\D+)*', input_string):
        return True
    else:
        return False


def ex12_solution(input_string):
    """KOntrollib, kas nullide arv sõnes on paaritu v mitte"""
    if re.match('[01]*$', input_string):
        if input_string.count('0') % 2 == 0:
            return True
        else:
            return False


def ex13_solution(input_string):
    """KOntrollib, kas kahendarv ei koosne samadest järjestikudest bitidest"""
    if re.match('[01]*$', input_string):
        if re.search('11', input_string) or re.search('00', input_string):
            return False
        else:
            return True
    else:
        return False


def ex14_solution(input_string):
    """kontrollib kas sõnes on olemas  "kastiauto ratas" või "kasti auto ratas" või "kasti-auto ratas" ja kas nende
    pärast on korrektne raha summa"""
    if re.match('kasti(\|\S)auto\sratas\z\$\D*', input_string):
        return True
    else:
        return False


def ex15_solution(input_string):
    """Kontrollib kas kahendarv jagab kolmega v mitte"""
    if re.match('[01]*$', input_string):
        if int(input_string, 2) % 3 == 0:
            return True
        else:
            return False
    else:
        return False




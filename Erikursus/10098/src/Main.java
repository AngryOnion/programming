import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * Created by Ilja on 5.03.2016.
 */
class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String start = "";
        int datasets = in.nextInt();
        int count = 0;
        while (datasets + 1 != count) {
            ArrayList<String> Permutations = new ArrayList<>();
            String input = in.nextLine();
            if (allSame(input)) {
                System.out.println(input);
            } else {
                permutation(start, input, Permutations);
                Collections.sort(Permutations, String.CASE_INSENSITIVE_ORDER);
                for (int i = 0; i < Permutations.size(); i++) {
                    System.out.println(Permutations.get(i));
                }
            }
            if (count < datasets) System.out.println();
            count++;
        }
    }

    public static void permutation(String start, String word, ArrayList<String> Permutations) {
        if (word.isEmpty()) {
            Permutations.add(start + word);
        } else {
            for (int i = 0; i < word.length(); i++) {
                permutation(start + word.charAt(i),
                        word.substring(0, i)
                                + word.substring(i+1, word.length()), Permutations );
            }
        }
    }

    public static boolean allSame(String word) {
        int s = 0;
        for(int i = 0; i < word.length(); i++) {
            if (!(word.charAt(i) == word.charAt(i+1))) {
                return false;
            }
        }
        return true;
    }
}

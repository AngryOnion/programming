import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Ilja on 8.03.2016.
 */
public class Main {

    public final int limit = 1000000;
    public static int[] primes;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()){
            int number = in.nextInt();
            if (number == 0 ) break;
            control(number);
        }
    }

    public static void sieve(){
        primes = new int[limit];
        for (int i = 2; i < limit; i++) {
            primes[i] = 1;
        }
        primes[0] = primes[1] = 0;


    }

}

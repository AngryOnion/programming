/** 10071 - Back to High School Physics
 * First test exercise for course ITV0101
 * made by Ilja Samoilov
 */
import java.util.Scanner;

class Main {
    /**A particle has initial velocity and acceleration.
     * If its velocity after certain time is v then what will its displacement be in twice of that time?
     *The input will contain two integers in each line. Each line makes one set of input
     * @param args two integers in one line
     */

    public static void main(String args[])
    {

        String EnteredValues;

        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            EnteredValues = in.nextLine();


            String[] parts = EnteredValues.split(" ");
            String part1 = parts[0];
            String part2 = parts[1];

            int v = Integer.parseInt(part1);
            int t = Integer.parseInt(part2);

            if((v <= 100) && (v >= -100) && (t >= 0) && (t <= 200)) {

            int displacement = 2 * v * t;

            System.out.println(displacement);
            }
            else {
                System.out.println("Wrong integers");
            }

        }


    }


}

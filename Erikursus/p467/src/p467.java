import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Ilja on 5.03.2016.
 */
class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int maxSignals = 10;
        int maxSeconds = 3600;
        int count = 1;
        while(in.hasNextLine()) {
            int lowestTimer = 90;
            String line = in.nextLine();
            line = line.trim();
            String[] signals = line.split(" ");
            ArrayList<Integer> Timers = new ArrayList<>();
            for (int i = 0; i < signals.length; i++) {
                int time = Integer.parseInt(signals[i]);
                Timers.add(time);
                if (time < lowestTimer) lowestTimer = time;
            }
            int controlTime = lowestTimer * 2; //Cycle to start from;
            while(!(controlIfAllGreen(controlTime, Timers)) && controlTime < maxSeconds + 1) {
                controlTime++;
            }
            if (controlTime >= maxSeconds + 1) {
                System.out.println("Set " + count + " is unable to synch after one hour.");
            } else {
                System.out.println("Set " + count + " synchs again at " + (controlTime / 60)
                        + " minute(s) and " + (controlTime % 60) + " second(s) after all turning green.");
            }
            count++;
        }
    }


    public static boolean controlIfAllGreen (int time, ArrayList<Integer> Signals) {
            for (int i = 0; i < Signals.size(); i++) {
                if ((time % (Signals.get(i) * 2)) >= (Signals.get(i) -5)) {
                    return false;
                }
            }
        return true;
    }
}

import java.util.Scanner;

/**
 * Created by Ilja on 7.02.2016.
 */
class Main {


    public static void main(String args[]) {

        String EnteredValues;

        Scanner in = new Scanner(System.in);


        while(in.hasNext()) {
            EnteredValues = in.nextLine();

            String[] a = EnteredValues.split(" ");

            for (int i =0;i<a.length;i++) {
                StringBuilder Scrambled = new StringBuilder(a[i]);
                if (i == a.length - 1) {
                    System.out.print(Scrambled.reverse());
                    break;
                }
                Scrambled.reverse();
                System.out.print(Scrambled + " ");
            }
            System.out.println();


        }

    }
}

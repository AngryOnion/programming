/**
 * Created by Ilja on 1.03.2016.
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

class Main {

    public static void main(String[] args) {
        HashMap hm = new HashMap();
        hm.put("A", "2");
        hm.put("B", "2");
        hm.put("C", "2");
        hm.put("D", "3");
        hm.put("E", "3");
        hm.put("F", "3");
        hm.put("G", "4");
        hm.put("H", "4");
        hm.put("I", "4");
        hm.put("J", "5");
        hm.put("K", "5");
        hm.put("L", "5");
        hm.put("M", "6");
        hm.put("N", "6");
        hm.put("O", "6");
        hm.put("P", "7");
        hm.put("R", "7");
        hm.put("S", "7");
        hm.put("T", "8");
        hm.put("U", "8");
        hm.put("V", "8");
        hm.put("W", "9");
        hm.put("X", "9");
        hm.put("Y", "9");
        hm.put("Q", "");
        hm.put("Z", "");
        Scanner in = new Scanner(System.in);
        int dataSets = in.nextInt();
        int count = 0;
        while(count != dataSets) {
            in.nextLine();
            String number = in.nextLine();
            if (number.length() == 0) number = in.nextLine();
            number = number.replaceAll(" ", "");
            int num = Integer.parseInt(number);
            ArrayList List = new ArrayList();
            for(int i = 0; i != num; i++) {
                String telNumber = in.nextLine();
                telNumber = telNumber.replaceAll(" ", "");
                telNumber = telNumber.replaceAll("-", "");
                if(isInteger(telNumber)) {
                    List.add(telNumber);
                } else {
                    for(int j = 0; j < telNumber.length(); j++) {
                        if(telNumber.charAt(j) > 64 && telNumber.charAt(j) < 91) {
                            String letter = telNumber.charAt(j) + "";
                            telNumber = telNumber.replaceAll(letter, hm.get(letter).toString());
                        } else if(telNumber.charAt(j) < 47 && telNumber.charAt(j) > 58) {
                            telNumber = telNumber.replaceAll(telNumber.charAt(j) + "", "");
                        }
                    }
                    if(telNumber.length() != 0) {
                        List.add(telNumber);
                    }
                }
            }
            Collections.sort(List);
            int numberCount = 1;
            int dublicate = 0;
            List.add("");
            for(int i = 0; i < List.size()-1; i++) {
                String first = List.get(i+1).toString();
                String zero = List.get(i).toString();
                if (first.equals(zero)) {
                    numberCount++;
                    dublicate++;
                } else if (numberCount > 1) {
                    String firstThree = List.get(i - 1).toString().substring(0, 3);
                    String lastFOur = List.get(i - 1).toString().substring(3, List.get(i - 1).toString().length());
                    System.out.println(firstThree + "-" + lastFOur + " " + numberCount);
                    numberCount = 1;
                }
            }
            if (dublicate == 0) System.out.println("No duplicates.");
            if (dataSets > count+1) System.out.println();
            count++;
        }
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException ex) {
            return false;
        }
    }
}
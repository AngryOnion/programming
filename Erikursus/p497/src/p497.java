import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

/**
 * Created by Ilja on 23.02.2016.
 */
public class p497 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int datasets = in.nextInt();
        int spaceNumbers = 0;
        List<Integer> list = new ArrayList<Integer>();
        System.out.println(datasets);
        do {
            String x = in.nextLine();
            if(Objects.equals(x, " ")) spaceNumbers++;
            else {
                int z = parseInt(x);
                list.add(z);
            }
        }
        while (spaceNumbers < datasets && in.hasNext());
    }

}

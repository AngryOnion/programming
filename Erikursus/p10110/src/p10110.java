import java.util.Scanner;

/**
 * Created by Ilja Samoilov, UNI ID 155407IAPB
 */
class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (true){
            long bulbs = in.nextLong();
            if (bulbs == 0) break;
            long square = (long) Math.floor((Math.sqrt(bulbs) + 0.5));
            if (square * square == bulbs ) {
                System.out.println("yes");
            } else {
                System.out.println("no");
            }
        }
    }
}

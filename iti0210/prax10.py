import numpy as np


def normalize(X):
    return X / np.max(np.abs(X), axis=0)


def fit_sgd(X, y, w, niter=100, alpha=0.01):
    for i in range(niter):
        rand = np.random.randint(0, X.shape[0])
        X_i = X[rand]
        y_i = y[rand]

        yhat = w[0] + (w[1:] * X_i).sum()

        error = y_i - yhat
        w[0] = w[0] + alpha * error
        for j in range(1, w.__len__()):
            w[j] = w[j] + alpha * error * w[j]

    return w  # weights vector


def test_model(w, X, y, threshold=0.5):
    yhat = w[0] + (w[1:] * X).sum(axis=1)  # value from linear model
    ypred = (yhat > threshold).astype(int)  # prediction (is somebody in the room?)
    ytrue = (ypred == y).astype(int)  # compare to true value
    return ytrue.sum() / y.shape[0]  # true count / total count


def sse_loss(w, X, y):
    yhat = w[0] + (w[1:] * X).sum(axis=1)
    err = y - yhat
    return np.square(err).sum()


if __name__ == '__main__':
    train = np.loadtxt("train.csv", delimiter=",")
    test = np.loadtxt("test.csv", delimiter=",")
    X = normalize(train[:, :5])  # columns 0..4
    y = normalize(train[:, 5])  # column 5  (classification, 0 or 1)

    X_test = normalize(test[:, :5])
    y_test = normalize(test[:, 5])

    w = np.random.rand(X.shape[1] + 1)

    while True:
        new_w = fit_sgd(X, y, w, 500, 0.01)
        if sse_loss(new_w, X, y) > sse_loss(w, X, y):
            w = new_w
        else:
            w = new_w
            break

    print(test_model(w, X_test, y_test))

import random
from multiprocessing import Process


def multiprocess():
    processes = list()
    for i in range(8):
        process = Process(target=solve_multiple_times, args=[i])
        processes.append(process)
        process.start()
    for i in processes:
        i.join()


def solve_multiple_times(number):
    failed = list()
    for i in range(1000):
        pos = NQPosition(8)
        best_pos, best_value = solve(pos)
        if best_value != 0:
            failed.append(best_value)
    print("Process number {} failed tries: {} ".format(str(number), str(failed.__len__())))


def solve(pos):
    best_pos, best_value = hill_climbing(pos)
    tries = 0
    while best_value != 0 and tries < 500:
        pos.randomize_line()
        position, value = hill_climbing(pos)
        if value < best_value:
            best_pos = position
            best_value = value
        # print("Try number {} value is {} board is {}".format(str(tries), str(value), str(pos.board)))
        tries += 1
    return best_pos, best_value


def hill_climbing(pos):
    curr_value = pos.value()
    while True:
        move, new_value = pos.best_move()
        if new_value >= curr_value:
            # no improvement, give up
            return pos, curr_value
        else:
            # position improves, keep searching
            curr_value = new_value
            pos.make_move(move)


class NQPosition:
    board = list()

    def __init__(self, N):
        self.board = list()
        for i in range(N):
            self.board.append(0)

    def randomize_line(self):
        self.board[random.randint(0, self.board.__len__() - 1)] = random.randint(0, self.board.__len__() - 1)

    def value(self):
        return self.value_board(self.board)

    def value_board(self, board):
        queens_in_conflict = list()
        for index in range(len(board)):

            if len(queens_in_conflict) >= len(board):
                break

            queen = board[index]

            if index not in queens_in_conflict:

                for other_index in range(len(board)):

                    if index != other_index:
                        other_queen = board[other_index]

                        if other_queen == queen:
                            self.add_conflicts(index, other_index, queens_in_conflict)
                            continue

                        if abs(index - other_index) == abs(queen - other_queen):
                            self.add_conflicts(index, other_index, queens_in_conflict)

        return len(queens_in_conflict)

    @staticmethod
    def add_conflicts(index, other_index, queens_in_conflict):
        if index not in queens_in_conflict:
            queens_in_conflict.append(index)
        if other_index not in queens_in_conflict:
            queens_in_conflict.append(other_index)

    def make_move(self, move):
        self.board[move[0]] = move[1]

    def best_move(self):
        value = len(self.board)
        move = (0, 0)
        for queen in range(len(self.board)):
            for new_position in range(len(self.board)):
                old_position = self.board[queen]
                self.board[queen] = new_position
                new_value = self.value()
                if new_value < value:
                    move = (queen, new_position)
                    value = new_value
                self.board[queen] = old_position
        return move, value


multiprocess()

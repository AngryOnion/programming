
exercise_1 = [[1, 1, 0],
                  ['?', 1, '?'],
              [1, 1, 0]]


# kb - teadmusbaas CNF kujul
# alpha - literaal, mida tahame kontrollida.


def subsumes(next, p):
    pass


def isEmpty(r):
    pass

def dissociate(op, args):
    """Given an associative op, return a flattened list result such
    that Expr(op, *result) means the same as Expr(op, *args)."""
    result = []
    def collect(subargs):
        for arg in subargs:
            if arg.op == op: collect(arg.args)
            else: result.append(arg)
    collect(args)
    return result

def disjuncts(s):
    """Return a list of the disjuncts in the sentence s.
    >>> disjuncts(A | B)
    [A, B]
    >>> disjuncts(A & B)
    [(A & B)]
    """
    return dissociate('|', [s])

def resolve(next, p):
    clauses = []
    for di in disjuncts(next):
        for dj in disjuncts(p):
            if di == ~dj or ~di == dj:
                dnew = unique(removeall(di, disjuncts(next)) +
                              removeall(dj, disjuncts(p)))
                clauses.append(associate('|', dnew))
    return clauses

def removeall(item, seq):
    """Return a copy of seq (or string) with all occurences of item removed.
    >>> removeall(3, [1, 2, 3, 3, 2, 1, 3])
    [1, 2, 2, 1]
    >>> removeall(4, [1, 2, 3])
    [1, 2, 3]
    """
    if isinstance(seq, str):
        return seq.replace(item, '')
    else:
        return [x for x in seq if x != item]

def unique(seq):
    """Remove duplicate elements from seq. Assumes hashable elements.
    >>> unique([1, 2, 3, 2, 1])
    [1, 2, 3]
    """
    return list(set(seq))

# def associate(op, args):
    """Given an associative op, return an expression with the same
    meaning as Expr(op, *args), but flattened -- that is, with nested
    instances of the same op promoted to the top level.
    >>> associate('&', [(A&B),(B|C),(B&C)])
    (A & B & (B | C) & B & C)
    >>> associate('|', [A|(B|(C|(A&B)))])
    (A | B | C | (A & B))
    """
    return 'A'
    # args = dissociate(op, args)
    # if len(args) == 1:
    #     return args[0]
    # else:
    #     # return Expr(op, *args)



def resolution(kb: list, alpha):
    candidates = kb.copy()
    candidates.remove(alpha)
    processed = list()

    while not candidates:
        next_element = candidates.pop()
        for p in processed:
            if subsumes(next_element, p):
                continue
        for p in processed:
            resolvents: list = resolve(next_element, p)
            for r in resolvents:
                if isEmpty(r):
                    return True
                candidates.append(r)
        processed.append(next_element)
    return False


print(disjuncts(('x1 & x2')))

import operator
import os
import functools
import math


def read_dir(dirn):
    cont_l = []
    for fn in os.listdir(dirn):
        with open(os.path.join(dirn, fn), encoding="latin-1") as f:
            words = [w.strip()
                     for w in f.read().replace("\n", " ").split(" ")
                     if not stop_word(w)
                     ]
            cont_l.append(words)
    return cont_l


def stop_word(wstr):
    w = wstr.strip()
    if len(w) < 4:
        return True
    return False


def count_words(list):
    dictionary = {}
    for word in list:
        if word in dictionary:
            dictionary[word] = dictionary[word] + 1
        else:
            dictionary[word] = 1
    return dictionary


def flatten_list(list):
    return functools.reduce(operator.iconcat, list, [])


def extract_words(message: str):
    return set([w.strip()
                for w in message.replace("\n", " ").split(" ")
                if not stop_word(w) and w in spam_word_frequency and w in ham_word_frequency
                ])


def calculate_word_probability(word, words_frequency, all_words):
    return math.log((words_frequency[word] + 1) / (len(all_words) + len(unique_words)))


def check_email(email_words, email_name):
    spam_probability = 0.0
    ham_probability = 0.0
    for word in email_words:
        spam_probability += calculate_word_probability(word, spam_word_frequency, flat_spam)
        ham_probability += calculate_word_probability(word, ham_word_frequency, flat_ham)
    if spam_probability > ham_probability:
        print('{} is spam'.format(email_name))
    else:
        print('{} is ham'.format(email_name))


ham_l = read_dir("enron6/ham")
spam_l = read_dir("enron6/spam")

flat_spam = flatten_list(spam_l)
flat_ham = flatten_list(ham_l)

unique_words = set(flat_spam)
unique_words.update(flat_ham)

spam_word_frequency = count_words(flat_spam)
# spam_word_frequency.pop('Subject')
ham_word_frequency = count_words(flat_ham)

print(len(ham_l), "ham messages")  # meaning, these are not spam
print(len(spam_l), "spam messages")

first_email = """
Subject: cleburne issues
daren , with megan gone i just wanted to touch base with you on the status of the enron payments owed to the cleburne plant . the current issues are as follows :
november gas sales $ 600 , 377 . 50
october payment to ena for txu pipeline charges $ 108 , 405 . 00
cleburne receivable from enron $ 708 , 782 . 50
less : november gas agency fees ( $ 54 , 000 . 00 )
net cleburne receivable from enron $ 654 , 782 . 50
per my discussions with megan , she stated that about $ 500 k of the $ 600 k nov gas sales was intercompany ( desk to desk ) sales , with the remainder from txu . are we able to settle any intercompany deals now ? are we able to settle with txu ?
additionally , you ' ll see that i included the oct txu payment in the receivable owed to cleburne also . this is because i always pay megan based upon the pipeline estimates in michael ' s file , even though they are not finalized until the next month . therefore in my november payment to enron , i paid ena for october ' s estimate , of which megan would have paid the final bill on 12 / 26 / 01 when it was finalized . however , i had to pay the october bill directly last month , even though i had already sent the funds to ena in november . therefore , i essentially paid this bill twice ( once to ena in nov & once to txu in dec ) . i deducted the november agency fees from these receivable totals to show the net amount owed to cleburne .
please advise as to the status of these bills . you can reach me at 713 - 853 - 7280 . thanks .
"""

second_email = """
Subject: immediate contract payment .
immediate contract payment . our ref : cbn / ird / cbx / 021 / 05
attn :
during the auditing and closing of all financial records of the central bank of nigeria ( cbn ) it was discovered from the records of outstanding foreign contractors due for payment with the federal government of nigeria in the year 2005 that your name and company is next on the list of those who will received their fund .
i wish to officially notify you that your payment is being processed and will be released to you as soon as you respond to this letter . also note that from the record in our file , your outstanding contract payment is usd $ 85 , 000 , 000 . 00 ( eighty - five million united states dollars ) .
kindly re - confirm to me if this is inline with what you have in your record and also re - confirm the information below to enable this office proceed and finalize your fund remittance without further delays .
1 ) your full name .
2 ) phone , fax and mobile # .
3 ) company name , position and address .
4 ) profession , age and marital status .
5 ) copy of drivers license i . d .
as soon as the above information are received , your payment will be made available to you via an international certified bank draft , which will be delivered to your doorstep for your confirmation . you should call my direct number as soon as you receive this letter for further discussion and more clarification . also get back to me on this e - mail address ( payment _ info _ 10 @ yahoo . com ) and ensure that you fax me all the details requested to my direct fax number as instructed .
best regards ,
prof . charles c . soludo .
executive governor
central bank of nigeria ( cbn )
tel : 234 - 1 - 476 - 5017
fax : 234 - 1 - 759 - 0130
website : www . cenbank . org
mail sent from webmail service at php - nuke powered site
- http : / / yoursite . com"""

first_email_words = extract_words(first_email)
second_email_words = extract_words(second_email)

check_email(first_email_words, 'first email')
check_email(second_email_words, 'second email')

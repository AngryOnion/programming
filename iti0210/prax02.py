import time
from queue import PriorityQueue


def read_map():
    with open("ITI0210_caves/cave300x300") as f:
        map_data = [l.strip() for l in f.readlines() if len(l) > 1]
        goal = find_daimond(map_data)
        clone = map_data.copy()
        find_greedy_path(map_data, goal, True)
        find_astar_path(clone, goal, True)

def find_greedy_path(level_map, goal, draw_map):
    start = find_start(level_map)
    start_time = time.time()
    frontier = PriorityQueue()
    frontier.put((0, start))
    visited = {start: None}

    current = None
    while not frontier.empty():
        _, current = frontier.get()

        if level_map[current[1]][current[0]] == 'D':
            break

        for next in find_free_neigbours(level_map, current, False):
            if next not in visited:
                priority = heuristic(next, goal)
                frontier.put((priority, next))
                visited[next] = current

    path = [current]
    while visited[current] is not None:
        current = visited[current]
        path.append(current)
    path.reverse()
    print("Path lenght: " + str(len(path)))
    print("Time elapsed: " + str(time.time() - start_time))
    if draw_map:
        for cell in path:
            x = cell[0]
            y = cell[1]
            if level_map[y][x] == ' ':
                level_map[y] = level_map[y][0:x] + '.' + level_map[y][x + 1:]
        for i in level_map:
            print(i)
    return path

def find_astar_path(level_map, goal, draw_map):
    start = find_start(level_map)

    start_time = time.time()

    frontier = PriorityQueue()
    frontier.put((0, start))
    visited = {start: None}

    cost_so_far = {start: 0}

    current = None
    while not frontier.empty():
        _, current = frontier.get()

        if level_map[current[1]][current[0]] == 'D':
            break

        for next in find_free_neigbours(level_map, current, False):
            new_cost = cost_so_far[current] + 1
            if next not in cost_so_far or new_cost < cost_so_far[next]:
                cost_so_far[next] = new_cost
                priority = new_cost + heuristic(next, goal)  # g(n) + h(n)
                frontier.put((priority, next))
                visited[next] = current

    path = [current]
    while visited[current] is not None:
        current = visited[current]
        path.append(current)
    path.reverse()
    print("Path lenght: " + str(len(path)))
    print("Time elapsed: " + str(time.time() - start_time))

    if draw_map:
        for cell in path:
            x = cell[0]
            y = cell[1]
            if level_map[y][x] == ' ':
                level_map[y] = level_map[y][0:x] + '.' + level_map[y][x + 1:]
        for i in level_map:
            print(i)
    return path

def heuristic(node, goal):
    return abs(node[0] - goal[0]) + abs(node[1] - goal[1])

def heuristic2(node, goal):
    return max(abs(node[0] - goal[0]), abs(node[1] - goal[1]))

def find_start(map):
    for row in map:
        for col in row:
            if col == 's':
                return row.index(col), map.index(row)

def find_daimond(map):
    for row in map:
        for col in row:
            if col == 'D':
                return row.index(col), map.index(row)


def find_free_neigbours(map, cell, add_diagonale):
    x = cell[0]
    y = cell[1]
    neighbours = []
    if inbounds(map, x, y + 1) and check_cell(map, x, y+1):
        neighbours.append((x, y + 1))
    if inbounds(map, x, y - 1) and check_cell(map, x, y - 1):
        neighbours.append((x, y - 1))
    if inbounds(map, x + 1, y) and check_cell(map, x + 1, y):
        neighbours.append((x + 1, y))
    if inbounds(map, x - 1, y) and check_cell(map, x - 1, y):
        neighbours.append((x - 1, y))
    if add_diagonale:
        neighbours = find_free_diagonale_neigbours(neighbours,map, cell)
    return neighbours

def find_free_diagonale_neigbours(neighbours, map, cell):
    x = cell[0]
    y = cell[1]
    if inbounds(map, x+1, y + 1) and check_cell(map, x+1, y + 1):
        neighbours.append((x+1, y + 1))
    if inbounds(map, x-1, y - 1) and check_cell(map, x-1, y - 1):
        neighbours.append((x-1, y - 1))
    if inbounds(map, x + 1, y - 1) and check_cell(map, x + 1, y - 1):
        neighbours.append((x + 1, y - 1))
    if inbounds(map, x - 1, y + 1) and check_cell(map, x - 1, y + 1):
        neighbours.append((x - 1, y + 1))
    return neighbours

def check_cell(map, x, y):
    cell = map[y][x]
    return cell == ' ' or cell == 'D'

def inbounds(map, x, y):
    size_y = map.__len__()
    size_x = map[0].__len__()
    if 0 <= x < size_x:
        if 0 <= y < size_y:
            return True
    return False

read_map()
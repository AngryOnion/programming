male(marcus).
lives(marcus, pompey).
male(jaan).
born(jaan, 1977).
born(marcus, 40).
lives(marcus, pompey).
male(francesco).
born(francesco, 1989).

mortal(X):-
    male(X).

died_in_pompei(X):-
   lives(X, pompey),
   born(X, Year),
   Year < 79.

died_from_old_age(X, Year_now):-
    mortal(X),
    born(X, Year),
    died_in_pompei(X),
    Year_now - Year > 150.


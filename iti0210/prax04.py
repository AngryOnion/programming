import random
import statistics
import math

AI = 0
PLAYER = 1

ROLL = 0
PASS = 1
rolled_list = []


def pig_game(ai_func):
    rolled = 0
    turn = PLAYER
    player_points = ai_points = 0

    while player_points < 100 and ai_points < 100:
        print("Your points", player_points,
              "AI points", ai_points,
              "holding", rolled)

        if turn == PLAYER:
            decision = dummy_ai(turn, rolled, player_points, ai_points)

            if decision == PASS:
                rolled = 0
                turn = AI
            else:
                dieroll = random.randint(1, 6)
                # print("You rolled...", dieroll)
                if dieroll == 1:
                    player_points -= rolled  # lose all points again
                    rolled = 0
                    turn = AI
                else:
                    rolled += dieroll
                    player_points += dieroll

        else:
            decision = ai_func(turn, rolled, ai_points, player_points)
            print('ai descision: {} '.format('PASS' if decision == 1 else 'ROLL'))

            if decision == PASS:
                rolled = 0
                turn = PLAYER
            else:
                dieroll = random.randint(1, 6)
                print('ai rolled: ', dieroll)
                rolled_list.append(dieroll)
                # print("-- AI rolled...", dieroll)
                if dieroll == 1:
                    ai_points -= rolled  # lose all points again
                    rolled = 0
                    turn = PLAYER
                else:
                    rolled += dieroll
                    ai_points += dieroll

    if player_points >= 100:
        print("DummyAI won!")
        return PLAYER
    elif ai_points >= 100:
        print("ExpMiniMax won.")
        return AI


def dummy_ai(turn, rolled, my_points, opp_points):
    if rolled < 21:
        return ROLL
    else:
        return PASS


# pig_game(dummy_ai)


def minimax_ai(turn, rolled, my_points, opp_points):
    pass_turn = exp_minimax(PLAYER, True, rolled, my_points, opp_points, 5)
    roll_turn = exp_minimax(AI, True, rolled, my_points, opp_points, 5)

    print('pass_turn: {} , rolled_turn : {}'.format(pass_turn, roll_turn))
    return ROLL if roll_turn >= pass_turn else PASS


def evaluate(my_points, opp_points, rolled):
    if my_points + rolled >= 100:
        return 200
    if opp_points + rolled >= 100:
        return 0
    return rolled + my_points + (100 - opp_points) * math.log10(100 - opp_points)


def exp_minimax(turn, chance, rolled, my_points, opp_points, depth):
    depth = depth - 1

    if turn == AI and my_points + rolled >= 100:
        return evaluate(my_points, opp_points, rolled)
    if turn == PLAYER and opp_points + rolled >= 100:
        return evaluate(my_points, opp_points, rolled)

    if depth == 0:
        if turn == PLAYER:
            return evaluate(my_points, opp_points, rolled)
        else:
            return evaluate(my_points, opp_points, rolled)

    if chance:
        chance_result = []
        for i in range(1, 7):
            if i == 1:
                chance_result.append(exp_minimax(get_opposite(turn), False, 0, my_points, opp_points, depth))
            else:
                chance_result.append(exp_minimax(turn, False, rolled + i, my_points, opp_points, depth))
        average = statistics.mean(chance_result)
        return average

    if turn == AI:
        pass_turn = exp_minimax(PLAYER, True, rolled, my_points, opp_points, depth)
        roll_turn = exp_minimax(AI, True, rolled, my_points, opp_points, depth)
        return max(roll_turn, pass_turn)

    if turn == PLAYER:
        pass_turn = exp_minimax(AI, True, rolled, my_points, opp_points, depth)
        roll_turn = exp_minimax(PLAYER, True, rolled, my_points, opp_points, depth)
        return min(roll_turn, pass_turn)


def get_opposite(turn):
    return AI if turn == PLAYER else PLAYER


wins = 0
for i in range(100):
    if pig_game(minimax_ai) == AI:
        wins += 1

print("Wins: " + str(wins))

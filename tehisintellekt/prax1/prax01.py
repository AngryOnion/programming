from queue import Queue


def find_path(level_map):
    start = find_start(level_map)

    frontier = Queue()
    frontier.put(start)
    visited = {}
    visited[start] = None

    current = None
    while not frontier.empty():
        current = frontier.get()

        if level_map[current[1]][current[0]] == 'D':
            break

        for next in find_free_naigbours(level_map, current):
            if next not in visited:
                frontier.put(next)
                visited[next] = current

    path = [current]
    while visited[current] is not None:
        current = visited[current]
        path.append(current)
    path.reverse()
    for cell in path:
        x = cell[0]
        y = cell[1]
        if level_map[y][x] == ' ':
            level_map[y] = level_map[y][0:x] + '.' + level_map[y][x + 1:]
    for i in level_map:
        print(i)
    return path

def find_start(map):
    for row in map:
        for col in row:
            if col == 's':
                return row.index(col), map.index(row)

def find_daimond(map):
    for row in map:
        for col in row:
            if col == 'D':
                return row.index(col), map.index(row)


def find_free_naigbours(map, cell):
    x = cell[0]
    y = cell[1]
    neighbours = []
    if inbounds(map, x, y + 1) and check_cell(map, x, y+1):
        neighbours.append((x, y + 1))
    if inbounds(map, x, y - 1) and check_cell(map, x, y - 1):
        neighbours.append((x, y - 1))
    if inbounds(map, x + 1, y) and check_cell(map, x + 1, y):
        neighbours.append((x + 1, y))
    if inbounds(map, x - 1, y) and check_cell(map, x - 1, y):
        neighbours.append((x - 1, y))
    return neighbours

def check_cell(map, x, y):
    cell = map[y][x]
    return cell == ' ' or cell == 'D'

def inbounds(map, x, y):
    size_y = map.__len__()
    size_x = map[0].__len__()
    if 0 <= x < size_x:
        if 0 <= y < size_y:
            return True
    return False


lava_map1 = [
    "      **               **      ",
    "     ***     D        ***      ",
    "     ***                       ",
    "                      *****    ",
    "           ****      ********  ",
    "           ***          *******",
    " **                      ******",
    "*****             ****     *** ",
    "*****              **          ",
    "***                            ",
    "              **         ******",
    "**            ***       *******",
    "***                      ***** ",
    "                               ",
    "                s              ",
]
lava_map2 = [
    "     **********************    ",
    "   *******   D    **********   ",
    "   *******                     ",
    " ****************    **********",
    "***********          ********  ",
    "            *******************",
    " ********    ******************",
    "********                   ****",
    "*****       ************       ",
    "***               *********    ",
    "*      ******      ************",
    "*****************       *******",
    "***      ****            ***** ",
    "                               ",
    "                s              ",
]
find_path(lava_map1)
find_path(lava_map2)